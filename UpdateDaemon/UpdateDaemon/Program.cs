﻿using System;
using log4net;
using log4net.Config;
using Quartz;
using Quartz.Impl;
using NaturalProductsWebsite.Code;

public class UpdateDaemon
{
    static IScheduler sched;
    private static readonly ILog log = LogManager.GetLogger(typeof(UpdateDaemon));

    private static void Main(string[] args)
    {
        XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
     
        
        log.Info("Update Daemon Starting!");
        try
        {
            // construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            sched = schedFact.GetScheduler();
            sched.Start();

            // construct job info
            IJobDetail jobDetail = JobBuilder.Create<UpdateJob>()
                        .WithIdentity("updateJob", "group1")
                        .Build();

            // fire every 15 minutes
            ITrigger trigger = TriggerBuilder.Create().StartNow().WithSchedule(SimpleScheduleBuilder.RepeatMinutelyForever(10)).Build();

            sched.ScheduleJob(jobDetail, trigger);
        }
        catch (Exception e)
        {
            log.Debug(e.ToString());
        }
    }
}

[DisallowConcurrentExecution]
public class UpdateJob : IJob
{
    private static readonly ILog log = LogManager.GetLogger(typeof(UpdateJob));

    public UpdateJob()
    {
        XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
        //Utilities.Email.send("conwaykyle@mail.com", "test", "test email " + DateTime.Now.ToLongTimeString());
    }

    public void Execute(IJobExecutionContext context)
    {
        try
        {
            log.Info("Job Starting");
            LongProcessWorker.startBackgroundProcessWorker();
            log.Info("Job Finished");
        }
        catch (Exception e)
        {
            log.Debug(e.ToString());
        }
    }
}