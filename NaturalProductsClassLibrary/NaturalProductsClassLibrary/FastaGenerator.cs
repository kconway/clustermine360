﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;
using log4net;
using log4net.Config;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    public class FastaGenerator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FastaGenerator));

        public static void PrepareFASTAFiles()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
            List<string> filestoprepare;
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                filestoprepare = db.Downloads.Where(x => x.Status == 0).Select(x => x.DownloadGuid).ToList();
            }

            if ((filestoprepare != null) && (filestoprepare.Count != 0))
            {
                foreach (var filerequestguid in filestoprepare)
                {
                    PrepareFASTAFile(filerequestguid);
                }
            }
        }

        public static void RemoveOldFASTAFiles()
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                DateTime thirtydaysold = DateTime.Now.AddDays(-30d);
                var recordstodelete = db.Downloads.Where(x => x.WhenRequested <= thirtydaysold).ToList();
                foreach (var record in recordstodelete)
                {
                    string filename = record.DownloadGuid + ".fasta";
                    string path = ConfigurationManager.AppSettings["DownloadTempFolderLocation"] + filename;
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception ex)
                    {
                        log.Info(ex.ToString());
                    }
                    db.Downloads.DeleteOnSubmit(record);
                }
                db.SubmitChanges();
            }
        }

        public static void PrepareZippedFASTAFiles(List<DomainSequence> sequences, Stream stream)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var domainsequence in sequences)
                {
                    string filename = domainsequence.DomainType + "_" + domainsequence.Identifier + "_" + domainsequence.Length + "AA.fasta";

                    int count = 2;
                    while (zip.ContainsEntry(filename))
                    {
                        filename = domainsequence.DomainType + "_" + domainsequence.Identifier + "_" + domainsequence.Length + "AA_" + count + ".fasta";
                        count++;
                    }

                    zip.AddEntry(filename, PrepareFASTAFile(domainsequence, false));
                }
                zip.Save(stream);
            }
        }

        public static string PrepareFASTAFile(int domainid, out string filename)
        {
            string filedata = "";
            filename = "sequence.fasta";

            DomainSequence sequence = DomainSequencesDataHandler.getDomainByID(domainid);
            if (sequence != null)
            {
                filedata = PrepareFASTAFile(sequence, false);
                filename = sequence.DomainType + "_" + sequence.Identifier + "_" + sequence.Length + "AA.fasta";
            }

            return filedata;
        }

        protected static string PrepareFASTAFile(DomainSequence domainsequence, bool simpleheader)
        {
            string filedata = BuildHeader(domainsequence, simpleheader) + WriteOutSequence(domainsequence);
            return filedata;
        }

        protected static void PrepareFASTAFile(string filerequestguid)
        {
            GetSequences(filerequestguid);
        }

        protected static void GetSequences(string downloadguid)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var record = db.Downloads.Where(x => x.DownloadGuid == downloadguid).SingleOrDefault();
            if (record != null)
            {
                try
                {
                    string ids = record.DomainIDs;
                    if (!String.IsNullOrEmpty(ids))
                    {
                       

                        List<int> domainids = StringTools.DeserializeFromSemiColonSeparatedList(ids).ConvertAll(x => int.Parse(x));
                        List<DomainSequence> sequences = new List<DomainSequence>();
                        List<DomainSequence> allsequences = DomainSequencesDataHandler.getAllDomainSequences();
                       
                        foreach (var id in domainids)
                        {
                            DomainSequence sequence = allsequences.Where(x => x.ID == id).SingleOrDefault();
                            if (sequence != null)
                            {
                                sequences.Add(sequence);
                            }
                        }
                       

                        SaveFASTAFile(sequences, downloadguid, record.SimpleHeaders);
                       
                        record.Status = 1;
                    }
                    else
                    {
                        record.Status = 20; // no results
                    }
                }
                catch (Exception ex)
                {
                    string error = "Error preparing FASTA file (downloadguid = " + downloadguid + ")" + Environment.NewLine + ex.ToString();
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error Creating FASTA File", error);
                    log.Debug(error);
                    record.Status = 25;
                }
            }
            db.SubmitChanges();
            db.Dispose();
        }

        protected static void SaveFASTAFile(List<DomainSequence> sequences, string downloadguid, bool simpleheaders)
        {
            string path = ConfigurationManager.AppSettings["DownloadTempFolderLocation"] + downloadguid + ".fasta";
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    foreach (DomainSequence sequence in sequences)
                    {
                        sw.Write(BuildHeader(sequence, simpleheaders));
                        sw.Write(WriteOutSequence(sequence));
                    }
                    sw.Flush();
                }
            }
        }

        private static string WriteOutSequence(DomainSequence sequence)
        {
            char[] sequenceData = StringTools.ByteArraytoString(sequence.Sequence).ToCharArray();
            int linecount = 0;
            StringBuilder formattedSequence = new StringBuilder();

            foreach (char letter in sequenceData)
            {
                if (linecount >= 80)
                {
                    linecount = 0;
                    formattedSequence.AppendLine();
                }
                formattedSequence.Append(letter);
                linecount++;
            }
            formattedSequence.AppendLine();

            return formattedSequence.ToString();
        }

        private static string BuildHeader(DomainSequence sequence, bool shortheader)
        {
            string genbankaccession = "";
            string genbankversion = "";
            string cfname = "";
            StringBuilder header = new StringBuilder();
            string formattedheader = "";
           
            try
            {
                if (sequence.ClusterID != null)
                {
                    genbankaccession = sequence.Cluster.ClusterBase.GenbankAccession;                 
                    genbankversion = sequence.Cluster.ClusterBase.GenbankVersion.ToString();                
                    cfname = sequence.Cluster.CompoundFamily.FamilyName;
                   
                }
                else if (sequence.LargeSequenceID != null)
                {
                    genbankaccession = sequence.LargeSequence.ClusterBase.GenbankAccession;
                    genbankversion = sequence.LargeSequence.ClusterBase.GenbankVersion.ToString();
                    cfname = "";
                }

                header.Append(">");
                if (shortheader)
                {
                    string org = sequence.Organism;
                    string[] namesubparts = org.Split(new char[] { ' ' });
                    string genus = namesubparts[0].Substring(0, 1);
                    int maxspecieslength = 15;
                    string species = new string(org.SkipWhile(x => x != ' ').Skip(1).TakeWhile((x, index) => index < maxspecieslength).ToArray());
                    
                    if (!String.IsNullOrEmpty(sequence.Identifier))
                    {
                        header.Append(sequence.Identifier + "_");
                    }
                    header.Append(genus + "_" + species + "_");
                    header.Append(sequence.ClusterType + "_" + sequence.DomainType);
                    if (sequence.IsATorA)
                    {
                        if (!String.IsNullOrEmpty(sequence.AorATPrediction))
                        {
                            header.Append("_" + sequence.AorATPrediction);
                        }
                    }
                    else if (sequence.IsKR)
                    {
                        if (!String.IsNullOrEmpty(sequence.KRActivityPrediction))
                        {
                            header.Append("_" + sequence.KRActivityPrediction);
                        }
                        if (!String.IsNullOrEmpty(sequence.KRStereoChemPrediction))
                        {
                            header.Append("_" + sequence.KRStereoChemPrediction);
                        }
                    }
                    header.AppendLine();
                    formattedheader = header.ToString().Replace(' ', '_');
                }
                else
                {
                    if (!String.IsNullOrEmpty(sequence.Identifier))
                    {
                        header.Append(sequence.Identifier + "|");
                    }
                    header.Append("gb|" + genbankaccession + "." + genbankversion + "| ");
                    header.Append(sequence.Enum_Phyla.Phylum + " - " + sequence.Organism + ", ");

                    if (!String.IsNullOrEmpty(cfname))
                    {
                        header.Append(cfname + ", ");
                    }

                    header.Append(sequence.ClusterType + " " + sequence.DomainType);

                    if (sequence.IsATorA)
                    {
                        if (!String.IsNullOrEmpty(sequence.AorATPrediction))
                        {
                            header.Append(" " + sequence.AorATPrediction);
                        }
                    }
                    else if (sequence.IsKR)
                    {
                        if (!String.IsNullOrEmpty(sequence.KRActivityPrediction))
                        {
                            header.Append(" " + sequence.KRActivityPrediction);
                        }
                        if (!String.IsNullOrEmpty(sequence.KRStereoChemPrediction))
                        {
                            header.Append(" " + sequence.KRStereoChemPrediction);
                        }
                    } 
                    header.AppendLine();
                formattedheader = header.ToString();
                }
              
               
            }
            catch (Exception ex)
            {
                log.Debug(ex.ToString() + Environment.NewLine + " Domain ID: " + sequence.ID);
            }

            return formattedheader;
        }
    }
}