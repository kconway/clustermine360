﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using log4net.Config;
using Utilities;
using HtmlAgilityPack;

namespace NaturalProductsWebsite.Code
{
    /// <summary>
    /// Summary description for DownloadAntiSmash
    /// </summary>
    public class AntiSmash
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AntiSmash));

        public static void triggerAntiSmashDownload()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));

            CheckToAllowDelayedSequences();

            for (int statusNum = 0; statusNum < 5; statusNum++)
            {
                DataClassesDataContext db = new DataClassesDataContext();
                var clusters = (from c in db.Clusters
                                where c.ClusterBase.AntiSmashStatus == statusNum
                                 && c.ClusterBase.Delayed == false
                                select c).ToList<Cluster>().ConvertAll(x => new ProcessEntity(x));

                var sequences = (from s in db.LargeSequences
                                 where s.ClusterBase.AntiSmashStatus == statusNum
                                 && s.ClusterBase.Delayed == false
                                 select s)
                                 .ToList<LargeSequence>().ConvertAll(x => new ProcessEntity(x));
                db.Dispose();

                clusters.AddRange(sequences);
                log.Info("# clusters of status " + statusNum + " to process for antiSMASH: " + clusters.Count);
                foreach (var cluster in clusters)
                {
                    if (statusNum < 3)
                    {
                        checkAntiSmashStatus(cluster);
                    }
                    else if (statusNum == 3)
                    {
                        downloadFiles(cluster);
                        if (cluster.ClusterBase.EmailContributor)
                        {
                            SendEmailToContributor(cluster);
                        }
                    }
                    else if (statusNum == 4)
                    {
                        DateTime recent = DateTime.Now.AddMinutes(-5d);
                        if (cluster.ClusterBase.AntiSmashModified.Value < recent)
                        {
                            ProcessEntity.UpdateProcessEntity(cluster, UpdateType.antiSMASH, 3);
                        }
                    }
                }
                log.Info("Finished processing antiSMASH clusters of status " + statusNum);
            }
        }

        private static void SendEmailToContributor(ProcessEntity cluster)
        {
            string contributoremail = UserInfos.getUserInfoByID(cluster.ClusterBase.ContributorID).Email;
            string emailsubject = "antiSMASH Data Available";
            string emailbody = "antiSMASH Data for " + cluster.ClusterBase.GenbankAccession + "." + cluster.ClusterBase.GenbankVersion + " is available at <a href=\"" + cluster.ClusterBase.AntiSmashLink + "\">" + cluster.ClusterBase.AntiSmashLink + "</a>.";
            Utilities.Email.Send(contributoremail, emailsubject, emailbody);
        }

        private static void CheckToAllowDelayedSequences()
        {
            int DelayedOnlyWhenNumProcessingSmallerThan = 5;

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                int NumSequencesProcessing = (from s in db.LargeSequences where s.ClusterBase.AntiSmashStatus < 5 && s.ClusterBase.Delayed == false select s.ID).Count();
                int NumClustersProcessing = (from c in db.Clusters where c.ClusterBase.AntiSmashStatus < 5 && c.ClusterBase.Delayed == false select c.ID).Count();
                int TotalProcessing = NumSequencesProcessing + NumClustersProcessing;
                if (TotalProcessing < DelayedOnlyWhenNumProcessingSmallerThan)
                {
                    int diff = DelayedOnlyWhenNumProcessingSmallerThan - TotalProcessing;
                    int NumClustersToProcess = (from c in db.Clusters where c.ClusterBase.AntiSmashStatus < 5 && c.ClusterBase.NCBIStatus == 2 && c.ClusterBase.Delayed == true select c.ID).Count();

                    // remove delay on delayed clusters
                    int NumClustersToTake = 0;
                    if (NumClustersToProcess > diff)
                    {
                        NumClustersToTake = diff;
                    }
                    else
                    {
                        NumClustersToTake = NumClustersToProcess;
                    }

                    if (NumClustersToTake > 0)
                    {
                        var clusterstoremovedelay = db.Clusters.Where(c => c.ClusterBase.AntiSmashStatus < 5 && c.ClusterBase.NCBIStatus == 2 && c.ClusterBase.Delayed == true).Select(x => x).Take(NumClustersToTake).ToList();
                        foreach (var seq in clusterstoremovedelay)
                        {
                            seq.ClusterBase.Delayed = false;
                        }
                    }

                    // remove delay on delayed sequences
                    int NumSequencesToTake = diff - NumClustersToTake;
                    if (NumSequencesToTake > 0)
                    {
                        var sequencestoremovedelay = db.LargeSequences.Where(s => s.ClusterBase.AntiSmashStatus < 5 && s.ClusterBase.NCBIStatus == 2 & s.ClusterBase.Delayed == true).Select(x => x).Take(NumSequencesToTake).ToList();
                        foreach (var seq in sequencestoremovedelay)
                        {
                            seq.ClusterBase.Delayed = false;
                        }
                    }

                    db.SubmitChanges();
                }
            }
        }

        private static void checkStatus(string antismashid, out int antismashstatus, out int sequenceextractionstatus)
        {
            // check status
            string url = "http://antismash.secondarymetabolites.org/display/" + antismashid;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ServicePoint.Expect100Continue = false;
            request.Method = "GET";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.Referer = "http://antismash.secondarymetabolites.org/";
            request.Headers.Add("Accept-Language", "en-CA");
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.KeepAlive = true;

            antismashstatus = 1;
            sequenceextractionstatus = 0;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream receiveStream = response.GetResponseStream();
                string Charset = response.CharacterSet;
                Encoding encode = Encoding.GetEncoding(Charset);
                StreamReader readStream = new StreamReader(receiveStream, encode);
                string responsestring = readStream.ReadToEnd();

                readStream.Close();
                receiveStream.Close();
                response.Close();

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(responsestring);
                HtmlNode parentnode = doc.DocumentNode.SelectSingleNode("/html/body");
                HtmlNode statusnode = parentnode.Descendants("span").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Equals("status")).SingleOrDefault();
                string cleanedstatus = "Unknown";
                if (statusnode != null)
                {
                    cleanedstatus = statusnode.InnerText.Trim();

                    //string statusregex = "Status: <span id=\"status\">.+\n.+.\n.";
                    //     Match statusmatch = Regex.Match(responsestring, statusregex);

                    //     string cleanregex = "Status: <span id=\"status\">.+\n.+";
                    //     string newlineregex = "\n.+";
                    //
                    //if (statusmatch.Success)
                    //{
                    //cleanedstatus = Regex.Replace(Regex.Replace(statusmatch.Value, cleanregex, String.Empty, RegexOptions.IgnoreCase), newlineregex, String.Empty, RegexOptions.IgnoreCase).Trim();
                    //}

                    //if (statusmatch.Success)
                    //{
                    if (cleanedstatus.Equals("done"))
                    {
                        antismashstatus = 3;
                    }
                    else if (cleanedstatus.Contains("queued") || cleanedstatus.Contains("running") || cleanedstatus.Contains("pending"))
                    {
                        antismashstatus = 2;
                    }
                    else if (cleanedstatus.Contains("No secondary metabolite biosynthesis gene clusters detected in this nucleotide file."))
                    {
                        antismashstatus = 6; // No secondary metabolite biosynthesis gene clusters detected in this nucleotide file.
                        sequenceextractionstatus = 20;
                    }
                }
                else
                {
                    antismashstatus = 100; // Unknown error
                    sequenceextractionstatus = 25;

                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "antiSMASH Error", "<b>antiSMASH Error</b><br /><br />cleanedstatus: " + cleanedstatus + "<br />antismashid: " + antismashid);
                }
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Connection timed out"))
                {
                    log.Debug("Error getting antiSMASH status: " + url + Environment.NewLine + e.ToString());
                }
                else
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "antiSMASH Error Checking Status", "<b>antiSMASH Error</b><br /><br />antismashid: " + antismashid + "<br />Error: " + e.Message + "<br /><br />" + e.InnerException + "<br />" + e.StackTrace + "<br />" + e.Data.ToString());
                }
            }
            
        }

        private static void getURL(string genbankaccession, int? bpstart, int? bpend, bool IsEukaryote, byte[] fs, out string antismashid, out int antismashstatus, out string antismashlink)
        {
            antismashid = "";
            antismashlink = "";
            antismashstatus = 0;
            try
            {
                NameValueCollection nvc = new NameValueCollection();
                // legacy mode
                nvc.Add("legacy", "on");
                
                nvc.Add("email", "");
                nvc.Add("file", "");

                nvc.Add("ncbi", genbankaccession);
                if ((bpstart != null) && (bpend != null))
                {
                    nvc.Add("from", bpstart.Value.ToString());
                    nvc.Add("to", bpend.Value.ToString());
                }

                if (IsEukaryote)
                {
                    nvc.Add("eukaryotic", "on");
                }

                for (int i = 2; i < 6; i++)
                {
                    nvc.Add("cluster_" + i.ToString(), "on");
                }

                nvc.Add("trans_table", "11");
                nvc.Add("gene_length", "50");
                nvc.Add("genome_conf", "linear");
                nvc.Add("smcogs", "on");
                nvc.Add("clusterblast", "on");
                nvc.Add("fullhmmer", "on");
                

                HttpWebResponse wr = AntiSmashHttpPostRequest("http://antismash.secondarymetabolites.org/", nvc, fs);

                Stream receiveStream = wr.GetResponseStream();
                Encoding encode = Encoding.UTF8;
                StreamReader readStream = new StreamReader(receiveStream, encode); // need to get id from webresponse, then query status until complete then download
                string responsestring = readStream.ReadToEnd();

                string statusregex = "/display/\\S+?\"";
                Match statusmatch = Regex.Match(responsestring, statusregex);
                if (statusmatch.Success)
                {
                    string cleanedstatusid = Regex.Replace(Regex.Replace(statusmatch.Value, "/display/", String.Empty, RegexOptions.IgnoreCase), "\"$", String.Empty, RegexOptions.IgnoreCase);
                    antismashid = cleanedstatusid;
                    antismashlink = "http://antismash.secondarymetabolites.org/upload/" + cleanedstatusid + "/display.xhtml";
                    antismashstatus = 2;
                }
                readStream.Close();
                receiveStream.Close();
                wr.Close();
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error Submitting antiSmash", "<b>Error Submitting antiSmash</b><br /><br />antiSMASH ID #: " + antismashid + "<br />Error: " + e.Message + "<br /><br />" + e.InnerException + "<br />" + e.StackTrace + "<br />" + e.Data.ToString());
                antismashstatus = 0;
            }
        }

        private static HttpWebResponse AntiSmashHttpPostRequest(string url, NameValueCollection nvc, byte[] filedata)
        {
            HttpWebResponse myHttpWebResponse = null;

            string boundary = "---------------------------7db291103b056a";
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            byte[] initialboundarybytes = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ServicePoint.Expect100Continue = false;
            wr.AllowAutoRedirect = false;
            wr.Method = "POST";
            wr.Accept = "text/html, application/xhtml+xml, */*";
            wr.Referer = "http://antismash.secondarymetabolites.org/";
            wr.Headers.Add("Accept-Language", "en-CA");
            wr.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Headers.Add("Accept-Encoding", "gzip, deflate");
            wr.KeepAlive = true;
            //wr.Headers.Add("Host", "antismash.secondarymetabolites.org");
            wr.Headers.Add("Pragma", "no-cache");

            Stream rs = wr.GetRequestStream();

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            int count = 1;
            foreach (string key in nvc.Keys)
            {
                if (count == 1)
                {
                    rs.Write(initialboundarybytes, 0, initialboundarybytes.Length);
                }
                else
                {
                    rs.Write(boundarybytes, 0, boundarybytes.Length);
                }

                count++;

                if ((key.Equals("file")) && (filedata != null))
                {
                    string formitem = "Content-Disposition: form-data; name=\"seq\"; filename=\"" + nvc["ncbi"] + ".gb\"\r\nContent-Type: application/octet-stream\r\n\r\n";
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    rs.Write(formitembytes, 0, formitembytes.Length);
                    rs.Write(filedata, 0, filedata.Length);
                }
                else if ((key.Equals("file")) && (filedata == null))
                {
                    string formitem = "Content-Disposition: form-data; name=\"seq\"; filename=\"\"\r\nContent-Type: application/octet-stream\r\n\r\n";
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    rs.Write(formitembytes, 0, formitembytes.Length);
                }
                else if ((key.Equals("ncbi")) && (filedata != null))
                {
                    // keep NCBI field blank if file exists
                    string formitem = string.Format(formdataTemplate, key, "");
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    rs.Write(formitembytes, 0, formitembytes.Length);
                }
                else
                {
                    string formitem = string.Format(formdataTemplate, key, nvc[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    rs.Write(formitembytes, 0, formitembytes.Length);
                }
            }
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            // Assign the response object of 'HttpWebRequest' to a 'HttpWebResponse' variable
            myHttpWebResponse = (HttpWebResponse)wr.GetResponse();

            return myHttpWebResponse;
        }

        private static void downloadFile(string cleanedrelativeurl, string datarootpath, string urlrootpath)
        {
            char forwardslash = '/';
            char separatorchar = Path.DirectorySeparatorChar;
            string cleanedrelativepath = cleanedrelativeurl.Replace(forwardslash, separatorchar);
            string absolutepath = datarootpath + cleanedrelativepath;
            string urlpath = urlrootpath + cleanedrelativeurl;

            //need to remove file and extension to check/create directory if needed

            string pathwithfilenameremoved = Path.GetDirectoryName(absolutepath) + Path.DirectorySeparatorChar;
            Directory.CreateDirectory(pathwithfilenameremoved);
            trydownload(urlpath, absolutepath);
        }

        public static byte[] getParsedGenBankFile(ProcessEntity entity)
        {
            // Download file

            string baseurl = @"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";
            string dbparam = "nuccore";
            string idparam = entity.ClusterBase.GenbankAccession;

            string rettypeparam = "gbwithparts";
            string retmodeparam = "text";
            int clusterstart = entity.BPStart.Value;
            int clusterend = entity.BPEnd.Value;

            string url = baseurl + "db=" + dbparam + "&id=" + idparam + "&rettype=" + rettypeparam + "&retmode=" + retmodeparam;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = true;
            request.ServicePoint.Expect100Continue = false;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream receiveStream = response.GetResponseStream();
            Encoding encode = Encoding.UTF8;
            StreamReader readStream = new StreamReader(receiveStream, encode);
            MemoryStream filedata = new MemoryStream();
            StreamWriter writeStream = new StreamWriter(filedata, Encoding.UTF8);

            // Parse
            string line = "";

            // move to source section
            while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("     source", StringComparison.InvariantCulture)))
            {
                writeStream.WriteLine(line);
            }

            // get end of source, change start of source
            string numberpattern = @"\d+\.\.\d+";
            Match numbers = Regex.Match(line, numberpattern);
            string[] location = numbers.Value.Split(new string[] { ".." }, StringSplitOptions.RemoveEmptyEntries);
            int sourcestart = int.Parse(location[0]);
            int sourceend = int.Parse(location[1]);
            line = "     source          " + clusterstart + ".." + clusterend;
            writeStream.WriteLine(line);

            // include org info
            while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("     gene", StringComparison.InvariantCulture)) && line.Contains("/"))
            {
                writeStream.WriteLine(line);
            }

            // move to gene section
            while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("     gene", StringComparison.InvariantCulture)))
            {
                // do nothing
            }

            do
            {
                // check if gene should be included
                numberpattern = @"\d+\.\.\W?\d+";
                numbers = Regex.Match(line, numberpattern);
                location = numbers.Value.Split(new string[] { ".." }, StringSplitOptions.RemoveEmptyEntries);
                location[0] = (Regex.Match(location[0], @"\d+")).Value;
                location[1] = (Regex.Match(location[1], @"\d+")).Value;
                int genestart = int.Parse(location[0]);
                int geneend = int.Parse(location[1]);

                // if good, write to stream until next gene, if not, continue to next gene without writing to stream
                if ((genestart >= clusterstart) && (geneend <= clusterend))
                {
                    writeStream.WriteLine(line);
                    while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("     gene", StringComparison.InvariantCulture)))
                    {
                        writeStream.WriteLine(line);
                    }
                }
                else if (genestart > clusterend)
                {
                    break;
                }
                else
                {
                    while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("     gene", StringComparison.InvariantCulture)))
                    {
                        // do nothing
                    }
                }
            } while (line != null);

            while (((line = readStream.ReadLine()) != null) && (!line.StartsWith("ORIGIN", StringComparison.InvariantCulture)))
            {
                // do nothing
            }

            if (line.Contains("ORIGIN"))
            {
                writeStream.WriteLine(line);
                while ((line = readStream.ReadLine()) != null)
                {
                    writeStream.WriteLine(line);
                }
            }

            response.Close();
            readStream.Close();
            receiveStream.Close();
            writeStream.Flush();

            byte[] returnArray = filedata.ToArray();

            writeStream.Close();
            return returnArray;
        }

        private static void trydownload(string urlpath, string absolutepath)
        {
            try
            {
                if (!File.Exists(absolutepath))
                {
                    WebClient wc = new WebClient();
                    wc.DownloadFile(urlpath, absolutepath);
                    wc.Dispose();
                }
            }
            catch (Exception e)
            {
                if (File.Exists(absolutepath))
                {
                    File.Delete(absolutepath);
                }

                log.Debug("Try Download Error: " + Environment.NewLine + e.ToString());
                if (e.Message.Equals("The operation has timed out."))
                {
                    trydownload(urlpath, absolutepath, 1);
                }
                else if (e.Message.Contains("used by another process"))
                {
                }
                else if (e.Message.Contains("The remote server returned an error: (404) Not Found."))
                {
                }
                else
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error Downloading antiSMASH Data", "<b>Error Downloading antiSMASH Data</b><br /><br />URL Path: " + urlpath + "<br />File Path:" + absolutepath + "<br /><br />Error: " + e.Message + "<br />" + e.InnerException + "<br />" + e.StackTrace + "<br />" + e.Data.ToString());
                }
            }
        }

        private static void trydownload(string urlpath, string absolutepath, int count)
        {
            if (count <= 5)
            {
                try
                {
                    if (!File.Exists(absolutepath))
                    {
                        WebClient wc = new WebClient();
                        wc.DownloadFile(urlpath, absolutepath);
                        wc.Dispose();
                    }
                }
                catch (Exception e)
                {
                    if (File.Exists(absolutepath))
                    {
                        File.Delete(absolutepath);
                    }

                    if (e.Message.Equals("The operation has timed out."))
                    {
                        System.Threading.Thread.Sleep(10000);
                        trydownload(urlpath, absolutepath, count + 1);
                    }
                }
            }
            else
            {
                log.Debug("Try Download Error Too Many Retries: " + Environment.NewLine + System.Web.HttpException.CreateFromLastError("Error downloading.").ToString());
            }
        }

        private static void checkAntiSmashStatus(ProcessEntity entity)
        {
            if (String.IsNullOrEmpty(entity.ClusterBase.AntiSmashID))
            {
                getURL(entity);
            }

            ProcessEntity newEntity = new ProcessEntity();
            if (entity.Type == ProcessEntityType.Cluster)
            {
                newEntity = new ProcessEntity(Clusters.getClusterByID(entity.ID));
            }
            else if (entity.Type == ProcessEntityType.LargeSequence)
            {
                newEntity = new ProcessEntity(LargeSequences.getLargeSequenceRecord(entity.ID));
            }

            // escape function if no url
            if (String.IsNullOrEmpty(newEntity.ClusterBase.AntiSmashLink))
            {
                return;
            }

            int antismashstatus;
            int sequenceextractionstatus;
            string antismashid = newEntity.ClusterBase.AntiSmashID;
            checkStatus(antismashid, out antismashstatus, out sequenceextractionstatus);
            ProcessEntity.UpdateProcessEntity(newEntity, UpdateType.antiSMASH, antismashstatus);
            if (sequenceextractionstatus != 0)
            {
                ProcessEntity.UpdateProcessEntity(newEntity, UpdateType.SequenceExtraction, sequenceextractionstatus);
            }

            if ((entity.Type == ProcessEntityType.Cluster) && (sequenceextractionstatus == 25 || sequenceextractionstatus == 20 || antismashstatus == 6 || antismashstatus == 100))
            {
                // need to notify that extract sequence will not be able to assign type automatically if it hasn't done so already
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    int cfid = CompoundFamilies.getCompoundFamilyIDByClusterID(entity.ID);
                    if (!db.ClusterPathwayTypes.Any(x => x.AddedFromAntiSmash == true && x.CompoundFamilyID == cfid))
                    {
                        string url = ConfigurationManager.AppSettings["WebsiteRootURL"] + "Contribute/EditCompoundFamily.aspx?cfid=" + cfid;
                        string familyname = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;
                        Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Auto-Assign Problem", "<b>An error with antiSMASH has prevented auto-assignment of the cluster type</b><br /><br />Pathway Type for " + familyname + " can be modified at <a href=\"" + url + "\">" + url + "</a>");
                    }
                }
            }
        }

        public static void downloadFiles(ProcessEntity entity)
        {
            try
            {
                if (entity.ClusterBase.AntiSmashStatus == 3)
                {
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.antiSMASH, 4);

                    string datarootpath = "";
                    string filepath = "";
                    if (entity.Type == ProcessEntityType.Cluster)
                    {
                        datarootpath = FileTools.getantiSMASHClustersFolder() + entity.ID + Path.DirectorySeparatorChar;
                        filepath = datarootpath + "display.xhtml";
                    }
                    else if (entity.Type == ProcessEntityType.LargeSequence)
                    {
                        datarootpath = FileTools.getantiSMASHSequencesFolder() + entity.ID + Path.DirectorySeparatorChar;
                        filepath = datarootpath + "display.xhtml";
                    }
                    Directory.CreateDirectory(datarootpath);

                    string url = entity.ClusterBase.AntiSmashLink;
                    string urlrootpath = Utilities.WebTools.getURLRootPath(url, 0);

                    trydownload(url, filepath);

                    string fileContent;
                    using (StreamReader reader = new StreamReader(filepath))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                    string svgjs = "jquery.svg.js";
                    string svgdomjs = "jquery.svgdom.js";
                    string jquery162js = "jquery-1.6.2.min.js";

                    fileContent = fileContent.Replace("../../", @"http://antismash.secondarymetabolites.org/");
                    fileContent = fileContent.Replace("images/", "../../resources/images/");
                    //fileContent = fileContent.Replace("antismash_favicon.ico", "../../resources/images/antismash_favicon.ico");
                    fileContent = fileContent.Replace("src=\"jquery.svg.js\"", "src=\"../../resources/js/" + svgjs + "\"");
                    fileContent = fileContent.Replace("src=\"jquery.svgdom.js\"", "src=\"../../resources/js/" + svgdomjs + "\"");
                    fileContent = fileContent.Replace("src=\"jquery-1.6.2.min.js\"", "src=\"../../resources/js/" + jquery162js + "\"");
                    fileContent = fileContent.Replace("@import \"style.css\";", "@import \"../../resources/style/style.css\";");

                    using (StreamWriter writer = new StreamWriter(filepath))
                    {
                        writer.AutoFlush = true;
                        writer.Write(fileContent);                       
                    }

                    string antismashonlyhrefregex = "href=\"(?!.*:.*).*?\"";
                    string antismashonlysrcregex = "src=\"(?!.*:.*)(?!../images/).*?\"";
                    string clusteronlyregex = "loadsvg\\(\\d+\\)";
                    string geneonlyregex = "loadcblastsvg\\(\\d+,\\d+\\)";

                    using (StreamReader mainfile = new StreamReader(filepath))
                    {
                        string line;
                        while ((line = mainfile.ReadLine()) != null)
                        {
                            // find links that are to antismash resources
                            MatchCollection hrefmatches = Regex.Matches(line, antismashonlyhrefregex, RegexOptions.IgnoreCase);
                            MatchCollection imgmatches = Regex.Matches(line, antismashonlysrcregex, RegexOptions.IgnoreCase);
                            MatchCollection clustermatches = Regex.Matches(line, clusteronlyregex, RegexOptions.IgnoreCase);
                            MatchCollection genematches = Regex.Matches(line, geneonlyregex, RegexOptions.IgnoreCase);

                            foreach (Match match in clustermatches)
                            {
                                int num;
                                bool isok = int.TryParse(((Regex.Match(match.Value, "\\d+")).Value), out num);

                                if (isok)
                                {
                                    string cleanedrelativeurl = "svg/genecluster" + num.ToString() + ".svg";
                                    downloadFile(cleanedrelativeurl, datarootpath, urlrootpath);
                                    string cleanedrelativeurl2 = "svg/clusterblast" + num.ToString() + "_all.svg";
                                    downloadFile(cleanedrelativeurl2, datarootpath, urlrootpath);
                                }
                            }

                            foreach (Match match in genematches)
                            {
                                int num1;
                                int num2;
                                Match genematch = Regex.Match(match.Value, "\\d+");
                                bool isok1 = int.TryParse(genematch.Value, out num1);
                                genematch = genematch.NextMatch();
                                bool isok2 = int.TryParse(genematch.Value, out num2);
                                if (isok1 && isok2)
                                {
                                    string cleanedrelativeurl = "svg/clusterblast" + num1.ToString() + "_" + num2.ToString() + ".svg";
                                    downloadFile(cleanedrelativeurl, datarootpath, urlrootpath);
                                }
                            }

                            foreach (Match match in imgmatches)
                            {
                                if (!match.Value.Contains(svgjs) && !match.Value.Contains(svgdomjs) && !match.Value.Contains(jquery162js))
                                {
                                    string cleanedrelativeurl = Regex.Replace(Regex.Replace(match.Value, "^src=\"", String.Empty, RegexOptions.IgnoreCase), "\"$", String.Empty, RegexOptions.IgnoreCase);
                                    downloadFile(cleanedrelativeurl, datarootpath, urlrootpath);
                                }
                            }

                            foreach (Match match in hrefmatches)
                            {
                                string cleanedrelativeurl = Regex.Replace(Regex.Replace(match.Value, "^href=\"", String.Empty, RegexOptions.IgnoreCase), "\"$", String.Empty, RegexOptions.IgnoreCase);
                                if (!cleanedrelativeurl.Contains(".ico"))
                                {
                                    if (entity.Type == ProcessEntityType.Cluster)
                                    {
                                        downloadFile(cleanedrelativeurl, datarootpath, urlrootpath);
                                    }
                                    else if (entity.Type == ProcessEntityType.LargeSequence)
                                    {
                                        if (cleanedrelativeurl.Contains(".zip") || cleanedrelativeurl.Contains(".embl") || cleanedrelativeurl.Contains(".fasta") || cleanedrelativeurl.Contains(".xls"))
                                        {
                                            // don't download
                                        }
                                        else if (cleanedrelativeurl.Contains("smcogs"))
                                        {
                                            // don't download
                                        }
                                        else
                                        {
                                            downloadFile(cleanedrelativeurl, datarootpath, urlrootpath);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.antiSMASH, 5);
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 0);
                }
            }
            catch (IOException ioex)
            {
                if (ioex.Message.Contains("being used by another process"))
                {
                }
                else
                {
                    throw ioex;
                }
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error Downloading antiSMASH Data", "<b>Error Downloading antiSMASH Data</b><br /><br />Entity Type #: " + entity.Type + "<br />Entity ID: " + entity.ID + "<br />Error: " + e.Message + "<br />" + e.InnerException + "<br />" + e.StackTrace + "<br />" + e.Data.ToString());
                // remove unknown if something else present
                ProcessEntity.UpdateProcessEntity(entity, UpdateType.antiSMASH, 100);
                ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 25);
            }
        }

        private static void getURL(ProcessEntity entity)
        {
            try
            {
                if ((entity.ClusterBase.AntiSmashStatus == 0) || String.IsNullOrEmpty(entity.ClusterBase.AntiSmashID))
                {
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.antiSMASH, 1);

                    string ncbiID = entity.ClusterBase.GenbankAccession;
                    byte[] fs = null;
                    //if ((entity.BPStart != null) && (entity.BPEnd != null))
                    //{
                    //    // get genbank file, parse and assign to filestream
                    //    fs = getParsedGenBankFile(entity);
                    //}

                    string antismashID = "";
                    int antismashstatus = 0;
                    string antismashlink = "";
                    bool IsEukaryote = false;
                    if (entity.ClusterBase.PhylumID == 37) // Eukaryota
                    {
                        IsEukaryote = true;
                    }

                    getURL(ncbiID, entity.BPStart, entity.BPEnd, IsEukaryote, fs, out antismashID, out antismashstatus, out antismashlink);

                    DataClassesDataContext db = new DataClassesDataContext();
                    if (entity.Type == ProcessEntityType.LargeSequence)
                    {
                        LargeSequence sequence = (from s in db.LargeSequences where s.ID == entity.ID select s).Single();
                        sequence.ClusterBase.AntiSmashID = antismashID;
                        sequence.ClusterBase.AntiSmashLink = antismashlink;
                    }
                    else if (entity.Type == ProcessEntityType.Cluster)
                    {
                        Cluster cluster = (from c in db.Clusters
                                           where c.ID == entity.ID
                                           select c).Single();
                        cluster.ClusterBase.AntiSmashID = antismashID;
                        cluster.ClusterBase.AntiSmashLink = antismashlink;
                    }
                    db.SubmitChanges();
                    db.Dispose();
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.antiSMASH, antismashstatus);
                }
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error Submitting antiSmash", "<b>Error Submitting antiSmash</b><br /><br />Entity Type: " + entity.Type + "<br />Entity ID: " + entity.ID + "<br />Error: " + e.Message + "<br /><br />" + e.InnerException + "<br />" + e.StackTrace + "<br />" + e.Data.ToString());
            }
        }
    }
}