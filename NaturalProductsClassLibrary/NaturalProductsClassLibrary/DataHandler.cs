﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NaturalProductsWebsite.Code;
using Utilities;

public enum UpdateType
{
    General,
    antiSMASH,
    NCBI,
    SequenceExtraction
}

public class Clusters
{
    public static Cluster getClusterByID(int ID)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        Cluster cluster = (from c in db.Clusters
                           where c.ID == ID
                           select c).Single();
        db.Dispose();
        return cluster;
    }

    public static List<Cluster> getClusters()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var cluster = (from c in db.Clusters
                       select c)
                                       .OrderBy(p => p.ClusterBase.PhylumID)
                                       .ThenBy(o => o.ClusterBase.OrgName)
                                       .ToList<Cluster>();
        db.Dispose();
        return cluster;
    }

    public static int getNumClusters()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var numclusters = (from c in db.Clusters
                       select c.ID).ToList().Count;
        db.Dispose();
        return numclusters;
    }

    public static List<Cluster> getClustersByFamilyName(string FamilyName)
    {
        var cluster = getClustersByCompoundFamilyID(CompoundFamilies.getCompoundFamilyByName(FamilyName).ID);
        return cluster;
    }

    public static List<Cluster> getClustersByCompoundFamilyID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var cluster = (from c in db.Clusters
                       where c.CompoundFamily.ID == cfid
                       select c)
                                         .OrderBy(p => p.ClusterBase.PhylumID)
                                         .ThenBy(o => o.ClusterBase.OrgName)
                                         .ToList<Cluster>();
        db.Dispose();
        return cluster;
    }

    public static List<Cluster> getClustersByTypeAndFamilyName(string Type, string FamilyName)
    {// not sure
        DataClassesDataContext db = new DataClassesDataContext();
        var cluster = db.Clusters.Where(c => c.CompoundFamily.FamilyName == FamilyName && c.CompoundFamily.ClusterPathwayTypes.Any(d => d.Enum_PathwayType.Type == Type))
                                        .OrderBy(p => p.ClusterBase.PhylumID)
                                        .ThenBy(o => o.ClusterBase.OrgName)
                                        .ToList<Cluster>();
        db.Dispose();
        return cluster;
    }

    public static List<Cluster> getClustersByPhylumOrgAndFamilyName(string Phylum, string OrgName, string FamilyName)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var cluster = (from c in db.Clusters
                       where c.ClusterBase.Enum_Phyla.Phylum == Phylum
                       && c.ClusterBase.OrgName == OrgName
                       && c.CompoundFamily.FamilyName == FamilyName
                       orderby c.ClusterBase.Enum_Phyla.Phylum, c.ClusterBase.OrgName
                       select c).ToList<Cluster>();
        db.Dispose();
        return cluster;
    }

    public static List<Cluster> getClustersByOrgAndFamilyName(string OrganismName, string CompoundFamilyName)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var cluster = (from c in db.Clusters
                       where c.ClusterBase.OrgName == OrganismName &&
                       c.CompoundFamily.FamilyName == CompoundFamilyName
                       select c)
                                        .OrderBy(p => p.ClusterBase.PhylumID)
                                        .ThenBy(o => o.ClusterBase.OrgName)
                                        .ToList<Cluster>();
        db.Dispose();
        return cluster;
    }

    public static int getNextClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var names = db.Clusters.Select(x => new KeyValuePair<int, string>(x.ID, x.CompoundFamily.FamilyName)).ToList();
        db.Dispose();
        int nextid = SortTools.GetNextIDByAlphabeticalOrder(names, clusterid);
        return nextid;
    }

    public static int getPreviousClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var names = db.Clusters.Select(x => new KeyValuePair<int, string>(x.ID, x.CompoundFamily.FamilyName)).ToList();
        db.Dispose();
        int nextid = SortTools.GetPreviousIDByAlphabeticalOrder(names, clusterid);
        return nextid;
    }

    public static void updateClusterLastModified(int ID, UpdateType type, int value)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        Cluster cluster = (from c in db.Clusters
                           where c.ID == ID
                           select c).Single();

        if (type == UpdateType.antiSMASH)
        {
            cluster.ClusterBase.AntiSmashModified = DateTime.Now;
            cluster.ClusterBase.AntiSmashStatus = value;
        }
        else if (type == UpdateType.NCBI)
        {
            cluster.ClusterBase.NCBIModified = DateTime.Now;
            cluster.ClusterBase.NCBIStatus = value;
        }
        else if (type == UpdateType.SequenceExtraction)
        {
            cluster.ClusterBase.SequenceExtractionModified = DateTime.Now;
            cluster.ClusterBase.SequenceExtractionStatus = value;
        }

        cluster.ClusterBase.LastModified = DateTime.Now;
        cluster.ClusterBase.LastModifiedUserID = 0;

        db.SubmitChanges();
        db.Dispose();
    }
}

public class DomainSequencesDataHandler
{
    public static void InsertDomainGrouping(string groupingid)
    { DataClassesDataContext db = new DataClassesDataContext();
            
        DomainGrouping group = db.DomainGroupings.Where(x => x.GroupingID == groupingid).SingleOrDefault();
        if (group == null)
        {
            group = new DomainGrouping();
            group.GroupingID = groupingid;
            db.DomainGroupings.InsertOnSubmit(group);
            db.SubmitChanges();
        }
        db.Dispose();
    }
    public static List<DomainSequence> getDomainsByClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<DomainSequence> sequences = db.DomainSequences.Where(x => x.ClusterID == clusterid).ToList();
        db.Dispose();
        return sequences;
    }

    public static List<DomainSequence> getAllDomainSequences()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<DomainSequence> sequences = db.DomainSequences.ToList();
        db.Dispose();
        return sequences;
    }

    public static List<DomainSequence> getDomainsByGroupingID(string groupingid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<DomainSequence> sequences = db.DomainSequences.Where(x => x.GroupingID == groupingid).ToList();
        db.Dispose();
        return sequences;
    }

    public static DomainSequence getDomainByID(int id)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        DomainSequence sequence = db.DomainSequences.Where(x => x.ID == id).SingleOrDefault();
        db.Dispose();
        return sequence;
    }

    public static List<string> getDomainTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<string> domains = new List<string>();
        domains = (from d in db.DomainSequences where d.DomainType != null select d.DomainType).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return domains;
    }

    public static List<Enum_PathwayType> getClusterTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        var clusterTypes = (from d in db.Enum_PathwayTypes select d).OrderBy(x => x.Type).ToList();
        db.Dispose();
        return clusterTypes;
    }

    public static List<string> getKRActivityTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> krTypes = new List<string>();
        krTypes = (from d in db.DomainSequences where d.KRActivityPrediction != null select d.KRActivityPrediction).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return krTypes;
    }

    public static List<string> getKRStereoChemTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> krTypes = new List<string>();
        krTypes = (from d in db.DomainSequences where d.KRStereoChemPrediction != null select d.KRStereoChemPrediction).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return krTypes;
    }

    public static List<string> getATTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> ATTypes = new List<string>();
        ATTypes = (from d in db.DomainSequences where d.AorATPrediction != null select d.AorATPrediction).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return ATTypes;
    }

    public static int NumDomainsInRepository()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        int count = (from x in db.DomainSequences select x.ID).ToList().Count;
        db.Dispose();
        return count;
    }

    public static List<ProcessEntity> GetUnprocessedDomains()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var clusters = (from c in db.Clusters
                        where (c.ClusterBase.AntiSmashStatus < 5 || c.ClusterBase.SequenceExtractionStatus < 2)
                        select c).ToList<Cluster>().ConvertAll(x => new ProcessEntity(x));

        var sequences = (from s in db.LargeSequences
                         where (s.ClusterBase.AntiSmashStatus < 5 || s.ClusterBase.SequenceExtractionStatus < 2)
                         select s).ToList<LargeSequence>().ConvertAll(x => new ProcessEntity(x));
        db.Dispose();

        clusters.AddRange(sequences);
        return clusters;
    }

    public static List<string> getPhylaNames()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> phyla = new List<string>();

        phyla = (from p in db.DomainSequences
                 select p.Enum_Phyla.Phylum)
                 .Distinct()
                 .OrderBy(x => x)
                 .ToList();
        db.Dispose();
        return phyla;
    }

    public static List<string> getOrganismNames()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> orgs = new List<string>();

        orgs = (from o in db.DomainSequences
                 select o.Organism)
                 .Distinct()
                 .OrderBy(x => x)
                 .ToList();
        db.Dispose();
        return orgs;
    }

    public static List<SequenceGroup> GetGroupedDomains()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var clusterids = db.DomainSequences
                          .Where(x => x.ClusterID != null)
                          .Select(s => s.GroupingID)
                          .Distinct()
                          .ToList();

        var clusters = clusterids.ToDictionary(x => x, x => db.DomainSequences.Where(y => y.GroupingID == x).Select(s => new SequenceEntity() {ParentID = s.ClusterID.Value, GroupingID = s.GroupingID, PathwayType = s.ClusterType, Phylum = s.Cluster.ClusterBase.Enum_Phyla.Phylum, Organism = s.Organism, ContributorID = s.Cluster.ClusterBase.ContributorID, EntityType = ProcessEntityType.Cluster, AorATPrediction = s.AorATPrediction, IsATorA = s.IsATorA, DomainType = s.DomainType, Identifier = s.Identifier, IsKR = s.IsKR, KRActivityPrediction = s.KRActivityPrediction, KRStereoChemPrediction = s.KRStereoChemPrediction, Length = s.Length, GenbankAccession = s.Cluster.ClusterBase.GenbankAccession, GenbankVersion = s.Cluster.ClusterBase.GenbankVersion, BPStart = s.Cluster.BPStart, BPEnd = s.Cluster.BPEnd, CompoundFamilyName = s.Cluster.CompoundFamily.FamilyName, DomainID = s.ID, ClusterNum = s.ClusterNum }).ToList());

        var sequenceids = db.DomainSequences
                         .Where(x => x.LargeSequenceID != null)
                         .Select(s => s.GroupingID)
                         .Distinct()
                         .ToList();

        var sequences = sequenceids.ToDictionary(x => x, x => db.DomainSequences.Where(y => y.GroupingID == x).Select(s => new SequenceEntity() {ParentID = s.LargeSequenceID.Value, GroupingID = s.GroupingID, PathwayType = s.ClusterType, Phylum = s.LargeSequence.ClusterBase.Enum_Phyla.Phylum, Organism = s.Organism, ContributorID = s.LargeSequence.ClusterBase.ContributorID, EntityType = ProcessEntityType.LargeSequence, AorATPrediction = s.AorATPrediction, IsATorA = s.IsATorA, DomainType = s.DomainType, Identifier = s.Identifier, IsKR = s.IsKR, KRActivityPrediction = s.KRActivityPrediction, KRStereoChemPrediction = s.KRStereoChemPrediction, Length = s.Length, GenbankAccession = s.LargeSequence.ClusterBase.GenbankAccession, GenbankVersion = s.LargeSequence.ClusterBase.GenbankVersion, BPStart = null, BPEnd = null, CompoundFamilyName = "Unknown", DomainID = s.ID, ClusterNum = s.ClusterNum }).ToList());
        db.Dispose();

        List<SequenceGroup> grouped = new List<SequenceGroup>();
        foreach (var item in clusters)
        {
            SequenceGroup group = new SequenceGroup();
            group.ParentID = item.Value.Select(x => x.ParentID).First();
            group.GroupingID = item.Value.Select(x => x.GroupingID).First();
            group.PathwayType = item.Value.Select(x => x.PathwayType).First();
            group.Phylum = item.Value.Select(x => x.Phylum).First();
            group.Organism = item.Value.Select(x => x.Organism).First();
            group.EntityType = ProcessEntityType.Cluster;
            group.ContributorID = item.Value.Select(x => x.ContributorID).First();
            group.GenbankAccession = item.Value.Select(x => x.GenbankAccession).First();
            group.GenbankVersion = item.Value.Select(x => x.GenbankVersion).First();
            group.CompoundFamilyName = item.Value.Select(x => x.CompoundFamilyName).First();
            group.ClusterNum = item.Value.Select(x => x.ClusterNum).First();
            group.Items = item.Value;
            grouped.Add(group);
        }

        foreach (var sequenceitem in sequences)
        {
            SequenceGroup group = new SequenceGroup();
            group.ParentID = sequenceitem.Value.Select(x => x.ParentID).First();
            group.GroupingID = sequenceitem.Value.Select(x => x.GroupingID).First();
            group.PathwayType = sequenceitem.Value.Select(x => x.PathwayType).First();
            group.Phylum = sequenceitem.Value.Select(x => x.Phylum).First();
            group.Organism = sequenceitem.Value.Select(x => x.Organism).First();
            group.EntityType = ProcessEntityType.LargeSequence;
            group.ContributorID = sequenceitem.Value.Select(x => x.ContributorID).First();
            group.CompoundFamilyName = sequenceitem.Value.Select(x => x.CompoundFamilyName).First();
            group.GenbankAccession = sequenceitem.Value.Select(x => x.GenbankAccession).First();
            group.GenbankVersion = sequenceitem.Value.Select(x => x.GenbankVersion).First();
            group.ClusterNum = sequenceitem.Value.Select(x => x.ClusterNum).First();
            group.Items = sequenceitem.Value;
            grouped.Add(group);
        }

        return grouped;
    }
}

public class Phyla
{
    public static string getPhylumName(string orgname)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        string phylum = "";
        phylum = (from p in db.Clusters
                  where p.ClusterBase.OrgName == orgname
                  select p.ClusterBase.Enum_Phyla.Phylum).First();
        db.Dispose();
        return phylum;
    }

    public static List<Enum_Phyla> getPhyla()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<Enum_Phyla> phyla = (from p in db.Clusters
                                  select p.ClusterBase.Enum_Phyla)
                                  .Distinct()
                                  .OrderBy(x => x.Phylum)
                                  .ToList();
        db.Dispose();
        return phyla;
    }

    public static List<string> getPhylaNames()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> phyla = new List<string>();

        phyla = (from p in db.Clusters
                 select p.ClusterBase.Enum_Phyla.Phylum)
                 .Distinct()
                 .OrderBy(x => x)
                 .ToList();
        db.Dispose();
        return phyla;
    }

    public static int getPhylumID(string phylumName)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        int phylumID = (from p in db.Enum_Phyla where p.Phylum == phylumName select p.ID).Single();
        db.Dispose();
        return phylumID;
    }

    public static string getPhylumName(int phylumID)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        string phylumName = (from p in db.Enum_Phyla where p.ID == phylumID select p.Phylum).Single();
        db.Dispose();
        return phylumName;
    }
}

public class Organisms
{
    public static List<string> getOrganismNamesByPhylum(string phylum)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> orgs = new List<string>();
        orgs = (from o in db.Clusters
                where o.ClusterBase.Enum_Phyla.Phylum == phylum
                select o.ClusterBase.OrgName).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return orgs;
    }

    public static List<string> getOrganismNamesByCompoundFamily(string compoundfamily)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> orgs = new List<string>();
        orgs = (from o in db.Clusters
                where o.CompoundFamily.FamilyName == compoundfamily
                select o.ClusterBase.OrgName).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return orgs;
    }

    public static List<string> getOrgNames()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> orgs = new List<string>();
        orgs = (from o in db.Clusters
                select o.ClusterBase.OrgName).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return orgs;
    }
}

public class Characteristics
{
    public static List<Enum_Characteristic> getCharacteristics()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<Enum_Characteristic> chars = (from c in db.Enum_Characteristics
                            select c).Distinct().OrderBy(x => x.Characteristic).ToList();
        db.Dispose();
        return chars;
    }

    public static List<Enum_CharacteristicsCategory> getCharacteristicsCategories()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<Enum_CharacteristicsCategory> charcats = (from c in db.Enum_CharacteristicsCategories
                                           select c).Distinct().OrderBy(x => x.CategoryName).ToList();
        db.Dispose();
        return charcats;
    }

    public static List<Enum_CharacteristicsCategory> getCharacteristicsCategoriesByCompoundFamilyID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<Enum_CharacteristicsCategory> charcats = (from c in db.ClusterCharacteristics where c.CompoundFamilyID == cfid
                                                       select c).ToList().Select(x => x.Enum_Characteristic.Enum_CharacteristicsCategory).ToList();
        db.Dispose();
        return charcats;
    }

    public static List<Enum_Characteristic> getCharacteristicsByCategory(int catid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<Enum_Characteristic> chars = (from c in db.Enum_Characteristics where c.Category == catid
                                                       select c).Distinct().OrderBy(x => x.Characteristic).ToList();
        db.Dispose();
        return chars;
    }

    public static List<Enum_Characteristic> getListOfCharacteristicsByCompoundFamilyID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();

        var chars = (from c in db.ClusterCharacteristics where c.CompoundFamilyID == cfid select c.Enum_Characteristic).OrderBy(x => x.Characteristic).ToList();
        db.Dispose();
        return chars;
    }


    public static List<string> getListOfCharacteristicsByCompoundFamilyIDAndCategoryID(int cfid, int catid)
    {
        DataClassesDataContext db = new DataClassesDataContext();

        List<string> chars = (from c in db.ClusterCharacteristics where c.CompoundFamilyID == cfid && c.Enum_Characteristic.Enum_CharacteristicsCategory.ID == catid select c.Enum_Characteristic.Characteristic).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return chars;
    }

    public static void DeleteCompoundFamilyToCharacteristicRelationship(int cfid, List<int> charids)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        foreach (int charid in charids)
        {
            ClusterCharacteristic charToDelete = (from x in db.ClusterCharacteristics where x.CompoundFamilyID == cfid && x.CharacteristicID == charid select x).SingleOrDefault();

            if (charToDelete != null)
            {
                db.ClusterCharacteristics.DeleteOnSubmit(charToDelete);
                db.SubmitChanges();
            }
        }
        db.Dispose();
    }

    public static void AddCompoundFamilyToCharacteristicRelationship(int cfid, List<int> charids)
    {
         DataClassesDataContext db = new DataClassesDataContext();
         foreach (int charid in charids)
         {


             ClusterCharacteristic charToAdd = (from x in db.ClusterCharacteristics where x.CompoundFamilyID == cfid && x.CharacteristicID == charid select x).SingleOrDefault();
             if (charToAdd == null)
             {
                 charToAdd = new ClusterCharacteristic();
                 charToAdd.CompoundFamilyID = cfid;
                 charToAdd.CharacteristicID = charid;

                 db.ClusterCharacteristics.InsertOnSubmit(charToAdd);
                 try
                 {
                     db.SubmitChanges();
                 }
                 catch (Exception ex)
                 {
                     Utilities.Email.Send(System.Configuration.ConfigurationManager.AppSettings["AdminEmail"], "Error adding characteristics", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                 }
             }
         }
                    db.Dispose();
    }
}

public class CompoundFamilies
{
    public static List<CompoundFamily> getCompoundFamilies()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<CompoundFamily> compoundfamilies = new List<CompoundFamily>();
        compoundfamilies = (from c in db.CompoundFamilies
                            select c).Distinct().OrderBy(x => x.FamilyName).ToList();
        db.Dispose();
        return compoundfamilies;
    }

    public static CompoundFamily getCompoundFamilyByName(string familyname)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        CompoundFamily compoundfamily = (from c in db.CompoundFamilies
                                         where c.FamilyName == familyname
                                         select c).Single();
        db.Dispose();
        return compoundfamily;
    }

    public static CompoundFamily getCompoundFamilyByID(int ID)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        CompoundFamily compoundfamily = (from c in db.CompoundFamilies
                                         where c.ID == ID
                                         select c).Single();
        db.Dispose();
        return compoundfamily;
    }

    public static CompoundFamily getCompoundFamilyByClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        CompoundFamily compoundfamily = (from c in db.Clusters
                                         where c.ID == clusterid
                                         select c.CompoundFamily).Single();
        db.Dispose();
        return compoundfamily;
    }

    public static int getCompoundFamilyIDByClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        int compoundfamilyid = (from c in db.Clusters
                                where c.ID == clusterid
                                select c.CompoundFamily.ID).Single();
        db.Dispose();
        return compoundfamilyid;
    }

    public static List<string> getNamesOfCompoundFamiliesByPathwayType(string pathwaytype)
    {//
        DataClassesDataContext db = new DataClassesDataContext(); ;
       
        var ptype = db.Enum_PathwayTypes.Where(p => p.Type == pathwaytype).Single();
        List<string> compoundfamilies = db.ClusterPathwayTypes.Where(x => x.PathwayTypeID == ptype.ID).Select(x => x.CompoundFamily.FamilyName).ToList(); 
           // .Select(x => x.CompoundFamily.FamilyName).ToList();           
        
        db.Dispose();
        return compoundfamilies;
    }

    public static List<string> getCompoundFamiliesByOrganism(string org)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> compoundfamilies = new List<string>();
        compoundfamilies = (from c in db.Clusters
                            where c.ClusterBase.OrgName == org
                            select c.CompoundFamily.FamilyName).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return compoundfamilies;
    }

    public static List<string> getNamesOfCompoundFamilies()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> compoundfamilies = new List<string>();
        compoundfamilies = (from c in db.Clusters
                            select c.CompoundFamily.FamilyName).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return compoundfamilies;
    }

    public static CompoundFamilyImage getCompoundFamilyImage(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        CompoundFamilyImage cfimage = (from i in db.CompoundFamilyImages
                                       where i.cfid == cfid
                                       select i).SingleOrDefault();
        return cfimage;
    }

    public static int getNextCFID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var names = db.CompoundFamilies.OrderBy(x => x.FamilyName).Select(x => new KeyValuePair<int, string>(x.ID, x.FamilyName)).ToList();
        db.Dispose();
        int nextid = SortTools.GetNextIDByAlphabeticalOrder(names, cfid);
        return nextid;      
    }

    public static int getPreviousCFID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var names = db.CompoundFamilies.OrderBy(x => x.FamilyName).Select(x => new KeyValuePair<int, string>(x.ID, x.FamilyName)).ToList();
        db.Dispose();
        int previousid = SortTools.GetPreviousIDByAlphabeticalOrder(names, cfid);
        return previousid;     
    }

    public static bool ImageExists(int cfid)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            bool exists = false;
            var compoundFamilyImageRecord = (from c in db.CompoundFamilyImages
                                             where c.cfid == cfid
                                             select c).SingleOrDefault();


            if (compoundFamilyImageRecord.Image != null && compoundFamilyImageRecord.Image.Length != 0)
            {
                exists = true;
            }
            return exists;
        }
    }

    public static void updateClusterLastModified(int cfid, int userid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        CompoundFamily cfamily = (from c in db.CompoundFamilies
                           where c.ID == cfid
                           select c).Single();

       
        cfamily.LastModified = DateTime.Now;
        cfamily.LastModifiedUserID = userid;

        db.SubmitChanges();
        db.Dispose();
    }

    public static void updateClusterLastModified(int cfid, string username)
    {
        updateClusterLastModified(cfid, UserInfos.getUserInfoByUserName(username).ID);
    }
}

public class RelatedCompoundFamilies
{
    public static List<string> getRelatedCompoundFamiliesByCompoundFamily(int compoundfamilyid)
    {
        List<string> returnList = new List<string>();

        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> primary = (from r in db.RelatedCompoundFamilies
                                where r.FamilyNameID == compoundfamilyid
                                select r.CompoundFamily1.FamilyName).ToList<string>();

        List<string> secondary = (from r in db.RelatedCompoundFamilies
                                  where r.RelatedFamilyNameID == compoundfamilyid
                                  select r.CompoundFamily.FamilyName).ToList<string>();
        db.Dispose();
        returnList = primary.Union(secondary).ToList<string>();
        returnList = returnList.Distinct().ToList<string>();
        returnList.Sort();
        return returnList;
    }

    public static List<string> getRelatedCompoundFamiliesByClusterID(int clusterid)
    {
        List<string> returnList = new List<string>();

        DataClassesDataContext db = new DataClassesDataContext(); ;
        int compoundfamilyid = (from c in db.Clusters
                                where c.ID == clusterid
                                select c.CompoundFamily.ID).Single();

        returnList = getRelatedCompoundFamiliesByCompoundFamily(compoundfamilyid);
        db.Dispose();
        return returnList;
    }

    public static void insertNewRelatedCompoundFamilyRelationship(int cfid1, int cfid2, bool auto)
    {
        if (cfid1 != cfid2)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            RelatedCompoundFamily relationshipToAdd = (from x in db.RelatedCompoundFamilies
                                                       where ((x.FamilyNameID == cfid1 && x.RelatedFamilyNameID == cfid2)
                                                       || (x.RelatedFamilyNameID == cfid1 && x.FamilyNameID == cfid2))
                                                       select x).SingleOrDefault();
            if (relationshipToAdd == null)
            {
                relationshipToAdd = new RelatedCompoundFamily();
                relationshipToAdd.FamilyNameID = cfid1;
                relationshipToAdd.RelatedFamilyNameID = cfid2;
                relationshipToAdd.Auto = auto;

                db.RelatedCompoundFamilies.InsertOnSubmit(relationshipToAdd);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(System.Configuration.ConfigurationManager.AppSettings["AdminEmail"], "Error adding related compound family relationship", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                }
            }
            db.Dispose();
        }
    }

    public static void insertNewRelatedCompoundFamilyRelationship(int cfid1, int cfid2)
    {
        insertNewRelatedCompoundFamilyRelationship(cfid1, cfid2, false);
    }
}

public class Synonyms
{
    public static List<Synonym> getSynonyms()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var synonyms = (from s in db.Synonyms
                        select s).ToList();
        db.Dispose();
        return synonyms;
    }

    public static List<Synonym> getSynonymsByCompoundFamilyID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;

        var synonyms = (from s in db.Synonyms
                        where s.CompoundFamilyID == cfid
                        orderby s.Synonym1 ascending
                        select s).ToList();
        db.Dispose();
        return synonyms;
    }

    public static List<Synonym> getSynonymsByClusterID(int clusterid)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        var cfid = (from c in db.Clusters
                    where c.ID == clusterid
                    select c.CompoundFamily.ID).Single();

        var synonyms = (from s in db.Synonyms
                        where s.CompoundFamilyID == cfid
                        orderby s.Synonym1 ascending
                        select s).ToList();
        db.Dispose();
        return synonyms;
    }

    public static bool insertSynonym(int cfid, string synonym)
    {
        bool good = false;
        DataClassesDataContext db = new DataClassesDataContext(); ;

        if (!db.Synonyms.Exists(x => x.CompoundFamilyID == cfid && x.Synonym1 == synonym))
        {
            try
            {
                Synonym newSynonym = new Synonym();
                newSynonym.CompoundFamilyID = cfid;
                newSynonym.Synonym1 = synonym;
                db.Synonyms.InsertOnSubmit(newSynonym);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + e.GetType().ToString() + "<br />" + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                good = false;
            }
        }
        else
        {
            good = true;
        }
        db.Dispose();
        return good;
    }

    public static bool deleteSynonym(int cfid, int synid)
    {
        bool returnValue = false;
        DataClassesDataContext db = new DataClassesDataContext(); ;
        var recordToBeDeleted = (from x in db.Synonyms
                                 where x.CompoundFamilyID == cfid && x.ID == synid
                                 select x).SingleOrDefault();

        if (recordToBeDeleted != null)
        {
            try
            {
                db.Synonyms.DeleteOnSubmit(recordToBeDeleted);
                db.SubmitChanges();
                returnValue = true;
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + e.GetType().ToString() + "<br />" + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                returnValue = false;
            }
        }
        else
        {
            returnValue = true;
        }
        db.Dispose();
        return returnValue;
    }

    public static bool updateSynonym(int cfid, int synid, string newSynonymSpelling)
    {
        bool returnValue = false;
        DataClassesDataContext db = new DataClassesDataContext(); ;
        var recordToBeUpdated = (from x in db.Synonyms
                                 where x.CompoundFamilyID == cfid && x.ID == synid
                                 select x).SingleOrDefault();

        if (recordToBeUpdated != null)
        {
            try
            {
                recordToBeUpdated.Synonym1 = newSynonymSpelling;
                db.SubmitChanges();
                returnValue = true;
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + e.GetType().ToString() + "<br />" + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                returnValue = false;
            }
        }
        else { returnValue = false; }
        db.Dispose();
        return returnValue;
    }
}

public class PathwayTypes
{
    public static List<string> getPathwayTypesByOrganism(string org)
    { //
        DataClassesDataContext db = new DataClassesDataContext();
        List<string> pathwaytypes = new List<string>();
        pathwaytypes = db.Clusters.Where(c => c.ClusterBase.OrgName == org).SelectMany(c => c.CompoundFamily.ClusterPathwayTypes).Select(x => x.Enum_PathwayType.Type).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return pathwaytypes;
    }

    public static List<string> getPathwayTypesByClusterID(int clusterid)
    { //
        int cfid = CompoundFamilies.getCompoundFamilyIDByClusterID(clusterid);
        return getPathwayTypesByCompoundFamilyID(cfid);
    }

    public static List<string> getPathwayTypesByCompoundFamilyID(int cfid)
    { //
        DataClassesDataContext db = new DataClassesDataContext();
        List<string> pathwaytypes = new List<string>();
        pathwaytypes = db.ClusterPathwayTypes.Where(x => x.CompoundFamilyID == cfid).Select(x => x.Enum_PathwayType.Type).OrderBy(x => x).ToList();
        db.Dispose();
        return pathwaytypes;
    }

    public static List<int> getPathwayTypeIDsByCompoundFamilyID(int cfid)
    { 
        DataClassesDataContext db = new DataClassesDataContext();
        List<int> pathwaytypes = db.ClusterPathwayTypes.Where(x => x.CompoundFamilyID == cfid).Select(x => x.PathwayTypeID).ToList();
        db.Dispose();
        return pathwaytypes;
    }

    public static List<Enum_PathwayType> getPathwayTypes()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<Enum_PathwayType> pathwaytypes = (from c in db.Enum_PathwayTypes select c).Distinct().OrderBy(x => x.Type).ToList();
        db.Dispose();
        return pathwaytypes;
    }

    public static List<string> getPathwayTypeNames()
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        List<string> pathwaytypes = new List<string>();
        pathwaytypes = (from p in db.Enum_PathwayTypes
                        select p.Type).Distinct().OrderBy(x => x).ToList();
        db.Dispose();
        return pathwaytypes;
    }

    public static int getPathwayID(string PathwayName)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        int pathwayid = (from c in db.Enum_PathwayTypes where c.Type == PathwayName select c.ID).Single();
        db.Dispose();
        return pathwayid;
    }

    public static void DeletePathwayToCompoundFamilyRelationship(int cfid, List<int> pathwayids)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        foreach (int pid in pathwayids)
        {
            ClusterPathwayType relationshipToDelete = (from x in db.ClusterPathwayTypes where x.CompoundFamilyID == cfid && x.PathwayTypeID == pid select x).SingleOrDefault();

            if (relationshipToDelete != null)
            {
                db.ClusterPathwayTypes.DeleteOnSubmit(relationshipToDelete);
                db.SubmitChanges();
            }
        }
        db.Dispose();
    }

    public static void AddPathwayTypeToCompoundFamilyRelationship(int cfid, List<int> pathwayids, bool isAntiSmash)
    {
         DataClassesDataContext db = new DataClassesDataContext();
         foreach (int pid in pathwayids)
         {


            ClusterPathwayType relationshipToAdd = (from x in db.ClusterPathwayTypes where x.CompoundFamilyID == cfid && x.PathwayTypeID == pid select x).SingleOrDefault();
             if (relationshipToAdd == null)
             {
                 relationshipToAdd = new ClusterPathwayType();
                 relationshipToAdd.CompoundFamilyID = cfid;
                 relationshipToAdd.PathwayTypeID = pid;
                 relationshipToAdd.AddedFromAntiSmash = isAntiSmash;

                 db.ClusterPathwayTypes.InsertOnSubmit(relationshipToAdd);
                 try
                 {
                     db.SubmitChanges();
                 }
                 catch (Exception ex)
                 {
                     Utilities.Email.Send(System.Configuration.ConfigurationManager.AppSettings["AdminEmail"], "Error adding pathway type to compound family relationship", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                 }
             }
         }
                    db.Dispose();
    }
    
}

public class DomainPathwayTypes
{
    public static List<int> GetPathwayTypesByGroupingID(string groupingid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var types = db.DomainPathwayTypes.Where(x => x.GroupingID == groupingid).Select(x => x.PathwayTypeID).ToList();
        db.Dispose();
        return types;
    }

    public static void DeleteDomainToPathwayTypeRelationship(string groupingid, List<int> pathwayids)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        foreach (int pid in pathwayids)
        {
            DomainPathwayType relationshipToDelete = (from x in db.DomainPathwayTypes where x.GroupingID == groupingid && x.PathwayTypeID == pid select x).SingleOrDefault();

            if (relationshipToDelete != null)
            {
                db.DomainPathwayTypes.DeleteOnSubmit(relationshipToDelete);
                db.SubmitChanges();
            }
        }
        db.Dispose();
    }

    public static void AddDomainToPathwayTypeRelationship(string groupingid, List<int> pathwayids)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        foreach (int pid in pathwayids)
        {


           DomainPathwayType relationshipToAdd = (from x in db.DomainPathwayTypes where x.GroupingID == groupingid && x.PathwayTypeID == pid select x).SingleOrDefault();
            if (relationshipToAdd == null)
            {
                relationshipToAdd = new DomainPathwayType();
                relationshipToAdd.GroupingID = groupingid;
                relationshipToAdd.PathwayTypeID = pid;

                db.DomainPathwayTypes.InsertOnSubmit(relationshipToAdd);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(System.Configuration.ConfigurationManager.AppSettings["AdminEmail"], "Error adding pathway type to domain relationship", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                }
            }
        }
        db.Dispose();
    }
}

public class PubChemPharmActionDataHandler
{
    public static List<PharmActionsType> getRootNodes()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<PharmActionsType> rootnodes = db.PharmActionsTypes.Where(x => x.ParentID == 0).OrderBy(x => x.TypeDescription).ToList();
        db.Dispose();
        return rootnodes;
    }

    public static PharmActionsType getNode(int parentid, string description)
    {        
        DataClassesDataContext db = new DataClassesDataContext();
        PharmActionsType node = db.PharmActionsTypes.Where(x => x.ParentID == parentid && x.TypeDescription == description).SingleOrDefault();
           
        db.Dispose();
        return node;
    }

    public static void addPharmActionLink(int cfid, int pubchemdbid, int typedbid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        PharmActions newactionlink = db.PharmActions.Where(x => x.cfid == cfid && x.Type == typedbid && x.Source == pubchemdbid).SingleOrDefault();
        if (newactionlink == null)
        {
            newactionlink = new PharmActions();
            newactionlink.Source = pubchemdbid;
            newactionlink.Type = typedbid;
            newactionlink.cfid = cfid;
            db.PharmActions.InsertOnSubmit(newactionlink);
            db.SubmitChanges();
        }
        db.Dispose();
    }


    public static void addPharmActionType(int parentid, string description)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        PharmActionsType newtype = db.PharmActionsTypes.Where(x => x.ParentID == parentid && x.TypeDescription == description).SingleOrDefault();
        if (newtype == null)
        {
            newtype = new PharmActionsType();
            newtype.TypeDescription = description;           
            newtype.ParentID = parentid;
            db.PharmActionsTypes.InsertOnSubmit(newtype);
            db.SubmitChanges();
        }
        db.Dispose();
    }
}
public class PubChemIDDataHandler
{

    public static List<PubChemIDs> getPubChemIDsForStructureSearch()
    {
        List<PubChemIDs> beststructureids = new List<PubChemIDs>();
        DataClassesDataContext db = new DataClassesDataContext();
        var ids = (from s in db.PubChemIDs
                               where s.Type == "C"                              
                               select s).ToList();
        List<int> cfids = ids.Select(x => x.cfid).ToList().Distinct().ToList();
        foreach (int cfid in cfids)
        {
            // find best match
            PubChemIDs bestmatch = ids.Where(x => x.cfid == cfid && x.NumCovalentUnits == 1 && x.IsomericSMILES != null).ToList().OrderByDescending(x => x.TotalDefinedCenters).FirstOrDefault();
            if (bestmatch != null)
            {
                beststructureids.Add(bestmatch);
            }
        }
        
        db.Dispose();
        return beststructureids;
    }
    public static List<PubChemIDs> getPubChemIDsByCFID(int cfid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<PubChemIDs> ids = db.PubChemIDs.Where(x => x.cfid == cfid).ToList();
        db.Dispose();
        return ids;
    }

    public static void addPubChemId(int cfid, string type, string pcid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        PubChemIDs recordtoadd = db.PubChemIDs.Where(x => x.cfid == cfid && x.Type == type && x.PubChemID == pcid).SingleOrDefault();
        if (recordtoadd == null)
        {
            recordtoadd = new PubChemIDs();
            recordtoadd.PubChemID = pcid;
            recordtoadd.cfid = cfid;
            recordtoadd.Type = type;
            
            db.PubChemIDs.InsertOnSubmit(recordtoadd);
            db.SubmitChanges();
        }
        db.Dispose();
    }

    public static void removePubChemId(int cfid, string type, string pcid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        PubChemIDs recordtoremove = db.PubChemIDs.Where(x => x.cfid == cfid && x.Type == type && x.PubChemID == pcid).SingleOrDefault();
        if (recordtoremove != null)
        {
            db.PubChemIDs.DeleteOnSubmit(recordtoremove);
            db.SubmitChanges();
        }
        db.Dispose();
    }
}

public class PubMedArticles
{
    private enum ReferenceType
    {
        Sequencing,
        User
    }

    public static List<PubMedArticle> getSequencingArticlesByClusterID(int ID)
    {
        return getArticles(ID, ReferenceType.Sequencing);
    }

    public static List<PubMedArticle> getUserArticlesByClusterID(int ID)
    {
        return getArticles(ID, ReferenceType.User);
    }

    private static List<PubMedArticle> getArticles(int clusterid, ReferenceType type)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        List<PubMedArticle> articles = new List<PubMedArticle>();

        Cluster cluster = (from c in db.Clusters
                           where c.ID == clusterid
                           select c).Single();

        List<long> pmids = new List<long>();

        if (type == ReferenceType.Sequencing)
        {
            pmids = StringTools.DeserializeFromSemiColonSeparatedList(cluster.SequencingReferences).Select(x => long.Parse(x)).ToList();
        }
        else if (type == ReferenceType.User)
        {
            pmids = StringTools.DeserializeFromSemiColonSeparatedList(cluster.UserReferences).Select(x => long.Parse(x)).ToList();
        }

        foreach (long pmid in pmids)
        {
            PubMedArticle article = (from p in db.PubMedArticles
                                     where p.PMID == pmid
                                     select p).SingleOrDefault();
            if (article != null)
            {
                articles.Add(article);
            }
        }

        db.Dispose();
        return articles;
    }
}

public class LargeSequences
{
    public static LargeSequence getLargeSequenceRecord(string genbankaccession, int genbankversion)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        LargeSequence sequenceRecord = (from s in db.LargeSequences where s.ClusterBase.GenbankAccession == genbankaccession && s.ClusterBase.GenbankVersion == genbankversion select s).SingleOrDefault();
        return sequenceRecord;
    }

    public static LargeSequence getLargeSequenceRecord(int sequenceID)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        LargeSequence sequenceRecord = (from s in db.LargeSequences where s.ID == sequenceID select s).SingleOrDefault();
        return sequenceRecord;
    }

    public static void updateLargeSequenceLastModified(int ID, UpdateType type, int value)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        LargeSequence sequence = (from c in db.LargeSequences
                                  where c.ID == ID
                                  select c).Single();

        if (type == UpdateType.antiSMASH)
        {
            sequence.ClusterBase.AntiSmashModified = DateTime.Now;
            sequence.ClusterBase.AntiSmashStatus = value;
        }
        else if (type == UpdateType.NCBI)
        {
            sequence.ClusterBase.NCBIModified = DateTime.Now;
            sequence.ClusterBase.NCBIStatus = value;
        }
        else if (type == UpdateType.SequenceExtraction)
        {
            sequence.ClusterBase.SequenceExtractionModified = DateTime.Now;
            sequence.ClusterBase.SequenceExtractionStatus = value;
        }

        sequence.ClusterBase.LastModified = DateTime.Now;
        sequence.ClusterBase.LastModifiedUserID = 0;

        db.SubmitChanges();
        db.Dispose();
    }
}

public class UserInfos
{
    public static UserInfo getUserInfoByID(int ID)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        UserInfo user = (from u in db.UserInfo where u.ID == ID select u).Single();
        db.Dispose();
        return user;
    }

    public static UserInfo getUserInfoByUserName(string username)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        UserInfo user = (from u in db.UserInfo where u.User_Name == username select u).Single();
        db.Dispose();
        return user;
    }
}