﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using log4net;
using log4net.Config;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    /// <summary>
    /// Summary description for NCBI
    /// </summary>
    public class NCBI
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NCBI));

        public static void triggerNCBIDownload()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
            DataClassesDataContext db = new DataClassesDataContext();
            DateTime recent = DateTime.Now.AddMinutes(-5d);
            var clusters = (from c in db.Clusters
                            where (c.ClusterBase.NCBIStatus == 0) || (c.ClusterBase.NCBIStatus == 1 && c.ClusterBase.NCBIModified.Value < recent)
                            select c).ToList<Cluster>().ConvertAll(x => new ProcessEntity(x));

            var largeSequences = (from c in db.LargeSequences
                                  where (c.ClusterBase.NCBIStatus == 0) || (c.ClusterBase.NCBIStatus == 1 && c.ClusterBase.NCBIModified.Value < recent)
                                  select c).ToList<LargeSequence>().ConvertAll(x => new ProcessEntity(x));
            db.Dispose();
            clusters.AddRange(largeSequences);

            int clustercount = clusters.Count;
            log.Info("NCBI clusters to Process: " + clustercount);
            if ((clustercount) > 0)
            {
                foreach (var cluster in clusters)
                {
                    getNCBIData(cluster);
                }
            }
            log.Info("Finished processing NCBI clusters.");
        }

        public static int GetSequenceLength(string accessionstring)
        {
            XPathNavigator nav = NCBIUtils.GetNCBIXPathNavigator(NCBIRequestType.eFetch, accessionstring, "nuccore", "fasta", "xml");
            int ginum = -1;
            if (nav != null)
            {
                string lengthasstring = nav.SelectSingleNode("/TSeqSet/TSeq/TSeq_length").InnerXml;
                int.TryParse(lengthasstring, out ginum);
            }

            return ginum;
        }

        private static XPathNavigator getXMLGenBankData(string idparam)
        {
            return NCBIUtils.GetNCBIXPathNavigator(NCBIRequestType.eFetch, idparam, "nuccore", "native", "xml");
        }

        public static void getNCBIData(ProcessEntity entity)
        {
            ProcessEntity.UpdateProcessEntity(entity, UpdateType.NCBI, 1);
            log.Info("Processing entity: " + entity.ID);
            try
            {
                XPathNavigator nav = getXMLGenBankData(entity.ClusterBase.GenbankAccession);
                if (nav != null)
                {
                    log.Info("Extracting data");
                    // update fields that are the same for both
                    try { entity.ClusterBase.OrgName = nav.SelectSingleNode(@"//Org-ref_taxname").InnerXml; }
                    catch { entity.ClusterBase.OrgName = "Unknown"; }
                    string lineage = ExtractLineage(nav);
                    entity.ClusterBase.PhylumID = ExtractKingdomOrPhylumID(lineage);

                    DataClassesDataContext db = new DataClassesDataContext();
                    if (entity.Type == ProcessEntityType.LargeSequence)
                    {
                        //get Large Sequence and update properties
                        LargeSequence sequence = (from s in db.LargeSequences where s.ID == entity.ID select s).Single();
                        sequence.ClusterBase.OrgName = entity.ClusterBase.OrgName != null ? entity.ClusterBase.OrgName : "";
                        sequence.ClusterBase.PhylumID = entity.ClusterBase.PhylumID;
                    }
                    else if (entity.Type == ProcessEntityType.Cluster)
                    {
                        //get cluster and update properties
                        Cluster cluster = (from c in db.Clusters where c.ID == entity.ID select c).Single();
                        cluster.ClusterBase.OrgName = entity.ClusterBase.OrgName != null ? entity.ClusterBase.OrgName : "";
                        cluster.ClusterBase.PhylumID = entity.ClusterBase.PhylumID;
                        cluster.ClusterBase.Lineage = lineage;
                        ExtractGenusSpeciesAndDescription(nav, cluster);
                        UpdateClusterDescription(cluster);
                        ExtractSequencingReferences(nav, cluster);
                        ExtractUserAddedReference(nav, cluster);
                    }
                    db.SubmitChanges();
                    db.Dispose();
                    log.Info("Finished extracting data");
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.NCBI, 2);
                }
            }
            catch (Exception e)
            {
                log.Debug("Error extracting NCBI data:" + Environment.NewLine + "GenBank Accession " + entity.ClusterBase.GenbankAccession + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);

                ProcessEntity.UpdateProcessEntity(entity, UpdateType.NCBI, 0);
            }
        }

        private static void UpdateClusterDescription(Cluster cluster)
        {
            if (cluster.BPEnd.HasValue && cluster.BPStart.HasValue)
            {
                NumberFormatInfo nf = new CultureInfo("en-US", false).NumberFormat;
                nf.NumberDecimalDigits = 0;
                cluster.ClusterBase.Description = cluster.CompoundFamily.FamilyName + " biosynthetic gene cluster from <i>" + cluster.ClusterBase.OrgName + "</i>." + "<br />" + "Cluster is located from " + cluster.BPStart.Value.ToString("N", nf) + " bp to " + cluster.BPEnd.Value.ToString("N", nf) + " bp in sequence file.";
            }
        }

        private static void ExtractUserAddedReference(XPathNavigator nav, Cluster cluster)
        {
            // update user added references
            try
            {
                List<string> pmidList = StringTools.DeserializeFromSemiColonSeparatedList(cluster.UserReferences);
                foreach (var pmid in pmidList)
                {
                    getPubMedArticleInfo(long.Parse(pmid), 1);
                }
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error parsing/updating user added references", "<b>Error parsing/updating user added references</b><br /><br />Cluster ID: " + cluster.ID + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
            }
        }

        private static void ExtractSequencingReferences(XPathNavigator nav, Cluster cluster)
        {
            try
            {
                List<string> pmidList = new List<string>();

                XPathNodeIterator pmids = nav.Select(@"//PubMedId");
                while (pmids.MoveNext())
                {
                    pmidList.Add(pmids.Current.InnerXml);
                }
                pmidList = pmidList.Distinct().ToList<string>();

                foreach (var pmid in pmidList)
                {
                    getPubMedArticleInfo(long.Parse(pmid), 0);
                }

                string pubmedField = StringTools.SerializeToSemiColonSeparatedList(pmidList);
                cluster.SequencingReferences = pubmedField;
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error parsing cluster reference data", "<b>Error parsing cluster reference data</b><br /><br />Cluster ID: " + cluster.ID + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
            }
        }

        private static void ExtractGenusSpeciesAndDescription(XPathNavigator nav, Cluster cluster)
        {
            try { cluster.ClusterBase.Genus = nav.SelectSingleNode(@"//BinomialOrgName_genus").InnerXml; }
            catch { }
            try { cluster.ClusterBase.Species = nav.SelectSingleNode(@"//BinomialOrgName_species").InnerXml; }
            catch { }
            try
            {
                string description = nav.SelectSingleNode(@"//Seqdesc_title").InnerXml;
                if (description.Length >= 2000)
                {
                    description = description.Substring(0, 1999);
                }
                cluster.ClusterBase.Description = description;
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error parsing/submitting cluster description data", "<b>Error parsing/submitting cluster description data</b><br /><br />Cluster ID: " + cluster.ID + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
            }
        }

        private static string ExtractLineage(XPathNavigator nav)
        {
            string lineage = "";
            try
            {
                lineage = nav.SelectSingleNode(@"//OrgName_lineage").InnerXml;
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error parsing lineage data", "<b>Error parsing lineage data</b><br /><br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
            }
            return lineage;
        }

        private static int ExtractKingdomOrPhylumID(string lineage)
        {
            int PhylumOrKingdomID = Phyla.getPhylumID("Unknown");
            try
            {
                string[] lineages = lineage.Split(new string[] { "; " }, StringSplitOptions.RemoveEmptyEntries);
                if (lineages.Length >= 2)
                {
                    DataClassesDataContext db = new DataClassesDataContext();
                    Enum_Phyla kingdom = (from p in db.Enum_Phyla where p.Phylum.Contains(lineages[1]) select p).FirstOrDefault();
                    Enum_Phyla phylum = (from p in db.Enum_Phyla where p.Phylum == lineages[1] select p).FirstOrDefault();

                    if (phylum != null)
                    {
                        PhylumOrKingdomID = phylum.ID;
                        if (phylum.Phylum == "Proteobacteria")
                        {
                            if (lineages.Length >= 3)
                            {
                                string subclass = lineages[2].Trim();
                                string phylumname = "Proteobacteria ";
                                switch (subclass)
                                {
                                    case "Alphaproteobacteria":
                                        phylumname += "(Alpha)";
                                        break;
                                    case "Betaproteobacteria":
                                        phylumname += "(Beta)";
                                        break;
                                    case "Deltaproteobacteria":
                                        phylumname += "(Delta)";
                                        break;
                                    case "Epsilonproteobacteria":
                                        phylumname += "(Epsilon)";
                                        break;
                                    case "Gammaproteobacteria":
                                        phylumname += "(Gamma)";
                                        break;
                                    case "Zetaproteobacteria":
                                        phylumname += "(Zeta)";
                                        break;
                                    default:
                                        phylumname = phylumname.Trim();
                                        break;
                                }
                                Enum_Phyla proteobacteria_phylum = (from p in db.Enum_Phyla where p.Phylum == phylumname select p).SingleOrDefault();
                                if (proteobacteria_phylum != null)
                                {
                                    PhylumOrKingdomID = proteobacteria_phylum.ID;
                                }
                            }
                        }
                    }
                    else if (kingdom != null)
                    {
                        PhylumOrKingdomID = kingdom.ID;
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error parsing lineage data", "<b>Error parsing lineage data</b><br /><br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
            }
            return PhylumOrKingdomID;
        }

        private static void getPubMedArticleInfo(long pmid, int type)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                try
                {
                    // check to see that it doesn't already exists
                    PubMedArticle article;
                    if (!db.PubMedArticles.Exists(x => x.PMID == pmid))
                    {
                        article = new PubMedArticle();
                        db.PubMedArticles.InsertOnSubmit(article);
                    }
                    else
                    {
                        article = db.PubMedArticles.Where(x => x.PMID == pmid).Single();
                    }

                    article.PMID = pmid;
                    article.Type = type;

                    try
                    {
                        XPathNavigator nav = NCBIUtils.GetNCBIXPathNavigator(NCBIRequestType.eFetch, pmid.ToString(), "pubmed", "", "xml");
                        string articleTitle = "";
                        string journalTitleAbbr = "";
                        string journalTitle = "";
                        string year = "";
                        string month = "";
                        string volume = "";
                        string issue = "";
                        string pages = "";

                        try { articleTitle = nav.SelectSingleNode(@"//ArticleTitle").InnerXml; }
                        catch { }
                        try { journalTitleAbbr = nav.SelectSingleNode(@"//Journal/ISOAbbreviation").InnerXml; }
                        catch { }
                        try { journalTitle = nav.SelectSingleNode(@"//Journal/Title").InnerXml; }
                        catch { }
                        try { year = nav.SelectSingleNode(@"//JournalIssue/PubDate/Year").InnerXml; }
                        catch { }
                        try { month = nav.SelectSingleNode(@"//JournalIssue/PubDate/Month").InnerXml; }
                        catch { }
                        try { volume = nav.SelectSingleNode(@"//Volume").InnerXml; }
                        catch { }
                        try { issue = nav.SelectSingleNode(@"//Issue").InnerXml; }
                        catch { }
                        try { pages = nav.SelectSingleNode(@"//Pagination/MedlinePgn").InnerXml; }
                        catch { }
                        // <b>article name</b>. <i>Journal Abbr</i> year month; if volume colume if issue (issue): pages. PMID: <a href=>pmid

                        StringBuilder articleDetails = new StringBuilder();
                        articleDetails.Append("<b>" + articleTitle + "</b> <i>");
                        if (!String.IsNullOrEmpty(journalTitleAbbr))
                        {
                            articleDetails.Append(journalTitleAbbr);
                        }
                        else
                        {
                            articleDetails.Append(journalTitle);
                        }
                        articleDetails.Append("</i> ");
                        articleDetails.Append(year + " " + month + "; ");
                        if (!String.IsNullOrEmpty(volume))
                        {
                            articleDetails.Append(volume);
                        }
                        if (!String.IsNullOrEmpty(issue))
                        {
                            articleDetails.Append("(" + issue + ")");
                        }
                        articleDetails.Append(": " + pages + ". ");
                        articleDetails.Append("PMID: ");
                        articleDetails.Append("<a target=\"_blank\" href=\"http://www.ncbi.nlm.nih.gov/pubmed/" + pmid + "\">" + pmid + "</a>");
                        string trimmedDetails = articleDetails.ToString();
                        if (trimmedDetails.Length > 2000)
                        {
                            trimmedDetails = trimmedDetails.Substring(0, 2000);
                        }
                        article.Details = trimmedDetails;
                    }
                    catch (Exception e)
                    {
                        Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error getting NCBI PubMed data", "<b>Error getting NCBI PubMed data</b><br /><br />PubMed ID: " + pmid + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                    }
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error getting NCBI PubMed data", "<b>Error getting NCBI PubMed data</b><br /><br />PubMed ID: " + pmid + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                }
            }
        }

        public static void getPubChemStructure(int cfid)
        {
            string canonicalsmilestring = "";
            string isomericsmilestring = "";
            int totalchiral = 0;
            int totaldefined = 0;
            int numheavyatoms = 0;
            int numunits = 0;

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                List<PubChemIDs> ids = db.PubChemIDs.Where(x => x.cfid == cfid && x.Type == "C").ToList();

                foreach (PubChemIDs id in ids)
                {
                    XDocument nav = NCBIUtils.GetNCBIPubChemFullRecordXDocument(id.PubChemID);
                    XNamespace defaultnamespace = "http://pubchem.ncbi.nlm.nih.gov";
                    if (nav != null)
                    {
                        XElement node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Urn_name" && x.Value == "Canonical").FirstOrDefault();
                        if (node != null)
                        {
                            XElement valuenode = node.Parent.Parent.Parent.Descendants().Where(x => x.Name.LocalName == "PC-InfoData_value_sval").FirstOrDefault();
                            if (valuenode != null)
                            {
                                canonicalsmilestring = valuenode.Value;
                            }
                        }

                        node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Urn_name" && x.Value == "Isomeric").FirstOrDefault();
                        if (node != null)
                        {
                            XElement valuenode = node.Parent.Parent.Parent.Descendants().Where(x => x.Name.LocalName == "PC-InfoData_value_sval").FirstOrDefault();
                            if (valuenode != null)
                            {
                                isomericsmilestring = valuenode.Value;
                            }
                        }

                        node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Count_atom-chiral").FirstOrDefault();
                        if (node != null)
                        {
                            totalchiral = int.Parse(node.Value);
                        }

                        node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Count_atom-chiral-def").FirstOrDefault();
                        if (node != null)
                        {
                            totaldefined = int.Parse(node.Value);
                        }

                        node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Count_heavy-atom").FirstOrDefault();
                        if (node != null)
                        {
                            numheavyatoms = int.Parse(node.Value);
                        }

                        node = nav.Root.Descendants().Where(x => x.Name.LocalName == "PC-Count_covalent-unit").FirstOrDefault();
                        if (node != null)
                        {
                            numunits = int.Parse(node.Value);
                        }

                        id.CanonicalSMILES = canonicalsmilestring;
                        id.IsomericSMILES = isomericsmilestring;
                        id.TotalChiralCenters = totalchiral;
                        id.TotalDefinedCenters = totaldefined;
                        id.HeavyAtomCount = numheavyatoms;
                        id.NumCovalentUnits = numunits;
                        // save to db
                    }
                    db.SubmitChanges();
                }
            }
        }

        private static void getPubChemIDs(int cfid, string name)
        {
            XDocument cidsnav = NCBIUtils.GetNCBIPubChemIDXDocument(name, "C");
            //XDocument sidsnav = NCBIUtils.GetNCBIPubChemIDXDocument(name, "S");

            List<string> cids = new List<string>();
            //List<string> sids = new List<string>();
            if (cidsnav != null)
            {
                try
                {
                    List<XElement> cidnodes = cidsnav.Root.Descendants().Where(x => x.Name.LocalName == "CID").ToList();
                    foreach (XElement cid in cidnodes)
                    {
                        cids.Add(cid.Value);
                    }
                }
                catch (Exception e)
                {
                    log.Debug(e.ToString());
                }
            }
            //if (sidsnav != null)
            //{
            //    try
            //    {
            //        List<XElement> sidnodes = sidsnav.Root.Descendants().Where(x => x.Name.LocalName == "SID").ToList();
            //        foreach (XElement sid in sidnodes)
            //        {
            //            sids.Add(sid.Value);
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        log.Debug(e.ToString());
            //    }
            //}

            foreach (string cid in cids)
            {
                PubChemIDDataHandler.addPubChemId(cfid, "C", cid);
            }

            //foreach (string sid in sids)
            //{
            //    PubChemIDDataHandler.addPubChemId(cfid, "S", sid);
            //}
        }

        public static void getPubChemData(int cfid)
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
            log.Info("Getting PubChem Data for cfid: " + cfid);
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            //delete existing IDs
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                List<PubChemIDs> currentids = db.PubChemIDs.Where(x => x.cfid == cfid).ToList();
                db.PubChemIDs.DeleteAllOnSubmit(currentids);

                try
                {
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    log.Debug(e.ToString());
                }
            }
            stopWatch.Stop();
            log.Info("Time to delete PubChemIDs: " + stopWatch.Elapsed.TotalMilliseconds.ToString());

            List<string> names = Synonyms.getSynonymsByCompoundFamilyID(cfid).Select(x => x.Synonym1).Where(x => x.Length > 3).ToList();
            names.Add(CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName);
            stopWatch.Reset();
            stopWatch.Start();
            foreach (string name in names)
            {
                getPubChemIDs(cfid, name);
            }
            stopWatch.Stop();
            log.Info("Total time for getting cid/sids " + stopWatch.Elapsed.TotalMilliseconds.ToString());

            stopWatch.Reset();
            stopWatch.Start();
            updatePharmActions(cfid);
            stopWatch.Stop();
            log.Info("Total time for updating pharmactions " + stopWatch.Elapsed.TotalMilliseconds.ToString());
            stopWatch.Reset();
            stopWatch.Start();
            getPubChemStructure(cfid);
            stopWatch.Stop();
            log.Info("Total time for getting structures " + stopWatch.Elapsed.TotalMilliseconds.ToString());
            log.Info("Finished getting PubChem data for cfid: " + cfid);
        }

        private static void updatePharmActions(int cfid)
        {
            List<PubChemIDs> ids = PubChemIDDataHandler.getPubChemIDsByCFID(cfid);
            foreach (PubChemIDs id in ids)
            {
                XDocument nav = NCBIUtils.GetNCBIPubChemPharmActionsXDocument(id.Type, id.PubChemID);
                XNamespace defaultnamespace = "http://pubchem.ncbi.nlm.nih.gov/hierarchy_data";
                if (nav != null)
                {
                    // parse xml and save
                    List<PharmActionsType> rootnodes = PubChemPharmActionDataHandler.getRootNodes();
                    foreach (PharmActionsType root in rootnodes)
                    {
                        XElement node = nav.Root.Descendants(defaultnamespace + "Name").Where(x => x.Value == root.TypeDescription).FirstOrDefault();
                        if (node != null)
                        {
                            // get node id then search for all children
                            XElement idnode = node.Parent.Parent.Elements().Where(x => x.Name.LocalName == "NodeID").SingleOrDefault();

                            if (idnode != null)
                            {
                                string xmlnodeid = idnode.Value;  // for searching for children
                                string description = node.Value;
                                PubChemPharmActionDataHandler.addPharmActionLink(cfid, id.ID, root.ID);
                                findChildren(nav, cfid, id.ID, xmlnodeid, root.ID);
                            }
                        }
                    }
                }
            }
        }

        private static void findChildren(XDocument nav, int cfid, int sourceid, string xmlparentnodeid, int dbparentnodeid)
        {
            XNamespace defaultnamespace = "http://pubchem.ncbi.nlm.nih.gov/hierarchy_data";
            // find children
            List<XElement> children = nav.Root.Descendants(defaultnamespace + "ParentID").Where(x => x.Value == xmlparentnodeid).ToList();
            foreach (XElement child in children)
            {
                XElement idnode = child.Parent.Descendants().Where(x => x.Name.LocalName == "NodeID").SingleOrDefault();
                XElement descriptionnode = child.Parent.Descendants().Where(x => x.Name.LocalName == "Name").SingleOrDefault();
                if (idnode != null && descriptionnode != null)
                {
                    string xmlnodeid = idnode.Value;
                    string description = descriptionnode.Value;
                    PubChemPharmActionDataHandler.addPharmActionType(dbparentnodeid, description);
                    int dbnodeid = PubChemPharmActionDataHandler.getNode(dbparentnodeid, description).ID;
                    PubChemPharmActionDataHandler.addPharmActionLink(cfid, sourceid, dbnodeid);

                    findChildren(nav, cfid, sourceid, xmlnodeid, dbnodeid);
                }
            }
        }
    }
}