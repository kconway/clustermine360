﻿using System.Collections.Generic;

namespace NaturalProductsWebsite.Code
{
    public class SequenceEntity : SequenceEntityTypes
    {
        public string DomainType { get; set; }
        public string Identifier { get; set; }
        public int Length { get; set; }
        public bool IsATorA { get; set; }
        public string AorATPrediction { get; set; }
        public bool IsKR { get; set; }
        public string KRActivityPrediction { get; set; }
        public string KRStereoChemPrediction { get; set; }
        public int DomainID { get; set; }
    }

    public class SequenceEntityTypes
    {
        public int ClusterNum { get; set; }
        public int ParentID { get; set; }
        public string GroupingID { get; set; }
        public string GenbankAccession { get; set; }
        public int GenbankVersion { get; set; }
        public int? BPStart { get; set; }
        public int? BPEnd { get; set; }
        public string Phylum { get; set; }
        public string Organism { get; set; }
        public string PathwayType { get; set; }
        public int ContributorID { get; set; }
        public ProcessEntityType EntityType { get; set; }
        public string CompoundFamilyName { get; set; }
        
    }

    public class SequenceGroup : SequenceEntityTypes
    {
        public List<SequenceEntity> Items { get; set; }
    }
}