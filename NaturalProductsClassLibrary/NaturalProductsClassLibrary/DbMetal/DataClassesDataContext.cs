// 
//  ____  _     __  __      _        _ 
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from naturalproductsdb on 2012-09-12 19:59:39Z.
// Please visit http://code.google.com/p/dblinq2007/ for more information.
//
using System;
using System.ComponentModel;
using System.Data;
#if MONO_STRICT
	using System.Data.Linq;
#else   // MONO_STRICT
	using DbLinq.Data.Linq;
	using DbLinq.Vendor;
#endif  // MONO_STRICT
	using System.Data.Linq.Mapping;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Configuration;


public partial class DataClassesDataContext : DataContext
{
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		#endregion
	
	
	public DataClassesDataContext() : 
			this(System.Configuration.ConfigurationManager.AppSettings["LinqConnectionString"])
	{
	}
	
	public DataClassesDataContext(string connectionString) : 
			base(connectionString)
	{
		this.OnCreated();
	}
	
	public DataClassesDataContext(string connection, MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		this.OnCreated();
	}
	
	public DataClassesDataContext(IDbConnection connection, MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		this.OnCreated();
	}
	
	public Table<Cluster> Clusters
	{
		get
		{
			return this.GetTable<Cluster>();
		}
	}
	
	public Table<ClusterBase> ClusterBases
	{
		get
		{
			return this.GetTable<ClusterBase>();
		}
	}
	
	public Table<ClusterCharacteristic> ClusterCharacteristics
	{
		get
		{
			return this.GetTable<ClusterCharacteristic>();
		}
	}
	
	public Table<ClusterPathwayType> ClusterPathwayTypes
	{
		get
		{
			return this.GetTable<ClusterPathwayType>();
		}
	}
	
	public Table<CompoundFamily> CompoundFamilies
	{
		get
		{
			return this.GetTable<CompoundFamily>();
		}
	}
	
	public Table<CompoundFamilyImage> CompoundFamilyImages
	{
		get
		{
			return this.GetTable<CompoundFamilyImage>();
		}
	}
	
	public Table<DomainGrouping> DomainGroupings
	{
		get
		{
			return this.GetTable<DomainGrouping>();
		}
	}
	
	public Table<DomainPathwayType> DomainPathwayTypes
	{
		get
		{
			return this.GetTable<DomainPathwayType>();
		}
	}
	
	public Table<DomainSequence> DomainSequences
	{
		get
		{
			return this.GetTable<DomainSequence>();
		}
	}
	
	public Table<Download> Downloads
	{
		get
		{
			return this.GetTable<Download>();
		}
	}
	
	public Table<Enum_AntiSmashStatus> Enum_AntiSmashStatus
	{
		get
		{
			return this.GetTable<Enum_AntiSmashStatus>();
		}
	}
	
	public Table<Enum_Characteristic> Enum_Characteristics
	{
		get
		{
			return this.GetTable<Enum_Characteristic>();
		}
	}
	
	public Table<Enum_CharacteristicsCategory> Enum_CharacteristicsCategories
	{
		get
		{
			return this.GetTable<Enum_CharacteristicsCategory>();
		}
	}
	
	public Table<Enum_NcbiStatus> Enum_NcbiStatus
	{
		get
		{
			return this.GetTable<Enum_NcbiStatus>();
		}
	}
	
	public Table<Enum_PathwayType> Enum_PathwayTypes
	{
		get
		{
			return this.GetTable<Enum_PathwayType>();
		}
	}
	
	public Table<Enum_Phyla> Enum_Phyla
	{
		get
		{
			return this.GetTable<Enum_Phyla>();
		}
	}
	
	public Table<Enum_SequenceExtractionStatus> Enum_SequenceExtractionStatus
	{
		get
		{
			return this.GetTable<Enum_SequenceExtractionStatus>();
		}
	}
	
	public Table<LargeSequence> LargeSequences
	{
		get
		{
			return this.GetTable<LargeSequence>();
		}
	}
	
	public Table<My_AspNet_Application> My_AspNet_Applications
	{
		get
		{
			return this.GetTable<My_AspNet_Application>();
		}
	}
	
	public Table<My_AspNet_Membership> My_AspNet_Memberships
	{
		get
		{
			return this.GetTable<My_AspNet_Membership>();
		}
	}
	
	public Table<My_AspNet_Profile> My_AspNet_Profiles
	{
		get
		{
			return this.GetTable<My_AspNet_Profile>();
		}
	}
	
	public Table<My_AspNet_Role> My_AspNet_Roles
	{
		get
		{
			return this.GetTable<My_AspNet_Role>();
		}
	}
	
	public Table<My_AspNet_SchemaVersion> My_AspNet_SchemaVersions
	{
		get
		{
			return this.GetTable<My_AspNet_SchemaVersion>();
		}
	}
	
	public Table<My_AspNet_Session> My_AspNet_Sessions
	{
		get
		{
			return this.GetTable<My_AspNet_Session>();
		}
	}
	
	public Table<My_AspNet_SessionCleanup> My_AspNet_SessionCleanup
	{
		get
		{
			return this.GetTable<My_AspNet_SessionCleanup>();
		}
	}
	
	public Table<My_AspNet_User> My_AspNet_Users
	{
		get
		{
			return this.GetTable<My_AspNet_User>();
		}
	}
	
	public Table<My_AspNet_UsersInRole> My_AspNet_UsersInRoles
	{
		get
		{
			return this.GetTable<My_AspNet_UsersInRole>();
		}
	}
	
	public Table<PhArmAction> PhArmActions
	{
		get
		{
			return this.GetTable<PhArmAction>();
		}
	}
	
	public Table<PhArmActionType> PhArmActionTypes
	{
		get
		{
			return this.GetTable<PhArmActionType>();
		}
	}
	
	public Table<PubCHemIDS> PubCHemIDS
	{
		get
		{
			return this.GetTable<PubCHemIDS>();
		}
	}
	
	public Table<PubMedArticle> PubMedArticles
	{
		get
		{
			return this.GetTable<PubMedArticle>();
		}
	}
	
	public Table<RelatedCompoundFamily> RelatedCompoundFamilies
	{
		get
		{
			return this.GetTable<RelatedCompoundFamily>();
		}
	}
	
	public Table<Synonym> Synonyms
	{
		get
		{
			return this.GetTable<Synonym>();
		}
	}
	
	public Table<UserInfo> UserInfo
	{
		get
		{
			return this.GetTable<UserInfo>();
		}
	}
}

#region Start MONO_STRICT
#if MONO_STRICT

public partial class DataClassesDataContext
{
	
	public DataClassesDataContext(IDbConnection connection) : 
			base(connection)
	{
		this.OnCreated();
	}
}
#region End MONO_STRICT
	#endregion
#else     // MONO_STRICT

public partial class DataClassesDataContext
{
	
	public DataClassesDataContext(IDbConnection connection) : 
			base(connection, new DbLinq.MySql.MySqlVendor())
	{
		this.OnCreated();
	}
	
	public DataClassesDataContext(IDbConnection connection, IVendor sqlDialect) : 
			base(connection, sqlDialect)
	{
		this.OnCreated();
	}
	
	public DataClassesDataContext(IDbConnection connection, MappingSource mappingSource, IVendor sqlDialect) : 
			base(connection, mappingSource, sqlDialect)
	{
		this.OnCreated();
	}
}
#region End Not MONO_STRICT
	#endregion
#endif     // MONO_STRICT
#endregion

[Table(Name="naturalproductsdb.clusters")]
public partial class Cluster : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _baseID;
	
	private System.Nullable<int> _bpeNd;
	
	private System.Nullable<int> _bpsTart;
	
	private System.Nullable<bool> _clusterIsEntireSequence;
	
	private int _compoundFamilyID;
	
	private int _id;
	
	private string _sequencingReferences;
	
	private string _userReferences;
	
	private EntitySet<DomainSequence> _domainSequences;
	
	private EntityRef<ClusterBase> _clusterBase = new EntityRef<ClusterBase>();
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnBaseIDChanged();
		
		partial void OnBaseIDChanging(int value);
		
		partial void OnBpeNdChanged();
		
		partial void OnBpeNdChanging(System.Nullable<int> value);
		
		partial void OnBpsTartChanged();
		
		partial void OnBpsTartChanging(System.Nullable<int> value);
		
		partial void OnClusterIsEntireSequenceChanged();
		
		partial void OnClusterIsEntireSequenceChanging(System.Nullable<bool> value);
		
		partial void OnCompoundFamilyIDChanged();
		
		partial void OnCompoundFamilyIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnSequencingReferencesChanged();
		
		partial void OnSequencingReferencesChanging(string value);
		
		partial void OnUserReferencesChanged();
		
		partial void OnUserReferencesChanging(string value);
		#endregion
	
	
	public Cluster()
	{
		_domainSequences = new EntitySet<DomainSequence>(new Action<DomainSequence>(this.DomainSequences_Attach), new Action<DomainSequence>(this.DomainSequences_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_baseID", Name="BaseID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int BaseID
	{
		get
		{
			return this._baseID;
		}
		set
		{
			if ((_baseID != value))
			{
				if (_clusterBase.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnBaseIDChanging(value);
				this.SendPropertyChanging();
				this._baseID = value;
				this.SendPropertyChanged("BaseID");
				this.OnBaseIDChanged();
			}
		}
	}
	
	[Column(Storage="_bpeNd", Name="BPEnd", DbType="int(10)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<int> BpeNd
	{
		get
		{
			return this._bpeNd;
		}
		set
		{
			if ((_bpeNd != value))
			{
				this.OnBpeNdChanging(value);
				this.SendPropertyChanging();
				this._bpeNd = value;
				this.SendPropertyChanged("BpeNd");
				this.OnBpeNdChanged();
			}
		}
	}
	
	[Column(Storage="_bpsTart", Name="BPStart", DbType="int(10)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<int> BpsTart
	{
		get
		{
			return this._bpsTart;
		}
		set
		{
			if ((_bpsTart != value))
			{
				this.OnBpsTartChanging(value);
				this.SendPropertyChanging();
				this._bpsTart = value;
				this.SendPropertyChanged("BpsTart");
				this.OnBpsTartChanged();
			}
		}
	}
	
	[Column(Storage="_clusterIsEntireSequence", Name="ClusterIsEntireSequence", DbType="bit(1)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<bool> ClusterIsEntireSequence
	{
		get
		{
			return this._clusterIsEntireSequence;
		}
		set
		{
			if ((_clusterIsEntireSequence != value))
			{
				this.OnClusterIsEntireSequenceChanging(value);
				this.SendPropertyChanging();
				this._clusterIsEntireSequence = value;
				this.SendPropertyChanged("ClusterIsEntireSequence");
				this.OnClusterIsEntireSequenceChanged();
			}
		}
	}
	
	[Column(Storage="_compoundFamilyID", Name="CompoundFamilyID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CompoundFamilyID
	{
		get
		{
			return this._compoundFamilyID;
		}
		set
		{
			if ((_compoundFamilyID != value))
			{
				if (_compoundFamily.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCompoundFamilyIDChanging(value);
				this.SendPropertyChanging();
				this._compoundFamilyID = value;
				this.SendPropertyChanged("CompoundFamilyID");
				this.OnCompoundFamilyIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_sequencingReferences", Name="SequencingReferences", DbType="varchar(250)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string SequencingReferences
	{
		get
		{
			return this._sequencingReferences;
		}
		set
		{
			if (((_sequencingReferences == value) 
						== false))
			{
				this.OnSequencingReferencesChanging(value);
				this.SendPropertyChanging();
				this._sequencingReferences = value;
				this.SendPropertyChanged("SequencingReferences");
				this.OnSequencingReferencesChanged();
			}
		}
	}
	
	[Column(Storage="_userReferences", Name="UserReferences", DbType="varchar(250)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string UserReferences
	{
		get
		{
			return this._userReferences;
		}
		set
		{
			if (((_userReferences == value) 
						== false))
			{
				this.OnUserReferencesChanging(value);
				this.SendPropertyChanging();
				this._userReferences = value;
				this.SendPropertyChanged("UserReferences");
				this.OnUserReferencesChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_domainSequences", OtherKey="ClusterID", ThisKey="ID", Name="domainsequence_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainSequence> DomainSequences
	{
		get
		{
			return this._domainSequences;
		}
		set
		{
			this._domainSequences = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_clusterBase", OtherKey="ID", ThisKey="BaseID", Name="clusters_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public ClusterBase ClusterBase
	{
		get
		{
			return this._clusterBase.Entity;
		}
		set
		{
			if (((this._clusterBase.Entity == value) 
						== false))
			{
				if ((this._clusterBase.Entity != null))
				{
					ClusterBase previousClusterBase = this._clusterBase.Entity;
					this._clusterBase.Entity = null;
					previousClusterBase.Clusters.Remove(this);
				}
				this._clusterBase.Entity = value;
				if ((value != null))
				{
					value.Clusters.Add(this);
					_baseID = value.ID;
				}
				else
				{
					_baseID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CompoundFamilyID", Name="clusters_ibfk_3", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.Clusters.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.Clusters.Add(this);
					_compoundFamilyID = value.ID;
				}
				else
				{
					_compoundFamilyID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void DomainSequences_Attach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.Cluster = this;
	}
	
	private void DomainSequences_Detach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.Cluster = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.clusterbase")]
public partial class ClusterBase : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _antiSmashID;
	
	private string _antiSmashLink;
	
	private System.Nullable<System.DateTime> _antiSmashModified;
	
	private int _antiSmashStatus;
	
	private int _contributorID;
	
	private System.DateTime _created;
	
	private bool _delayed;
	
	private string _description;
	
	private bool _emailContributor;
	
	private string _genbankAccession;
	
	private int _genbankVersion;
	
	private string _genus;
	
	private int _id;
	
	private System.DateTime _lastModified;
	
	private int _lastModifiedUserID;
	
	private string _lineage;
	
	private System.Nullable<System.DateTime> _ncbimOdified;
	
	private int _ncbisTatus;
	
	private string _orgName;
	
	private int _phylumID;
	
	private System.Nullable<System.DateTime> _sequenceExtractionModified;
	
	private int _sequenceExtractionStatus;
	
	private string _species;
	
	private EntitySet<Cluster> _clusters;
	
	private EntitySet<LargeSequence> _largeSequences;
	
	private EntityRef<Enum_AntiSmashStatus> _enum_aNtiSmashStatus = new EntityRef<Enum_AntiSmashStatus>();
	
	private EntityRef<Enum_NcbiStatus> _enum_nCbiStatus = new EntityRef<Enum_NcbiStatus>();
	
	private EntityRef<Enum_SequenceExtractionStatus> _enum_sEquenceExtractionStatus = new EntityRef<Enum_SequenceExtractionStatus>();
	
	private EntityRef<Enum_Phyla> _enum_pHyla = new EntityRef<Enum_Phyla>();
	
	private EntityRef<UserInfo> _userInfo = new EntityRef<UserInfo>();
	
	private EntityRef<UserInfo> _userInfo1 = new EntityRef<UserInfo>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAntiSmashIDChanged();
		
		partial void OnAntiSmashIDChanging(string value);
		
		partial void OnAntiSmashLinkChanged();
		
		partial void OnAntiSmashLinkChanging(string value);
		
		partial void OnAntiSmashModifiedChanged();
		
		partial void OnAntiSmashModifiedChanging(System.Nullable<System.DateTime> value);
		
		partial void OnAntiSmashStatusChanged();
		
		partial void OnAntiSmashStatusChanging(int value);
		
		partial void OnContributorIDChanged();
		
		partial void OnContributorIDChanging(int value);
		
		partial void OnCreatedChanged();
		
		partial void OnCreatedChanging(System.DateTime value);
		
		partial void OnDelayedChanged();
		
		partial void OnDelayedChanging(bool value);
		
		partial void OnDescriptionChanged();
		
		partial void OnDescriptionChanging(string value);
		
		partial void OnEmailContributorChanged();
		
		partial void OnEmailContributorChanging(bool value);
		
		partial void OnGenbankAccessionChanged();
		
		partial void OnGenbankAccessionChanging(string value);
		
		partial void OnGenbankVersionChanged();
		
		partial void OnGenbankVersionChanging(int value);
		
		partial void OnGenusChanged();
		
		partial void OnGenusChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnLastModifiedChanged();
		
		partial void OnLastModifiedChanging(System.DateTime value);
		
		partial void OnLastModifiedUserIDChanged();
		
		partial void OnLastModifiedUserIDChanging(int value);
		
		partial void OnLineageChanged();
		
		partial void OnLineageChanging(string value);
		
		partial void OnNcbimOdifiedChanged();
		
		partial void OnNcbimOdifiedChanging(System.Nullable<System.DateTime> value);
		
		partial void OnNcbisTatusChanged();
		
		partial void OnNcbisTatusChanging(int value);
		
		partial void OnOrgNameChanged();
		
		partial void OnOrgNameChanging(string value);
		
		partial void OnPhylumIDChanged();
		
		partial void OnPhylumIDChanging(int value);
		
		partial void OnSequenceExtractionModifiedChanged();
		
		partial void OnSequenceExtractionModifiedChanging(System.Nullable<System.DateTime> value);
		
		partial void OnSequenceExtractionStatusChanged();
		
		partial void OnSequenceExtractionStatusChanging(int value);
		
		partial void OnSpeciesChanged();
		
		partial void OnSpeciesChanging(string value);
		#endregion
	
	
	public ClusterBase()
	{
		_clusters = new EntitySet<Cluster>(new Action<Cluster>(this.Clusters_Attach), new Action<Cluster>(this.Clusters_Detach));
		_largeSequences = new EntitySet<LargeSequence>(new Action<LargeSequence>(this.LargeSequences_Attach), new Action<LargeSequence>(this.LargeSequences_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_antiSmashID", Name="AntiSmashID", DbType="varchar(100)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string AntiSmashID
	{
		get
		{
			return this._antiSmashID;
		}
		set
		{
			if (((_antiSmashID == value) 
						== false))
			{
				this.OnAntiSmashIDChanging(value);
				this.SendPropertyChanging();
				this._antiSmashID = value;
				this.SendPropertyChanged("AntiSmashID");
				this.OnAntiSmashIDChanged();
			}
		}
	}
	
	[Column(Storage="_antiSmashLink", Name="AntiSmashLink", DbType="varchar(200)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string AntiSmashLink
	{
		get
		{
			return this._antiSmashLink;
		}
		set
		{
			if (((_antiSmashLink == value) 
						== false))
			{
				this.OnAntiSmashLinkChanging(value);
				this.SendPropertyChanging();
				this._antiSmashLink = value;
				this.SendPropertyChanged("AntiSmashLink");
				this.OnAntiSmashLinkChanged();
			}
		}
	}
	
	[Column(Storage="_antiSmashModified", Name="AntiSmashModified", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> AntiSmashModified
	{
		get
		{
			return this._antiSmashModified;
		}
		set
		{
			if ((_antiSmashModified != value))
			{
				this.OnAntiSmashModifiedChanging(value);
				this.SendPropertyChanging();
				this._antiSmashModified = value;
				this.SendPropertyChanged("AntiSmashModified");
				this.OnAntiSmashModifiedChanged();
			}
		}
	}
	
	[Column(Storage="_antiSmashStatus", Name="AntiSmashStatus", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int AntiSmashStatus
	{
		get
		{
			return this._antiSmashStatus;
		}
		set
		{
			if ((_antiSmashStatus != value))
			{
				if (_enum_aNtiSmashStatus.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnAntiSmashStatusChanging(value);
				this.SendPropertyChanging();
				this._antiSmashStatus = value;
				this.SendPropertyChanged("AntiSmashStatus");
				this.OnAntiSmashStatusChanged();
			}
		}
	}
	
	[Column(Storage="_contributorID", Name="ContributorID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ContributorID
	{
		get
		{
			return this._contributorID;
		}
		set
		{
			if ((_contributorID != value))
			{
				if (_userInfo.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnContributorIDChanging(value);
				this.SendPropertyChanging();
				this._contributorID = value;
				this.SendPropertyChanged("ContributorID");
				this.OnContributorIDChanged();
			}
		}
	}
	
	[Column(Storage="_created", Name="Created", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime Created
	{
		get
		{
			return this._created;
		}
		set
		{
			if ((_created != value))
			{
				this.OnCreatedChanging(value);
				this.SendPropertyChanging();
				this._created = value;
				this.SendPropertyChanged("Created");
				this.OnCreatedChanged();
			}
		}
	}
	
	[Column(Storage="_delayed", Name="Delayed", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool Delayed
	{
		get
		{
			return this._delayed;
		}
		set
		{
			if ((_delayed != value))
			{
				this.OnDelayedChanging(value);
				this.SendPropertyChanging();
				this._delayed = value;
				this.SendPropertyChanged("Delayed");
				this.OnDelayedChanged();
			}
		}
	}
	
	[Column(Storage="_description", Name="Description", DbType="varchar(2000)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Description
	{
		get
		{
			return this._description;
		}
		set
		{
			if (((_description == value) 
						== false))
			{
				this.OnDescriptionChanging(value);
				this.SendPropertyChanging();
				this._description = value;
				this.SendPropertyChanged("Description");
				this.OnDescriptionChanged();
			}
		}
	}
	
	[Column(Storage="_emailContributor", Name="EmailContributor", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool EmailContributor
	{
		get
		{
			return this._emailContributor;
		}
		set
		{
			if ((_emailContributor != value))
			{
				this.OnEmailContributorChanging(value);
				this.SendPropertyChanging();
				this._emailContributor = value;
				this.SendPropertyChanged("EmailContributor");
				this.OnEmailContributorChanged();
			}
		}
	}
	
	[Column(Storage="_genbankAccession", Name="GenbankAccession", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string GenbankAccession
	{
		get
		{
			return this._genbankAccession;
		}
		set
		{
			if (((_genbankAccession == value) 
						== false))
			{
				this.OnGenbankAccessionChanging(value);
				this.SendPropertyChanging();
				this._genbankAccession = value;
				this.SendPropertyChanged("GenbankAccession");
				this.OnGenbankAccessionChanged();
			}
		}
	}
	
	[Column(Storage="_genbankVersion", Name="GenbankVersion", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int GenbankVersion
	{
		get
		{
			return this._genbankVersion;
		}
		set
		{
			if ((_genbankVersion != value))
			{
				this.OnGenbankVersionChanging(value);
				this.SendPropertyChanging();
				this._genbankVersion = value;
				this.SendPropertyChanged("GenbankVersion");
				this.OnGenbankVersionChanged();
			}
		}
	}
	
	[Column(Storage="_genus", Name="Genus", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Genus
	{
		get
		{
			return this._genus;
		}
		set
		{
			if (((_genus == value) 
						== false))
			{
				this.OnGenusChanging(value);
				this.SendPropertyChanging();
				this._genus = value;
				this.SendPropertyChanged("Genus");
				this.OnGenusChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_lastModified", Name="LastModified", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime LastModified
	{
		get
		{
			return this._lastModified;
		}
		set
		{
			if ((_lastModified != value))
			{
				this.OnLastModifiedChanging(value);
				this.SendPropertyChanging();
				this._lastModified = value;
				this.SendPropertyChanged("LastModified");
				this.OnLastModifiedChanged();
			}
		}
	}
	
	[Column(Storage="_lastModifiedUserID", Name="LastModifiedUserID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int LastModifiedUserID
	{
		get
		{
			return this._lastModifiedUserID;
		}
		set
		{
			if ((_lastModifiedUserID != value))
			{
				if (_userInfo1.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnLastModifiedUserIDChanging(value);
				this.SendPropertyChanging();
				this._lastModifiedUserID = value;
				this.SendPropertyChanged("LastModifiedUserID");
				this.OnLastModifiedUserIDChanged();
			}
		}
	}
	
	[Column(Storage="_lineage", Name="Lineage", DbType="varchar(500)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Lineage
	{
		get
		{
			return this._lineage;
		}
		set
		{
			if (((_lineage == value) 
						== false))
			{
				this.OnLineageChanging(value);
				this.SendPropertyChanging();
				this._lineage = value;
				this.SendPropertyChanged("Lineage");
				this.OnLineageChanged();
			}
		}
	}
	
	[Column(Storage="_ncbimOdified", Name="NCBIModified", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> NcbimOdified
	{
		get
		{
			return this._ncbimOdified;
		}
		set
		{
			if ((_ncbimOdified != value))
			{
				this.OnNcbimOdifiedChanging(value);
				this.SendPropertyChanging();
				this._ncbimOdified = value;
				this.SendPropertyChanged("NcbimOdified");
				this.OnNcbimOdifiedChanged();
			}
		}
	}
	
	[Column(Storage="_ncbisTatus", Name="NCBIStatus", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int NcbisTatus
	{
		get
		{
			return this._ncbisTatus;
		}
		set
		{
			if ((_ncbisTatus != value))
			{
				this.OnNcbisTatusChanging(value);
				this.SendPropertyChanging();
				this._ncbisTatus = value;
				this.SendPropertyChanged("NcbisTatus");
				this.OnNcbisTatusChanged();
			}
		}
	}
	
	[Column(Storage="_orgName", Name="OrgName", DbType="varchar(100)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string OrgName
	{
		get
		{
			return this._orgName;
		}
		set
		{
			if (((_orgName == value) 
						== false))
			{
				this.OnOrgNameChanging(value);
				this.SendPropertyChanging();
				this._orgName = value;
				this.SendPropertyChanged("OrgName");
				this.OnOrgNameChanged();
			}
		}
	}
	
	[Column(Storage="_phylumID", Name="PhylumID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PhylumID
	{
		get
		{
			return this._phylumID;
		}
		set
		{
			if ((_phylumID != value))
			{
				if (_enum_pHyla.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnPhylumIDChanging(value);
				this.SendPropertyChanging();
				this._phylumID = value;
				this.SendPropertyChanged("PhylumID");
				this.OnPhylumIDChanged();
			}
		}
	}
	
	[Column(Storage="_sequenceExtractionModified", Name="SequenceExtractionModified", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> SequenceExtractionModified
	{
		get
		{
			return this._sequenceExtractionModified;
		}
		set
		{
			if ((_sequenceExtractionModified != value))
			{
				this.OnSequenceExtractionModifiedChanging(value);
				this.SendPropertyChanging();
				this._sequenceExtractionModified = value;
				this.SendPropertyChanged("SequenceExtractionModified");
				this.OnSequenceExtractionModifiedChanged();
			}
		}
	}
	
	[Column(Storage="_sequenceExtractionStatus", Name="SequenceExtractionStatus", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int SequenceExtractionStatus
	{
		get
		{
			return this._sequenceExtractionStatus;
		}
		set
		{
			if ((_sequenceExtractionStatus != value))
			{
				if (_enum_sEquenceExtractionStatus.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnSequenceExtractionStatusChanging(value);
				this.SendPropertyChanging();
				this._sequenceExtractionStatus = value;
				this.SendPropertyChanged("SequenceExtractionStatus");
				this.OnSequenceExtractionStatusChanged();
			}
		}
	}
	
	[Column(Storage="_species", Name="Species", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Species
	{
		get
		{
			return this._species;
		}
		set
		{
			if (((_species == value) 
						== false))
			{
				this.OnSpeciesChanging(value);
				this.SendPropertyChanging();
				this._species = value;
				this.SendPropertyChanged("Species");
				this.OnSpeciesChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusters", OtherKey="BaseID", ThisKey="ID", Name="clusters_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<Cluster> Clusters
	{
		get
		{
			return this._clusters;
		}
		set
		{
			this._clusters = value;
		}
	}
	
	[Association(Storage="_largeSequences", OtherKey="BaseID", ThisKey="ID", Name="largesequences_ibfk_7")]
	[DebuggerNonUserCode()]
	public EntitySet<LargeSequence> LargeSequences
	{
		get
		{
			return this._largeSequences;
		}
		set
		{
			this._largeSequences = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_enum_aNtiSmashStatus", OtherKey="StatusNum", ThisKey="AntiSmashStatus", Name="clusterbase_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_AntiSmashStatus Enum_AntiSmashStatus
	{
		get
		{
			return this._enum_aNtiSmashStatus.Entity;
		}
		set
		{
			if (((this._enum_aNtiSmashStatus.Entity == value) 
						== false))
			{
				if ((this._enum_aNtiSmashStatus.Entity != null))
				{
					Enum_AntiSmashStatus previousEnum_AntiSmashStatus = this._enum_aNtiSmashStatus.Entity;
					this._enum_aNtiSmashStatus.Entity = null;
					previousEnum_AntiSmashStatus.ClusterBases.Remove(this);
				}
				this._enum_aNtiSmashStatus.Entity = value;
				if ((value != null))
				{
					value.ClusterBases.Add(this);
					_antiSmashStatus = value.StatusNum;
				}
				else
				{
					_antiSmashStatus = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_enum_nCbiStatus", OtherKey="StatusNum", ThisKey="NcbisTatus", Name="clusterbase_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_NcbiStatus Enum_NcbiStatus
	{
		get
		{
			return this._enum_nCbiStatus.Entity;
		}
		set
		{
			if (((this._enum_nCbiStatus.Entity == value) 
						== false))
			{
				if ((this._enum_nCbiStatus.Entity != null))
				{
					Enum_NcbiStatus previousEnum_NcbiStatus = this._enum_nCbiStatus.Entity;
					this._enum_nCbiStatus.Entity = null;
					previousEnum_NcbiStatus.ClusterBases.Remove(this);
				}
				this._enum_nCbiStatus.Entity = value;
				if ((value != null))
				{
					value.ClusterBases.Add(this);
					_ncbisTatus = value.StatusNum;
				}
				else
				{
					_ncbisTatus = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_enum_sEquenceExtractionStatus", OtherKey="StatusNum", ThisKey="SequenceExtractionStatus", Name="clusterbase_ibfk_3", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_SequenceExtractionStatus Enum_SequenceExtractionStatus
	{
		get
		{
			return this._enum_sEquenceExtractionStatus.Entity;
		}
		set
		{
			if (((this._enum_sEquenceExtractionStatus.Entity == value) 
						== false))
			{
				if ((this._enum_sEquenceExtractionStatus.Entity != null))
				{
					Enum_SequenceExtractionStatus previousEnum_SequenceExtractionStatus = this._enum_sEquenceExtractionStatus.Entity;
					this._enum_sEquenceExtractionStatus.Entity = null;
					previousEnum_SequenceExtractionStatus.ClusterBases.Remove(this);
				}
				this._enum_sEquenceExtractionStatus.Entity = value;
				if ((value != null))
				{
					value.ClusterBases.Add(this);
					_sequenceExtractionStatus = value.StatusNum;
				}
				else
				{
					_sequenceExtractionStatus = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_enum_pHyla", OtherKey="ID", ThisKey="PhylumID", Name="clusterbase_ibfk_4", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_Phyla Enum_Phyla
	{
		get
		{
			return this._enum_pHyla.Entity;
		}
		set
		{
			if (((this._enum_pHyla.Entity == value) 
						== false))
			{
				if ((this._enum_pHyla.Entity != null))
				{
					Enum_Phyla previousEnum_Phyla = this._enum_pHyla.Entity;
					this._enum_pHyla.Entity = null;
					previousEnum_Phyla.ClusterBases.Remove(this);
				}
				this._enum_pHyla.Entity = value;
				if ((value != null))
				{
					value.ClusterBases.Add(this);
					_phylumID = value.ID;
				}
				else
				{
					_phylumID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_userInfo", OtherKey="ID", ThisKey="ContributorID", Name="clusterbase_ibfk_5", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public UserInfo UserInfo
	{
		get
		{
			return this._userInfo.Entity;
		}
		set
		{
			if (((this._userInfo.Entity == value) 
						== false))
			{
				if ((this._userInfo.Entity != null))
				{
					UserInfo previousUserInfo = this._userInfo.Entity;
					this._userInfo.Entity = null;
					previousUserInfo.ClusterBases.Remove(this);
				}
				this._userInfo.Entity = value;
				if ((value != null))
				{
					value.ClusterBases.Add(this);
					_contributorID = value.ID;
				}
				else
				{
					_contributorID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_userInfo1", OtherKey="ID", ThisKey="LastModifiedUserID", Name="clusterbase_ibfk_6", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public UserInfo UserInfo1
	{
		get
		{
			return this._userInfo1.Entity;
		}
		set
		{
			if (((this._userInfo1.Entity == value) 
						== false))
			{
				if ((this._userInfo1.Entity != null))
				{
					UserInfo previousUserInfo = this._userInfo1.Entity;
					this._userInfo1.Entity = null;
					previousUserInfo.ClusterBases1.Remove(this);
				}
				this._userInfo1.Entity = value;
				if ((value != null))
				{
					value.ClusterBases1.Add(this);
					_lastModifiedUserID = value.ID;
				}
				else
				{
					_lastModifiedUserID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void Clusters_Attach(Cluster entity)
	{
		this.SendPropertyChanging();
		entity.ClusterBase = this;
	}
	
	private void Clusters_Detach(Cluster entity)
	{
		this.SendPropertyChanging();
		entity.ClusterBase = null;
	}
	
	private void LargeSequences_Attach(LargeSequence entity)
	{
		this.SendPropertyChanging();
		entity.ClusterBase = this;
	}
	
	private void LargeSequences_Detach(LargeSequence entity)
	{
		this.SendPropertyChanging();
		entity.ClusterBase = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.clustercharacteristics")]
public partial class ClusterCharacteristic : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _characteristicID;
	
	private int _compoundFamilyID;
	
	private int _id;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	private EntityRef<Enum_Characteristic> _enum_cHaracteristic = new EntityRef<Enum_Characteristic>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCharacteristicIDChanged();
		
		partial void OnCharacteristicIDChanging(int value);
		
		partial void OnCompoundFamilyIDChanged();
		
		partial void OnCompoundFamilyIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		#endregion
	
	
	public ClusterCharacteristic()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_characteristicID", Name="CharacteristicID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CharacteristicID
	{
		get
		{
			return this._characteristicID;
		}
		set
		{
			if ((_characteristicID != value))
			{
				if (_enum_cHaracteristic.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCharacteristicIDChanging(value);
				this.SendPropertyChanging();
				this._characteristicID = value;
				this.SendPropertyChanged("CharacteristicID");
				this.OnCharacteristicIDChanged();
			}
		}
	}
	
	[Column(Storage="_compoundFamilyID", Name="CompoundFamilyID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CompoundFamilyID
	{
		get
		{
			return this._compoundFamilyID;
		}
		set
		{
			if ((_compoundFamilyID != value))
			{
				if (_compoundFamily.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCompoundFamilyIDChanging(value);
				this.SendPropertyChanging();
				this._compoundFamilyID = value;
				this.SendPropertyChanged("CompoundFamilyID");
				this.OnCompoundFamilyIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CompoundFamilyID", Name="clustercharacteristics_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.ClusterCharacteristics.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.ClusterCharacteristics.Add(this);
					_compoundFamilyID = value.ID;
				}
				else
				{
					_compoundFamilyID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_enum_cHaracteristic", OtherKey="ID", ThisKey="CharacteristicID", Name="clustercharacteristics_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_Characteristic Enum_Characteristic
	{
		get
		{
			return this._enum_cHaracteristic.Entity;
		}
		set
		{
			if (((this._enum_cHaracteristic.Entity == value) 
						== false))
			{
				if ((this._enum_cHaracteristic.Entity != null))
				{
					Enum_Characteristic previousEnum_Characteristic = this._enum_cHaracteristic.Entity;
					this._enum_cHaracteristic.Entity = null;
					previousEnum_Characteristic.ClusterCharacteristics.Remove(this);
				}
				this._enum_cHaracteristic.Entity = value;
				if ((value != null))
				{
					value.ClusterCharacteristics.Add(this);
					_characteristicID = value.ID;
				}
				else
				{
					_characteristicID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.clusterpathwaytype")]
public partial class ClusterPathwayType : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private bool _addedFromAntiSmash;
	
	private int _compoundFamilyID;
	
	private int _id;
	
	private int _pathwayTypeID;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	private EntityRef<Enum_PathwayType> _enum_pAthwayType = new EntityRef<Enum_PathwayType>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAddedFromAntiSmashChanged();
		
		partial void OnAddedFromAntiSmashChanging(bool value);
		
		partial void OnCompoundFamilyIDChanged();
		
		partial void OnCompoundFamilyIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnPathwayTypeIDChanged();
		
		partial void OnPathwayTypeIDChanging(int value);
		#endregion
	
	
	public ClusterPathwayType()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_addedFromAntiSmash", Name="AddedFromAntiSmash", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool AddedFromAntiSmash
	{
		get
		{
			return this._addedFromAntiSmash;
		}
		set
		{
			if ((_addedFromAntiSmash != value))
			{
				this.OnAddedFromAntiSmashChanging(value);
				this.SendPropertyChanging();
				this._addedFromAntiSmash = value;
				this.SendPropertyChanged("AddedFromAntiSmash");
				this.OnAddedFromAntiSmashChanged();
			}
		}
	}
	
	[Column(Storage="_compoundFamilyID", Name="CompoundFamilyID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CompoundFamilyID
	{
		get
		{
			return this._compoundFamilyID;
		}
		set
		{
			if ((_compoundFamilyID != value))
			{
				if (_compoundFamily.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCompoundFamilyIDChanging(value);
				this.SendPropertyChanging();
				this._compoundFamilyID = value;
				this.SendPropertyChanged("CompoundFamilyID");
				this.OnCompoundFamilyIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_pathwayTypeID", Name="PathwayTypeID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PathwayTypeID
	{
		get
		{
			return this._pathwayTypeID;
		}
		set
		{
			if ((_pathwayTypeID != value))
			{
				if (_enum_pAthwayType.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnPathwayTypeIDChanging(value);
				this.SendPropertyChanging();
				this._pathwayTypeID = value;
				this.SendPropertyChanged("PathwayTypeID");
				this.OnPathwayTypeIDChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CompoundFamilyID", Name="clusterpathwaytype_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.ClusterPathwayTypes.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.ClusterPathwayTypes.Add(this);
					_compoundFamilyID = value.ID;
				}
				else
				{
					_compoundFamilyID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_enum_pAthwayType", OtherKey="ID", ThisKey="PathwayTypeID", Name="clusterpathwaytype_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_PathwayType Enum_PathwayType
	{
		get
		{
			return this._enum_pAthwayType.Entity;
		}
		set
		{
			if (((this._enum_pAthwayType.Entity == value) 
						== false))
			{
				if ((this._enum_pAthwayType.Entity != null))
				{
					Enum_PathwayType previousEnum_PathwayType = this._enum_pAthwayType.Entity;
					this._enum_pAthwayType.Entity = null;
					previousEnum_PathwayType.ClusterPathwayTypes.Remove(this);
				}
				this._enum_pAthwayType.Entity = value;
				if ((value != null))
				{
					value.ClusterPathwayTypes.Add(this);
					_pathwayTypeID = value.ID;
				}
				else
				{
					_pathwayTypeID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.compoundfamilies")]
public partial class CompoundFamily : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _contributorID;
	
	private System.DateTime _created;
	
	private string _familyName;
	
	private int _id;
	
	private System.DateTime _lastModified;
	
	private int _lastModifiedUserID;
	
	private int _pathwayTypeID;
	
	private int _pubChemStatus;
	
	private EntitySet<ClusterCharacteristic> _clusterCharacteristics;
	
	private EntitySet<ClusterPathwayType> _clusterPathwayTypes;
	
	private EntitySet<Cluster> _clusters;
	
	private EntitySet<CompoundFamilyImage> _compoundFamilyImages;
	
	private EntitySet<RelatedCompoundFamily> _relatedCompoundFamilies;
	
	private EntitySet<RelatedCompoundFamily> _relatedCompoundFamilies1;
	
	private EntitySet<Synonym> _synonyms;
	
	private EntitySet<PhArmAction> _phArmActions;
	
	private EntitySet<PubCHemIDS> _pubChEmIds;
	
	private EntityRef<UserInfo> _userInfo = new EntityRef<UserInfo>();
	
	private EntityRef<UserInfo> _userInfo1 = new EntityRef<UserInfo>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnContributorIDChanged();
		
		partial void OnContributorIDChanging(int value);
		
		partial void OnCreatedChanged();
		
		partial void OnCreatedChanging(System.DateTime value);
		
		partial void OnFamilyNameChanged();
		
		partial void OnFamilyNameChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnLastModifiedChanged();
		
		partial void OnLastModifiedChanging(System.DateTime value);
		
		partial void OnLastModifiedUserIDChanged();
		
		partial void OnLastModifiedUserIDChanging(int value);
		
		partial void OnPathwayTypeIDChanged();
		
		partial void OnPathwayTypeIDChanging(int value);
		
		partial void OnPubChemStatusChanged();
		
		partial void OnPubChemStatusChanging(int value);
		#endregion
	
	
	public CompoundFamily()
	{
		_clusterCharacteristics = new EntitySet<ClusterCharacteristic>(new Action<ClusterCharacteristic>(this.ClusterCharacteristics_Attach), new Action<ClusterCharacteristic>(this.ClusterCharacteristics_Detach));
		_clusterPathwayTypes = new EntitySet<ClusterPathwayType>(new Action<ClusterPathwayType>(this.ClusterPathwayTypes_Attach), new Action<ClusterPathwayType>(this.ClusterPathwayTypes_Detach));
		_clusters = new EntitySet<Cluster>(new Action<Cluster>(this.Clusters_Attach), new Action<Cluster>(this.Clusters_Detach));
		_compoundFamilyImages = new EntitySet<CompoundFamilyImage>(new Action<CompoundFamilyImage>(this.CompoundFamilyImages_Attach), new Action<CompoundFamilyImage>(this.CompoundFamilyImages_Detach));
		_relatedCompoundFamilies = new EntitySet<RelatedCompoundFamily>(new Action<RelatedCompoundFamily>(this.RelatedCompoundFamilies_Attach), new Action<RelatedCompoundFamily>(this.RelatedCompoundFamilies_Detach));
		_relatedCompoundFamilies1 = new EntitySet<RelatedCompoundFamily>(new Action<RelatedCompoundFamily>(this.RelatedCompoundFamilies1_Attach), new Action<RelatedCompoundFamily>(this.RelatedCompoundFamilies1_Detach));
		_synonyms = new EntitySet<Synonym>(new Action<Synonym>(this.Synonyms_Attach), new Action<Synonym>(this.Synonyms_Detach));
		_phArmActions = new EntitySet<PhArmAction>(new Action<PhArmAction>(this.PhArmActions_Attach), new Action<PhArmAction>(this.PhArmActions_Detach));
		_pubChEmIds = new EntitySet<PubCHemIDS>(new Action<PubCHemIDS>(this.PubCHemIDS_Attach), new Action<PubCHemIDS>(this.PubCHemIDS_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_contributorID", Name="ContributorID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ContributorID
	{
		get
		{
			return this._contributorID;
		}
		set
		{
			if ((_contributorID != value))
			{
				if (_userInfo.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnContributorIDChanging(value);
				this.SendPropertyChanging();
				this._contributorID = value;
				this.SendPropertyChanged("ContributorID");
				this.OnContributorIDChanged();
			}
		}
	}
	
	[Column(Storage="_created", Name="Created", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime Created
	{
		get
		{
			return this._created;
		}
		set
		{
			if ((_created != value))
			{
				this.OnCreatedChanging(value);
				this.SendPropertyChanging();
				this._created = value;
				this.SendPropertyChanged("Created");
				this.OnCreatedChanged();
			}
		}
	}
	
	[Column(Storage="_familyName", Name="FamilyName", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string FamilyName
	{
		get
		{
			return this._familyName;
		}
		set
		{
			if (((_familyName == value) 
						== false))
			{
				this.OnFamilyNameChanging(value);
				this.SendPropertyChanging();
				this._familyName = value;
				this.SendPropertyChanged("FamilyName");
				this.OnFamilyNameChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_lastModified", Name="LastModified", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime LastModified
	{
		get
		{
			return this._lastModified;
		}
		set
		{
			if ((_lastModified != value))
			{
				this.OnLastModifiedChanging(value);
				this.SendPropertyChanging();
				this._lastModified = value;
				this.SendPropertyChanged("LastModified");
				this.OnLastModifiedChanged();
			}
		}
	}
	
	[Column(Storage="_lastModifiedUserID", Name="LastModifiedUserID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int LastModifiedUserID
	{
		get
		{
			return this._lastModifiedUserID;
		}
		set
		{
			if ((_lastModifiedUserID != value))
			{
				if (_userInfo1.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnLastModifiedUserIDChanging(value);
				this.SendPropertyChanging();
				this._lastModifiedUserID = value;
				this.SendPropertyChanged("LastModifiedUserID");
				this.OnLastModifiedUserIDChanged();
			}
		}
	}
	
	[Column(Storage="_pathwayTypeID", Name="PathwayTypeID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PathwayTypeID
	{
		get
		{
			return this._pathwayTypeID;
		}
		set
		{
			if ((_pathwayTypeID != value))
			{
				this.OnPathwayTypeIDChanging(value);
				this.SendPropertyChanging();
				this._pathwayTypeID = value;
				this.SendPropertyChanged("PathwayTypeID");
				this.OnPathwayTypeIDChanged();
			}
		}
	}
	
	[Column(Storage="_pubChemStatus", Name="PubChemStatus", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PubChemStatus
	{
		get
		{
			return this._pubChemStatus;
		}
		set
		{
			if ((_pubChemStatus != value))
			{
				this.OnPubChemStatusChanging(value);
				this.SendPropertyChanging();
				this._pubChemStatus = value;
				this.SendPropertyChanged("PubChemStatus");
				this.OnPubChemStatusChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterCharacteristics", OtherKey="CompoundFamilyID", ThisKey="ID", Name="clustercharacteristics_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterCharacteristic> ClusterCharacteristics
	{
		get
		{
			return this._clusterCharacteristics;
		}
		set
		{
			this._clusterCharacteristics = value;
		}
	}
	
	[Association(Storage="_clusterPathwayTypes", OtherKey="CompoundFamilyID", ThisKey="ID", Name="clusterpathwaytype_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterPathwayType> ClusterPathwayTypes
	{
		get
		{
			return this._clusterPathwayTypes;
		}
		set
		{
			this._clusterPathwayTypes = value;
		}
	}
	
	[Association(Storage="_clusters", OtherKey="CompoundFamilyID", ThisKey="ID", Name="clusters_ibfk_3")]
	[DebuggerNonUserCode()]
	public EntitySet<Cluster> Clusters
	{
		get
		{
			return this._clusters;
		}
		set
		{
			this._clusters = value;
		}
	}
	
	[Association(Storage="_compoundFamilyImages", OtherKey="CfID", ThisKey="ID", Name="compoundfamilyimages_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<CompoundFamilyImage> CompoundFamilyImages
	{
		get
		{
			return this._compoundFamilyImages;
		}
		set
		{
			this._compoundFamilyImages = value;
		}
	}
	
	[Association(Storage="_relatedCompoundFamilies", OtherKey="FamilyNameID", ThisKey="ID", Name="FK_RelatedCompoundFamilies_CompoundFamilies")]
	[DebuggerNonUserCode()]
	public EntitySet<RelatedCompoundFamily> RelatedCompoundFamilies
	{
		get
		{
			return this._relatedCompoundFamilies;
		}
		set
		{
			this._relatedCompoundFamilies = value;
		}
	}
	
	[Association(Storage="_relatedCompoundFamilies1", OtherKey="RelatedFamilyNameID", ThisKey="ID", Name="FK_RelatedCompoundFamilies_CompoundFamilies1")]
	[DebuggerNonUserCode()]
	public EntitySet<RelatedCompoundFamily> RelatedCompoundFamilies1
	{
		get
		{
			return this._relatedCompoundFamilies1;
		}
		set
		{
			this._relatedCompoundFamilies1 = value;
		}
	}
	
	[Association(Storage="_synonyms", OtherKey="CompoundFamilyID", ThisKey="ID", Name="FK_Synonyms_CompoundFamilies")]
	[DebuggerNonUserCode()]
	public EntitySet<Synonym> Synonyms
	{
		get
		{
			return this._synonyms;
		}
		set
		{
			this._synonyms = value;
		}
	}
	
	[Association(Storage="_phArmActions", OtherKey="CfID", ThisKey="ID", Name="pharmactions_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<PhArmAction> PhArmActions
	{
		get
		{
			return this._phArmActions;
		}
		set
		{
			this._phArmActions = value;
		}
	}
	
	[Association(Storage="_pubChEmIds", OtherKey="CfID", ThisKey="ID", Name="pubchemids_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<PubCHemIDS> PubCHemIDS
	{
		get
		{
			return this._pubChEmIds;
		}
		set
		{
			this._pubChEmIds = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_userInfo", OtherKey="ID", ThisKey="ContributorID", Name="compoundfamilies_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public UserInfo UserInfo
	{
		get
		{
			return this._userInfo.Entity;
		}
		set
		{
			if (((this._userInfo.Entity == value) 
						== false))
			{
				if ((this._userInfo.Entity != null))
				{
					UserInfo previousUserInfo = this._userInfo.Entity;
					this._userInfo.Entity = null;
					previousUserInfo.CompoundFamilies.Remove(this);
				}
				this._userInfo.Entity = value;
				if ((value != null))
				{
					value.CompoundFamilies.Add(this);
					_contributorID = value.ID;
				}
				else
				{
					_contributorID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_userInfo1", OtherKey="ID", ThisKey="LastModifiedUserID", Name="compoundfamilies_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public UserInfo UserInfo1
	{
		get
		{
			return this._userInfo1.Entity;
		}
		set
		{
			if (((this._userInfo1.Entity == value) 
						== false))
			{
				if ((this._userInfo1.Entity != null))
				{
					UserInfo previousUserInfo = this._userInfo1.Entity;
					this._userInfo1.Entity = null;
					previousUserInfo.CompoundFamilies1.Remove(this);
				}
				this._userInfo1.Entity = value;
				if ((value != null))
				{
					value.CompoundFamilies1.Add(this);
					_lastModifiedUserID = value.ID;
				}
				else
				{
					_lastModifiedUserID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterCharacteristics_Attach(ClusterCharacteristic entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void ClusterCharacteristics_Detach(ClusterCharacteristic entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void ClusterPathwayTypes_Attach(ClusterPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void ClusterPathwayTypes_Detach(ClusterPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void Clusters_Attach(Cluster entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void Clusters_Detach(Cluster entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void CompoundFamilyImages_Attach(CompoundFamilyImage entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void CompoundFamilyImages_Detach(CompoundFamilyImage entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void RelatedCompoundFamilies_Attach(RelatedCompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void RelatedCompoundFamilies_Detach(RelatedCompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void RelatedCompoundFamilies1_Attach(RelatedCompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily1 = this;
	}
	
	private void RelatedCompoundFamilies1_Detach(RelatedCompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily1 = null;
	}
	
	private void Synonyms_Attach(Synonym entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void Synonyms_Detach(Synonym entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void PhArmActions_Attach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void PhArmActions_Detach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	
	private void PubCHemIDS_Attach(PubCHemIDS entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = this;
	}
	
	private void PubCHemIDS_Detach(PubCHemIDS entity)
	{
		this.SendPropertyChanging();
		entity.CompoundFamily = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.compoundfamilyimages")]
public partial class CompoundFamilyImage : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _cfID;
	
	private int _id;
	
	private byte[] _image;
	
	private string _imageType;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCfIDChanged();
		
		partial void OnCfIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnImageChanged();
		
		partial void OnImageChanging(byte[] value);
		
		partial void OnImageTypeChanged();
		
		partial void OnImageTypeChanging(string value);
		#endregion
	
	
	public CompoundFamilyImage()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_cfID", Name="cfid", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CfID
	{
		get
		{
			return this._cfID;
		}
		set
		{
			if ((_cfID != value))
			{
				this.OnCfIDChanging(value);
				this.SendPropertyChanging();
				this._cfID = value;
				this.SendPropertyChanged("CfID");
				this.OnCfIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_image", Name="image", DbType="mediumblob", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public byte[] Image
	{
		get
		{
			return this._image;
		}
		set
		{
			if (((_image == value) 
						== false))
			{
				this.OnImageChanging(value);
				this.SendPropertyChanging();
				this._image = value;
				this.SendPropertyChanged("Image");
				this.OnImageChanged();
			}
		}
	}
	
	[Column(Storage="_imageType", Name="imagetype", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string ImageType
	{
		get
		{
			return this._imageType;
		}
		set
		{
			if (((_imageType == value) 
						== false))
			{
				this.OnImageTypeChanging(value);
				this.SendPropertyChanging();
				this._imageType = value;
				this.SendPropertyChanged("ImageType");
				this.OnImageTypeChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CfID", Name="compoundfamilyimages_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.CompoundFamilyImages.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.CompoundFamilyImages.Add(this);
					_cfID = value.ID;
				}
				else
				{
					_cfID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.domaingroupings")]
public partial class DomainGrouping : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _groupingID;
	
	private int _id;
	
	private EntitySet<DomainPathwayType> _domainPathwayTypes;
	
	private EntitySet<DomainSequence> _domainSequences;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnGroupingIDChanged();
		
		partial void OnGroupingIDChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		#endregion
	
	
	public DomainGrouping()
	{
		_domainPathwayTypes = new EntitySet<DomainPathwayType>(new Action<DomainPathwayType>(this.DomainPathwayTypes_Attach), new Action<DomainPathwayType>(this.DomainPathwayTypes_Detach));
		_domainSequences = new EntitySet<DomainSequence>(new Action<DomainSequence>(this.DomainSequences_Attach), new Action<DomainSequence>(this.DomainSequences_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_groupingID", Name="GroupingID", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string GroupingID
	{
		get
		{
			return this._groupingID;
		}
		set
		{
			if (((_groupingID == value) 
						== false))
			{
				this.OnGroupingIDChanging(value);
				this.SendPropertyChanging();
				this._groupingID = value;
				this.SendPropertyChanged("GroupingID");
				this.OnGroupingIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_domainPathwayTypes", OtherKey="GroupingID", ThisKey="GroupingID", Name="domainpathwaytypes_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainPathwayType> DomainPathwayTypes
	{
		get
		{
			return this._domainPathwayTypes;
		}
		set
		{
			this._domainPathwayTypes = value;
		}
	}
	
	[Association(Storage="_domainSequences", OtherKey="GroupingID", ThisKey="GroupingID", Name="domainsequence_ibfk_5")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainSequence> DomainSequences
	{
		get
		{
			return this._domainSequences;
		}
		set
		{
			this._domainSequences = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void DomainPathwayTypes_Attach(DomainPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.DomainGrouping = this;
	}
	
	private void DomainPathwayTypes_Detach(DomainPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.DomainGrouping = null;
	}
	
	private void DomainSequences_Attach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.DomainGrouping = this;
	}
	
	private void DomainSequences_Detach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.DomainGrouping = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.domainpathwaytypes")]
public partial class DomainPathwayType : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _groupingID;
	
	private int _id;
	
	private int _pathwayTypeID;
	
	private EntityRef<Enum_PathwayType> _enum_pAthwayType = new EntityRef<Enum_PathwayType>();
	
	private EntityRef<DomainGrouping> _domainGrouping = new EntityRef<DomainGrouping>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnGroupingIDChanged();
		
		partial void OnGroupingIDChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnPathwayTypeIDChanged();
		
		partial void OnPathwayTypeIDChanging(int value);
		#endregion
	
	
	public DomainPathwayType()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_groupingID", Name="GroupingID", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string GroupingID
	{
		get
		{
			return this._groupingID;
		}
		set
		{
			if (((_groupingID == value) 
						== false))
			{
				if (_domainGrouping.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnGroupingIDChanging(value);
				this.SendPropertyChanging();
				this._groupingID = value;
				this.SendPropertyChanged("GroupingID");
				this.OnGroupingIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_pathwayTypeID", Name="PathwayTypeID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PathwayTypeID
	{
		get
		{
			return this._pathwayTypeID;
		}
		set
		{
			if ((_pathwayTypeID != value))
			{
				if (_enum_pAthwayType.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnPathwayTypeIDChanging(value);
				this.SendPropertyChanging();
				this._pathwayTypeID = value;
				this.SendPropertyChanged("PathwayTypeID");
				this.OnPathwayTypeIDChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_enum_pAthwayType", OtherKey="ID", ThisKey="PathwayTypeID", Name="domainpathwaytypes_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_PathwayType Enum_PathwayType
	{
		get
		{
			return this._enum_pAthwayType.Entity;
		}
		set
		{
			if (((this._enum_pAthwayType.Entity == value) 
						== false))
			{
				if ((this._enum_pAthwayType.Entity != null))
				{
					Enum_PathwayType previousEnum_PathwayType = this._enum_pAthwayType.Entity;
					this._enum_pAthwayType.Entity = null;
					previousEnum_PathwayType.DomainPathwayTypes.Remove(this);
				}
				this._enum_pAthwayType.Entity = value;
				if ((value != null))
				{
					value.DomainPathwayTypes.Add(this);
					_pathwayTypeID = value.ID;
				}
				else
				{
					_pathwayTypeID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_domainGrouping", OtherKey="GroupingID", ThisKey="GroupingID", Name="domainpathwaytypes_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public DomainGrouping DomainGrouping
	{
		get
		{
			return this._domainGrouping.Entity;
		}
		set
		{
			if (((this._domainGrouping.Entity == value) 
						== false))
			{
				if ((this._domainGrouping.Entity != null))
				{
					DomainGrouping previousDomainGrouping = this._domainGrouping.Entity;
					this._domainGrouping.Entity = null;
					previousDomainGrouping.DomainPathwayTypes.Remove(this);
				}
				this._domainGrouping.Entity = value;
				if ((value != null))
				{
					value.DomainPathwayTypes.Add(this);
					_groupingID = value.GroupingID;
				}
				else
				{
					_groupingID = default(string);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.domainsequence")]
public partial class DomainSequence : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _aorAtpRediction;
	
	private System.Nullable<int> _clusterID;
	
	private int _clusterNum;
	
	private string _clusterType;
	
	private string _domainType;
	
	private string _groupingID;
	
	private string _hashedSequence;
	
	private int _id;
	
	private string _identifier;
	
	private bool _isAtOrA;
	
	private bool _isKr;
	
	private string _kraCtivityPrediction;
	
	private string _krsTereoChemPrediction;
	
	private System.Nullable<int> _largeSequenceID;
	
	private int _length;
	
	private string _organism;
	
	private int _phylumID;
	
	private byte[] _sequence;
	
	private EntityRef<Cluster> _cluster = new EntityRef<Cluster>();
	
	private EntityRef<LargeSequence> _largeSequence = new EntityRef<LargeSequence>();
	
	private EntityRef<DomainGrouping> _domainGrouping = new EntityRef<DomainGrouping>();
	
	private EntityRef<Enum_Phyla> _enum_pHyla = new EntityRef<Enum_Phyla>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAorAtpRedictionChanged();
		
		partial void OnAorAtpRedictionChanging(string value);
		
		partial void OnClusterIDChanged();
		
		partial void OnClusterIDChanging(System.Nullable<int> value);
		
		partial void OnClusterNumChanged();
		
		partial void OnClusterNumChanging(int value);
		
		partial void OnClusterTypeChanged();
		
		partial void OnClusterTypeChanging(string value);
		
		partial void OnDomainTypeChanged();
		
		partial void OnDomainTypeChanging(string value);
		
		partial void OnGroupingIDChanged();
		
		partial void OnGroupingIDChanging(string value);
		
		partial void OnHashedSequenceChanged();
		
		partial void OnHashedSequenceChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnIdentifierChanged();
		
		partial void OnIdentifierChanging(string value);
		
		partial void OnIsAtOrAChanged();
		
		partial void OnIsAtOrAChanging(bool value);
		
		partial void OnIsKrChanged();
		
		partial void OnIsKrChanging(bool value);
		
		partial void OnKraCtivityPredictionChanged();
		
		partial void OnKraCtivityPredictionChanging(string value);
		
		partial void OnKrsTereoChemPredictionChanged();
		
		partial void OnKrsTereoChemPredictionChanging(string value);
		
		partial void OnLargeSequenceIDChanged();
		
		partial void OnLargeSequenceIDChanging(System.Nullable<int> value);
		
		partial void OnLengthChanged();
		
		partial void OnLengthChanging(int value);
		
		partial void OnOrganismChanged();
		
		partial void OnOrganismChanging(string value);
		
		partial void OnPhylumIDChanged();
		
		partial void OnPhylumIDChanging(int value);
		
		partial void OnSequenceChanged();
		
		partial void OnSequenceChanging(byte[] value);
		#endregion
	
	
	public DomainSequence()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_aorAtpRediction", Name="AorATPrediction", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string AorAtpRediction
	{
		get
		{
			return this._aorAtpRediction;
		}
		set
		{
			if (((_aorAtpRediction == value) 
						== false))
			{
				this.OnAorAtpRedictionChanging(value);
				this.SendPropertyChanging();
				this._aorAtpRediction = value;
				this.SendPropertyChanged("AorAtpRediction");
				this.OnAorAtpRedictionChanged();
			}
		}
	}
	
	[Column(Storage="_clusterID", Name="ClusterID", DbType="int(10)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<int> ClusterID
	{
		get
		{
			return this._clusterID;
		}
		set
		{
			if ((_clusterID != value))
			{
				if (_cluster.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnClusterIDChanging(value);
				this.SendPropertyChanging();
				this._clusterID = value;
				this.SendPropertyChanged("ClusterID");
				this.OnClusterIDChanged();
			}
		}
	}
	
	[Column(Storage="_clusterNum", Name="ClusterNum", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ClusterNum
	{
		get
		{
			return this._clusterNum;
		}
		set
		{
			if ((_clusterNum != value))
			{
				this.OnClusterNumChanging(value);
				this.SendPropertyChanging();
				this._clusterNum = value;
				this.SendPropertyChanged("ClusterNum");
				this.OnClusterNumChanged();
			}
		}
	}
	
	[Column(Storage="_clusterType", Name="ClusterType", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string ClusterType
	{
		get
		{
			return this._clusterType;
		}
		set
		{
			if (((_clusterType == value) 
						== false))
			{
				this.OnClusterTypeChanging(value);
				this.SendPropertyChanging();
				this._clusterType = value;
				this.SendPropertyChanged("ClusterType");
				this.OnClusterTypeChanged();
			}
		}
	}
	
	[Column(Storage="_domainType", Name="DomainType", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string DomainType
	{
		get
		{
			return this._domainType;
		}
		set
		{
			if (((_domainType == value) 
						== false))
			{
				this.OnDomainTypeChanging(value);
				this.SendPropertyChanging();
				this._domainType = value;
				this.SendPropertyChanged("DomainType");
				this.OnDomainTypeChanged();
			}
		}
	}
	
	[Column(Storage="_groupingID", Name="GroupingID", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string GroupingID
	{
		get
		{
			return this._groupingID;
		}
		set
		{
			if (((_groupingID == value) 
						== false))
			{
				if (_domainGrouping.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnGroupingIDChanging(value);
				this.SendPropertyChanging();
				this._groupingID = value;
				this.SendPropertyChanged("GroupingID");
				this.OnGroupingIDChanged();
			}
		}
	}
	
	[Column(Storage="_hashedSequence", Name="HashedSequence", DbType="varchar(100)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string HashedSequence
	{
		get
		{
			return this._hashedSequence;
		}
		set
		{
			if (((_hashedSequence == value) 
						== false))
			{
				this.OnHashedSequenceChanging(value);
				this.SendPropertyChanging();
				this._hashedSequence = value;
				this.SendPropertyChanged("HashedSequence");
				this.OnHashedSequenceChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_identifier", Name="Identifier", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Identifier
	{
		get
		{
			return this._identifier;
		}
		set
		{
			if (((_identifier == value) 
						== false))
			{
				this.OnIdentifierChanging(value);
				this.SendPropertyChanging();
				this._identifier = value;
				this.SendPropertyChanged("Identifier");
				this.OnIdentifierChanged();
			}
		}
	}
	
	[Column(Storage="_isAtOrA", Name="IsATorA", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool IsAtOrA
	{
		get
		{
			return this._isAtOrA;
		}
		set
		{
			if ((_isAtOrA != value))
			{
				this.OnIsAtOrAChanging(value);
				this.SendPropertyChanging();
				this._isAtOrA = value;
				this.SendPropertyChanged("IsAtOrA");
				this.OnIsAtOrAChanged();
			}
		}
	}
	
	[Column(Storage="_isKr", Name="IsKR", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool IsKr
	{
		get
		{
			return this._isKr;
		}
		set
		{
			if ((_isKr != value))
			{
				this.OnIsKrChanging(value);
				this.SendPropertyChanging();
				this._isKr = value;
				this.SendPropertyChanged("IsKr");
				this.OnIsKrChanged();
			}
		}
	}
	
	[Column(Storage="_kraCtivityPrediction", Name="KRActivityPrediction", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string KraCtivityPrediction
	{
		get
		{
			return this._kraCtivityPrediction;
		}
		set
		{
			if (((_kraCtivityPrediction == value) 
						== false))
			{
				this.OnKraCtivityPredictionChanging(value);
				this.SendPropertyChanging();
				this._kraCtivityPrediction = value;
				this.SendPropertyChanged("KraCtivityPrediction");
				this.OnKraCtivityPredictionChanged();
			}
		}
	}
	
	[Column(Storage="_krsTereoChemPrediction", Name="KRStereoChemPrediction", DbType="varchar(50)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string KrsTereoChemPrediction
	{
		get
		{
			return this._krsTereoChemPrediction;
		}
		set
		{
			if (((_krsTereoChemPrediction == value) 
						== false))
			{
				this.OnKrsTereoChemPredictionChanging(value);
				this.SendPropertyChanging();
				this._krsTereoChemPrediction = value;
				this.SendPropertyChanged("KrsTereoChemPrediction");
				this.OnKrsTereoChemPredictionChanged();
			}
		}
	}
	
	[Column(Storage="_largeSequenceID", Name="LargeSequenceID", DbType="int", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<int> LargeSequenceID
	{
		get
		{
			return this._largeSequenceID;
		}
		set
		{
			if ((_largeSequenceID != value))
			{
				if (_largeSequence.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnLargeSequenceIDChanging(value);
				this.SendPropertyChanging();
				this._largeSequenceID = value;
				this.SendPropertyChanged("LargeSequenceID");
				this.OnLargeSequenceIDChanged();
			}
		}
	}
	
	[Column(Storage="_length", Name="Length", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Length
	{
		get
		{
			return this._length;
		}
		set
		{
			if ((_length != value))
			{
				this.OnLengthChanging(value);
				this.SendPropertyChanging();
				this._length = value;
				this.SendPropertyChanged("Length");
				this.OnLengthChanged();
			}
		}
	}
	
	[Column(Storage="_organism", Name="Organism", DbType="varchar(100)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Organism
	{
		get
		{
			return this._organism;
		}
		set
		{
			if (((_organism == value) 
						== false))
			{
				this.OnOrganismChanging(value);
				this.SendPropertyChanging();
				this._organism = value;
				this.SendPropertyChanged("Organism");
				this.OnOrganismChanged();
			}
		}
	}
	
	[Column(Storage="_phylumID", Name="PhylumID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int PhylumID
	{
		get
		{
			return this._phylumID;
		}
		set
		{
			if ((_phylumID != value))
			{
				if (_enum_pHyla.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnPhylumIDChanging(value);
				this.SendPropertyChanging();
				this._phylumID = value;
				this.SendPropertyChanged("PhylumID");
				this.OnPhylumIDChanged();
			}
		}
	}
	
	[Column(Storage="_sequence", Name="Sequence", DbType="mediumblob", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public byte[] Sequence
	{
		get
		{
			return this._sequence;
		}
		set
		{
			if (((_sequence == value) 
						== false))
			{
				this.OnSequenceChanging(value);
				this.SendPropertyChanging();
				this._sequence = value;
				this.SendPropertyChanged("Sequence");
				this.OnSequenceChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_cluster", OtherKey="ID", ThisKey="ClusterID", Name="domainsequence_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Cluster Cluster
	{
		get
		{
			return this._cluster.Entity;
		}
		set
		{
			if (((this._cluster.Entity == value) 
						== false))
			{
				if ((this._cluster.Entity != null))
				{
					Cluster previousCluster = this._cluster.Entity;
					this._cluster.Entity = null;
					previousCluster.DomainSequences.Remove(this);
				}
				this._cluster.Entity = value;
				if ((value != null))
				{
					value.DomainSequences.Add(this);
					_clusterID = value.ID;
				}
				else
				{
					_clusterID = null;
				}
			}
		}
	}
	
	[Association(Storage="_largeSequence", OtherKey="ID", ThisKey="LargeSequenceID", Name="domainsequence_ibfk_3", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public LargeSequence LargeSequence
	{
		get
		{
			return this._largeSequence.Entity;
		}
		set
		{
			if (((this._largeSequence.Entity == value) 
						== false))
			{
				if ((this._largeSequence.Entity != null))
				{
					LargeSequence previousLargeSequence = this._largeSequence.Entity;
					this._largeSequence.Entity = null;
					previousLargeSequence.DomainSequences.Remove(this);
				}
				this._largeSequence.Entity = value;
				if ((value != null))
				{
					value.DomainSequences.Add(this);
					_largeSequenceID = value.ID;
				}
				else
				{
					_largeSequenceID = null;
				}
			}
		}
	}
	
	[Association(Storage="_domainGrouping", OtherKey="GroupingID", ThisKey="GroupingID", Name="domainsequence_ibfk_5", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public DomainGrouping DomainGrouping
	{
		get
		{
			return this._domainGrouping.Entity;
		}
		set
		{
			if (((this._domainGrouping.Entity == value) 
						== false))
			{
				if ((this._domainGrouping.Entity != null))
				{
					DomainGrouping previousDomainGrouping = this._domainGrouping.Entity;
					this._domainGrouping.Entity = null;
					previousDomainGrouping.DomainSequences.Remove(this);
				}
				this._domainGrouping.Entity = value;
				if ((value != null))
				{
					value.DomainSequences.Add(this);
					_groupingID = value.GroupingID;
				}
				else
				{
					_groupingID = default(string);
				}
			}
		}
	}
	
	[Association(Storage="_enum_pHyla", OtherKey="ID", ThisKey="PhylumID", Name="FK_DomainSequence_Enum_Phyla", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_Phyla Enum_Phyla
	{
		get
		{
			return this._enum_pHyla.Entity;
		}
		set
		{
			if (((this._enum_pHyla.Entity == value) 
						== false))
			{
				if ((this._enum_pHyla.Entity != null))
				{
					Enum_Phyla previousEnum_Phyla = this._enum_pHyla.Entity;
					this._enum_pHyla.Entity = null;
					previousEnum_Phyla.DomainSequences.Remove(this);
				}
				this._enum_pHyla.Entity = value;
				if ((value != null))
				{
					value.DomainSequences.Add(this);
					_phylumID = value.ID;
				}
				else
				{
					_phylumID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.downloads")]
public partial class Download : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _domainIds;
	
	private string _downloadGuid;
	
	private int _id;
	
	private bool _simpleHeaders;
	
	private int _status;
	
	private System.DateTime _whenRequested;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDomainIDSChanged();
		
		partial void OnDomainIDSChanging(string value);
		
		partial void OnDownloadGuidChanged();
		
		partial void OnDownloadGuidChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnSimpleHeadersChanged();
		
		partial void OnSimpleHeadersChanging(bool value);
		
		partial void OnStatusChanged();
		
		partial void OnStatusChanging(int value);
		
		partial void OnWhenRequestedChanged();
		
		partial void OnWhenRequestedChanging(System.DateTime value);
		#endregion
	
	
	public Download()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_domainIds", Name="domainids", DbType="text", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string DomainIDS
	{
		get
		{
			return this._domainIds;
		}
		set
		{
			if (((_domainIds == value) 
						== false))
			{
				this.OnDomainIDSChanging(value);
				this.SendPropertyChanging();
				this._domainIds = value;
				this.SendPropertyChanged("DomainIDS");
				this.OnDomainIDSChanged();
			}
		}
	}
	
	[Column(Storage="_downloadGuid", Name="DownloadGUID", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string DownloadGuid
	{
		get
		{
			return this._downloadGuid;
		}
		set
		{
			if (((_downloadGuid == value) 
						== false))
			{
				this.OnDownloadGuidChanging(value);
				this.SendPropertyChanging();
				this._downloadGuid = value;
				this.SendPropertyChanged("DownloadGuid");
				this.OnDownloadGuidChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_simpleHeaders", Name="SimpleHeaders", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool SimpleHeaders
	{
		get
		{
			return this._simpleHeaders;
		}
		set
		{
			if ((_simpleHeaders != value))
			{
				this.OnSimpleHeadersChanging(value);
				this.SendPropertyChanging();
				this._simpleHeaders = value;
				this.SendPropertyChanged("SimpleHeaders");
				this.OnSimpleHeadersChanged();
			}
		}
	}
	
	[Column(Storage="_status", Name="Status", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Status
	{
		get
		{
			return this._status;
		}
		set
		{
			if ((_status != value))
			{
				this.OnStatusChanging(value);
				this.SendPropertyChanging();
				this._status = value;
				this.SendPropertyChanged("Status");
				this.OnStatusChanged();
			}
		}
	}
	
	[Column(Storage="_whenRequested", Name="WhenRequested", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime WhenRequested
	{
		get
		{
			return this._whenRequested;
		}
		set
		{
			if ((_whenRequested != value))
			{
				this.OnWhenRequestedChanging(value);
				this.SendPropertyChanging();
				this._whenRequested = value;
				this.SendPropertyChanged("WhenRequested");
				this.OnWhenRequestedChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.enum_antismashstatus")]
public partial class Enum_AntiSmashStatus : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _id;
	
	private string _statusDescription;
	
	private int _statusNum;
	
	private EntitySet<ClusterBase> _clusterBases;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnStatusDescriptionChanged();
		
		partial void OnStatusDescriptionChanging(string value);
		
		partial void OnStatusNumChanged();
		
		partial void OnStatusNumChanging(int value);
		#endregion
	
	
	public Enum_AntiSmashStatus()
	{
		_clusterBases = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases_Attach), new Action<ClusterBase>(this.ClusterBases_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_statusDescription", Name="StatusDescription", DbType="varchar(150)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string StatusDescription
	{
		get
		{
			return this._statusDescription;
		}
		set
		{
			if (((_statusDescription == value) 
						== false))
			{
				this.OnStatusDescriptionChanging(value);
				this.SendPropertyChanging();
				this._statusDescription = value;
				this.SendPropertyChanged("StatusDescription");
				this.OnStatusDescriptionChanged();
			}
		}
	}
	
	[Column(Storage="_statusNum", Name="StatusNum", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int StatusNum
	{
		get
		{
			return this._statusNum;
		}
		set
		{
			if ((_statusNum != value))
			{
				this.OnStatusNumChanging(value);
				this.SendPropertyChanging();
				this._statusNum = value;
				this.SendPropertyChanged("StatusNum");
				this.OnStatusNumChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterBases", OtherKey="AntiSmashStatus", ThisKey="StatusNum", Name="clusterbase_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases
	{
		get
		{
			return this._clusterBases;
		}
		set
		{
			this._clusterBases = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterBases_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_AntiSmashStatus = this;
	}
	
	private void ClusterBases_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_AntiSmashStatus = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_characteristics")]
public partial class Enum_Characteristic : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _category;
	
	private string _characteristic;
	
	private int _id;
	
	private EntitySet<ClusterCharacteristic> _clusterCharacteristics;
	
	private EntityRef<Enum_CharacteristicsCategory> _enum_cHaracteristicsCategory = new EntityRef<Enum_CharacteristicsCategory>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCategoryChanged();
		
		partial void OnCategoryChanging(int value);
		
		partial void OnCharacteristicChanged();
		
		partial void OnCharacteristicChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		#endregion
	
	
	public Enum_Characteristic()
	{
		_clusterCharacteristics = new EntitySet<ClusterCharacteristic>(new Action<ClusterCharacteristic>(this.ClusterCharacteristics_Attach), new Action<ClusterCharacteristic>(this.ClusterCharacteristics_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_category", Name="Category", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Category
	{
		get
		{
			return this._category;
		}
		set
		{
			if ((_category != value))
			{
				if (_enum_cHaracteristicsCategory.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCategoryChanging(value);
				this.SendPropertyChanging();
				this._category = value;
				this.SendPropertyChanged("Category");
				this.OnCategoryChanged();
			}
		}
	}
	
	[Column(Storage="_characteristic", Name="Characteristic", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Characteristic
	{
		get
		{
			return this._characteristic;
		}
		set
		{
			if (((_characteristic == value) 
						== false))
			{
				this.OnCharacteristicChanging(value);
				this.SendPropertyChanging();
				this._characteristic = value;
				this.SendPropertyChanged("Characteristic");
				this.OnCharacteristicChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterCharacteristics", OtherKey="CharacteristicID", ThisKey="ID", Name="clustercharacteristics_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterCharacteristic> ClusterCharacteristics
	{
		get
		{
			return this._clusterCharacteristics;
		}
		set
		{
			this._clusterCharacteristics = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_enum_cHaracteristicsCategory", OtherKey="ID", ThisKey="Category", Name="enum_characteristics_ibfk_3", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public Enum_CharacteristicsCategory Enum_CharacteristicsCategory
	{
		get
		{
			return this._enum_cHaracteristicsCategory.Entity;
		}
		set
		{
			if (((this._enum_cHaracteristicsCategory.Entity == value) 
						== false))
			{
				if ((this._enum_cHaracteristicsCategory.Entity != null))
				{
					Enum_CharacteristicsCategory previousEnum_CharacteristicsCategory = this._enum_cHaracteristicsCategory.Entity;
					this._enum_cHaracteristicsCategory.Entity = null;
					previousEnum_CharacteristicsCategory.Enum_Characteristics.Remove(this);
				}
				this._enum_cHaracteristicsCategory.Entity = value;
				if ((value != null))
				{
					value.Enum_Characteristics.Add(this);
					_category = value.ID;
				}
				else
				{
					_category = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterCharacteristics_Attach(ClusterCharacteristic entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Characteristic = this;
	}
	
	private void ClusterCharacteristics_Detach(ClusterCharacteristic entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Characteristic = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_characteristicscategories")]
public partial class Enum_CharacteristicsCategory : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _categoryName;
	
	private int _id;
	
	private EntitySet<Enum_Characteristic> _enum_cHaracteristics;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCategoryNameChanged();
		
		partial void OnCategoryNameChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		#endregion
	
	
	public Enum_CharacteristicsCategory()
	{
		_enum_cHaracteristics = new EntitySet<Enum_Characteristic>(new Action<Enum_Characteristic>(this.Enum_Characteristics_Attach), new Action<Enum_Characteristic>(this.Enum_Characteristics_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_categoryName", Name="CategoryName", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string CategoryName
	{
		get
		{
			return this._categoryName;
		}
		set
		{
			if (((_categoryName == value) 
						== false))
			{
				this.OnCategoryNameChanging(value);
				this.SendPropertyChanging();
				this._categoryName = value;
				this.SendPropertyChanged("CategoryName");
				this.OnCategoryNameChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_enum_cHaracteristics", OtherKey="Category", ThisKey="ID", Name="enum_characteristics_ibfk_3")]
	[DebuggerNonUserCode()]
	public EntitySet<Enum_Characteristic> Enum_Characteristics
	{
		get
		{
			return this._enum_cHaracteristics;
		}
		set
		{
			this._enum_cHaracteristics = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void Enum_Characteristics_Attach(Enum_Characteristic entity)
	{
		this.SendPropertyChanging();
		entity.Enum_CharacteristicsCategory = this;
	}
	
	private void Enum_Characteristics_Detach(Enum_Characteristic entity)
	{
		this.SendPropertyChanging();
		entity.Enum_CharacteristicsCategory = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_ncbistatus")]
public partial class Enum_NcbiStatus : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _id;
	
	private string _status;
	
	private int _statusNum;
	
	private EntitySet<ClusterBase> _clusterBases;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnStatusChanged();
		
		partial void OnStatusChanging(string value);
		
		partial void OnStatusNumChanged();
		
		partial void OnStatusNumChanging(int value);
		#endregion
	
	
	public Enum_NcbiStatus()
	{
		_clusterBases = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases_Attach), new Action<ClusterBase>(this.ClusterBases_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_status", Name="Status", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Status
	{
		get
		{
			return this._status;
		}
		set
		{
			if (((_status == value) 
						== false))
			{
				this.OnStatusChanging(value);
				this.SendPropertyChanging();
				this._status = value;
				this.SendPropertyChanged("Status");
				this.OnStatusChanged();
			}
		}
	}
	
	[Column(Storage="_statusNum", Name="StatusNum", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int StatusNum
	{
		get
		{
			return this._statusNum;
		}
		set
		{
			if ((_statusNum != value))
			{
				this.OnStatusNumChanging(value);
				this.SendPropertyChanging();
				this._statusNum = value;
				this.SendPropertyChanged("StatusNum");
				this.OnStatusNumChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterBases", OtherKey="NcbisTatus", ThisKey="StatusNum", Name="clusterbase_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases
	{
		get
		{
			return this._clusterBases;
		}
		set
		{
			this._clusterBases = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterBases_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_NcbiStatus = this;
	}
	
	private void ClusterBases_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_NcbiStatus = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_pathwaytypes")]
public partial class Enum_PathwayType : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _displayOrder;
	
	private int _id;
	
	private string _type;
	
	private EntitySet<ClusterPathwayType> _clusterPathwayTypes;
	
	private EntitySet<DomainPathwayType> _domainPathwayTypes;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDisplayOrderChanged();
		
		partial void OnDisplayOrderChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnTypeChanged();
		
		partial void OnTypeChanging(string value);
		#endregion
	
	
	public Enum_PathwayType()
	{
		_clusterPathwayTypes = new EntitySet<ClusterPathwayType>(new Action<ClusterPathwayType>(this.ClusterPathwayTypes_Attach), new Action<ClusterPathwayType>(this.ClusterPathwayTypes_Detach));
		_domainPathwayTypes = new EntitySet<DomainPathwayType>(new Action<DomainPathwayType>(this.DomainPathwayTypes_Attach), new Action<DomainPathwayType>(this.DomainPathwayTypes_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_displayOrder", Name="DisplayOrder", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int DisplayOrder
	{
		get
		{
			return this._displayOrder;
		}
		set
		{
			if ((_displayOrder != value))
			{
				this.OnDisplayOrderChanging(value);
				this.SendPropertyChanging();
				this._displayOrder = value;
				this.SendPropertyChanged("DisplayOrder");
				this.OnDisplayOrderChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_type", Name="Type", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Type
	{
		get
		{
			return this._type;
		}
		set
		{
			if (((_type == value) 
						== false))
			{
				this.OnTypeChanging(value);
				this.SendPropertyChanging();
				this._type = value;
				this.SendPropertyChanged("Type");
				this.OnTypeChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterPathwayTypes", OtherKey="PathwayTypeID", ThisKey="ID", Name="clusterpathwaytype_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterPathwayType> ClusterPathwayTypes
	{
		get
		{
			return this._clusterPathwayTypes;
		}
		set
		{
			this._clusterPathwayTypes = value;
		}
	}
	
	[Association(Storage="_domainPathwayTypes", OtherKey="PathwayTypeID", ThisKey="ID", Name="domainpathwaytypes_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainPathwayType> DomainPathwayTypes
	{
		get
		{
			return this._domainPathwayTypes;
		}
		set
		{
			this._domainPathwayTypes = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterPathwayTypes_Attach(ClusterPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.Enum_PathwayType = this;
	}
	
	private void ClusterPathwayTypes_Detach(ClusterPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.Enum_PathwayType = null;
	}
	
	private void DomainPathwayTypes_Attach(DomainPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.Enum_PathwayType = this;
	}
	
	private void DomainPathwayTypes_Detach(DomainPathwayType entity)
	{
		this.SendPropertyChanging();
		entity.Enum_PathwayType = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_phyla")]
public partial class Enum_Phyla : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _id;
	
	private string _phylum;
	
	private EntitySet<ClusterBase> _clusterBases;
	
	private EntitySet<DomainSequence> _domainSequences;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnPhylumChanged();
		
		partial void OnPhylumChanging(string value);
		#endregion
	
	
	public Enum_Phyla()
	{
		_clusterBases = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases_Attach), new Action<ClusterBase>(this.ClusterBases_Detach));
		_domainSequences = new EntitySet<DomainSequence>(new Action<DomainSequence>(this.DomainSequences_Attach), new Action<DomainSequence>(this.DomainSequences_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_phylum", Name="Phylum", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Phylum
	{
		get
		{
			return this._phylum;
		}
		set
		{
			if (((_phylum == value) 
						== false))
			{
				this.OnPhylumChanging(value);
				this.SendPropertyChanging();
				this._phylum = value;
				this.SendPropertyChanged("Phylum");
				this.OnPhylumChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterBases", OtherKey="PhylumID", ThisKey="ID", Name="clusterbase_ibfk_4")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases
	{
		get
		{
			return this._clusterBases;
		}
		set
		{
			this._clusterBases = value;
		}
	}
	
	[Association(Storage="_domainSequences", OtherKey="PhylumID", ThisKey="ID", Name="FK_DomainSequence_Enum_Phyla")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainSequence> DomainSequences
	{
		get
		{
			return this._domainSequences;
		}
		set
		{
			this._domainSequences = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterBases_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Phyla = this;
	}
	
	private void ClusterBases_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Phyla = null;
	}
	
	private void DomainSequences_Attach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Phyla = this;
	}
	
	private void DomainSequences_Detach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.Enum_Phyla = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.enum_sequenceextractionstatus")]
public partial class Enum_SequenceExtractionStatus : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _statusDescription;
	
	private int _statusNum;
	
	private EntitySet<ClusterBase> _clusterBases;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnStatusDescriptionChanged();
		
		partial void OnStatusDescriptionChanging(string value);
		
		partial void OnStatusNumChanged();
		
		partial void OnStatusNumChanging(int value);
		#endregion
	
	
	public Enum_SequenceExtractionStatus()
	{
		_clusterBases = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases_Attach), new Action<ClusterBase>(this.ClusterBases_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_statusDescription", Name="StatusDescription", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string StatusDescription
	{
		get
		{
			return this._statusDescription;
		}
		set
		{
			if (((_statusDescription == value) 
						== false))
			{
				this.OnStatusDescriptionChanging(value);
				this.SendPropertyChanging();
				this._statusDescription = value;
				this.SendPropertyChanged("StatusDescription");
				this.OnStatusDescriptionChanged();
			}
		}
	}
	
	[Column(Storage="_statusNum", Name="StatusNum", DbType="int(10)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int StatusNum
	{
		get
		{
			return this._statusNum;
		}
		set
		{
			if ((_statusNum != value))
			{
				this.OnStatusNumChanging(value);
				this.SendPropertyChanging();
				this._statusNum = value;
				this.SendPropertyChanged("StatusNum");
				this.OnStatusNumChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterBases", OtherKey="SequenceExtractionStatus", ThisKey="StatusNum", Name="clusterbase_ibfk_3")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases
	{
		get
		{
			return this._clusterBases;
		}
		set
		{
			this._clusterBases = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterBases_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_SequenceExtractionStatus = this;
	}
	
	private void ClusterBases_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.Enum_SequenceExtractionStatus = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.largesequences")]
public partial class LargeSequence : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _baseID;
	
	private int _id;
	
	private EntitySet<DomainSequence> _domainSequences;
	
	private EntityRef<ClusterBase> _clusterBase = new EntityRef<ClusterBase>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnBaseIDChanged();
		
		partial void OnBaseIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		#endregion
	
	
	public LargeSequence()
	{
		_domainSequences = new EntitySet<DomainSequence>(new Action<DomainSequence>(this.DomainSequences_Attach), new Action<DomainSequence>(this.DomainSequences_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_baseID", Name="BaseID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int BaseID
	{
		get
		{
			return this._baseID;
		}
		set
		{
			if ((_baseID != value))
			{
				if (_clusterBase.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnBaseIDChanging(value);
				this.SendPropertyChanging();
				this._baseID = value;
				this.SendPropertyChanged("BaseID");
				this.OnBaseIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_domainSequences", OtherKey="LargeSequenceID", ThisKey="ID", Name="domainsequence_ibfk_3")]
	[DebuggerNonUserCode()]
	public EntitySet<DomainSequence> DomainSequences
	{
		get
		{
			return this._domainSequences;
		}
		set
		{
			this._domainSequences = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_clusterBase", OtherKey="ID", ThisKey="BaseID", Name="largesequences_ibfk_7", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public ClusterBase ClusterBase
	{
		get
		{
			return this._clusterBase.Entity;
		}
		set
		{
			if (((this._clusterBase.Entity == value) 
						== false))
			{
				if ((this._clusterBase.Entity != null))
				{
					ClusterBase previousClusterBase = this._clusterBase.Entity;
					this._clusterBase.Entity = null;
					previousClusterBase.LargeSequences.Remove(this);
				}
				this._clusterBase.Entity = value;
				if ((value != null))
				{
					value.LargeSequences.Add(this);
					_baseID = value.ID;
				}
				else
				{
					_baseID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void DomainSequences_Attach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.LargeSequence = this;
	}
	
	private void DomainSequences_Detach(DomainSequence entity)
	{
		this.SendPropertyChanging();
		entity.LargeSequence = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.my_aspnet_applications")]
public partial class My_AspNet_Application : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _description;
	
	private int _id;
	
	private string _name;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDescriptionChanged();
		
		partial void OnDescriptionChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnNameChanged();
		
		partial void OnNameChanging(string value);
		#endregion
	
	
	public My_AspNet_Application()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_description", Name="description", DbType="varchar(256)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Description
	{
		get
		{
			return this._description;
		}
		set
		{
			if (((_description == value) 
						== false))
			{
				this.OnDescriptionChanging(value);
				this.SendPropertyChanging();
				this._description = value;
				this.SendPropertyChanged("Description");
				this.OnDescriptionChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="id", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_name", Name="name", DbType="varchar(256)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Name
	{
		get
		{
			return this._name;
		}
		set
		{
			if (((_name == value) 
						== false))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_membership")]
public partial class My_AspNet_Membership : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _comment;
	
	private System.Nullable<System.DateTime> _creationDate;
	
	private string _email;
	
	private System.Nullable<uint> _failedPasswordAnswerAttemptCount;
	
	private System.Nullable<System.DateTime> _failedPasswordAnswerAttemptWindowStart;
	
	private System.Nullable<uint> _failedPasswordAttemptCount;
	
	private System.Nullable<System.DateTime> _failedPasswordAttemptWindowStart;
	
	private System.Nullable<sbyte> _isApproved;
	
	private System.Nullable<sbyte> _isLockedOut;
	
	private System.Nullable<System.DateTime> _lastActivityDate;
	
	private System.Nullable<System.DateTime> _lastLockedOutDate;
	
	private System.Nullable<System.DateTime> _lastLoginDate;
	
	private System.Nullable<System.DateTime> _lastPasswordChangedDate;
	
	private string _password;
	
	private string _passwordAnswer;
	
	private System.Nullable<sbyte> _passwordFormat;
	
	private string _passwordKey;
	
	private string _passwordQuestion;
	
	private int _userID;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCommentChanged();
		
		partial void OnCommentChanging(string value);
		
		partial void OnCreationDateChanged();
		
		partial void OnCreationDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnEmailChanged();
		
		partial void OnEmailChanging(string value);
		
		partial void OnFailedPasswordAnswerAttemptCountChanged();
		
		partial void OnFailedPasswordAnswerAttemptCountChanging(System.Nullable<uint> value);
		
		partial void OnFailedPasswordAnswerAttemptWindowStartChanged();
		
		partial void OnFailedPasswordAnswerAttemptWindowStartChanging(System.Nullable<System.DateTime> value);
		
		partial void OnFailedPasswordAttemptCountChanged();
		
		partial void OnFailedPasswordAttemptCountChanging(System.Nullable<uint> value);
		
		partial void OnFailedPasswordAttemptWindowStartChanged();
		
		partial void OnFailedPasswordAttemptWindowStartChanging(System.Nullable<System.DateTime> value);
		
		partial void OnIsApprovedChanged();
		
		partial void OnIsApprovedChanging(System.Nullable<sbyte> value);
		
		partial void OnIsLockedOutChanged();
		
		partial void OnIsLockedOutChanging(System.Nullable<sbyte> value);
		
		partial void OnLastActivityDateChanged();
		
		partial void OnLastActivityDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnLastLockedOutDateChanged();
		
		partial void OnLastLockedOutDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnLastLoginDateChanged();
		
		partial void OnLastLoginDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnLastPasswordChangedDateChanged();
		
		partial void OnLastPasswordChangedDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnPasswordChanged();
		
		partial void OnPasswordChanging(string value);
		
		partial void OnPasswordAnswerChanged();
		
		partial void OnPasswordAnswerChanging(string value);
		
		partial void OnPasswordFormatChanged();
		
		partial void OnPasswordFormatChanging(System.Nullable<sbyte> value);
		
		partial void OnPasswordKeyChanged();
		
		partial void OnPasswordKeyChanging(string value);
		
		partial void OnPasswordQuestionChanged();
		
		partial void OnPasswordQuestionChanging(string value);
		
		partial void OnUserIDChanged();
		
		partial void OnUserIDChanging(int value);
		#endregion
	
	
	public My_AspNet_Membership()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_comment", Name="Comment", DbType="varchar(255)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Comment
	{
		get
		{
			return this._comment;
		}
		set
		{
			if (((_comment == value) 
						== false))
			{
				this.OnCommentChanging(value);
				this.SendPropertyChanging();
				this._comment = value;
				this.SendPropertyChanged("Comment");
				this.OnCommentChanged();
			}
		}
	}
	
	[Column(Storage="_creationDate", Name="CreationDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> CreationDate
	{
		get
		{
			return this._creationDate;
		}
		set
		{
			if ((_creationDate != value))
			{
				this.OnCreationDateChanging(value);
				this.SendPropertyChanging();
				this._creationDate = value;
				this.SendPropertyChanged("CreationDate");
				this.OnCreationDateChanged();
			}
		}
	}
	
	[Column(Storage="_email", Name="Email", DbType="varchar(128)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Email
	{
		get
		{
			return this._email;
		}
		set
		{
			if (((_email == value) 
						== false))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[Column(Storage="_failedPasswordAnswerAttemptCount", Name="FailedPasswordAnswerAttemptCount", DbType="int unsigned", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<uint> FailedPasswordAnswerAttemptCount
	{
		get
		{
			return this._failedPasswordAnswerAttemptCount;
		}
		set
		{
			if ((_failedPasswordAnswerAttemptCount != value))
			{
				this.OnFailedPasswordAnswerAttemptCountChanging(value);
				this.SendPropertyChanging();
				this._failedPasswordAnswerAttemptCount = value;
				this.SendPropertyChanged("FailedPasswordAnswerAttemptCount");
				this.OnFailedPasswordAnswerAttemptCountChanged();
			}
		}
	}
	
	[Column(Storage="_failedPasswordAnswerAttemptWindowStart", Name="FailedPasswordAnswerAttemptWindowStart", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> FailedPasswordAnswerAttemptWindowStart
	{
		get
		{
			return this._failedPasswordAnswerAttemptWindowStart;
		}
		set
		{
			if ((_failedPasswordAnswerAttemptWindowStart != value))
			{
				this.OnFailedPasswordAnswerAttemptWindowStartChanging(value);
				this.SendPropertyChanging();
				this._failedPasswordAnswerAttemptWindowStart = value;
				this.SendPropertyChanged("FailedPasswordAnswerAttemptWindowStart");
				this.OnFailedPasswordAnswerAttemptWindowStartChanged();
			}
		}
	}
	
	[Column(Storage="_failedPasswordAttemptCount", Name="FailedPasswordAttemptCount", DbType="int unsigned", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<uint> FailedPasswordAttemptCount
	{
		get
		{
			return this._failedPasswordAttemptCount;
		}
		set
		{
			if ((_failedPasswordAttemptCount != value))
			{
				this.OnFailedPasswordAttemptCountChanging(value);
				this.SendPropertyChanging();
				this._failedPasswordAttemptCount = value;
				this.SendPropertyChanged("FailedPasswordAttemptCount");
				this.OnFailedPasswordAttemptCountChanged();
			}
		}
	}
	
	[Column(Storage="_failedPasswordAttemptWindowStart", Name="FailedPasswordAttemptWindowStart", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> FailedPasswordAttemptWindowStart
	{
		get
		{
			return this._failedPasswordAttemptWindowStart;
		}
		set
		{
			if ((_failedPasswordAttemptWindowStart != value))
			{
				this.OnFailedPasswordAttemptWindowStartChanging(value);
				this.SendPropertyChanging();
				this._failedPasswordAttemptWindowStart = value;
				this.SendPropertyChanged("FailedPasswordAttemptWindowStart");
				this.OnFailedPasswordAttemptWindowStartChanged();
			}
		}
	}
	
	[Column(Storage="_isApproved", Name="IsApproved", DbType="tinyint(1)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<sbyte> IsApproved
	{
		get
		{
			return this._isApproved;
		}
		set
		{
			if ((_isApproved != value))
			{
				this.OnIsApprovedChanging(value);
				this.SendPropertyChanging();
				this._isApproved = value;
				this.SendPropertyChanged("IsApproved");
				this.OnIsApprovedChanged();
			}
		}
	}
	
	[Column(Storage="_isLockedOut", Name="IsLockedOut", DbType="tinyint(1)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<sbyte> IsLockedOut
	{
		get
		{
			return this._isLockedOut;
		}
		set
		{
			if ((_isLockedOut != value))
			{
				this.OnIsLockedOutChanging(value);
				this.SendPropertyChanging();
				this._isLockedOut = value;
				this.SendPropertyChanged("IsLockedOut");
				this.OnIsLockedOutChanged();
			}
		}
	}
	
	[Column(Storage="_lastActivityDate", Name="LastActivityDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> LastActivityDate
	{
		get
		{
			return this._lastActivityDate;
		}
		set
		{
			if ((_lastActivityDate != value))
			{
				this.OnLastActivityDateChanging(value);
				this.SendPropertyChanging();
				this._lastActivityDate = value;
				this.SendPropertyChanged("LastActivityDate");
				this.OnLastActivityDateChanged();
			}
		}
	}
	
	[Column(Storage="_lastLockedOutDate", Name="LastLockedOutDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> LastLockedOutDate
	{
		get
		{
			return this._lastLockedOutDate;
		}
		set
		{
			if ((_lastLockedOutDate != value))
			{
				this.OnLastLockedOutDateChanging(value);
				this.SendPropertyChanging();
				this._lastLockedOutDate = value;
				this.SendPropertyChanged("LastLockedOutDate");
				this.OnLastLockedOutDateChanged();
			}
		}
	}
	
	[Column(Storage="_lastLoginDate", Name="LastLoginDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> LastLoginDate
	{
		get
		{
			return this._lastLoginDate;
		}
		set
		{
			if ((_lastLoginDate != value))
			{
				this.OnLastLoginDateChanging(value);
				this.SendPropertyChanging();
				this._lastLoginDate = value;
				this.SendPropertyChanged("LastLoginDate");
				this.OnLastLoginDateChanged();
			}
		}
	}
	
	[Column(Storage="_lastPasswordChangedDate", Name="LastPasswordChangedDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> LastPasswordChangedDate
	{
		get
		{
			return this._lastPasswordChangedDate;
		}
		set
		{
			if ((_lastPasswordChangedDate != value))
			{
				this.OnLastPasswordChangedDateChanging(value);
				this.SendPropertyChanging();
				this._lastPasswordChangedDate = value;
				this.SendPropertyChanged("LastPasswordChangedDate");
				this.OnLastPasswordChangedDateChanged();
			}
		}
	}
	
	[Column(Storage="_password", Name="Password", DbType="varchar(128)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Password
	{
		get
		{
			return this._password;
		}
		set
		{
			if (((_password == value) 
						== false))
			{
				this.OnPasswordChanging(value);
				this.SendPropertyChanging();
				this._password = value;
				this.SendPropertyChanged("Password");
				this.OnPasswordChanged();
			}
		}
	}
	
	[Column(Storage="_passwordAnswer", Name="PasswordAnswer", DbType="varchar(255)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string PasswordAnswer
	{
		get
		{
			return this._passwordAnswer;
		}
		set
		{
			if (((_passwordAnswer == value) 
						== false))
			{
				this.OnPasswordAnswerChanging(value);
				this.SendPropertyChanging();
				this._passwordAnswer = value;
				this.SendPropertyChanged("PasswordAnswer");
				this.OnPasswordAnswerChanged();
			}
		}
	}
	
	[Column(Storage="_passwordFormat", Name="PasswordFormat", DbType="tinyint(4)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<sbyte> PasswordFormat
	{
		get
		{
			return this._passwordFormat;
		}
		set
		{
			if ((_passwordFormat != value))
			{
				this.OnPasswordFormatChanging(value);
				this.SendPropertyChanging();
				this._passwordFormat = value;
				this.SendPropertyChanged("PasswordFormat");
				this.OnPasswordFormatChanged();
			}
		}
	}
	
	[Column(Storage="_passwordKey", Name="PasswordKey", DbType="char(32)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string PasswordKey
	{
		get
		{
			return this._passwordKey;
		}
		set
		{
			if (((_passwordKey == value) 
						== false))
			{
				this.OnPasswordKeyChanging(value);
				this.SendPropertyChanging();
				this._passwordKey = value;
				this.SendPropertyChanged("PasswordKey");
				this.OnPasswordKeyChanged();
			}
		}
	}
	
	[Column(Storage="_passwordQuestion", Name="PasswordQuestion", DbType="varchar(255)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string PasswordQuestion
	{
		get
		{
			return this._passwordQuestion;
		}
		set
		{
			if (((_passwordQuestion == value) 
						== false))
			{
				this.OnPasswordQuestionChanging(value);
				this.SendPropertyChanging();
				this._passwordQuestion = value;
				this.SendPropertyChanged("PasswordQuestion");
				this.OnPasswordQuestionChanged();
			}
		}
	}
	
	[Column(Storage="_userID", Name="userId", DbType="int", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int UserID
	{
		get
		{
			return this._userID;
		}
		set
		{
			if ((_userID != value))
			{
				this.OnUserIDChanging(value);
				this.SendPropertyChanging();
				this._userID = value;
				this.SendPropertyChanged("UserID");
				this.OnUserIDChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_profiles")]
public partial class My_AspNet_Profile : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private byte[] _binaryData;
	
	private System.DateTime _lastUpdatedDate;
	
	private string _stringData;
	
	private int _userID;
	
	private string _valueIndex;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnBinaryDataChanged();
		
		partial void OnBinaryDataChanging(byte[] value);
		
		partial void OnLastUpdatedDateChanged();
		
		partial void OnLastUpdatedDateChanging(System.DateTime value);
		
		partial void OnStringDataChanged();
		
		partial void OnStringDataChanging(string value);
		
		partial void OnUserIDChanged();
		
		partial void OnUserIDChanging(int value);
		
		partial void OnValueIndexChanged();
		
		partial void OnValueIndexChanging(string value);
		#endregion
	
	
	public My_AspNet_Profile()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_binaryData", Name="binarydata", DbType="longblob", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public byte[] BinaryData
	{
		get
		{
			return this._binaryData;
		}
		set
		{
			if (((_binaryData == value) 
						== false))
			{
				this.OnBinaryDataChanging(value);
				this.SendPropertyChanging();
				this._binaryData = value;
				this.SendPropertyChanged("BinaryData");
				this.OnBinaryDataChanged();
			}
		}
	}
	
	[Column(Storage="_lastUpdatedDate", Name="lastUpdatedDate", DbType="timestamp", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime LastUpdatedDate
	{
		get
		{
			return this._lastUpdatedDate;
		}
		set
		{
			if ((_lastUpdatedDate != value))
			{
				this.OnLastUpdatedDateChanging(value);
				this.SendPropertyChanging();
				this._lastUpdatedDate = value;
				this.SendPropertyChanged("LastUpdatedDate");
				this.OnLastUpdatedDateChanged();
			}
		}
	}
	
	[Column(Storage="_stringData", Name="stringdata", DbType="longtext", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string StringData
	{
		get
		{
			return this._stringData;
		}
		set
		{
			if (((_stringData == value) 
						== false))
			{
				this.OnStringDataChanging(value);
				this.SendPropertyChanging();
				this._stringData = value;
				this.SendPropertyChanged("StringData");
				this.OnStringDataChanged();
			}
		}
	}
	
	[Column(Storage="_userID", Name="userId", DbType="int", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int UserID
	{
		get
		{
			return this._userID;
		}
		set
		{
			if ((_userID != value))
			{
				this.OnUserIDChanging(value);
				this.SendPropertyChanging();
				this._userID = value;
				this.SendPropertyChanged("UserID");
				this.OnUserIDChanged();
			}
		}
	}
	
	[Column(Storage="_valueIndex", Name="valueindex", DbType="longtext", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string ValueIndex
	{
		get
		{
			return this._valueIndex;
		}
		set
		{
			if (((_valueIndex == value) 
						== false))
			{
				this.OnValueIndexChanging(value);
				this.SendPropertyChanging();
				this._valueIndex = value;
				this.SendPropertyChanged("ValueIndex");
				this.OnValueIndexChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_roles")]
public partial class My_AspNet_Role : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _applicationID;
	
	private int _id;
	
	private string _name;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnApplicationIDChanged();
		
		partial void OnApplicationIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnNameChanged();
		
		partial void OnNameChanging(string value);
		#endregion
	
	
	public My_AspNet_Role()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_applicationID", Name="applicationId", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ApplicationID
	{
		get
		{
			return this._applicationID;
		}
		set
		{
			if ((_applicationID != value))
			{
				this.OnApplicationIDChanging(value);
				this.SendPropertyChanging();
				this._applicationID = value;
				this.SendPropertyChanged("ApplicationID");
				this.OnApplicationIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="id", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_name", Name="name", DbType="varchar(255)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Name
	{
		get
		{
			return this._name;
		}
		set
		{
			if (((_name == value) 
						== false))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_schemaversion")]
public partial class My_AspNet_SchemaVersion
{
	
	private System.Nullable<int> _version;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnVersionChanged();
		
		partial void OnVersionChanging(System.Nullable<int> value);
		#endregion
	
	
	public My_AspNet_SchemaVersion()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_version", Name="version", DbType="int", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<int> Version
	{
		get
		{
			return this._version;
		}
		set
		{
			if ((_version != value))
			{
				this.OnVersionChanging(value);
				this._version = value;
				this.OnVersionChanged();
			}
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_sessions")]
public partial class My_AspNet_Session : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _applicationID;
	
	private System.DateTime _created;
	
	private System.DateTime _expires;
	
	private int _flags;
	
	private System.DateTime _lockDate;
	
	private sbyte _locked;
	
	private int _lockID;
	
	private string _sessionID;
	
	private byte[] _sessionItems;
	
	private int _timeout;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnApplicationIDChanged();
		
		partial void OnApplicationIDChanging(int value);
		
		partial void OnCreatedChanged();
		
		partial void OnCreatedChanging(System.DateTime value);
		
		partial void OnExpiresChanged();
		
		partial void OnExpiresChanging(System.DateTime value);
		
		partial void OnFlagsChanged();
		
		partial void OnFlagsChanging(int value);
		
		partial void OnLockDateChanged();
		
		partial void OnLockDateChanging(System.DateTime value);
		
		partial void OnLockedChanged();
		
		partial void OnLockedChanging(sbyte value);
		
		partial void OnLockIDChanged();
		
		partial void OnLockIDChanging(int value);
		
		partial void OnSessionIDChanged();
		
		partial void OnSessionIDChanging(string value);
		
		partial void OnSessionItemsChanged();
		
		partial void OnSessionItemsChanging(byte[] value);
		
		partial void OnTimeoutChanged();
		
		partial void OnTimeoutChanging(int value);
		#endregion
	
	
	public My_AspNet_Session()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_applicationID", Name="ApplicationId", DbType="int", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ApplicationID
	{
		get
		{
			return this._applicationID;
		}
		set
		{
			if ((_applicationID != value))
			{
				this.OnApplicationIDChanging(value);
				this.SendPropertyChanging();
				this._applicationID = value;
				this.SendPropertyChanged("ApplicationID");
				this.OnApplicationIDChanged();
			}
		}
	}
	
	[Column(Storage="_created", Name="Created", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime Created
	{
		get
		{
			return this._created;
		}
		set
		{
			if ((_created != value))
			{
				this.OnCreatedChanging(value);
				this.SendPropertyChanging();
				this._created = value;
				this.SendPropertyChanged("Created");
				this.OnCreatedChanged();
			}
		}
	}
	
	[Column(Storage="_expires", Name="Expires", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime Expires
	{
		get
		{
			return this._expires;
		}
		set
		{
			if ((_expires != value))
			{
				this.OnExpiresChanging(value);
				this.SendPropertyChanging();
				this._expires = value;
				this.SendPropertyChanged("Expires");
				this.OnExpiresChanged();
			}
		}
	}
	
	[Column(Storage="_flags", Name="Flags", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Flags
	{
		get
		{
			return this._flags;
		}
		set
		{
			if ((_flags != value))
			{
				this.OnFlagsChanging(value);
				this.SendPropertyChanging();
				this._flags = value;
				this.SendPropertyChanged("Flags");
				this.OnFlagsChanged();
			}
		}
	}
	
	[Column(Storage="_lockDate", Name="LockDate", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime LockDate
	{
		get
		{
			return this._lockDate;
		}
		set
		{
			if ((_lockDate != value))
			{
				this.OnLockDateChanging(value);
				this.SendPropertyChanging();
				this._lockDate = value;
				this.SendPropertyChanged("LockDate");
				this.OnLockDateChanged();
			}
		}
	}
	
	[Column(Storage="_locked", Name="Locked", DbType="tinyint(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public sbyte Locked
	{
		get
		{
			return this._locked;
		}
		set
		{
			if ((_locked != value))
			{
				this.OnLockedChanging(value);
				this.SendPropertyChanging();
				this._locked = value;
				this.SendPropertyChanged("Locked");
				this.OnLockedChanged();
			}
		}
	}
	
	[Column(Storage="_lockID", Name="LockId", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int LockID
	{
		get
		{
			return this._lockID;
		}
		set
		{
			if ((_lockID != value))
			{
				this.OnLockIDChanging(value);
				this.SendPropertyChanging();
				this._lockID = value;
				this.SendPropertyChanged("LockID");
				this.OnLockIDChanged();
			}
		}
	}
	
	[Column(Storage="_sessionID", Name="SessionId", DbType="varchar(255)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string SessionID
	{
		get
		{
			return this._sessionID;
		}
		set
		{
			if (((_sessionID == value) 
						== false))
			{
				this.OnSessionIDChanging(value);
				this.SendPropertyChanging();
				this._sessionID = value;
				this.SendPropertyChanged("SessionID");
				this.OnSessionIDChanged();
			}
		}
	}
	
	[Column(Storage="_sessionItems", Name="SessionItems", DbType="longblob", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public byte[] SessionItems
	{
		get
		{
			return this._sessionItems;
		}
		set
		{
			if (((_sessionItems == value) 
						== false))
			{
				this.OnSessionItemsChanging(value);
				this.SendPropertyChanging();
				this._sessionItems = value;
				this.SendPropertyChanged("SessionItems");
				this.OnSessionItemsChanged();
			}
		}
	}
	
	[Column(Storage="_timeout", Name="Timeout", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Timeout
	{
		get
		{
			return this._timeout;
		}
		set
		{
			if ((_timeout != value))
			{
				this.OnTimeoutChanging(value);
				this.SendPropertyChanging();
				this._timeout = value;
				this.SendPropertyChanged("Timeout");
				this.OnTimeoutChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_sessioncleanup")]
public partial class My_AspNet_SessionCleanup
{
	
	private int _intervalMinutes;
	
	private System.DateTime _lastRun;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIntervalMinutesChanged();
		
		partial void OnIntervalMinutesChanging(int value);
		
		partial void OnLastRunChanged();
		
		partial void OnLastRunChanging(System.DateTime value);
		#endregion
	
	
	public My_AspNet_SessionCleanup()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_intervalMinutes", Name="IntervalMinutes", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int IntervalMinutes
	{
		get
		{
			return this._intervalMinutes;
		}
		set
		{
			if ((_intervalMinutes != value))
			{
				this.OnIntervalMinutesChanging(value);
				this._intervalMinutes = value;
				this.OnIntervalMinutesChanged();
			}
		}
	}
	
	[Column(Storage="_lastRun", Name="LastRun", DbType="datetime", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public System.DateTime LastRun
	{
		get
		{
			return this._lastRun;
		}
		set
		{
			if ((_lastRun != value))
			{
				this.OnLastRunChanging(value);
				this._lastRun = value;
				this.OnLastRunChanged();
			}
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_users")]
public partial class My_AspNet_User : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _applicationID;
	
	private int _id;
	
	private sbyte _isAnonymous;
	
	private System.Nullable<System.DateTime> _lastActivityDate;
	
	private string _name;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnApplicationIDChanged();
		
		partial void OnApplicationIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnIsAnonymousChanged();
		
		partial void OnIsAnonymousChanging(sbyte value);
		
		partial void OnLastActivityDateChanged();
		
		partial void OnLastActivityDateChanging(System.Nullable<System.DateTime> value);
		
		partial void OnNameChanged();
		
		partial void OnNameChanging(string value);
		#endregion
	
	
	public My_AspNet_User()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_applicationID", Name="applicationId", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ApplicationID
	{
		get
		{
			return this._applicationID;
		}
		set
		{
			if ((_applicationID != value))
			{
				this.OnApplicationIDChanging(value);
				this.SendPropertyChanging();
				this._applicationID = value;
				this.SendPropertyChanged("ApplicationID");
				this.OnApplicationIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="id", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_isAnonymous", Name="isAnonymous", DbType="tinyint(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public sbyte IsAnonymous
	{
		get
		{
			return this._isAnonymous;
		}
		set
		{
			if ((_isAnonymous != value))
			{
				this.OnIsAnonymousChanging(value);
				this.SendPropertyChanging();
				this._isAnonymous = value;
				this.SendPropertyChanged("IsAnonymous");
				this.OnIsAnonymousChanged();
			}
		}
	}
	
	[Column(Storage="_lastActivityDate", Name="lastActivityDate", DbType="datetime", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public System.Nullable<System.DateTime> LastActivityDate
	{
		get
		{
			return this._lastActivityDate;
		}
		set
		{
			if ((_lastActivityDate != value))
			{
				this.OnLastActivityDateChanging(value);
				this.SendPropertyChanging();
				this._lastActivityDate = value;
				this.SendPropertyChanged("LastActivityDate");
				this.OnLastActivityDateChanged();
			}
		}
	}
	
	[Column(Storage="_name", Name="name", DbType="varchar(256)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Name
	{
		get
		{
			return this._name;
		}
		set
		{
			if (((_name == value) 
						== false))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.my_aspnet_usersinroles")]
public partial class My_AspNet_UsersInRole : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _roleID;
	
	private int _userID;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnRoleIDChanged();
		
		partial void OnRoleIDChanging(int value);
		
		partial void OnUserIDChanged();
		
		partial void OnUserIDChanging(int value);
		#endregion
	
	
	public My_AspNet_UsersInRole()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_roleID", Name="roleId", DbType="int", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int RoleID
	{
		get
		{
			return this._roleID;
		}
		set
		{
			if ((_roleID != value))
			{
				this.OnRoleIDChanging(value);
				this.SendPropertyChanging();
				this._roleID = value;
				this.SendPropertyChanged("RoleID");
				this.OnRoleIDChanged();
			}
		}
	}
	
	[Column(Storage="_userID", Name="userId", DbType="int", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int UserID
	{
		get
		{
			return this._userID;
		}
		set
		{
			if ((_userID != value))
			{
				this.OnUserIDChanging(value);
				this.SendPropertyChanging();
				this._userID = value;
				this.SendPropertyChanged("UserID");
				this.OnUserIDChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.pharmactions")]
public partial class PhArmAction : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _cfID;
	
	private int _id;
	
	private int _source;
	
	private int _type;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	private EntityRef<PubCHemIDS> _pubChEmIds = new EntityRef<PubCHemIDS>();
	
	private EntityRef<PhArmActionType> _phArmActionType = new EntityRef<PhArmActionType>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCfIDChanged();
		
		partial void OnCfIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnSourceChanged();
		
		partial void OnSourceChanging(int value);
		
		partial void OnTypeChanged();
		
		partial void OnTypeChanging(int value);
		#endregion
	
	
	public PhArmAction()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_cfID", Name="CFID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CfID
	{
		get
		{
			return this._cfID;
		}
		set
		{
			if ((_cfID != value))
			{
				this.OnCfIDChanging(value);
				this.SendPropertyChanging();
				this._cfID = value;
				this.SendPropertyChanged("CfID");
				this.OnCfIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_source", Name="Source", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Source
	{
		get
		{
			return this._source;
		}
		set
		{
			if ((_source != value))
			{
				if (_pubChEmIds.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnSourceChanging(value);
				this.SendPropertyChanging();
				this._source = value;
				this.SendPropertyChanged("Source");
				this.OnSourceChanged();
			}
		}
	}
	
	[Column(Storage="_type", Name="Type", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Type
	{
		get
		{
			return this._type;
		}
		set
		{
			if ((_type != value))
			{
				if (_phArmActionType.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnTypeChanging(value);
				this.SendPropertyChanging();
				this._type = value;
				this.SendPropertyChanged("Type");
				this.OnTypeChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CfID", Name="pharmactions_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.PhArmActions.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.PhArmActions.Add(this);
					_cfID = value.ID;
				}
				else
				{
					_cfID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_pubChEmIds", OtherKey="ID", ThisKey="Source", Name="pharmactions_ibfk_2", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public PubCHemIDS PubCHemIDS
	{
		get
		{
			return this._pubChEmIds.Entity;
		}
		set
		{
			if (((this._pubChEmIds.Entity == value) 
						== false))
			{
				if ((this._pubChEmIds.Entity != null))
				{
					PubCHemIDS previousPubCHemIDS = this._pubChEmIds.Entity;
					this._pubChEmIds.Entity = null;
					previousPubCHemIDS.PhArmActions.Remove(this);
				}
				this._pubChEmIds.Entity = value;
				if ((value != null))
				{
					value.PhArmActions.Add(this);
					_source = value.ID;
				}
				else
				{
					_source = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_phArmActionType", OtherKey="ID", ThisKey="Type", Name="pharmactions_ibfk_3", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public PhArmActionType PhArmActionType
	{
		get
		{
			return this._phArmActionType.Entity;
		}
		set
		{
			if (((this._phArmActionType.Entity == value) 
						== false))
			{
				if ((this._phArmActionType.Entity != null))
				{
					PhArmActionType previousPhArmActionType = this._phArmActionType.Entity;
					this._phArmActionType.Entity = null;
					previousPhArmActionType.PhArmActions.Remove(this);
				}
				this._phArmActionType.Entity = value;
				if ((value != null))
				{
					value.PhArmActions.Add(this);
					_type = value.ID;
				}
				else
				{
					_type = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.pharmactiontypes")]
public partial class PhArmActionType : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _id;
	
	private int _parentID;
	
	private string _typeDescription;
	
	private EntitySet<PhArmAction> _phArmActions;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnParentIDChanged();
		
		partial void OnParentIDChanging(int value);
		
		partial void OnTypeDescriptionChanged();
		
		partial void OnTypeDescriptionChanging(string value);
		#endregion
	
	
	public PhArmActionType()
	{
		_phArmActions = new EntitySet<PhArmAction>(new Action<PhArmAction>(this.PhArmActions_Attach), new Action<PhArmAction>(this.PhArmActions_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_parentID", Name="ParentID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ParentID
	{
		get
		{
			return this._parentID;
		}
		set
		{
			if ((_parentID != value))
			{
				this.OnParentIDChanging(value);
				this.SendPropertyChanging();
				this._parentID = value;
				this.SendPropertyChanged("ParentID");
				this.OnParentIDChanged();
			}
		}
	}
	
	[Column(Storage="_typeDescription", Name="TypeDescription", DbType="text", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string TypeDescription
	{
		get
		{
			return this._typeDescription;
		}
		set
		{
			if (((_typeDescription == value) 
						== false))
			{
				this.OnTypeDescriptionChanging(value);
				this.SendPropertyChanging();
				this._typeDescription = value;
				this.SendPropertyChanged("TypeDescription");
				this.OnTypeDescriptionChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_phArmActions", OtherKey="Type", ThisKey="ID", Name="pharmactions_ibfk_3")]
	[DebuggerNonUserCode()]
	public EntitySet<PhArmAction> PhArmActions
	{
		get
		{
			return this._phArmActions;
		}
		set
		{
			this._phArmActions = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void PhArmActions_Attach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.PhArmActionType = this;
	}
	
	private void PhArmActions_Detach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.PhArmActionType = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.pubchemids")]
public partial class PubCHemIDS : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _canonicalSmiles;
	
	private int _cfID;
	
	private int _heavyAtomCount;
	
	private int _id;
	
	private string _isomericSmiles;
	
	private int _numCovalentUnits;
	
	private string _pubChemID;
	
	private int _totalChiralCenters;
	
	private int _totalDefinedCenters;
	
	private string _type;
	
	private EntitySet<PhArmAction> _phArmActions;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCanonicalSmilesChanged();
		
		partial void OnCanonicalSmilesChanging(string value);
		
		partial void OnCfIDChanged();
		
		partial void OnCfIDChanging(int value);
		
		partial void OnHeavyAtomCountChanged();
		
		partial void OnHeavyAtomCountChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnIsomericSmilesChanged();
		
		partial void OnIsomericSmilesChanging(string value);
		
		partial void OnNumCovalentUnitsChanged();
		
		partial void OnNumCovalentUnitsChanging(int value);
		
		partial void OnPubChemIDChanged();
		
		partial void OnPubChemIDChanging(string value);
		
		partial void OnTotalChiralCentersChanged();
		
		partial void OnTotalChiralCentersChanging(int value);
		
		partial void OnTotalDefinedCentersChanged();
		
		partial void OnTotalDefinedCentersChanging(int value);
		
		partial void OnTypeChanged();
		
		partial void OnTypeChanging(string value);
		#endregion
	
	
	public PubCHemIDS()
	{
		_phArmActions = new EntitySet<PhArmAction>(new Action<PhArmAction>(this.PhArmActions_Attach), new Action<PhArmAction>(this.PhArmActions_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_canonicalSmiles", Name="CanonicalSMILES", DbType="text", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string CanonicalSmiles
	{
		get
		{
			return this._canonicalSmiles;
		}
		set
		{
			if (((_canonicalSmiles == value) 
						== false))
			{
				this.OnCanonicalSmilesChanging(value);
				this.SendPropertyChanging();
				this._canonicalSmiles = value;
				this.SendPropertyChanged("CanonicalSmiles");
				this.OnCanonicalSmilesChanged();
			}
		}
	}
	
	[Column(Storage="_cfID", Name="CFID", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CfID
	{
		get
		{
			return this._cfID;
		}
		set
		{
			if ((_cfID != value))
			{
				this.OnCfIDChanging(value);
				this.SendPropertyChanging();
				this._cfID = value;
				this.SendPropertyChanged("CfID");
				this.OnCfIDChanged();
			}
		}
	}
	
	[Column(Storage="_heavyAtomCount", Name="HeavyAtomCount", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int HeavyAtomCount
	{
		get
		{
			return this._heavyAtomCount;
		}
		set
		{
			if ((_heavyAtomCount != value))
			{
				this.OnHeavyAtomCountChanging(value);
				this.SendPropertyChanging();
				this._heavyAtomCount = value;
				this.SendPropertyChanged("HeavyAtomCount");
				this.OnHeavyAtomCountChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_isomericSmiles", Name="IsomericSMILES", DbType="text", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string IsomericSmiles
	{
		get
		{
			return this._isomericSmiles;
		}
		set
		{
			if (((_isomericSmiles == value) 
						== false))
			{
				this.OnIsomericSmilesChanging(value);
				this.SendPropertyChanging();
				this._isomericSmiles = value;
				this.SendPropertyChanged("IsomericSmiles");
				this.OnIsomericSmilesChanged();
			}
		}
	}
	
	[Column(Storage="_numCovalentUnits", Name="NumCovalentUnits", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int NumCovalentUnits
	{
		get
		{
			return this._numCovalentUnits;
		}
		set
		{
			if ((_numCovalentUnits != value))
			{
				this.OnNumCovalentUnitsChanging(value);
				this.SendPropertyChanging();
				this._numCovalentUnits = value;
				this.SendPropertyChanged("NumCovalentUnits");
				this.OnNumCovalentUnitsChanged();
			}
		}
	}
	
	[Column(Storage="_pubChemID", Name="PubChemID", DbType="varchar(50)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string PubChemID
	{
		get
		{
			return this._pubChemID;
		}
		set
		{
			if (((_pubChemID == value) 
						== false))
			{
				this.OnPubChemIDChanging(value);
				this.SendPropertyChanging();
				this._pubChemID = value;
				this.SendPropertyChanged("PubChemID");
				this.OnPubChemIDChanged();
			}
		}
	}
	
	[Column(Storage="_totalChiralCenters", Name="TotalChiralCenters", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int TotalChiralCenters
	{
		get
		{
			return this._totalChiralCenters;
		}
		set
		{
			if ((_totalChiralCenters != value))
			{
				this.OnTotalChiralCentersChanging(value);
				this.SendPropertyChanging();
				this._totalChiralCenters = value;
				this.SendPropertyChanged("TotalChiralCenters");
				this.OnTotalChiralCentersChanged();
			}
		}
	}
	
	[Column(Storage="_totalDefinedCenters", Name="TotalDefinedCenters", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int TotalDefinedCenters
	{
		get
		{
			return this._totalDefinedCenters;
		}
		set
		{
			if ((_totalDefinedCenters != value))
			{
				this.OnTotalDefinedCentersChanging(value);
				this.SendPropertyChanging();
				this._totalDefinedCenters = value;
				this.SendPropertyChanged("TotalDefinedCenters");
				this.OnTotalDefinedCentersChanged();
			}
		}
	}
	
	[Column(Storage="_type", Name="Type", DbType="char(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Type
	{
		get
		{
			return this._type;
		}
		set
		{
			if (((_type == value) 
						== false))
			{
				this.OnTypeChanging(value);
				this.SendPropertyChanging();
				this._type = value;
				this.SendPropertyChanged("Type");
				this.OnTypeChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_phArmActions", OtherKey="Source", ThisKey="ID", Name="pharmactions_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<PhArmAction> PhArmActions
	{
		get
		{
			return this._phArmActions;
		}
		set
		{
			this._phArmActions = value;
		}
	}
	#endregion
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CfID", Name="pubchemids_ibfk_1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.PubCHemIDS.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.PubCHemIDS.Add(this);
					_cfID = value.ID;
				}
				else
				{
					_cfID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void PhArmActions_Attach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.PubCHemIDS = this;
	}
	
	private void PhArmActions_Detach(PhArmAction entity)
	{
		this.SendPropertyChanging();
		entity.PubCHemIDS = null;
	}
	#endregion
}

[Table(Name="naturalproductsdb.pubmedarticles")]
public partial class PubMedArticle : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _details;
	
	private long _pmID;
	
	private int _type;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnDetailsChanged();
		
		partial void OnDetailsChanging(string value);
		
		partial void OnPMidChanged();
		
		partial void OnPMidChanging(long value);
		
		partial void OnTypeChanged();
		
		partial void OnTypeChanging(int value);
		#endregion
	
	
	public PubMedArticle()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_details", Name="Details", DbType="varchar(2000)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Details
	{
		get
		{
			return this._details;
		}
		set
		{
			if (((_details == value) 
						== false))
			{
				this.OnDetailsChanging(value);
				this.SendPropertyChanging();
				this._details = value;
				this.SendPropertyChanged("Details");
				this.OnDetailsChanged();
			}
		}
	}
	
	[Column(Storage="_pmID", Name="PMID", DbType="bigint(20)", IsPrimaryKey=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public long PMid
	{
		get
		{
			return this._pmID;
		}
		set
		{
			if ((_pmID != value))
			{
				this.OnPMidChanging(value);
				this.SendPropertyChanging();
				this._pmID = value;
				this.SendPropertyChanged("PMid");
				this.OnPMidChanged();
			}
		}
	}
	
	[Column(Storage="_type", Name="Type", DbType="int", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Type
	{
		get
		{
			return this._type;
		}
		set
		{
			if ((_type != value))
			{
				this.OnTypeChanging(value);
				this.SendPropertyChanging();
				this._type = value;
				this.SendPropertyChanged("Type");
				this.OnTypeChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.relatedcompoundfamilies")]
public partial class RelatedCompoundFamily : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private bool _auto;
	
	private int _familyNameID;
	
	private int _id;
	
	private int _relatedFamilyNameID;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	private EntityRef<CompoundFamily> _compoundFamily1 = new EntityRef<CompoundFamily>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnAutoChanged();
		
		partial void OnAutoChanging(bool value);
		
		partial void OnFamilyNameIDChanged();
		
		partial void OnFamilyNameIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnRelatedFamilyNameIDChanged();
		
		partial void OnRelatedFamilyNameIDChanging(int value);
		#endregion
	
	
	public RelatedCompoundFamily()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_auto", Name="Auto", DbType="bit(1)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public bool Auto
	{
		get
		{
			return this._auto;
		}
		set
		{
			if ((_auto != value))
			{
				this.OnAutoChanging(value);
				this.SendPropertyChanging();
				this._auto = value;
				this.SendPropertyChanged("Auto");
				this.OnAutoChanged();
			}
		}
	}
	
	[Column(Storage="_familyNameID", Name="FamilyNameID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int FamilyNameID
	{
		get
		{
			return this._familyNameID;
		}
		set
		{
			if ((_familyNameID != value))
			{
				if (_compoundFamily.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnFamilyNameIDChanging(value);
				this.SendPropertyChanging();
				this._familyNameID = value;
				this.SendPropertyChanged("FamilyNameID");
				this.OnFamilyNameIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_relatedFamilyNameID", Name="RelatedFamilyNameID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int RelatedFamilyNameID
	{
		get
		{
			return this._relatedFamilyNameID;
		}
		set
		{
			if ((_relatedFamilyNameID != value))
			{
				if (_compoundFamily1.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnRelatedFamilyNameIDChanging(value);
				this.SendPropertyChanging();
				this._relatedFamilyNameID = value;
				this.SendPropertyChanged("RelatedFamilyNameID");
				this.OnRelatedFamilyNameIDChanged();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="FamilyNameID", Name="FK_RelatedCompoundFamilies_CompoundFamilies", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.RelatedCompoundFamilies.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.RelatedCompoundFamilies.Add(this);
					_familyNameID = value.ID;
				}
				else
				{
					_familyNameID = default(int);
				}
			}
		}
	}
	
	[Association(Storage="_compoundFamily1", OtherKey="ID", ThisKey="RelatedFamilyNameID", Name="FK_RelatedCompoundFamilies_CompoundFamilies1", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily1
	{
		get
		{
			return this._compoundFamily1.Entity;
		}
		set
		{
			if (((this._compoundFamily1.Entity == value) 
						== false))
			{
				if ((this._compoundFamily1.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily1.Entity;
					this._compoundFamily1.Entity = null;
					previousCompoundFamily.RelatedCompoundFamilies1.Remove(this);
				}
				this._compoundFamily1.Entity = value;
				if ((value != null))
				{
					value.RelatedCompoundFamilies1.Add(this);
					_relatedFamilyNameID = value.ID;
				}
				else
				{
					_relatedFamilyNameID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.synonyms")]
public partial class Synonym : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private int _compoundFamilyID;
	
	private int _id;
	
	private string _synonym1;
	
	private EntityRef<CompoundFamily> _compoundFamily = new EntityRef<CompoundFamily>();
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnCompoundFamilyIDChanged();
		
		partial void OnCompoundFamilyIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnSynonym1Changed();
		
		partial void OnSynonym1Changing(string value);
		#endregion
	
	
	public Synonym()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_compoundFamilyID", Name="CompoundFamilyID", DbType="int(10)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int CompoundFamilyID
	{
		get
		{
			return this._compoundFamilyID;
		}
		set
		{
			if ((_compoundFamilyID != value))
			{
				if (_compoundFamily.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnCompoundFamilyIDChanging(value);
				this.SendPropertyChanging();
				this._compoundFamilyID = value;
				this.SendPropertyChanged("CompoundFamilyID");
				this.OnCompoundFamilyIDChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_synonym1", Name="Synonym", DbType="varchar(250)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Synonym1
	{
		get
		{
			return this._synonym1;
		}
		set
		{
			if (((_synonym1 == value) 
						== false))
			{
				this.OnSynonym1Changing(value);
				this.SendPropertyChanging();
				this._synonym1 = value;
				this.SendPropertyChanged("Synonym1");
				this.OnSynonym1Changed();
			}
		}
	}
	
	#region Parents
	[Association(Storage="_compoundFamily", OtherKey="ID", ThisKey="CompoundFamilyID", Name="FK_Synonyms_CompoundFamilies", IsForeignKey=true)]
	[DebuggerNonUserCode()]
	public CompoundFamily CompoundFamily
	{
		get
		{
			return this._compoundFamily.Entity;
		}
		set
		{
			if (((this._compoundFamily.Entity == value) 
						== false))
			{
				if ((this._compoundFamily.Entity != null))
				{
					CompoundFamily previousCompoundFamily = this._compoundFamily.Entity;
					this._compoundFamily.Entity = null;
					previousCompoundFamily.Synonyms.Remove(this);
				}
				this._compoundFamily.Entity = value;
				if ((value != null))
				{
					value.Synonyms.Add(this);
					_compoundFamilyID = value.ID;
				}
				else
				{
					_compoundFamilyID = default(int);
				}
			}
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}

[Table(Name="naturalproductsdb.userinfo")]
public partial class UserInfo : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _email;
	
	private string _first_nAme;
	
	private int _id;
	
	private string _lab_nAme;
	
	private string _lab_uRl;
	
	private string _last_nAme;
	
	private string _user_nAme;
	
	private EntitySet<ClusterBase> _clusterBases;
	
	private EntitySet<ClusterBase> _clusterBases1;
	
	private EntitySet<CompoundFamily> _compoundFamilies;
	
	private EntitySet<CompoundFamily> _compoundFamilies1;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnEmailChanged();
		
		partial void OnEmailChanging(string value);
		
		partial void OnFirst_nAmeChanged();
		
		partial void OnFirst_nAmeChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnLab_nAmeChanged();
		
		partial void OnLab_nAmeChanging(string value);
		
		partial void OnLab_uRlChanged();
		
		partial void OnLab_uRlChanging(string value);
		
		partial void OnLast_nAmeChanged();
		
		partial void OnLast_nAmeChanging(string value);
		
		partial void OnUser_nAmeChanged();
		
		partial void OnUser_nAmeChanging(string value);
		#endregion
	
	
	public UserInfo()
	{
		_clusterBases = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases_Attach), new Action<ClusterBase>(this.ClusterBases_Detach));
		_clusterBases1 = new EntitySet<ClusterBase>(new Action<ClusterBase>(this.ClusterBases1_Attach), new Action<ClusterBase>(this.ClusterBases1_Detach));
		_compoundFamilies = new EntitySet<CompoundFamily>(new Action<CompoundFamily>(this.CompoundFamilies_Attach), new Action<CompoundFamily>(this.CompoundFamilies_Detach));
		_compoundFamilies1 = new EntitySet<CompoundFamily>(new Action<CompoundFamily>(this.CompoundFamilies1_Attach), new Action<CompoundFamily>(this.CompoundFamilies1_Detach));
		this.OnCreated();
	}
	
	[Column(Storage="_email", Name="Email", DbType="varchar(256)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Email
	{
		get
		{
			return this._email;
		}
		set
		{
			if (((_email == value) 
						== false))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[Column(Storage="_first_nAme", Name="First_Name", DbType="varchar(256)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string First_nAme
	{
		get
		{
			return this._first_nAme;
		}
		set
		{
			if (((_first_nAme == value) 
						== false))
			{
				this.OnFirst_nAmeChanging(value);
				this.SendPropertyChanging();
				this._first_nAme = value;
				this.SendPropertyChanged("First_nAme");
				this.OnFirst_nAmeChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="ID", DbType="int(10)", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_lab_nAme", Name="Lab_Name", DbType="varchar(256)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Lab_nAme
	{
		get
		{
			return this._lab_nAme;
		}
		set
		{
			if (((_lab_nAme == value) 
						== false))
			{
				this.OnLab_nAmeChanging(value);
				this.SendPropertyChanging();
				this._lab_nAme = value;
				this.SendPropertyChanged("Lab_nAme");
				this.OnLab_nAmeChanged();
			}
		}
	}
	
	[Column(Storage="_lab_uRl", Name="Lab_Url", DbType="varchar(256)", AutoSync=AutoSync.Never)]
	[DebuggerNonUserCode()]
	public string Lab_uRl
	{
		get
		{
			return this._lab_uRl;
		}
		set
		{
			if (((_lab_uRl == value) 
						== false))
			{
				this.OnLab_uRlChanging(value);
				this.SendPropertyChanging();
				this._lab_uRl = value;
				this.SendPropertyChanged("Lab_uRl");
				this.OnLab_uRlChanged();
			}
		}
	}
	
	[Column(Storage="_last_nAme", Name="Last_Name", DbType="varchar(256)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Last_nAme
	{
		get
		{
			return this._last_nAme;
		}
		set
		{
			if (((_last_nAme == value) 
						== false))
			{
				this.OnLast_nAmeChanging(value);
				this.SendPropertyChanging();
				this._last_nAme = value;
				this.SendPropertyChanged("Last_nAme");
				this.OnLast_nAmeChanged();
			}
		}
	}
	
	[Column(Storage="_user_nAme", Name="User_Name", DbType="varchar(256)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string User_nAme
	{
		get
		{
			return this._user_nAme;
		}
		set
		{
			if (((_user_nAme == value) 
						== false))
			{
				this.OnUser_nAmeChanging(value);
				this.SendPropertyChanging();
				this._user_nAme = value;
				this.SendPropertyChanged("User_nAme");
				this.OnUser_nAmeChanged();
			}
		}
	}
	
	#region Children
	[Association(Storage="_clusterBases", OtherKey="ContributorID", ThisKey="ID", Name="clusterbase_ibfk_5")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases
	{
		get
		{
			return this._clusterBases;
		}
		set
		{
			this._clusterBases = value;
		}
	}
	
	[Association(Storage="_clusterBases1", OtherKey="LastModifiedUserID", ThisKey="ID", Name="clusterbase_ibfk_6")]
	[DebuggerNonUserCode()]
	public EntitySet<ClusterBase> ClusterBases1
	{
		get
		{
			return this._clusterBases1;
		}
		set
		{
			this._clusterBases1 = value;
		}
	}
	
	[Association(Storage="_compoundFamilies", OtherKey="ContributorID", ThisKey="ID", Name="compoundfamilies_ibfk_1")]
	[DebuggerNonUserCode()]
	public EntitySet<CompoundFamily> CompoundFamilies
	{
		get
		{
			return this._compoundFamilies;
		}
		set
		{
			this._compoundFamilies = value;
		}
	}
	
	[Association(Storage="_compoundFamilies1", OtherKey="LastModifiedUserID", ThisKey="ID", Name="compoundfamilies_ibfk_2")]
	[DebuggerNonUserCode()]
	public EntitySet<CompoundFamily> CompoundFamilies1
	{
		get
		{
			return this._compoundFamilies1;
		}
		set
		{
			this._compoundFamilies1 = value;
		}
	}
	#endregion
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
	
	#region Attachment handlers
	private void ClusterBases_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo = this;
	}
	
	private void ClusterBases_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo = null;
	}
	
	private void ClusterBases1_Attach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo1 = this;
	}
	
	private void ClusterBases1_Detach(ClusterBase entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo1 = null;
	}
	
	private void CompoundFamilies_Attach(CompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo = this;
	}
	
	private void CompoundFamilies_Detach(CompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo = null;
	}
	
	private void CompoundFamilies1_Attach(CompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo1 = this;
	}
	
	private void CompoundFamilies1_Detach(CompoundFamily entity)
	{
		this.SendPropertyChanging();
		entity.UserInfo1 = null;
	}
	#endregion
}
