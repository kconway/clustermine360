﻿using System.Linq;
using log4net;
using log4net.Config;
using NaturalProductsWebsite.Code;
using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for BackgroundWorker
/// </summary>
///

public class LongProcessWorker
{
    private static readonly ILog log = LogManager.GetLogger(typeof(LongProcessWorker));

    public static void startBackgroundProcessWorker()
    {
        XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
        int count = 0;

        bool fastafiles = AnyFASTAFilesToProcess();
        bool ncbi = AnyNCBIToProcess();
        bool antismash = AnyAntiSMASHToProcess();
        bool extractsequences = AnyExtractionToProcess();
        bool pubchem = AnyPubChemToProcess();

        while ((ncbi || antismash || extractsequences || fastafiles || pubchem) && count < 5)
        {
            ProcessFASTAFiles();
            ProcessClustersAndLargeSequences(ncbi, antismash, extractsequences);
            ProcessPubChem();
            fastafiles = AnyFASTAFilesToProcess();
            ncbi = AnyNCBIToProcess();
            antismash = AnyAntiSMASHToProcess();
            extractsequences = AnyExtractionToProcess();
            pubchem = AnyPubChemToProcess();
            count++;


        }
    }

    private static void ProcessPubChem()
    {

        log.Info("Processing - PubChem");
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            List<CompoundFamily> cfs = db.CompoundFamilies.Where(x => x.PubChemStatus == 0).ToList();

            foreach (CompoundFamily cf in cfs)
            {
                NCBI.getPubChemData(cf.ID);
                cf.PubChemStatus = 1;
                db.SubmitChanges();
            }
           
        }
        log.Info("Finished Processing - PubChem");
    }

    private static void ProcessFASTAFiles()
    {
        log.Info("Processing - FASTA files");
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (db.Downloads.Exists(x => x.Status == 0))
            {
                FastaGenerator.PrepareFASTAFiles();
            }
            DateTime thirtydaysold = DateTime.Now.AddDays(-30d);
            if (db.Downloads.Exists(x => x.WhenRequested <= thirtydaysold))
            {
                FastaGenerator.RemoveOldFASTAFiles();
            }
        }


    }

    private static void ProcessClustersAndLargeSequences(bool ncbi, bool antismash, bool extractsequences)
    {
        log.Info("Processing - NCBI: " + ncbi + " AntiSMASH: " + antismash + " ExtractSequences: " + extractsequences);
        
        if (ncbi == true)
        {
           NCBI.triggerNCBIDownload();
        }
        
        if (antismash == true)
        {
            AntiSmash.triggerAntiSmashDownload();
        }

        if (extractsequences == true)
        {
            ExtractSequences.triggerClusterSequenceExtraction();
        }
        log.Info("Finished processing round.");
    }

    private static bool AnyFASTAFilesToProcess()
    {
       bool process = false;
       try
       {
           using (DataClassesDataContext db = new DataClassesDataContext())
           {
               process = db.Downloads.Exists(x => x.Status == 0);
           }

       }
       catch (Exception e)
       {
           log.Debug("Any FASTA files to Process:" + Environment.NewLine + e.ToString());
       }

       return process;
    }

    private static bool AnyPubChemToProcess()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            return db.CompoundFamilies.Exists(x => x.PubChemStatus == 0);
        }
    }

    private static bool AnyNCBIToProcess()
    {
        
            bool process = false;
            try
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    int clustersToProcessForNCBI = (from c in db.Clusters
                                                    where c.ClusterBase.NCBIStatus == 0 || c.ClusterBase.NCBIStatus == 1
                                                    select c).ToList().Count;

                    int sequencesToProcessForNCBI = (from c in db.LargeSequences
                                                     where c.ClusterBase.NCBIStatus == 0 || c.ClusterBase.NCBIStatus == 1
                                                     select c).ToList().Count;

                    if ((clustersToProcessForNCBI > 0) || (sequencesToProcessForNCBI > 0))
                    {
                        process = true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Debug("Any NCBI to Process:" + Environment.NewLine + e.ToString());
            }

            return process;
        
    }

    private static bool AnyAntiSMASHToProcess()
    {
        bool process = false;
        try{
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            int clustersToProcessForAntiSmash = (from c in db.Clusters
                                                 where c.ClusterBase.AntiSmashStatus < 5
                                                 select c).ToList().Count;

            int sequencesToProcessForAntiSmash = (from c in db.LargeSequences
                                                  where c.ClusterBase.AntiSmashStatus < 5
                                                  select c).ToList().Count;

            if ((clustersToProcessForAntiSmash > 0) || (sequencesToProcessForAntiSmash > 0))
            {
                process = true;
            }
        }
        }
            catch (Exception e)
            {
                log.Debug("Any antiSMASH to Process:" + Environment.NewLine + e.ToString());
            }
        return process;
    }

    private static bool AnyExtractionToProcess()
    {
        bool process = false;
        try
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                int clustersToProcessForSequenceExtraction = (from c in db.Clusters
                                                              where (c.ClusterBase.SequenceExtractionStatus == 0 || c.ClusterBase.SequenceExtractionStatus == 1) && c.ClusterBase.AntiSmashStatus == 5
                                                              select c).ToList().Count;

                int sequencesToProcessForSequenceExtraction = (from c in db.LargeSequences
                                                               where (c.ClusterBase.SequenceExtractionStatus == 0 || c.ClusterBase.SequenceExtractionStatus == 1) && c.ClusterBase.AntiSmashStatus == 5
                                                               select c).ToList().Count;
                if ((clustersToProcessForSequenceExtraction > 0) || (sequencesToProcessForSequenceExtraction > 0))
                {
                    process = true;
                }
            }
        }
        
            catch (Exception e)
            {
                log.Debug("Any Sequences to Process:" + Environment.NewLine + e.ToString());
            }
        return process;
    }
}