﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using log4net;
using log4net.Config;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    /// <summary>
    /// Summary description for ExtractSequences
    /// </summary>
    public class ExtractSequences
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ExtractSequences));

        public static void triggerClusterSequenceExtraction()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));

            DataClassesDataContext db = new DataClassesDataContext();
            DateTime recent = DateTime.Now.AddMinutes(-30d);
            var clusters = (from c in db.Clusters
                            where ((c.ClusterBase.AntiSmashStatus == 5 && c.ClusterBase.NCBIStatus == 2) && (c.ClusterBase.SequenceExtractionStatus == 0 || (c.ClusterBase.SequenceExtractionStatus == 1 && c.ClusterBase.SequenceExtractionModified < recent)))
                            select c).ToList<Cluster>().ConvertAll(x => new ProcessEntity(x));

            var sequences = (from s in db.LargeSequences
                             where ((s.ClusterBase.AntiSmashStatus == 5 && s.ClusterBase.NCBIStatus == 2) && (s.ClusterBase.SequenceExtractionStatus == 0 || (s.ClusterBase.SequenceExtractionStatus == 1 && s.ClusterBase.SequenceExtractionModified < recent)))
                             select s).ToList<LargeSequence>().ConvertAll(x => new ProcessEntity(x));
            db.Dispose();

            clusters.AddRange(sequences);

            int clustercount = clusters.Count;
            log.Info("# clusters to extract sequences: " + clustercount);
            if ((clustercount) > 0)
            {
                foreach (var cluster in clusters)
                {
                    sequenceExtraction(cluster);
                }
            }
            log.Info("Finished extracting sequences from clusters.");
        }

        private static void sequenceExtraction(ProcessEntity entity)
        {
            if ((entity.ClusterBase.AntiSmashStatus == 5) && (entity.ClusterBase.NCBIStatus == 2))
            {
                ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 1);
                try
                {
                    string path = "";
                    if (entity.Type == ProcessEntityType.Cluster)
                    {
                        path = FileTools.getantiSMASHClustersFolder() + entity.ID + Path.DirectorySeparatorChar + "display.xhtml";
                    }
                    else if (entity.Type == ProcessEntityType.LargeSequence)
                    {
                        path = FileTools.getantiSMASHSequencesFolder() + entity.ID + Path.DirectorySeparatorChar + "display.xhtml"; 
                    }
                    extractPKSNRPSSequences(path, entity);
                }
                catch (Exception e)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error extracting sequence data", "<b>Error extracting sequence data</b><br /><br />Entity Type: " + entity.Type + "<br />Entity ID: " + entity.ID + "<br />Exception Type: " + e.GetType() + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 25);
                }
            }
            else if ((entity.ClusterBase.AntiSmashStatus == 6) || (entity.ClusterBase.AntiSmashStatus == 100))
            {
                ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 20);
            }
        }

        public static void extractPKSNRPSSequences(string path, ProcessEntity entity)
        {
            try
            {
                if (File.Exists(path))
                {
                    log.Info("Extracting sequence for entity " + entity.ID);

                    // are there sequences already linked to this entity? could cause problems if num clusters changes
                    // also need to try to make clusters and sequences process as similar as possible
                    DeleteExistingDomainsAndRelationShips(entity);

                    FileStream file = File.Open(path, FileMode.Open);
                    StreamReader sr = new StreamReader(file);

                    string filedata = sr.ReadToEnd();
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(filedata);

                    Regex rgx = new Regex("javascript:displaycluster\\(\\d+?\\)");
                    MatchCollection matches = rgx.Matches(filedata);

                    int numberofclusters = matches.Count;
                    log.Info("# clusters to check for extraction: " + numberofclusters);
                    HtmlNode parentnode = doc.DocumentNode.SelectSingleNode("/html/body");

                    // if cluster (but not sequence record), associate related clusters
                    if (entity.Type == ProcessEntityType.Cluster)
                    {
                        AssociateWithSimilarClusters(entity, parentnode);
                    }

                    // Extracting Clusters
                    for (int count = 1; count <= numberofclusters; count++)
                    {
                        log.Info("Extracting cluster " + count);
                        string clusternodeid = "genecluster" + count;

                        string groupingid = "";
                        if (entity.Type == ProcessEntityType.Cluster)
                        {
                            groupingid = "clust_" + entity.ID + "_" + count;
                        }
                        else if (entity.Type == ProcessEntityType.LargeSequence)
                        {
                            groupingid = "sequ_" + entity.ID + "_" + count;
                        }
                        DomainSequencesDataHandler.InsertDomainGrouping(groupingid);
                        ExtractCluster(entity, parentnode, clusternodeid, groupingid);
                    }

                    file.Dispose();
                    sr.Dispose();
                    log.Info("Completed extracting sequences for entity " + entity.ID);
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 2);
                }
                else
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "antiSmash File Does Not Exists", "<b>antiSmash File Does Not Exist</b><br /><br />Entity Type: " + entity.Type + "<br />Entity ID: " + entity.ID + "<br />May need to adjust pathway types manually.");
                    ProcessEntity.UpdateProcessEntity(entity, UpdateType.SequenceExtraction, 25);
                }
            }
            catch (IOException ioex)
            {
                if (ioex.Message.Contains("being used by another process"))
                {
                    log.Info("File is being used by another process. (Entity ID: " + entity.ID + ")");
                }
                else
                {
                    throw ioex;
                }
            }
        }

        private static void AssociateWithSimilarClusters(ProcessEntity entity, HtmlNode parentnode)
        {
            try
            {
                HtmlNode blastsectionnode = parentnode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("clusterblastview")).SingleOrDefault();
                if (blastsectionnode != null)
                {
                    List<HtmlNode> outernodes = blastsectionnode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("qcluster")).ToList();
                    foreach (HtmlNode innernode in outernodes)
                    {
                        List<HtmlNode> blastnodes = innernode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("hitcluster")).ToList();

                        int count = 0;
                        const int numtoinclude = 2;
                        foreach (HtmlNode relatedclusternode in blastnodes)
                        {
                            if (count < numtoinclude) // only include top numtoinclude related since matches after that are not usually that close
                            {
                                HtmlNode ncbiinfonode = relatedclusternode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("pub_pics")).SingleOrDefault();
                                if (ncbiinfonode != null)
                                {
                                    string ncbiurl = ncbiinfonode.FirstChild.Attributes["href"].Value;
                                    if (ncbiurl.Contains("nuccore"))
                                    {
                                        Regex getncbiregex = new Regex("[^/]+$");
                                        string ncbiaccession = getncbiregex.Match(ncbiurl).Value;

                                        // search clusters for matche (that are not genomes)
                                        DataClassesDataContext db = new DataClassesDataContext();
                                        Cluster matchingaccession = db.Clusters.Where(c => c.ClusterBase.GenbankAccession == ncbiaccession && c.ClusterIsEntireSequence == true).SingleOrDefault();
                                        if (matchingaccession != null)
                                        {
                                            //Associate

                                        }
                                        else // else check clusters from genomes with same accession and +/- 5000 bp
                                        {
                                            // get start and end range
                                            HtmlNode startnode = relatedclusternode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.StartsWith("q") && d.Attributes["class"].Value.Contains("hidden popup")).FirstOrDefault();
                                            HtmlNode endnode = relatedclusternode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.StartsWith("q") && d.Attributes["class"].Value.Contains("hidden popup")).LastOrDefault();

                                            if (startnode != null && endnode != null)
                                            {
                                                int start;
                                                int end;
                                                List<Cluster> possiblematches = db.Clusters.Where(c => c.ClusterBase.GenbankAccession == ncbiaccession).ToList();
                                                foreach (Cluster possiblematch in possiblematches)
                                                {

                                                }
                                            }


                                        }

                                    }
                                }
                            }
                            count++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Debug("Error associating related clusters for cluster id " + entity.ID + Environment.NewLine + ex.ToString());
            }
        

        }

        private static void ExtractCluster(ProcessEntity entity, HtmlNode parentnode, string clusternodeid, string groupingid)
        {
            List<HtmlNode> nodes = parentnode.Descendants("div").Where(d => d.Attributes.Contains("id") && d.Attributes["id"].Value.Contains("genecluster")).ToList();

            HtmlNode clusternode = nodes.Where(n => n.Attributes.Contains("id") && n.Attributes["id"].Value == clusternodeid).First();
            HtmlNode clustertypenode = clusternode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value == "clusterdescr").First();

            // extract type and compare just to be sure
            Regex type = new Regex("Type = \\S+\\.{1}");
            string rawclustertype = type.Match(clustertypenode.InnerText).Value;
            string cleanedclustertype = rawclustertype.Replace("Type = ", String.Empty);
            cleanedclustertype = cleanedclustertype.Replace(".", String.Empty);
            cleanedclustertype = cleanedclustertype.Trim().ToUpperInvariant();
            if (cleanedclustertype.Contains("PKS") || cleanedclustertype.Contains("NRPS"))
            { // assign pathwaytypes
                bool isModular = clusternode.InnerHtml.Contains("modular");
                bool isIterative = clusternode.InnerHtml.Contains("iterative");
                if (entity.Type == ProcessEntityType.Cluster)
                {
                    AssignPathwayTypeToCF(entity.ID, cleanedclustertype, isModular, isIterative);
                }
              

                // Assign Pathway Type to Domain Record
                AssignPathwayTypeToDomainRecords(groupingid, cleanedclustertype, isModular, isIterative);


                ExtractDomains(entity, clusternode, groupingid, cleanedclustertype);
            }

        }
        
        private static void ExtractDomains(ProcessEntity entity, HtmlNode clusternode, string groupingid, string cleanedclustertype)
        {
            // Need to get genes and sequences, and types
            List<HtmlNode> domains = clusternode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Equals("hidden popup") && d.Attributes.Contains("id") && d.Attributes["id"].Value.StartsWith("b")).ToList();


            foreach (HtmlNode domain in domains)
            {
                try
                {
                    // parse some more!!!
                    string genedataraw = domain.FirstChild.InnerText;
                    Regex geneidentifierregex = new Regex("\\(\\S+?\\)");
                    string geneidentifier = geneidentifierregex.Match(genedataraw).Value.Replace("(", String.Empty).Replace(")", String.Empty).Trim();

                    Regex domaintyperegex = new Regex("Domain \\S+");
                    string domaintype = domaintyperegex.Match(genedataraw).Value.Replace("Domain ", String.Empty).Replace("PKS_", String.Empty);
                    char last = domaintype[domaintype.Length - 1];
                    if (Char.IsDigit(last))
                    {
                        domaintype = domaintype.Substring(0, domaintype.Length - 1);
                    }
                    domaintype = Regex.Replace(domaintype, "_", ": ");
                    if (domaintype.Equals("AMP-binding"))
                    {
                        domaintype = "Adenylation";
                    }
                    HtmlNode at_or_a_predict = (from g in domain.ChildNodes
                                                where g.NodeType == HtmlNodeType.Text
                                                && g.InnerText.Contains("Predicted substrate")
                                                select g).SingleOrDefault();

                    bool isAorAt = false;
                    string at_or_a_predicted = "";
                    if (at_or_a_predict != null)
                    {
                        isAorAt = true;
                        at_or_a_predicted = at_or_a_predict.InnerText;
                        at_or_a_predicted = at_or_a_predicted.Replace("Predicted substrate: ", String.Empty).Trim();
                    }

                    HtmlNode kr_activity_predict = (from g in domain.ChildNodes
                                                    where g.NodeType == HtmlNodeType.Text
                                                    && g.InnerText.Contains("KR activity")
                                                    select g).SingleOrDefault();
                    bool isKR = false;
                    string kr_activity_predicted = "";
                    if (kr_activity_predict != null)
                    {
                        isKR = true;
                        kr_activity_predicted = kr_activity_predict.InnerText;
                        kr_activity_predicted = kr_activity_predicted.Replace("KR activity: ", String.Empty).Trim();
                    }

                    HtmlNode kr_stereochem_predict = (from g in domain.ChildNodes
                                                      where g.NodeType == HtmlNodeType.Text
                                                      && g.InnerText.Contains("KR stereochemistry")
                                                      select g).SingleOrDefault();

                    string kr_stereochem_predicted = "";
                    if (kr_stereochem_predict != null)
                    {
                        kr_stereochem_predicted = kr_stereochem_predict.InnerText;
                        kr_stereochem_predicted = kr_stereochem_predicted.Replace("KR stereochemistry: ", String.Empty).Trim();
                    }

                    HtmlNode link = (from g in domain.ChildNodes
                                     where g.Attributes.Contains("href") && g.Attributes["href"].Value.Contains("http://blast.ncbi.nlm.nih.gov/")
                                     select g).FirstOrDefault();

                    string linkdata = link.Attributes["href"].Value;
                    Regex sequenceregex = new Regex("QUERY=\\w+&");
                    string sequencedataraw = sequenceregex.Match(linkdata).Value;
                    string sequencedata = sequencedataraw.Replace("QUERY=", String.Empty).Replace("&", String.Empty);

                    SHA256Managed hasher = new SHA256Managed();
                    byte[] sequenceBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sequencedata);
                    byte[] sequenceHash = hasher.ComputeHash(sequenceBytes);
                    string hashedSequence = Convert.ToBase64String(sequenceHash, 0, sequenceHash.Length);


                    using (DataClassesDataContext db = new DataClassesDataContext())
                    {
                        DomainSequence sequenceRecord = (from s in db.DomainSequences
                                                         where s.Identifier == geneidentifier
                                                         && s.DomainType == domaintype
                                                         && s.HashedSequence == hashedSequence
                                                         select s).SingleOrDefault();
                        if (sequenceRecord == null)
                        {
                            sequenceRecord = new DomainSequence();
                            if (entity.Type == ProcessEntityType.Cluster)
                            {
                                sequenceRecord.ClusterID = entity.ID;
                            }
                            else if (entity.Type == ProcessEntityType.LargeSequence)
                            {
                                sequenceRecord.LargeSequenceID = entity.ID;
                            }
                            db.DomainSequences.InsertOnSubmit(sequenceRecord);
                        }
                        else
                        {
                            if ((sequenceRecord.ClusterID == null) && (entity.Type == ProcessEntityType.Cluster))
                            {
                                sequenceRecord.ClusterID = entity.ID;
                                sequenceRecord.LargeSequenceID = null;
                            }
                        }

                        sequenceRecord.GroupingID = groupingid;
                        string[] ids = groupingid.Split(new char[] { '_' });
                        sequenceRecord.ClusterNum = int.Parse(ids[2]);
                        sequenceRecord.ClusterType = cleanedclustertype;
                        sequenceRecord.DomainType = domaintype;
                        sequenceRecord.PhylumID = entity.ClusterBase.PhylumID;
                        sequenceRecord.Organism = entity.ClusterBase.OrgName != null ? entity.ClusterBase.OrgName : "";
                        sequenceRecord.Identifier = geneidentifier;
                        sequenceRecord.Sequence = StringTools.StringToByteArray(sequencedata);
                        sequenceRecord.Length = sequencedata.Length;
                        sequenceRecord.HashedSequence = hashedSequence;
                        sequenceRecord.IsKR = false;
                        sequenceRecord.IsATorA = false;

                        if (isAorAt)
                        {
                            sequenceRecord.IsATorA = isAorAt;
                            sequenceRecord.AorATPrediction = at_or_a_predicted;
                        }
                        else if (isKR)
                        {
                            sequenceRecord.IsKR = isKR;
                            sequenceRecord.KRActivityPrediction = kr_activity_predicted;
                            sequenceRecord.KRStereoChemPrediction = kr_stereochem_predicted;
                        }


                        try
                        {
                            db.SubmitChanges();
                        }
                        catch (SqlException sqlEx)
                        {
                            if (sqlEx.Message.Contains("Cannot insert duplicate key"))
                            {
                                // ok
                                log.Info("Cannot insert duplicate key for " + sequenceRecord.Identifier);
                            }
                            else
                            {
                                throw sqlEx;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    // error with gene extraction
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error extracting sequence data for a specific gene", "<b>Error extracting sequence data</b><br /><br />CEntity Type: " + entity.Type + "<br />Entity ID: " + entity.ID + "<br />Exception Type: " + e.GetType() + "<br />Error Message: " + e.Message + "<br />" + e.InnerException + "<br />" + e.Data + "<br />" + e.StackTrace);
                }
            }
        }

        private static void DeleteExistingDomainsAndRelationShips(ProcessEntity entity)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                List<DomainSequence> sequencesToRemove = new List<DomainSequence>();
                if (entity.Type == ProcessEntityType.Cluster)
                {
                    sequencesToRemove = db.DomainSequences.Where(x => x.ClusterID == entity.ID).ToList();
                }
                else if (entity.Type == ProcessEntityType.LargeSequence)
                {
                    sequencesToRemove = db.DomainSequences.Where(x => x.LargeSequenceID == entity.ID).ToList();
                }
                db.DomainSequences.DeleteAllOnSubmit(sequencesToRemove);
                List<string> groupsToRemove = sequencesToRemove.Select(x => x.GroupingID).Distinct().ToList();
                foreach (var groupid in groupsToRemove)
                {
                    var relationshipsToDelete = db.DomainPathwayTypes.Where(x => x.GroupingID == groupid).ToList();
                    db.DomainPathwayTypes.DeleteAllOnSubmit(relationshipsToDelete);
                }
                db.SubmitChanges();
            }
        }

        private static List<int> extractPathwayIDs(string cleanedclustertype, bool isModular, bool isIterative)
        {
            List<int> newpids = new List<int>();
            if (cleanedclustertype.Contains("PKS") && cleanedclustertype.Contains("NRPS"))
            {
                newpids.Add(2); //pid for hybrid
            }

            if (cleanedclustertype.Contains("PKS"))
            {
                if (isModular)
                {
                    newpids.Add(12); // Modular
                }

                if (isIterative)
                {
                    newpids.Add(13); // Iterative
                }

                if (cleanedclustertype.Contains("T1"))
                {
                    newpids.Add(3); // T1PKS
                }
                if (cleanedclustertype.Contains("T2"))
                {
                    newpids.Add(4); // T2PKS
                }
                if (cleanedclustertype.Contains("T3"))
                {
                    newpids.Add(6); // T3PKS
                }

                if (cleanedclustertype.Contains("TRANSAT"))
                {
                    newpids.Add(3);
                    newpids.Add(14); // trans=AT
                }
            }

            if (cleanedclustertype.Contains("NRPS"))
            {
                newpids.Add(1); //NRPS
            }
            return newpids;
        }

        private static void AssignPathwayTypeToDomainRecords(string groupingid, string cleanedclustertype, bool isModular, bool isIterative)
        {
            try
            {
                List<int> currentpids = DomainPathwayTypes.GetPathwayTypesByGroupingID(groupingid);

                List<int> newpids = extractPathwayIDs(cleanedclustertype, isModular, isIterative);

                int unknownpid = PathwayTypes.getPathwayID("Unknown");
                if (newpids.Count == 0)
                {
                    newpids.Add(unknownpid);
                }
                currentpids.Sort();
                newpids.Sort();

                bool areequal = currentpids.SequenceEqual(newpids);

                if (!areequal)
                {
                    List<int> idsToDelete = currentpids.Except(newpids).ToList();
                    List<int> idsToAdd = newpids; // method will only add if not already there
                    DomainPathwayTypes.DeleteDomainToPathwayTypeRelationship(groupingid, idsToDelete);
                    DomainPathwayTypes.AddDomainToPathwayTypeRelationship(groupingid, idsToAdd);
                }

            }

            catch (Exception e)
            {
                log.Debug("Assign Cluster Type " + Environment.NewLine + e.ToString());
            }
        }

        private static void AssignPathwayTypeToCF(int clusterid, string cleanedclustertype, bool isModular, bool isIterative)
        {
            try
            {
                int cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;

                List<int> currentpids = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ClusterPathwayTypes.Select(x => x.PathwayTypeID).ToList();
                List<int> antismashaddedpids = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ClusterPathwayTypes.Where(y => y.AddedFromAntiSmash == true).Select(x => x.PathwayTypeID).ToList();
                List<int> newpids = extractPathwayIDs(cleanedclustertype, isModular, isIterative);

                int unknownpid = PathwayTypes.getPathwayID("Unknown");

                currentpids.Sort();
                antismashaddedpids.Sort();
                newpids.Sort();

                bool areequal = currentpids.SequenceEqual(newpids);

                if (!areequal)
                {
                    List<int> idsToDelete = currentpids.Except(antismashaddedpids).Except(newpids).ToList();
                    List<int> idsToAdd = newpids; // method will only add if not already there
                    PathwayTypes.DeletePathwayToCompoundFamilyRelationship(cfid, idsToDelete);
                    PathwayTypes.AddPathwayTypeToCompoundFamilyRelationship(cfid, idsToAdd, true);

                    CompoundFamily cfamily = CompoundFamilies.getCompoundFamilyByID(cfid);
                    using (DataClassesDataContext db = new DataClassesDataContext())
                    {
                        List<string> previouspathways = db.Enum_PathwayTypes.ToList().Where(y => currentpids.Contains(y.ID)).Select(x => x.Type).OrderBy(x => x).ToList();
                        List<string> currentpathways = db.ClusterPathwayTypes.Where(x => x.CompoundFamilyID == cfid).Select(x => x.Enum_PathwayType.Type).ToList();
                        Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Detected pathway types do not match assigned pathway types", "The pathway types for cluster #" + clusterid + " were " + StringTools.SerializeToSemiColonSeparatedList(previouspathways) + ". However, based on antiSMASH analysis, the pathway types are now: " + StringTools.SerializeToSemiColonSeparatedList(currentpathways) + ". The pathway types have been changed to match the antiSMASH results. If, however, this change should not have been made, go to the Compound Family record for this cluster (in this case, <a href=\"" + ConfigurationManager.AppSettings["WebsiteRootUrl"] + "Contribute/EditCompoundFamily.aspx?cfid=" + cfid + "\">" + cfamily.FamilyName + "</a>) and correct the pathway types.");
                    }
                }
                else
                {
                    // do nothing
                }
            }

            catch (Exception e)
            {
                log.Debug("Assign Cluster Type " + Environment.NewLine + e.ToString());
            }
        }
    }
}