﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaturalProductsWebsite.Code
{
    public enum ProcessEntityType
    {
        Cluster, LargeSequence
    }

    public class ProcessEntity
    {
        public ProcessEntityType Type { get; set; }
        public int ID { get; set; }
        //public int AntiSmashStatus { get; set; }
        //public DateTime? AntiSmashModified { get; set; }
        //public int NCBIStatus { get; set; }
        //public DateTime? NCBIModified { get; set; }
        //public int SequenceExtractionStatus { get; set; }
        //public DateTime? SequenceExtractionModified { get; set; }
        //public string GenbankAccession { get; set; }
        //public int GenbankVersion { get; set; }
        public bool? ClusterIsEntireSequence { get; set; }
        public int? BPStart { get; set; }
        public int? BPEnd { get; set; }
        //public string OrgName { get; set; }
        //public int PhylumID { get; set; }
        //public string AntiSmashLink { get; set; }
        //public string AntiSmashID { get; set; }
        //public int LastModifiedUserID { get; set; }
        //public DateTime LastModified { get; set; }
        //public int ContributorID { get; set; }
        //public bool Delayed { get; set; }
        //public bool EmailContributor { get; set; }
        public ClusterBase ClusterBase { get; set; }

        public ProcessEntity()
        {
            ClusterBase = new ClusterBase();
        }

        public ProcessEntity(Cluster cluster)
        {
            ClusterBase = new ClusterBase();
            this.Type = ProcessEntityType.Cluster;
            this.ID = cluster.ID;
            this.ClusterBase.AntiSmashStatus = cluster.ClusterBase.AntiSmashStatus;
            this.ClusterBase.AntiSmashModified = cluster.ClusterBase.AntiSmashModified;
            this.ClusterBase.NCBIStatus = cluster.ClusterBase.NCBIStatus;
            this.ClusterBase.NCBIModified = cluster.ClusterBase.NCBIModified;
            this.ClusterBase.SequenceExtractionStatus = cluster.ClusterBase.SequenceExtractionStatus;
            this.ClusterBase.SequenceExtractionModified = cluster.ClusterBase.SequenceExtractionModified;
            this.ClusterBase.GenbankAccession = cluster.ClusterBase.GenbankAccession;
            this.ClusterBase.GenbankVersion = cluster.ClusterBase.GenbankVersion;
            this.ClusterIsEntireSequence = cluster.ClusterIsEntireSequence;
            this.BPStart = cluster.BPStart;
            this.BPEnd = cluster.BPEnd;
            this.ClusterBase.OrgName = cluster.ClusterBase.OrgName;
            this.ClusterBase.PhylumID = cluster.ClusterBase.PhylumID;
            this.ClusterBase.AntiSmashLink = cluster.ClusterBase.AntiSmashLink;
            this.ClusterBase.AntiSmashID = cluster.ClusterBase.AntiSmashID;
            this.ClusterBase.LastModifiedUserID = cluster.ClusterBase.LastModifiedUserID;
            this.ClusterBase.LastModified = cluster.ClusterBase.LastModified;
            this.ClusterBase.ContributorID = cluster.ClusterBase.ContributorID;
            this.ClusterBase.Delayed = cluster.ClusterBase.Delayed;
            this.ClusterBase.EmailContributor = cluster.ClusterBase.EmailContributor;
        }

        public ProcessEntity(LargeSequence largesequence)
        {
            ClusterBase = new ClusterBase();
            this.Type = ProcessEntityType.LargeSequence;
            this.ID = largesequence.ID;
            this.ClusterBase.AntiSmashStatus = largesequence.ClusterBase.AntiSmashStatus;
            this.ClusterBase.AntiSmashModified = largesequence.ClusterBase.AntiSmashModified;
            this.ClusterBase.NCBIStatus = largesequence.ClusterBase.NCBIStatus;
            this.ClusterBase.NCBIModified = largesequence.ClusterBase.NCBIModified;
            this.ClusterBase.SequenceExtractionStatus = largesequence.ClusterBase.SequenceExtractionStatus;
            this.ClusterBase.SequenceExtractionModified = largesequence.ClusterBase.SequenceExtractionModified;
            this.ClusterBase.GenbankAccession = largesequence.ClusterBase.GenbankAccession;
            this.ClusterBase.GenbankVersion = largesequence.ClusterBase.GenbankVersion;
            this.ClusterIsEntireSequence = true;
            this.BPStart = null;
            this.BPEnd = null;
            this.ClusterBase.OrgName = largesequence.ClusterBase.OrgName;
            this.ClusterBase.PhylumID = largesequence.ClusterBase.PhylumID;
            this.ClusterBase.AntiSmashLink = largesequence.ClusterBase.AntiSmashLink;
            this.ClusterBase.AntiSmashID = largesequence.ClusterBase.AntiSmashID;
            this.ClusterBase.LastModifiedUserID = largesequence.ClusterBase.LastModifiedUserID;
            this.ClusterBase.LastModified = largesequence.ClusterBase.LastModified;
            this.ClusterBase.ContributorID = largesequence.ClusterBase.ContributorID;
            this.ClusterBase.Delayed = largesequence.ClusterBase.Delayed;
            this.ClusterBase.EmailContributor = largesequence.ClusterBase.EmailContributor;
        }

        public static List<ProcessEntity> Cast(List<Cluster> clusters)
        {
            List<ProcessEntity> processentities = new List<ProcessEntity>();
            foreach (Cluster cluster in clusters)
            {
                processentities.Add(new ProcessEntity(cluster));
            }

            return processentities;
        }

        public static List<ProcessEntity> Cast(List<LargeSequence> sequences)
        {
            List<ProcessEntity> processentities = new List<ProcessEntity>();
            foreach (LargeSequence sequence in sequences)
            {
                processentities.Add(new ProcessEntity(sequence));
            }

            return processentities;
        }

        public static void UpdateProcessEntity(ProcessEntity entity, UpdateType type, int value)
        {
            if (entity.Type == ProcessEntityType.Cluster)
            {
                Clusters.updateClusterLastModified(entity.ID, type, value);
            }
            else if (entity.Type == ProcessEntityType.LargeSequence)
            {
                LargeSequences.updateLargeSequenceLastModified(entity.ID, type, value);
            }
        }

        public static List<ProcessEntity> GetAllClustersAndLargeSequencesAsProcessEntities()
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var clusters = (from c in db.Clusters
                            select c).ToList<Cluster>().ConvertAll(x => new ProcessEntity(x));

            var sequences = (from s in db.LargeSequences
                             select s).ToList<LargeSequence>().ConvertAll(x => new ProcessEntity(x));
            db.Dispose();

            clusters.AddRange(sequences);
            return clusters;
        }
    }
}