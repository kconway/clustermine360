﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using log4net;
using log4net.Config;

namespace Utilities
{

    public static class SortTools
    {
        public static int GetNextIDByAlphabeticalOrder(List<KeyValuePair<int, string>> data, int currentid)
        {
            data.OrderBy(x => x.Value);

            int currentindex = data.FindIndex(x => x.Key == currentid);
            int nextindex = currentindex + 1;
            if (nextindex == data.Count)
            {
                nextindex = 0;
            }
            return data[nextindex].Key;
        }

        public static int GetPreviousIDByAlphabeticalOrder(List<KeyValuePair<int, string>> data, int currentid)
        {
            data.OrderBy(x => x.Value);

            int currentindex = data.FindIndex(x => x.Key == currentid);
            int nextindex = currentindex - 1;
            if (nextindex < 0)
            {
                nextindex = data.Count - 1;
            }

            return data[nextindex].Key;
        }
    }

    public static class StringTools
    {
        public static string SerializeToSemiColonSeparatedList(List<string> values)
        {
            string result = "";
            char delimiter = ';';
            StringBuilder sb = new StringBuilder();

            foreach (var val in values)
            {
                sb.Append(val + delimiter);
            }
            result = sb.ToString().TrimEnd(delimiter);
            return result;
        }

        public static List<string> DeserializeFromSemiColonSeparatedList(string value)
        {
            List<string> values = new List<string>();
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    string[] stringvalues = value.Split(';');
                    foreach (string stringval in stringvalues)
                    {
                        values.Add(stringval);
                    }
                }
                catch
                {
                }
            }
            return values;
        }

        // C# to convert a string to a byte array.
        public static byte[] StringToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        // C# to convert a string to a byte array.
        public static string ByteArraytoString(byte[] dBytes)
        {
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            string str = enc.GetString(dBytes);
            return str;
        }

        // The difference function will match the two soundex strings and return 0 to 4.
        // 0 means Not Matched
        // 4 means Strongly Matched

        public static int Difference(string data1, string data2)
        {
            int result = 0;
            if (data1.Equals(string.Empty) || data2.Equals(string.Empty))
                return 0;

            string soundex1 = Soundex(data1);
            string soundex2 = Soundex(data2);

            if (soundex1.Equals(soundex2))
                result = 4;
            else
            {
                if (soundex1[0] == soundex2[0])
                    result = 1;

                string sub1 = soundex1.Substring(1, 3); //characters 2, 3, 4

                if (soundex2.IndexOf(sub1) > -1)
                {
                    result += 3;
                    return result;
                }

                string sub2 = soundex1.Substring(2, 2); //characters 3, 4

                if (soundex2.IndexOf(sub2) > -1)
                {
                    result += 2;
                    return result;
                }

                string sub3 = soundex1.Substring(1, 2); //characters 2, 3

                if (soundex2.IndexOf(sub3) > -1)
                {
                    result += 2;
                    return result;
                }

                char sub4 = soundex1[1];

                if (soundex2.IndexOf(sub4) > -1)
                    result++;

                char sub5 = soundex1[2];

                if (soundex2.IndexOf(sub5) > -1)
                    result++;

                char sub6 = soundex1[3];

                if (soundex2.IndexOf(sub6) > -1)
                    result++;
            }

            return result;
        }

        public static string Soundex(string data)
        {
            StringBuilder result = new StringBuilder();

            if (data != null && data.Length > 0)
            {
                string previousCode, currentCode;
                result.Append(Char.ToUpper(data[0]));
                previousCode = string.Empty;
                for (int i = 1; i < data.Length; i++)
                {
                    currentCode = EncodeChar(data[i]);
                    if (currentCode != previousCode)
                        result.Append(currentCode);

                    if (result.Length == 4) break;

                    if (!currentCode.Equals(string.Empty))
                        previousCode = currentCode;
                }
            }

            if (result.Length < 4)
                result.Append(new String('0', 4 - result.Length));

            return result.ToString();
        }

        private static string EncodeChar(char c)
        {
            switch (Char.ToLower(c))
            {
                case 'b':
                case 'f':
                case 'p':
                case 'v':
                    return "1";

                case 'c':
                case 'g':
                case 'j':
                case 'k':
                case 'q':
                case 's':
                case 'x':
                case 'z':
                    return "2";

                case 'd':
                case 't':
                    return "3";

                case 'l':
                    return "4";

                case 'm':
                case 'n':
                    return "5";

                case 'r':
                    return "6";

                default:

                    return string.Empty;
            }
        }

        public static string ToUpperCaseFirstLetterOnly(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return UppercaseFirst(s.ToLower());
        }

        private static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

    }

    public static class ObjectTools
    {
        public class ObjectDumper
        {
            public static void Write(object element)
            {
                Write(element, 0);
            }

            public static void Write(object element, int depth)
            {
                Write(element, depth, Console.Out);
            }

            public static void Write(object element, int depth, TextWriter log)
            {
                ObjectDumper dumper = new ObjectDumper(depth);
                dumper.writer = log;
                dumper.WriteObject(null, element);
            }

            TextWriter writer;
            int pos;
            int level;
            int depth;

            private ObjectDumper(int depth)
            {
                this.depth = depth;
            }

            private void Write(string s)
            {
                if (s != null)
                {
                    writer.Write(s);
                    pos += s.Length;
                }
            }

            private void WriteIndent()
            {
                for (int i = 0; i < level; i++) writer.Write(" ");
            }

            private void WriteLine()
            {
                writer.WriteLine();
                pos = 0;
            }

            private void WriteTab()
            {
                Write(" ");
                while (pos % 8 != 0) Write(" ");
            }

            private void WriteObject(string prefix, object element)
            {
                if (element == null || element is ValueType || element is string)
                {
                    WriteIndent();
                    Write(prefix);
                    WriteValue(element);
                    WriteLine();
                }
                else
                {
                    IEnumerable enumerableElement = element as IEnumerable;
                    if (enumerableElement != null)
                    {
                        foreach (object item in enumerableElement)
                        {
                            if (item is IEnumerable && !(item is string))
                            {
                                WriteIndent();
                                Write(prefix);
                                Write("...");
                                WriteLine();
                                if (level < depth)
                                {
                                    level++;
                                    WriteObject(prefix, item);
                                    level--;
                                }
                            }
                            else
                            {
                                WriteObject(prefix, item);
                                WriteLine();
                            }
                        }
                    }
                    else
                    {
                        MemberInfo[] members = element.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance /*| BindingFlags.NonPublic */);
                        writer.WriteLine(members.Length + " Members.");
                        WriteIndent();
                        Write(prefix);
                        bool propWritten = false;
                        foreach (MemberInfo m in members)
                        {
                            FieldInfo f = m as FieldInfo;
                            PropertyInfo p = m as PropertyInfo;
                            if (f != null || p != null)
                            {
                                if (propWritten)
                                {
                                    WriteTab();
                                }
                                else
                                {
                                    propWritten = true;
                                }
                                Write(m.Name);
                                Write("=");
                                Type t = f != null ? f.FieldType : p.PropertyType;
                                object o = null;
                                if (t.IsValueType || t == typeof(string))
                                {
                                    try
                                    {
                                        o = f != null ? f.GetValue(element) : p.GetValue(element, null);
                                        WriteValue(o);
                                    }
                                    catch (Exception)
                                    {
                                        // Had to do this because exceptions are thrown on NumberFormatInfo and other types
                                        // at high depth levels, even though there is a legitimate value obtained.
                                        //Console.WriteLine(ex.Message);
                                        WriteValue(o);
                                    }
                                    writer.WriteLine();
                                }
                                else
                                {
                                    if (typeof(IEnumerable).IsAssignableFrom(t))
                                    {
                                        Write("...");
                                        writer.WriteLine();
                                    }
                                    else
                                    {
                                        Write("{ }");
                                        writer.WriteLine();
                                    }
                                }
                            }
                        }
                        if (propWritten) WriteLine();
                        if (level < depth)
                        {
                            foreach (MemberInfo m in members)
                            {
                                FieldInfo f = m as FieldInfo;
                                PropertyInfo p = m as PropertyInfo;
                                if (f != null || p != null)
                                {
                                    Type t = f != null ? f.FieldType : p.PropertyType;
                                    object value = null;
                                    if (!(t.IsValueType || t == typeof(string)))
                                    {
                                        try
                                        {
                                            value = f != null ? f.GetValue(element) : p.GetValue(element, null);
                                        }
                                        catch
                                        {
                                        }

                                        if (value != null)
                                        {
                                            level++;
                                            WriteObject(m.Name + ": ", value);
                                            level--;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            private void WriteValue(object o)
            {
                if (o == null)
                {
                    Write("null");
                }
                else if (o is DateTime)
                {
                    Write(((DateTime)o).ToShortDateString());
                }
                else if (o is ValueType || o is string)
                {
                    Write(o.ToString());
                }
                else if (o is IEnumerable)
                {
                    Write("...");
                    writer.WriteLine();
                }
                else
                {
                    Write("{ }");
                    writer.WriteLine();
                }
            }
        }
    }

    public static class StreamTools
    {
        public static long CopyTo(this Stream source, Stream destination)
        {
            byte[] buffer = new byte[2048];
            int bytesRead;
            long totalBytes = 0;
            while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                destination.Write(buffer, 0, bytesRead);
                totalBytes += bytesRead;
            }
            return totalBytes;
        }
    }

    public static class WebTools
    {
        public static string callWebPage(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            string responsestring = "";
            StreamReader readStream = new StreamReader(receiveStream);
            if (readStream != null)
            {
                responsestring = readStream.ReadToEnd();
            }
            readStream.Close();
            receiveStream.Close();
            response.Close();

            return responsestring;
        }

        public static string getURLRootPath(string f, int folderDepth)
        {
            string returnValue = f;
            for (int i = 0; i < folderDepth + 1; i++)
            {
                returnValue = getURLRootPath(returnValue);
            }

            return returnValue + "/";
        }

        private static string getURLRootPath(string f)
        {
            try
            {
                return f.Substring(0, f.LastIndexOf('/'));
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool urlIsValid(string url)
        {
            if (!String.IsNullOrEmpty(url))
            {
                try
                {
                    HttpWebRequest reqFP = (HttpWebRequest)HttpWebRequest.Create(url);
                    HttpWebResponse rspFP = (HttpWebResponse)reqFP.GetResponse();
                    if (HttpStatusCode.OK == rspFP.StatusCode)
                    {
                        // HTTP = 200 - Internet connection available, server online
                        rspFP.Close();
                        return true;
                    }
                    else
                    {
                        // Other status - Server or connection not available
                        rspFP.Close();
                        return false;
                    }
                }
                catch (WebException)
                {
                    // Exception - connection not available
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    public static class DebugTools
    {
        private class DebugTextWriter : System.IO.TextWriter
        {
            public override void Write(char[] buffer, int index, int count)
            {
                System.Diagnostics.Debug.Write(new String(buffer, index, count));
            }

            public override void Write(string value)
            {
                System.Diagnostics.Debug.Write(value);
            }

            public override Encoding Encoding
            {
                get { return System.Text.Encoding.Default; }
            }
        }
    }

    public static class ImageTools
    {
        /// <summary>
        /// Provides various image untilities, such as high quality resizing and the ability to save a JPEG.
        /// </summary>
        public static class ImageUtilities
        {
            /// <summary>
            /// A quick lookup for getting image encoders
            /// </summary>
            private static Dictionary<string, ImageCodecInfo> encoders = null;

            /// <summary>
            /// A quick lookup for getting image encoders
            /// </summary>
            public static Dictionary<string, ImageCodecInfo> Encoders
            {
                //get accessor that creates the dictionary on demand
                get
                {
                    //if the quick lookup isn't initialised, initialise it
                    if (encoders == null)
                    {
                        encoders = new Dictionary<string, ImageCodecInfo>();
                    }

                    //if there are no codecs, try loading them
                    if (encoders.Count == 0)
                    {
                        //get all the codecs
                        foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                        {
                            //add each codec to the quick lookup
                            encoders.Add(codec.MimeType.ToLower(), codec);
                        }
                    }

                    //return the lookup
                    return encoders;
                }
            }

            /// <summary>
            /// Resize the image to the specified width and height.
            /// </summary>
            /// <param name="image">The image to resize.</param>
            /// <param name="width">The width to resize to.</param>
            /// <param name="height">The height to resize to.</param>
            /// <returns>The resized image.</returns>
            public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
            {
                //a holder for the result
                Bitmap result = new Bitmap(width, height);

                //use a graphics object to draw the resized image into the bitmap
                using (Graphics graphics = Graphics.FromImage(result))
                {
                    //set the resize quality modes to high quality
                    graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    //draw the image into the target bitmap
                    graphics.DrawImage(image, 0, 0, result.Width, result.Height);
                }

                //return the resulting bitmap
                return result;
            }

            /// <summary>
            /// Returns the image codec with the given mime type
            /// </summary>
            public static ImageCodecInfo GetEncoderInfo(string mimeType)
            {
                //do a case insensitive search for the mime type
                string lookupKey = mimeType.ToLower();

                //the codec to return, default to null
                ImageCodecInfo foundCodec = null;

                //if we have the encoder, get it to return
                if (Encoders.ContainsKey(lookupKey))
                {
                    //pull the codec from the lookup
                    foundCodec = Encoders[lookupKey];
                }

                return foundCodec;
            }
        }
    }

    public static class FileTools
    {
        public static string getantiSMASHSequencesFolder()
        {
            return getantiSMASHFolder() + "Sequences" + Path.DirectorySeparatorChar;
        }

        public static string getantiSMASHClustersFolder()
        {
            return getantiSMASHFolder() + "Clusters" + Path.DirectorySeparatorChar;
        }
        private static string getantiSMASHFolder()
        {
            return System.Configuration.ConfigurationManager.AppSettings["antiSMASHFolderLocation"];
        }
    }

    public static class Email
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Email));

      public static void SendMultiple(List<String> to, String subject, String message)
        {
            MailMessage email = new MailMessage();
            email.IsBodyHtml = true;
            foreach (string address in to)
            {
                email.To.Add(new MailAddress(address));
            }
            email.Subject = subject;
            email.Body = message;
            Send(email);
        }
        
        public static void Send(MailMessage message)
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            SmtpClient client = new SmtpClient();
            client.EnableSsl = true;
            log.Info(message.Subject + Environment.NewLine + message.Body);
            if (message.Body.Contains("System.Threading.ThreadAbortException") || message.Body.Contains("timed out") || message.Body.Contains("The request body is incomplete") || message.Body.Contains("HttpRequestValidationException"))
            {
            }
            else
            {
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    log.Debug(ex.ToString());
                }
            }
        }

        public static void Send(String to, String subject, String message)
        {
            MailMessage email = new MailMessage();
            email.IsBodyHtml = true;
            email.To.Add(new MailAddress(to));
            email.Subject = subject;
            email.Body = message;
            Send(email);
        }
    }
}