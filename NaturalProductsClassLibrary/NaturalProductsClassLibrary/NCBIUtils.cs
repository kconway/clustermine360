﻿using System;
using System.Net;
using System.Web;
using System.Xml.XPath;
using log4net;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace NaturalProductsWebsite.Code
{

    enum NCBIRequestType
    {
        eFetch,
        eSummary
    }
    public class NCBIUtils
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NCBIUtils));


        public static string GetNCBIeFetchBaseURL()
        {
            return @"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";
        }

        public static string GetNCBIDisplayURL()
        {
            return "http://www.ncbi.nlm.nih.gov/nuccore/";
        }

        public static string GetNCBIeSummaryBaseURL()
        {
            return "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?";
        }

        internal static XPathNavigator GetNCBIXPathNavigator(NCBIRequestType type, string idparam, string dbparam, string rettypeparam, string retmodeparam)
        {
            XPathNavigator nav = null;
            try
            {
                HttpWebResponse response = GetNCBIHttpWebResponse(type, idparam, dbparam, rettypeparam, retmodeparam);
                XPathDocument docnav = new XPathDocument(response.GetResponseStream());
                nav = docnav.CreateNavigator();
                response.Close();
            }
            catch (Exception e)
            {
                log.Debug("Error converting NCBI response to XPathNavigator:" + Environment.NewLine + "GenBank Accession " + idparam + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);
            }
            return nav;
        }

        internal static XDocument GetNCBIPubChemIDXDocument(string name, string type)
        {
            XDocument nav = null;
            string typestring1 = "";
            string typestring2 = "";
            string url = "";
            try
            {
                //if (type == "S")
                //{
                //    typestring1 = "substance";
                //    typestring2 = "/sids";
                //}
                //else
                if (type == "C")
                {
                    typestring1 = "compound";
                    typestring2 = "/cids";
                }
                url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/" + typestring1 + "/name/" + HttpUtility.UrlEncode(name) + typestring2 + "/XML?name_type=word";
                HttpWebResponse response = null;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Method = "GET";
                response = (HttpWebResponse)request.GetResponse();

                nav = LoadFromStream(response.GetResponseStream());
                response.Close();
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("404"))
                {
                    log.Debug("Error getting or converting NCBI response to XDocument " + Environment.NewLine + "PubChem Name/ID " + name + Environment.NewLine + "URL: " + url + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);
                }
            }
            return nav;
        }

        internal static XDocument GetNCBIPubChemFullRecordXDocument(string id)
        {
            XDocument docnav = null;
            string url = "";       

            try
            {
                url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/" + id + "/record/XML";
                HttpWebResponse response = null;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Method = "GET";
                response = (HttpWebResponse)request.GetResponse();
                docnav = LoadFromStream(response.GetResponseStream());

                response.Close();
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("404"))
                {
                    log.Debug("Error getting or converting NCBI response to XDocument " + Environment.NewLine + "PubChemID " + id + Environment.NewLine + "URL: " + url + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);
                }
            }

            return docnav;
        }


        internal static XDocument GetNCBIPubChemPharmActionsXDocument(string type, string id)
        {
            XDocument docnav = null;
            string url = "";
            string typestring = "";
           
            if (type == "S")
            {
                typestring = "substance/sid/";
            }
            else if (type == "C")
            {
                typestring = "compound/cid/";
            }

            try
            {
                url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/" + typestring + id + "/classification/XML";
                HttpWebResponse response = null;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Method = "GET";
                response = (HttpWebResponse)request.GetResponse();
                docnav = LoadFromStream(response.GetResponseStream());
                       
                response.Close();
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("404"))
                {
                    log.Debug("Error getting or converting NCBI response to XDocument " + Environment.NewLine + "PubChemID " + id + Environment.NewLine + "URL: " + url + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);
                }
            }

            return docnav;
        }

        static XDocument LoadFromStream(Stream stream)
        {
            using (XmlReader reader = XmlReader.Create(stream))
            {
                return XDocument.Load(reader);
            }
        } 

        internal static HttpWebResponse GetNCBIHttpWebResponse(NCBIRequestType type, string idparam, string dbparam, string rettypeparam, string retmodeparam)
        {

            HttpWebResponse response = null;

            string baseurl = "";
            if (type == NCBIRequestType.eFetch)
            {
                baseurl = GetNCBIeFetchBaseURL();
            }
            else if (type == NCBIRequestType.eSummary)
            {
                baseurl = GetNCBIeSummaryBaseURL();
            }
            else
            {
                return response;
            }

            if (String.IsNullOrEmpty(dbparam))
            {
                dbparam = "nuccore";
            }

            string url = baseurl + "db=" + dbparam + "&id=" + idparam + "&retmode=" + retmodeparam;
            if (!String.IsNullOrEmpty(rettypeparam))
            {
                url = url + "&rettype=" + rettypeparam;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.Method = "GET";
            try
            {
                response = (HttpWebResponse)request.GetResponse();

            }
            catch (Exception e)
            {
                log.Debug("Error getting NCBI HttpWebResponse:" + Environment.NewLine + "GenBank Accession " + idparam + Environment.NewLine + "URL: " + url + Environment.NewLine + "Error Message: " + e.Message + Environment.NewLine + e.InnerException + Environment.NewLine + e.Data + Environment.NewLine + e.StackTrace);
            }

            return response;

        }




    }
}
