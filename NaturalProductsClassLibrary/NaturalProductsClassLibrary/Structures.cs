﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.XPath;
using com.ggasoftware.indigo;
using HtmlAgilityPack;
using log4net;
using log4net.Config;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    /// <summary>
    /// Summary description for Structures
    /// </summary>
    public class Structure
    {
        private const string token = "236364ae-06d7-4185-bf91-40ff16b958cc";
        private static readonly ILog log = LogManager.GetLogger(typeof(Structure));

        public static Dictionary<int, string> searchStructures(string smiles)
        {
            Dictionary<int, string> cfids = new Dictionary<int, string>();
            List<PubChemIDs> structuresToCheck = PubChemIDDataHandler.getPubChemIDsForStructureSearch();
            try
            {
                using (Indigo indigoInstance = new Indigo())
                {
                    using (IndigoObject query = indigoInstance.loadQueryMolecule(smiles))
                    {
                        query.aromatize();

                        // get array of smiles strings to compare against from database

                        foreach (PubChemIDs compound in structuresToCheck)
                        {
                            IndigoObject target = indigoInstance.loadMolecule(compound.IsomericSMILES);
                            target.aromatize();

                            IndigoObject matcher = indigoInstance.substructureMatcher(target);
                            int nummatch = matcher.countMatches(query);

                            if (nummatch > 0)
                            {
                                cfids.Add(compound.cfid, compound.CompoundFamily.FamilyName); // add cfid
                            }
                        }
                    }
                }
            }
            catch (IndigoException ex)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error searching by SMILES string", "Error searching by SMILES string: <br />" + ex.Message );

            }

            return cfids;
        }

        // ChemSpider token 236364ae-06d7-4185-bf91-40ff16b958cc
        public byte[] getStructureImage(int csid)
        {
            byte[] image = null;

            string url = "http://www.chemspider.com/ImagesHandler.ashx?id=" + csid + "&w=500&h=500";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = true;
            request.Accept = "image/png, image/svg+xml, image/*;q=0.8, */*;q=0.5";
            request.Referer = "http://www.chemspider.com/ImageView.aspx?id=" + HttpUtility.UrlEncode(csid.ToString());
            request.Method = "GET";
            // request.Headers.Add("Host", "www.chemspider.com"); //request.Host = "www.chemspider.com";
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            request.Timeout = 2500;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                MemoryStream memoryStream = new MemoryStream();
                responseStream.CopyTo(memoryStream);
                image = memoryStream.ToArray();
            }
            catch (WebException ex)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
            }

            return image;
        }

        public List<string> retrieveSynonyms(int csid)
        {
            List<string> returnlist = new List<string>();
            string url = "http://www.chemspider.com/Chemical-Structure." + csid + ".html";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = true;
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.Referer = "http://www.chemspider.com/";
            request.Method = "GET";
            //request.Headers.Add("Host", "www.chemspider.com");
            //request.Host = "www.chemspider.com";
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            request.Timeout = 2500;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                string Charset = response.CharacterSet;
                Encoding encode = Encoding.GetEncoding(Charset);
                StreamReader readStream = new StreamReader(responseStream, encode);

                string input = readStream.ReadToEnd();

                readStream.Close();
                responseStream.Close();
                response.Close();

                HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                XPathNavigator nav = doc.CreateNavigator();
                doc.LoadHtml(input);
                List<HtmlNode> nodes = doc.DocumentNode.Descendants("p").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value == ("syn")).ToList();

                foreach (HtmlNode node in nodes)
                {
                    string value = node.ChildNodes[1].InnerText;
                    if ((value.Length < 150) && ContainsAlphabeticCharacters(value))
                    {
                        returnlist.Add(value);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
            }
            return returnlist;
        }

        public static bool ContainsAlphabeticCharacters(string value)
        {
            string pattern = @"[a-zA-Z]";
            return Regex.IsMatch(value, pattern);
        }

        public static int? nextCSID(int cfid, int currentcsid)
        {
            int? nextcsid;

            NaturalProductsWebsite.Code.ChemSpider.Search client = new NaturalProductsWebsite.Code.ChemSpider.Search();
            string cfName = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;
            int[] csids = client.SimpleSearch(cfName, token);

            int position = csids.FindIndex(x => x == currentcsid);
            int newposition = 0;
            if (position >= (csids.Length - 1))
            {
                nextcsid = null;
            }
            else
            {
                newposition = position + 1;
                nextcsid = csids[newposition];
            }

            return nextcsid;
        }

        public static int? previousCSID(int cfid, int currentcsid)
        {
            int? previouscsid;

            ChemSpider.Search client = new ChemSpider.Search();
            string cfName = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;
            int[] csids = client.SimpleSearch(cfName, token);

            int position = csids.FindIndex(x => x == currentcsid);
            int newposition = position - 1;
            if (newposition < 0)
            {
                previouscsid = null;
            }
            else
            {
                previouscsid = csids[newposition];
            }

            return previouscsid;
        }

        public int[] getCSIDs(int cfid)
        {
            string cfName = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;
            int[] csids = getCSIDs(cfName);
            return csids;
        }

        private int[] getCSIDs(string compoundfamilyname)
        {
            int[] csids = null;

            try
            {
                ChemSpider.Search client = new ChemSpider.Search();
                client.Timeout = 2500;
                csids = client.SimpleSearch(compoundfamilyname, token);
            }
            catch (Exception ex)
            {
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
            }
            return csids;
        }

        public static byte[] structureFromSMILES(string smiles)
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
            byte[] structureimage = null;

            using (Indigo img = new Indigo())
            {
                using (IndigoObject mol1 = img.loadMolecule(smiles))
                {
                    mol1.layout();

                    IndigoRenderer renderer = new IndigoRenderer(img);
                    img.setOption("render-output-format", "png");
                    img.setOption("render-image-size", 500, 500);
                    img.setOption("render-margins", 10, 10);
                    img.setOption("render-stereo-style", "none");
                    img.setOption("render-label-mode", "hetero");
                    img.setOption("render-coloring", true);
                    img.setOption("render-background-color", Color.White);

                    structureimage = renderer.renderToBuffer(mol1);
                }
            }

            return structureimage;
        }

        public static byte[] GenerateMissingImageNotice(string sImageText)
        {
            Bitmap objBmpImage = new Bitmap(500, 500);

            // Create the Font object for the image text drawing.
            Font objFont = new Font("Arial", 45, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);

            // Create a graphics object to measure the text's width and height.
            Graphics objGraphics = Graphics.FromImage(objBmpImage);

            // Set Background color
            objGraphics.Clear(Color.White);
            //objGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            //objGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            objGraphics.DrawString(sImageText, objFont, new SolidBrush(Color.FromArgb(102, 102, 102)), new RectangleF(0, 0, 500, 500), format);
            objGraphics.Flush();

            using (MemoryStream stream = new MemoryStream())
            {
                objBmpImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                return stream.ToArray();
            }
        }
    }
}