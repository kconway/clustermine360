﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

namespace NaturalProductsWebsite
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResultsPanel.Visible = false;
            }
            else
            {
                ResultsPanel.Visible = true;
            }
        }

        protected void NewSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Search.aspx");
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            int totalhits = 0;
            string searchString = SearchTextBox.Text;
            List<SearchResults> clusterResults = new List<SearchResults>();
            List<SearchResults> cfResults = new List<SearchResults>();
            if (RadioButton1.Checked)
            {
                // search clusters
                DataClassesDataContext db = new DataClassesDataContext();

                var genbankaccessions = (from c in db.Clusters where c.ClusterBase.GenbankAccession.Contains(searchString) select c)
                    .Select(x => new SearchResults { ClusterID = x.ID, ColumnName = "Genbank Accession #", ColumnContents = x.ClusterBase.GenbankAccession })
                    .ToList();
                clusterResults.AddRange(genbankaccessions);

                var orgnames = (from c in db.Clusters where c.ClusterBase.OrgName.Contains(searchString) select c)
                    .Select(x => new SearchResults { ClusterID = x.ID, ColumnName = "Organism Name", ColumnContents = x.ClusterBase.OrgName })
                    .ToList();
                clusterResults.AddRange(orgnames);

                var lineages = (from c in db.Clusters where c.ClusterBase.Lineage.Contains(searchString) select c)
                    .Select(x => new SearchResults { ClusterID = x.ID, ColumnName = "Lineage", ColumnContents = x.ClusterBase.Lineage })
                    .ToList();
                clusterResults.AddRange(lineages);

                var descriptions = (from c in db.Clusters where c.ClusterBase.Description.Contains(searchString) select c)
                    .Select(x => new SearchResults { ClusterID = x.ID, ColumnName = "Description", ColumnContents = x.ClusterBase.Description })
                    .ToList();
                clusterResults.AddRange(descriptions);

                var compoundfamilynames = (from c in db.Clusters where c.CompoundFamily.FamilyName.Contains(searchString) select c)
                   .Select(x => new SearchResults { ClusterID = x.ID, ColumnName = "Compound Family Name", ColumnContents = x.CompoundFamily.FamilyName })
                   .ToList();
                clusterResults.AddRange(compoundfamilynames);

                var compoundfamilysynonyms = (from synonyms in db.Synonyms
                                              where synonyms.Synonym1.Contains(searchString)
                                              join c in db.Clusters on
                                synonyms.CompoundFamilyID equals c.CompoundFamilyID
                                              select new SearchResults { ClusterID = c.ID, ColumnName = "Compound Family Synonym", ColumnContents = synonyms.Synonym1 }).ToList();

                //                  select new SearchResults { ClusterID = c.ID, ColumnName = "Compound Family Synonym", ColumnContents = synonyms.Synonym1 }).ToList();

                clusterResults.AddRange(compoundfamilysynonyms);

                // search compound families

                var cfs = (from cf in db.CompoundFamilies where cf.FamilyName.Contains(searchString) select cf)
                  .Select(x => new SearchResults { CompoundFamilyID = x.ID, ColumnName = "Compound Family Name", ColumnContents = x.FamilyName })
                  .ToList();
                cfResults.AddRange(cfs);

                var cfsynonyms = (from synonyms in db.Synonyms
                                  where synonyms.Synonym1.Contains(searchString)
                                  join cf in db.CompoundFamilies on
                                  synonyms.CompoundFamilyID equals cf.ID
                                  select new SearchResults { CompoundFamilyID = cf.ID, ColumnName = "Synonym", ColumnContents = synonyms.Synonym1 }).ToList();
                cfResults.AddRange(cfsynonyms);

                var pharmresults = (from actions in db.PharmActions
                                    where actions.PharmActionsType.TypeDescription.Contains(searchString)

                                    select new SearchResults { CompoundFamilyID = actions.cfid, ColumnName = "Pharmacological Identifier", ColumnContents = actions.CompoundFamily.FamilyName + "<br />" + actions.PharmActionsType.TypeDescription }).ToList().GroupBy(x => new { x.CompoundFamilyID, x.ColumnContents }).Select(x => x.First()).OrderBy(x => x.ColumnContents).ToList();
                cfResults.AddRange(pharmresults);
            }
            else
            {
                Dictionary<int, string> structures = Structure.searchStructures(searchString);
                foreach (var structure in structures)
                {
                    SearchResults structureResult = new SearchResults();
                    structureResult.CompoundFamilyID = structure.Key;
                    structureResult.ColumnName = "Sub-Structure Match";
                    structureResult.ColumnContents = structure.Value + "<br /><a href=\"CompoundFamilyDetails.aspx?cfid=" + structure.Key + "\" target=\"_blank\"><img class=\"smallimages dottedborder\" src=\"StructureImageHandler.ashx?cfid=" + structure.Key + "\" /> </a>";
                    cfResults.Add(structureResult);
                }
                
            }
            cfResults = cfResults.OrderBy(x => x.ColumnContents).ToList();
            clusterResults = clusterResults.OrderBy(x => x.ColumnContents).ToList();

            // bind results
            if (clusterResults.Count > 0)
            {
                ClusterResultsRepeater.DataSource = clusterResults;
                ClusterResultsRepeater.ItemDataBound += ClusterResultsRepeater_ItemDataBound;
                ClusterResultsRepeater.DataBind();
                ClusterResults.Visible = true;
            }
            else
            {
                ClusterResults.Visible = false;
            }

            if (cfResults.Count > 0)
            {
                CompoundFamilyResultsRepeater.DataSource = cfResults;
                CompoundFamilyResultsRepeater.ItemDataBound += CompoundFamilyResultsRepeater_ItemDataBound;
                CompoundFamilyResultsRepeater.DataBind();
                CompoundFamilyResults.Visible = true;
            }
            else
            {
                CompoundFamilyResults.Visible = false;
            }

            totalhits = clusterResults.Count + cfResults.Count;
            if (totalhits == 0)
            {
                NoResultsLabel.Visible = true;
            }
            else
            {
                NoResultsLabel.Visible = false;
                StringBuilder script = new StringBuilder();
                script.Append("$('span').replaceText(/(" + searchString + ")/gi, '<span class=\"highlight\">$1</span>');");

                Page.ClientScript.RegisterStartupScript(this.GetType(), "highlighter", script.ToString(), true);
            }
        }

        protected void ClusterResultsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label columnname = (Label)e.Item.FindControl("ColumnName");
            columnname.Text = (string)DataBinder.Eval(e.Item.DataItem, "ColumnName");

            Label columncontents = (Label)e.Item.FindControl("ColumnContents");
            columncontents.Text = (string)DataBinder.Eval(e.Item.DataItem, "ColumnContents");

            HyperLink viewlink = (HyperLink)e.Item.FindControl("ClusterLink");
            viewlink.Target = "_blank";
            viewlink.NavigateUrl = "~/ClusterDetails.aspx?clusterid=" + (int)DataBinder.Eval(e.Item.DataItem, "ClusterID");
        }

        protected void CompoundFamilyResultsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label columnname = (Label)e.Item.FindControl("ColumnName");
            columnname.Text = (string)DataBinder.Eval(e.Item.DataItem, "ColumnName");

            Label columncontents = (Label)e.Item.FindControl("ColumnContents");
            columncontents.Text = (string)DataBinder.Eval(e.Item.DataItem, "ColumnContents");

            HyperLink viewlink = (HyperLink)e.Item.FindControl("CompoundFamilyLink");
            viewlink.Target = "_blank";
            viewlink.NavigateUrl = "~/CompoundFamilyDetails.aspx?cfid=" + (int)DataBinder.Eval(e.Item.DataItem, "CompoundFamilyID");
        }
    }

    public class SearchResults
    {
        public string ColumnName { get; set; }

        public string ColumnContents { get; set; }

        public int ClusterID { get; set; }

        public int CompoundFamilyID { get; set; }
    }
}