﻿<%@ Application Language="C#" %>
<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
     
    }

    void Application_End(object sender, EventArgs e)
    {

    }

    void Application_Error(object sender, EventArgs e)
    {

        // Get the error details
        HttpException lastErrorWrapper = Server.GetLastError() as HttpException;
        if (lastErrorWrapper != null)
        {
            if (lastErrorWrapper.GetHttpCode() != 404)
            {
                Exception lastError = lastErrorWrapper;
                if (lastErrorWrapper.InnerException != null)
                    lastError = lastErrorWrapper.InnerException;

                string lastErrorTypeName = lastError.GetType().ToString();
                string lastErrorMessage = lastError.Message;
                string lastErrorStackTrace = lastError.StackTrace;
                string to = ConfigurationManager.AppSettings["AdminEmail"];
                const string Subject = "An Error Has Occurred!";
                string urlreferrer = "";
                if (Request.UrlReferrer != null)
                {
                    urlreferrer = Request.UrlReferrer.ToString();
                }
                // Create the MailMessage object
                System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage();

                string bodytext = string.Format(@"<html><body>  <h1>An Error Has Occurred!</h1>  <table cellpadding=""5"" cellspacing=""0"" border=""1"">  <tr>  <td style=""text-align: right;font-weight: bold"">URL:</td>  <td>{0}</td>  </tr><tr>  <td style=""text-align: right;font-weight: bold"">URL Referrer:</td>  <td>{1}</td>  </tr>  <tr>  <td style=""text-align: right;font-weight: bold"">User:</td>  <td>{2}</td>  </tr>  <tr>  <td style=""text-align: right;font-weight: bold"">Exception Type:</td>  <td>{3}</td>  </tr>  <tr>  <td style=""text-align: right;font-weight: bold"">Message:</td>  <td>{4}</td>  </tr>  <tr>  <td style=""text-align: right;font-weight: bold"">Stack Trace:</td>  <td>{5}</td>  </tr>   </table></body></html>",
                         Request.RawUrl,
                         urlreferrer,
                         User.Identity.Name,
                         lastErrorTypeName,
                         lastErrorMessage,
                         lastErrorStackTrace.Replace(Environment.NewLine, "<br />"));

                // Attach the Yellow Screen of Death for this error
                string YSODmarkup = lastErrorWrapper.GetHtmlErrorMessage();
                if (!string.IsNullOrEmpty(YSODmarkup))
                {
                    System.Net.Mail.Attachment YSOD = System.Net.Mail.Attachment.CreateAttachmentFromString(YSODmarkup, "YSOD.htm");
                    email.Attachments.Add(YSOD);
                }

                email.IsBodyHtml = true;
                email.To.Add(new System.Net.Mail.MailAddress(to));
                email.Subject = Subject;
                email.Body = bodytext;

              Utilities.Email.Send(email);

            }
        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends.
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer
        // or SQLServer, the event is not raised.

    }

    void Application_EndRequest(object sender, EventArgs e)
    {
        if (Response.RedirectLocation != null && Response.RedirectLocation.Contains("ReturnUrl"))
        {
            Response.RedirectLocation = string.Format("{0}ReturnUrl={1}",
                Response.RedirectLocation.Remove(Response.RedirectLocation.IndexOf("ReturnUrl")),
                (Request.RawUrl.Contains("ReturnUrl") ?
                Request.RawUrl.Substring(Request.RawUrl.IndexOf("ReturnUrl") + 10) : Request.RawUrl));
        }
    }

</script>