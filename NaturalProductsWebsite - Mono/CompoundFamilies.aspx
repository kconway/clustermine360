﻿<%@ Page Title="Compound Families" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="CompoundFamiliesPage" CodeBehind="CompoundFamilies.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".smallimageparent").children("img").addClass("smallimages");
        });</script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <h2 class="floatleft">
        Compound Families
    </h2><asp:HyperLink
        ID="ViewImagesToggle" runat="server" CssClass="navbuttons"></asp:HyperLink>
        <br class="clear" />
    <hr />
    <br />
    <span style="margin-left:10px"><b>Total # Compound Families: <asp:Label ID="NumCompoundFamilies" runat="server"></asp:Label></b></span>
    
    <br />
    <br />
     
    <asp:Repeater ID="mainList" runat="server">
        <HeaderTemplate>
            <div style="float: left; padding-right: 50px; padding-left: 50px">
        </HeaderTemplate>
        <SeparatorTemplate>
            </div><div style="float: left; padding-right: 50px; padding-left: 50px;">
        </SeparatorTemplate>
        <FooterTemplate>
            </div></FooterTemplate>
    </asp:Repeater>
</asp:Content>