﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace NaturalProductsWebsite
{
    public partial class Browse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            insertLineageScript();
        }

        protected void insertLineageScript()
        {
            StringBuilder lineageScript = new StringBuilder();
            lineageScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
            lineageScript.AppendLine("function toggleLineagePanel(imageID, phylumPanelID, lineagePanelID) {");
            lineageScript.AppendLine("phylumPanelID = formatID(phylumPanelID);");
            lineageScript.AppendLine("lineagePanelID = formatID(lineagePanelID);");
            lineageScript.AppendLine("imageID = formatID(imageID);");
            lineageScript.AppendLine("if ($(phylumPanelID).is(\":visible\")) {");
            lineageScript.AppendLine("$(phylumPanelID).hide('fast');");
            lineageScript.AppendLine("$(lineagePanelID).show('fast');");
            lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_remove.gif\");");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("else {");
            lineageScript.AppendLine("$(lineagePanelID).hide('fast');");
            lineageScript.AppendLine("$(phylumPanelID).show('fast');");
            lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_add2.gif\")");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine();
            lineageScript.AppendLine("function formatID(id) {");
            lineageScript.AppendLine("var formattedID = '\"' + \"[id$=\" + \"'\" + id + \"'\" + \"]\" + '\"'");
            lineageScript.AppendLine("return formattedID;");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "lineageScript", lineageScript.ToString(), false);
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 0 = Compound Family
            // 1 = Pathway Type
            // 2 = Phylum
            int rbvalue = int.Parse(RadioButtonList1.SelectedValue);
            switch (rbvalue)
            {
                case 0:
                    ucCompoundFamilyThenOrganismControl.Visible = true;
                    ucPathwayTypeThenCompoundFamilyControl.Visible = false;
                    ucPhylumThenOrganismThenCompoundFamilyControl.Visible = false;
                    break;
                case 1:
                    ucCompoundFamilyThenOrganismControl.Visible = false;
                    ucPathwayTypeThenCompoundFamilyControl.Visible = true;
                    ucPhylumThenOrganismThenCompoundFamilyControl.Visible = false;
                    break;

                case 2:
                    ucCompoundFamilyThenOrganismControl.Visible = false;
                    ucPathwayTypeThenCompoundFamilyControl.Visible = false;
                    ucPhylumThenOrganismThenCompoundFamilyControl.Visible = true;
                    break;
                default:
                    ucCompoundFamilyThenOrganismControl.Visible = true;
                    ucPathwayTypeThenCompoundFamilyControl.Visible = false;
                    ucPhylumThenOrganismThenCompoundFamilyControl.Visible = false;
                    break;
            }
        }
    }
}