﻿<%@ WebHandler Language="C#" Class="StructureImageHandler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class StructureImageHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        int cfid = int.Parse(context.Request["cfid"]);

        DataClassesDataContext db = new DataClassesDataContext();
        var compoundFamilyImageRecord = (from c in db.CompoundFamilyImages
                                         where c.cfid == cfid
                                         select c).SingleOrDefault();

        try
        {
            if (compoundFamilyImageRecord.Image != null && compoundFamilyImageRecord.Image.Length != 0)
            {
                byte[] imageData = compoundFamilyImageRecord.Image;
                context.Response.ContentType = compoundFamilyImageRecord.ImageType;
                context.Response.OutputStream.Write(imageData, 0, imageData.Length);
            }
            else
            {
                byte[] imageData = NaturalProductsWebsite.Code.Structure.GenerateMissingImageNotice("No image for " + compoundFamilyImageRecord.CompoundFamily.FamilyName + ".");
                context.Response.ContentType = "image/png";
                context.Response.OutputStream.Write(imageData, 0, imageData.Length);

            }
        }
        catch (Exception ex)
        {
            //Utilities.Email.send(System.Configuration.ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
        }
        db.Dispose();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}