﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    public static class CommonDataBinding
    {
        public static void CompoundFamilyLiteral_DataBind(Object sender, EventArgs e)
        {
            Literal label = (Literal)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            string cfname = (string)DataBinder.Eval(container.DataItem, "CompoundFamily.FamilyName");
            label.Text = cfname;
        }

        public static void DescriptionLiteral_DataBind(Object sender, EventArgs e)
        {
            Literal label = (Literal)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            string description = (string)DataBinder.Eval(container.DataItem, "ClusterBase.Description");
            label.Text = description;
        }

        public static void HeaderLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            if (((DataBinder.Eval(container.DataItem, "BPStart")) != null) && ((DataBinder.Eval(container.DataItem, "BPEnd")) != null))
            {
                // do nothing
            }
            else
            {
                label.Visible = false;
            }
        }

        public static void GenbankStartEnd_DataBind(Object sender, EventArgs e)
        {
            Literal label = (Literal)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            if (((DataBinder.Eval(container.DataItem, "BPStart")) != null) && ((DataBinder.Eval(container.DataItem, "BPEnd")) != null))
            {
                NumberFormatInfo nf = new CultureInfo("en-US", false).NumberFormat;
                nf.NumberDecimalDigits = 0;
                int bpstartnum = (int)(DataBinder.Eval(container.DataItem, "BPStart"));
                int bpendnum = (int)(DataBinder.Eval(container.DataItem, "BPEnd"));
                string bpstart = bpstartnum.ToString("N", nf);
                string bpend = bpendnum.ToString("N", nf);

                string fromString = "From " + bpstart;
                string toString = "To " + bpend;
                int amtToAdd = toString.Length - fromString.Length;
                string finalFromString = "From" + "&nbsp;";
                string finalToString = "To" + "&nbsp;";

                if (amtToAdd > 0)
                {
                    for (int i = 0; i <= amtToAdd; i++)
                    {
                        finalFromString = finalFromString + "&nbsp;";
                    }
                }
                else if (amtToAdd < 0)
                {
                    finalToString = finalToString + "&nbsp;";
                    for (int i = 0; i >= amtToAdd; i--)
                    {
                        finalToString = finalToString + "&nbsp;";
                    }
                }

                finalFromString = finalFromString + bpstart;
                finalToString = finalToString + bpend;

                label.Text = finalFromString + "<br />" + finalToString;
            }
            else
            {
                label.Visible = false;
            }
        }

        public static void GenbankLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            string genbankaccession = "";
            int gbversion = 1;
            if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)link.NamingContainer;
                genbankaccession = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.GenbankAccession"));
                gbversion = (int)DataBinder.Eval(container.DataItem, "ClusterBase.GenbankVersion");
            }
            else if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)link.NamingContainer;
                genbankaccession = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.GenbankAccession"));
                gbversion = (int)DataBinder.Eval(container.DataItem, "ClusterBase.GenbankVersion");
            }

            string gbtext = genbankaccession;
            if (gbversion != 0)
            {
                gbtext = gbtext + "." + gbversion;
            }

            link.Text = gbtext;
            link.NavigateUrl = NaturalProductsWebsite.Code.NCBIUtils.GetNCBIDisplayURL() + gbtext;
            link.Target = "_blank";
        }

        public static void TogglePlaceholder_DataBind(Object sender, EventArgs e)
        {
            PlaceHolder ph = (PlaceHolder)sender;
            HtmlImage image = (HtmlImage)ph.FindControl("toggleimage");
            Panel lineagepanel = (Panel)ph.FindControl("lineagepanel");
            Panel phylumpanel = (Panel)ph.FindControl("phylumpanel");
            lineagepanel.Attributes.Add("style", "display:none");
            int imageidlength = image.ClientID.Length;
            string shortImageClientID = image.ClientID.Substring(imageidlength - 25, 25);
            string shortPhylumPanelClientID = phylumpanel.ClientID.Substring(phylumpanel.ClientID.Length - 25, 25);
            string shortLineagePanelClientID = lineagepanel.ClientID.Substring(lineagepanel.ClientID.Length - 25, 25);
            image.Attributes.Add("onClick", "toggleLineagePanel('" + shortImageClientID + "','" + shortPhylumPanelClientID + "','" + shortLineagePanelClientID + "');");
        }

        public static void PhylumLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            if ((DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum")) != null)
            {
                label.Text = (string)DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum");
            }
        }

        public static void LineageLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            string formattedlineage = "";
            if (DataBinder.Eval(container.DataItem, "ClusterBase.Lineage") != null)
            {
                formattedlineage = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.Lineage"));
                if (formattedlineage == null)
                {
                    formattedlineage = "";
                }
                formattedlineage = formattedlineage.Replace("Bacteria;", "");
                formattedlineage = formattedlineage.Replace(";", "<br />");
            }

            label.Text = formattedlineage;
        }

        public static void PathwayType_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            Cluster cluster = (Cluster)container.DataItem;

            label.Text = StringTools.SerializeToSemiColonSeparatedList(cluster.CompoundFamily.ClusterPathwayTypes.Select(x => x.Enum_PathwayType.Type).OrderBy(x => x).ToList()).Replace(";", "<br />");
        }

        public static void antiSmashLink_DataBind(object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;

            GridViewRow container = (GridViewRow)link.NamingContainer;
            string clusterid = (DataBinder.Eval(container.DataItem, "ID")).ToString();

            if (Directory.Exists(Utilities.FileTools.getantiSMASHClustersFolder() + clusterid))
            {
                link.NavigateUrl = "~/antiSMASH/Clusters/" + clusterid + "/display.xhtml";
                link.Visible = true;
            }
        }

        public static void NameLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            string name = "";

            GridViewRow container = (GridViewRow)label.NamingContainer;
            int contributorid = (int)DataBinder.Eval(container.DataItem, "ClusterBase.ContributorID");
            UserInfo user = UserInfos.getUserInfoByID(contributorid);
            name = user.First_Name + " " + user.Last_Name;

            label.Text = name;
        }

        public static void LabLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;

            GridViewRow container = (GridViewRow)link.NamingContainer;
            int contributorid = (int)DataBinder.Eval(container.DataItem, "ClusterBase.ContributorID");
            UserInfo user = UserInfos.getUserInfoByID(contributorid);

            link.Text = user.Lab_Name;
            link.Target = "_blank";
            link.NavigateUrl = user.Lab_Url;
        }

        public static void ImageLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink imagelink = (HyperLink)sender;
            GridViewRow container = (GridViewRow)imagelink.NamingContainer;
            string clusterid = ((int)DataBinder.Eval(container.DataItem, "ID")).ToString();
            string clientclick = "~/ClusterDetails.aspx?clusterid=" + clusterid;
            imagelink.Target = "_blank";
            imagelink.NavigateUrl = clientclick;
        }
    }
}