﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Utilities;

namespace NaturalProductsWebsite.Code
{
    public class CodeControls
    {
        enum RecordType
	{
	         Cluster,
            CompoundFamily
	}

        public Control getControl(Dictionary<string, object> parameters)
        {
            string templateType = (string)parameters["TemplateType"];
            Control container = new Control();
            switch (templateType)
            {
                case "Contributor":
                    ContributorLayoutCreator(container, RecordType.Cluster);
                    break;

                case "CFContributor":
                    ContributorLayoutCreator(container, RecordType.CompoundFamily);
                    break;
      
                case "OrgLineageDetailsView":

                    Panel lineagepaneldv = new Panel();
                    lineagepaneldv.ID = "lineagepaneldv";

                    Label lineageLabeldv = new Label();
                    lineageLabeldv.ID = "lineagelabeldv";
                    lineageLabeldv.DataBinding += lineageLabel_DataBind;
                    lineagepaneldv.Controls.Add(lineageLabeldv);
                    container.Controls.Add(lineagepaneldv);
                    break;

                

                case "SynonymsClusterView":
                    Panel synonymspaneldv = new Panel();
                    synonymspaneldv.ID = "relatedcompoundfamiliespaneldv";

                    Controls_SynonymsControl synControl = new Controls_SynonymsControl();
                    synControl.mode = 0;
                    synControl.DataBinding += synonymsClusterView_DataBinding;
                    synonymspaneldv.Controls.Add(synControl);
                    container.Controls.Add(synonymspaneldv);
                    break;

               

                case "BPStartEnd":
                    Label bpstartend = new Label();
                    bpstartend.ID = "bpstartendlabel";
                    bpstartend.DataBinding += bpstartend_DataBind;
                    container.Controls.Add(bpstartend);
                    break;

                case "PathwayType":
                    Label pathwaytype = new Label();
                    pathwaytype.ID = "pathwaytypelabel";
                    pathwaytype.DataBinding += pathwaytype_DataBind;
                    container.Controls.Add(pathwaytype);
                    break;

                case "SequenceExtractionStatus":
                    Label extractionstatus = new Label();
                    extractionstatus.ID = "extractionstatuslabel";
                    extractionstatus.DataBinding += extractionstatus_DataBind;
                    container.Controls.Add(extractionstatus);
                    break;

                case "Phylum":
                    Label phylumName = new Label();
                    phylumName.ID = "phylumNamelabel";
                    phylumName.DataBinding += phylumLabel_DataBind;
                    container.Controls.Add(phylumName);
                    break;

                case "FamilyName":
                    HyperLink FamilyName = new HyperLink();
                    FamilyName.ID = "FamilyNamelabel";
                    FamilyName.DataBinding += familyname_DataBind;
                    container.Controls.Add(FamilyName);
                    break;

                case "antiSmashLink":
                    HyperLink link = new HyperLink();
                    link.Text = "View";
                    link.Target = "_blank";
                    link.Visible = false;
                    link.DataBinding += new EventHandler(antiSmashLink_DataBinding);
                    container.Controls.Add(link);
                    break;
            }
            return container;
        }

        private void ContributorLayoutCreator(Control container, RecordType type)
        {
           
                    Label nameLabel = new Label();
                    if (type == RecordType.Cluster)
                    {
                        nameLabel.DataBinding += ClusterNameLabel_DataBind;
                    }
                    else
                    {
nameLabel.DataBinding += NameLabel_DataBind;
                    }
                    container.Controls.Add(nameLabel);

                    Literal newline = new Literal();
                    newline.Text = "<br />";
                    container.Controls.Add(newline);

                    HyperLink labLink = new HyperLink();
                    if (type == RecordType.Cluster)
                    {
                        labLink.DataBinding += ClusterLabLink_DataBind;
                    }
                    else
                    {
 labLink.DataBinding += LabLink_DataBind;
                    }
                    container.Controls.Add(labLink);
                   
        }

        private void antiSmashLink_DataBinding(object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            string antiSmashFolder = Utilities.FileTools.getantiSMASHClustersFolder();
            if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)link.NamingContainer;
                string clusterid = (DataBinder.Eval(container.DataItem, "ID")).ToString();

                if (Directory.Exists(antiSmashFolder + clusterid))
                {
                    link.NavigateUrl = "~/antiSMASH/Clusters/" + clusterid + "/display.xhtml";
                    link.Visible = true;
                }
            }
            else if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)link.NamingContainer;
                string clusterid = (DataBinder.Eval(container.DataItem, "ID")).ToString();

                if (Directory.Exists(antiSmashFolder + clusterid))
                {
                    link.NavigateUrl = "~/antiSMASH/Clusters/" + clusterid + "/display.xhtml";
                    link.Text = "View";
                    link.Visible = true;
                }
                else
                {
                    link.Text = "Status: " + (DataBinder.Eval(container.DataItem, "ClusterBase.Enum_AntiSmashStatus.StatusDescription")).ToString();
                    link.Visible = true;
                }
            }
        }

        private void NameLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            string name = "";
            string columntobind = "ContributorID";
            if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)label.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, columntobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);
                name = user.First_Name + " " + user.Last_Name;
            }
            else if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)label.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, columntobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);
                name = user.First_Name + " " + user.Last_Name;
            }
            label.Text = name;
        }
        private void ClusterNameLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            string name = "";
            string columntobind = "ClusterBase.ContributorID";
            if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)label.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, columntobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);
                name = user.First_Name + " " + user.Last_Name;
            }
            else if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)label.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, columntobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);
                name = user.First_Name + " " + user.Last_Name;
            }
            label.Text = name;
        }

        private void LabLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            string fieldtobind = "ContributorID";
      
            if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)link.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, fieldtobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);

                link.Text = user.Lab_Name;
                link.Target = "_blank";
                link.NavigateUrl = user.Lab_Url;
            }
            else if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)link.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, fieldtobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);

                link.Text = user.Lab_Name;
                link.Target = "_blank";
                link.NavigateUrl = user.Lab_Url;
            }
        }

        private void ClusterLabLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            string fieldtobind = "ClusterBase.ContributorID";

            if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)link.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, fieldtobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);

                link.Text = user.Lab_Name;
                link.Target = "_blank";
                link.NavigateUrl = user.Lab_Url;
            }
            else if (link.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)link.NamingContainer;
                int contributorid = (int)DataBinder.Eval(container.DataItem, fieldtobind);
                UserInfo user = UserInfos.getUserInfoByID(contributorid);

                link.Text = user.Lab_Name;
                link.Target = "_blank";
                link.NavigateUrl = user.Lab_Url;
            }
        }

        private void lineageLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)label.NamingContainer;
                string formattedlineage = "";
                if ((DataBinder.Eval(container.DataItem, "ClusterBase.Lineage")) != null)
                {
                    formattedlineage = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.Lineage"));
                    if (formattedlineage == null)
                    {
                        formattedlineage = "";
                    }
                    formattedlineage = formattedlineage.Replace("Bacteria;", "");
                    formattedlineage = formattedlineage.Replace(";", "<br />");
                }

                label.Text = formattedlineage;
            }
            else if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)label.NamingContainer;
                string formattedlineage = "";
                if ((DataBinder.Eval(container.DataItem, "ClusterBase.Lineage")) != null)
                {
                    formattedlineage = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.Lineage"));
                    formattedlineage = formattedlineage.Replace("Bacteria;", "");
                    formattedlineage = formattedlineage.Replace(";", "<br />");
                }

                label.Text = formattedlineage;
            }
        }      

        private void synonymsClusterView_DataBinding(Object sender, EventArgs e)
        {
            Controls_SynonymsControl synControl = (Controls_SynonymsControl)sender;
            if (synControl.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
            }
            else if (synControl.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)synControl.NamingContainer;
                int clusterid = (int)DataBinder.Eval(container.DataItem, "ID");
                int cfid = Clusters.getClusterByID(clusterid).CompoundFamilyID;
                synControl.cfid = cfid;
            }
        }

        private void phylumLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)label.NamingContainer;
                if ((DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum")) != null)
                {
                    label.Text = (string)DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum");
                }
            }
            else if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)label.NamingContainer;
                if ((DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum")) != null)
                {
                    label.Text = (string)DataBinder.Eval(container.DataItem, "ClusterBase.Enum_Phyla.Phylum");
                }
            }
        }

        private void bpstartend_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            DetailsView container = (DetailsView)label.NamingContainer;
            if (((DataBinder.Eval(container.DataItem, "BPStart")) != null) && ((DataBinder.Eval(container.DataItem, "BPEnd")) != null))
            {
                NumberFormatInfo nf = new CultureInfo("en-US", false).NumberFormat;
                nf.NumberDecimalDigits = 0;
                int bpstartnum = (int)(DataBinder.Eval(container.DataItem, "BPStart"));
                int bpendnum = (int)(DataBinder.Eval(container.DataItem, "BPEnd"));
                string bpstart = bpstartnum.ToString("N", nf);
                string bpend = bpendnum.ToString("N", nf);

                label.Text = bpstart + " to " + bpend;
            }
            else
            {
                label.Visible = false;
            }
        }

        private void pathwaytype_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.GridViewRow"))
            {
                GridViewRow container = (GridViewRow)label.NamingContainer;
                Cluster cluster = (Cluster)container.DataItem;

                label.Text = StringTools.SerializeToSemiColonSeparatedList(cluster.CompoundFamily.ClusterPathwayTypes.Select(x => x.Enum_PathwayType.Type).ToList()).Replace(";", "<br />");
            }
            else if (label.NamingContainer.GetType().ToString().Equals("System.Web.UI.WebControls.DetailsView"))
            {
                DetailsView container = (DetailsView)label.NamingContainer;
                Cluster cluster = (Cluster)container.DataItem;

                label.Text = StringTools.SerializeToSemiColonSeparatedList(cluster.CompoundFamily.ClusterPathwayTypes.Select(x => x.Enum_PathwayType.Type).ToList()).Replace(";", "<br />");
                
            }
        }

        private void extractionstatus_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            DetailsView container = (DetailsView)label.NamingContainer;
            label.Text = (string)DataBinder.Eval(container.DataItem, "ClusterBase.Enum_SequenceExtractionStatus.StatusDescription");
        }

        private void familyname_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            DetailsView container = (DetailsView)link.NamingContainer;
            link.Text = (string)DataBinder.Eval(container.DataItem, "CompoundFamily.FamilyName");
            link.NavigateUrl = "~/CompoundFamilyDetails.aspx?cfid=" + CompoundFamilies.getCompoundFamilyByName(link.Text).ID;
            link.Font.Underline = false;
        }
      
    }
}