﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Utilities;
using System.Web.UI.HtmlControls;

public class LayoutTemplate : ITemplate
{
    private string placeholderid;

    public LayoutTemplate()
    {
        placeholderid = "itemPlaceHolder";
    }

    public LayoutTemplate(string PlaceHolderID)
    {
        placeholderid = PlaceHolderID;
    }

    public void InstantiateIn(Control container)
    {
        PlaceHolder plholder = new PlaceHolder();
        plholder.ID = placeholderid;
        container.Controls.Add(plholder);
    }
}

public class CompoundFamilyAlphabetTemplate : ITemplate
{
    private List<CompoundFamily> families;
    private bool includeimage = true;
    public CompoundFamilyAlphabetTemplate(List<CompoundFamily> listOfFamilies, bool IncludeImage)
    {
        families = listOfFamilies;
        includeimage = IncludeImage;
    }

    public void InstantiateIn(System.Web.UI.Control container)
    {
        container.DataBinding += compoundfamilylist_DataBind;
    }

    private void compoundfamilylist_DataBind(Object sender, EventArgs e)
    {
        RepeaterItem container = (RepeaterItem)sender;
        char firstLetter = (char)DataBinder.Eval(container, "DataItem");

        Literal alphabetLetter = new Literal();
        alphabetLetter.Text = "<b>"+ firstLetter.ToString() + "</b><br />";

        ListView compoundfamilylist = new ListView();
        compoundfamilylist.ItemPlaceholderID = "compoundfamilyTopListPlaceHolder";
        compoundfamilylist.ID = "compoundFamilyList";
        compoundfamilylist.LayoutTemplate = new LayoutTemplate(compoundfamilylist.ItemPlaceholderID);
        compoundfamilylist.ItemTemplate = new CompoundFamilySimpleListTemplate(includeimage);
        if (firstLetter != '#')
        {
            compoundfamilylist.DataSource = families.FindAll(x => x.FamilyName.Substring(0, 1) == firstLetter.ToString());
        }
        else
        {
            compoundfamilylist.DataSource = families.FindAll(x => Char.IsNumber(x.FamilyName.ToCharArray()[0]));
        }

        Literal spacer = new Literal();
        spacer.Text = "<br />";

        container.Controls.Add(alphabetLetter);
        HtmlGenericControl list = new HtmlGenericControl("ul");
        list.Attributes.Add("class", "cfimageholder");
        list.Controls.Add(compoundfamilylist);
        container.Controls.Add(list);
        container.Controls.Add(spacer);
    }
}

public class CompoundFamilySimpleListTemplate : ITemplate
{
    private bool includeimage = false;
   
    public CompoundFamilySimpleListTemplate(bool IncludeImage)
    {
        includeimage = IncludeImage;
    }

   
    public void InstantiateIn(System.Web.UI.Control container)
    {
       
        HyperLink cflink = new HyperLink();
        cflink.CssClass = "browse";
        cflink.ID = "CompoundFamily";
        cflink.Target = "_blank";
        cflink.DataBinding += cfNameAndLink_DataBind;

        if (includeimage == true)
        {
            HtmlGenericControl listitem = new HtmlGenericControl("li");
                     
            HyperLink image = new HyperLink();
            image.DataBinding += StructureImage_DataBind;


            image.CssClass = "smallimageparent";
            listitem.Controls.Add(cflink);
            listitem.Controls.Add(image);
            container.Controls.Add(listitem); 
        }
        else
        {
container.Controls.Add(cflink);
        }
        
    }

    private void cfNameAndLink_DataBind(Object sender, EventArgs e)
    {
        HyperLink cfLink = (HyperLink)sender;

        ListViewDataItem container = (ListViewDataItem)cfLink.NamingContainer;
        int cfid = (int)DataBinder.Eval(container.DataItem, "ID");
        string cfName = (string)DataBinder.Eval(container.DataItem, "FamilyName");
        cfLink.NavigateUrl = "~/CompoundFamilyDetails.aspx?cfid=" + cfid;
        cfLink.Text = cfName;
    }

    private void StructureImage_DataBind(Object sender, EventArgs e)
    {
        HyperLink simage = (HyperLink)sender;
        ListViewDataItem container = (ListViewDataItem)simage.NamingContainer;
        int cfid = (int)DataBinder.Eval(container.DataItem, "ID");
        string cfName = (string)DataBinder.Eval(container.DataItem, "FamilyName");
        
        simage.ImageUrl = "~/StructureImageHandler.ashx?cfid=" + cfid;
        simage.NavigateUrl = "~/CompoundFamilyDetails.aspx?cfid=" + cfid;
        
        
    }


}



public static class GridViewTemplate
{
    public static void applyStyletoGridView(GridView gv)
    {
        gv.CellPadding = 4;
        gv.CssClass = "gridview";
        gv.ForeColor = ColorTranslator.FromHtml("#333333");
        gv.GridLines = GridLines.None;
        gv.EditRowStyle.BackColor = ColorTranslator.FromHtml("#999999");
        gv.HeaderStyle.BackColor = ColorTranslator.FromHtml("#5D7B9D");
        gv.HeaderStyle.Font.Bold = true;
        gv.HeaderStyle.ForeColor = Color.White;
        gv.FooterStyle.BackColor = ColorTranslator.FromHtml("#5D7B9D");
        gv.FooterStyle.Font.Bold = true;
        gv.FooterStyle.ForeColor = Color.White;
        gv.PagerStyle.BackColor = ColorTranslator.FromHtml("#5D7B9D");
        gv.PagerStyle.Font.Bold = true;
        gv.PagerStyle.ForeColor = Color.White;
        gv.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
        gv.RowStyle.BackColor = ColorTranslator.FromHtml("#F7F6F3");
        gv.RowStyle.ForeColor = Color.Black; //ColorTranslator.FromHtml("#333333");
        gv.RowStyle.HorizontalAlign = HorizontalAlign.Center;
        gv.RowStyle.VerticalAlign = VerticalAlign.Top;
        gv.SelectedRowStyle.BackColor = ColorTranslator.FromHtml("#E2DED6");
        gv.SelectedRowStyle.Font.Bold = true;
        gv.SelectedRowStyle.ForeColor = ColorTranslator.FromHtml("#333333");

    }
}