﻿<%@ Page Title="Contact Form" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="Contact" CodeBehind="Contact.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="main maxwidth indent" rows="10">
         <h3>Contact Information</h3>
         <br />
       <a href="http://mysite.science.uottawa.ca/cboddy/">Lab of Dr Chris Boddy</a><br />
       University of Ottawa
       <br />
       <br />
       <b>Your Email</b>
       <br />
<asp:TextBox ID="EmailText" runat="server" CssClass="textEntry"></asp:TextBox>
  <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="EmailText"
                                    CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="<br />E-mail is required."
                                    ValidationGroup="SubmitMessageValidationGroup" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="<br />Please enter a valid email address."
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="failureNotification"
                                    ControlToValidate="EmailText" ValidationGroup="SubmitMessageValidationGroup" Display="Dynamic"></asp:RegularExpressionValidator>
       <br />
       <br />
        <b>Subject</b>
        <br />
        <asp:TextBox ID="SubjectText" CssClass="textEntry" runat="server" ></asp:TextBox>
        <asp:RequiredFieldValidator
            ID="SubjectRequiredField" runat="server" ErrorMessage="<br />A subject is required." ValidationGroup="SubmitMessageValidationGroup" Display="Dynamic" CssClass="failureNotification" ControlToValidate="SubjectText" ></asp:RequiredFieldValidator>
        <br />
        <br />
        <b>Message</b>
        <br />
        <asp:TextBox ID="MessageText" runat="server" CssClass="textEntry" TextMode="MultiLine" Rows="10"></asp:TextBox>
        
        <asp:RequiredFieldValidator
            ID="MessageRequiredField" runat="server" ErrorMessage="<br />A message is required." ValidationGroup="SubmitMessageValidationGroup" Display="Dynamic" CssClass="failureNotification" ControlToValidate="MessageText" ></asp:RequiredFieldValidator>
        <br />
        <br />
<asp:Button ID="SubmitButton" runat="server" Text="Submit" 
             ValidationGroup="SubmitMessageValidationGroup" onclick="SubmitButton_Click" />
<br />
     
        <asp:Label ID="ErrorLabel" runat="server" Visible="false" Text="An error has occured. Please try again later" CssClass="error"></asp:Label>
       </div>
</asp:Content>
