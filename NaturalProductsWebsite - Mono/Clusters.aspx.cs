﻿using System;
using System.Text;
using System.Web.UI;

public partial class ClustersPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        NumClusters.Text = Clusters.getNumClusters().ToString();
        insertLineageScript();
    }

    protected void insertLineageScript()
    {
        StringBuilder lineageScript = new StringBuilder();
        lineageScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
        lineageScript.AppendLine("function toggleLineagePanel(imageID, phylumPanelID, lineagePanelID) {");
        lineageScript.AppendLine("phylumPanelID = formatID(phylumPanelID);");
        lineageScript.AppendLine("lineagePanelID = formatID(lineagePanelID);");
        lineageScript.AppendLine("imageID = formatID(imageID);");
        lineageScript.AppendLine("if ($(phylumPanelID).is(\":visible\")) {");
        lineageScript.AppendLine("$(phylumPanelID).hide('fast');");
        lineageScript.AppendLine("$(lineagePanelID).show('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_remove.gif\");");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("else {");
        lineageScript.AppendLine("$(lineagePanelID).hide('fast');");
        lineageScript.AppendLine("$(phylumPanelID).show('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_add2.gif\")");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine();
        lineageScript.AppendLine("function formatID(id) {");
        lineageScript.AppendLine("var formattedID = '\"' + \"[id$=\" + \"'\" + id + \"'\" + \"]\" + '\"'");
        lineageScript.AppendLine("return formattedID;");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("</script>");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "lineageScript", lineageScript.ToString(), false);
    }
}