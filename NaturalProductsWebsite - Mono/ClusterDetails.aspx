﻿<%@ Page Title="Cluster Details" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="ClusterDetails" CodeBehind="ClusterDetails.aspx.cs" %>

<%@ Register Src="~/Controls/ClusterCharacteristicsControl.ascx" TagPrefix="uc" TagName="ClusterCharacteristicsControl" %>
<%@ Register Src="~/Controls/UserReferencesControl.ascx" TagPrefix="uc" TagName="UserReferencesControl" %>
<%@ Register Src="~/Controls/LastModifiedControl.ascx" TagPrefix="uc" TagName="LastModifiedControl" %>
<%@ Register Src="~/Controls/RelatedFamiliesControl.ascx" TagPrefix="uc" TagName="RelatedFamiliesControl" %>
<%@ Register Src="~/Controls/SynonymsControl.ascx" TagPrefix="uc" TagName="SynonymsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
       <asp:ImageButton ID="NextButton" runat="server" 
        CssClass="navbuttons" AlternateText="Next" 
        ImageUrl="~/Images/icon_move_right.png" onclick="NextButton_Click" /> <asp:ImageButton ID="PreviousButton" runat="server" CssClass="navbuttons" 
        AlternateText="Previous" ImageUrl="~/Images/icon_move_left.png" 
        onclick="PreviousButton_Click" />
    <h2>
        Cluster Details
    </h2>
    <br />
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Clusters" SelectMethod="getClusterByID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="clusterid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="PubMedArticles"
        SelectMethod="getSequencingArticlesByClusterID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="clusterid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" TypeName="PubMedArticles"
        SelectMethod="getUserArticlesByClusterID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="clusterid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="dv" runat="server" AutoGenerateRows="False" CellPadding="4"
        DataKeyNames="ID" DataSourceID="ObjectDataSource1" ForeColor="#333333" GridLines="None"
        OnDataBinding="DetailsView1_DataBinding" OnDataBound="DetailsView1_DataBound"
        RowStyle-VerticalAlign="Middle" EditRowStyle-VerticalAlign="Middle">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>          
                <asp:TemplateField HeaderText="Accession #">
                <ItemTemplate>
                    <asp:HyperLink ID="GenbankLink" runat="server" OnDataBinding="GenbankLink_DataBind"></asp:HyperLink>
                </ItemTemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="Cluster Bases<br /><i><small>If cluster is a subsegment of the GenBank sequence.</small></i>">
                <ItemTemplate>
                    <asp:PlaceHolder ID="BPStartEnd" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name of Compound Family">
                <ItemTemplate>
                    <asp:PlaceHolder ID="FamilyName" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Synonyms">
                <ItemTemplate>
                    <uc:SynonymsControl runat="server" ID="ucSynonymsControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Related Compound Families">
                <ItemTemplate>
                    <uc:RelatedFamiliesControl runat="server" ID="ucRelatedFamiliesControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Pathway Type">
                <ItemTemplate>
                    <asp:PlaceHolder ID="PathwayType" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Characteristics">
                <ItemTemplate>
                   <uc:ClusterCharacteristicsControl runat="server" ID="ucClusterCharacteristicsControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Producing Organism">
            <ItemTemplate>
            <i><asp:Label runat="server" ID="OrganismLabel"></asp:Label></i>
            </ItemTemplate>
                
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Phylum">
                <ItemTemplate>
                    <asp:PlaceHolder ID="Phylum" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Organism Lineage">
                <ItemTemplate>
                    <asp:PlaceHolder ID="Lineage" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Description"><ItemTemplate >
            <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClusterBase.Description") %>'></asp:Label></ItemTemplate>
            </asp:TemplateField>
        
            <asp:TemplateField HeaderText="Sequencing References">
                <ItemTemplate>
                    <asp:Repeater ID="SequencingReferencesRepeater" runat="server" DataSourceID="ObjectDataSource2">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Details") %>
                        </ItemTemplate>
                        <HeaderTemplate>
                            <div class="toppadding5" />
                        </HeaderTemplate>
                        <SeparatorTemplate>
                            <div class="toppadding10" />
                        </SeparatorTemplate>
                        <FooterTemplate>
                            <div class="toppadding5" />
                        </FooterTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Other References">
                <ItemTemplate>
                    <uc:UserReferencesControl runat="server" ID="ucUserReferencesControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
            <HeaderTemplate>
            Domains
            </HeaderTemplate>
            <ItemTemplate>
              
                                    <img runat="server" id="toggleimage" class="toggle" style="top: 1px" src="~/Images/action_add2.gif"
                                        alt="Toggle" />&nbsp;View Domains
                                <asp:Panel runat="server" ID="TogglePanel" CssClass="indentsmall">
                                    <asp:ListView ID="InnerDomainsListView" runat="server" OnItemDataBound="InnerDomainsListView_ItemDataBound">
                                        <LayoutTemplate>
                                            <table runat="server" id="InnerHeaderTable" style="padding: 5px; text-align: left">
                                                <tr id="Tr1" runat="server">
                                                 <th style="width: 20%; text-align: left">
                                                 Download (.fasta)                                                       
                                                    </th>
                                                    <th style="width: 25%; text-align: left">
                                                        Domain Type
                                                    </th>
                                                    <th style="width: 10%; text-align: left">
                                                        Identifier
                                                    </th>
                                                    <th style="width: 15%; text-align: left">
                                                        Length (AA)
                                                    </th>
                                                    <th style="width: 30%; text-align: left">
                                                        Additional Info
                                                    </th>                                                   
                                                </tr>
                                                <tr runat="server" id="itemPlaceholder" />
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr runat="server">
                                            <td>
                                                <asp:LinkButton ID="DownloadLinkButton" runat="server" OnClick="DownloadLinkButton_Click">Download</asp:LinkButton>
                                            </td>
                                                <td>
                                                    <asp:Label ID="DomainTypeLabel" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="IdentifierLabel" runat="server"></asp:Label>
                                                </td>
                                                <td style="text-align:right; padding-right:15px">
                                                    <asp:Label ID="LengthLabel" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="AdditionalInfoLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        
                                    </asp:ListView>  <br />
             <asp:LinkButton ID="DownloadAllLinkButton" OnClick="DownloadAllLinkButton_Click" runat="server" Text="Download all of the sequences above (.zip)" > </asp:LinkButton><asp:Label
                 ID="NoDomainsLabel" runat="server" Visible="false" Text="No domains for this cluster could be found in the sequence repository." CssClass="error"></asp:Label>

                                </asp:Panel>
              
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="antiSMASH Status">
                <ItemTemplate>
                    <asp:PlaceHolder ID="antiSmash" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status of Domain Sequence Extraction">
                <ItemTemplate>
                    <asp:PlaceHolder ID="DomainSequenceStatus" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contributor">
                <ItemTemplate>
                    <asp:PlaceHolder ID="Contributor" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <asp:Button ID="Button1" runat="server" CssClass="submitButton" Text="Edit" OnClick="Button1_Click" />
    <asp:Button runat="server" CssClass="submitButton" Text="Close" OnClientClick="window.close();"
        ID="closeButton" />
    <br />
    <uc:LastModifiedControl runat="server" ID="ucLastModifiedControl" />
</asp:Content>