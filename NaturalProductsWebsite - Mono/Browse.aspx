﻿<%@ Page Title="Browse" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Browse.aspx.cs" Inherits="NaturalProductsWebsite.Browse" %>


<%@ Register Src="~/Controls/PhylumThenOrganismThenCompoundFamilyControl.ascx" TagPrefix="uc"
    TagName="PhylumThenOrganismThenCompoundFamilyControl" %>
<%@ Register Src="~/Controls/PathwayTypeThenCompoundFamilyControl.ascx" TagPrefix="uc"
    TagName="PathwayTypeThenCompoundFamilyControl" %>
<%@ Register Src="~/Controls/CompoundFamilyThenOrganismControl.ascx" TagPrefix="uc"
    TagName="CompoundFamilyThenOrganismControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
   
    <h2>
        Browse
    </h2>

    <div style="position: relative">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="True">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="RadioButtonList1" />
            </Triggers>
            <ContentTemplate>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img id="Img1" runat="server" alt="Updating" src="~/Images/ajax-loader.gif" class="updateImage" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <h3><strong>Sort By</strong></h3>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" CellSpacing="5"
                    OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" CssClass="browselist">
                    <asp:ListItem Text="Compound Family &rarr; Organism" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Pathway Type &rarr; Compound Family" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Phylum &rarr; Organism &rarr; Compound Family" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <uc:CompoundFamilyThenOrganismControl runat="server" ID="ucCompoundFamilyThenOrganismControl" />
                <uc:PathwayTypeThenCompoundFamilyControl runat="server" ID="ucPathwayTypeThenCompoundFamilyControl"
                    Visible="false" />
                <uc:PhylumThenOrganismThenCompoundFamilyControl runat="server" ID="ucPhylumThenOrganismThenCompoundFamilyControl"
                    Visible="false" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
