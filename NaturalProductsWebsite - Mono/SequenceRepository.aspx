﻿<%@ Page Title="Sequence Repository" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="SequenceRepository.aspx.cs" Inherits="NaturalProductsWebsite.SequenceRepository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .linklist li
    {
        padding:5px;
        font-size:medium;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>Microbial PKS/NRPS Sequence Repository</h2>
   
        <br />
        
        <ul class="linklist">
        <li>
        <a href="Repository/RepositorySummary.aspx">Overview of Repository Contents</a></li>
        <li><a href="Repository/RepositoryDetails.aspx">Details of Clusters/Sequences in Repository</a></li>
         <li><a href="Repository/ProcessingDetails.aspx">Currently Processing</a></li>
         <li><a href="Repository/DownloadSequences.aspx">Download Sequences</a></li>
        </ul>
        <br />
        
       

</asp:Content>