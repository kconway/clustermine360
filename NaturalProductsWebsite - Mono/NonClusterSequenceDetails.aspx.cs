﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
using Utilities;
using System.Web.UI;
using System.Text;
using System.Web.UI.HtmlControls;

public partial class NonClusterSequenceDetails : System.Web.UI.Page
{
    int clusterid;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["clusterid"] != "" && Request.QueryString["clusterid"] != null)
        {
            clusterid = int.Parse(Request.QueryString["clusterid"]);
            DataClassesDataContext db = new DataClassesDataContext();
            if (!db.Clusters.Exists(x => x.ID == clusterid))
            {
                Response.Redirect("~/Clusters.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        //dv.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dv.RowStyle.VerticalAlign = VerticalAlign.Top;
        dv.Width = Unit.Percentage(90.00);
        dv.FieldHeaderStyle.Width = Unit.Percentage(20.00);
        dv.RowStyle.Width = Unit.Percentage(40.00);

        if (User.IsInRole("restricted"))
        {
            Button1.Visible = false;
        }
        else
        {
            Button1.Visible = true;
        }

        
        InsertToggleScript();
    }

    protected void InsertToggleScript()
    {
        string plusimageurl = ResolveUrl("~/Images/action_add2.gif");
        string minusimageurl = ResolveUrl("~/Images/action_remove.gif");
        StringBuilder lineageScript = new StringBuilder();
        lineageScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
        lineageScript.AppendLine("function togglePanel(imageID, PanelID) {");
        lineageScript.AppendLine("PanelID = formatID(PanelID);");
        lineageScript.AppendLine("imageID = formatID(imageID);");
        lineageScript.AppendLine("if ($(PanelID).is(\":visible\")) {");
        lineageScript.AppendLine("$(PanelID).hide('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"" + plusimageurl + "\");");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("else {");
        lineageScript.AppendLine("$(PanelID).show('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"" + minusimageurl + "\");");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine();
        lineageScript.AppendLine("function formatID(id) {");
        lineageScript.AppendLine("var formattedID = '\"' + \"[id$=\" + \"'\" + id + \"'\" + \"]\" + '\"'");
        lineageScript.AppendLine("return formattedID;");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("</script>");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ToggleScript", lineageScript.ToString(), false);
    }

    protected void DownloadLinkButton_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        int domainid = int.Parse(lb.CommandArgument);
        string filename = "";
        string filedata = FastaGenerator.PrepareFASTAFile(domainid, out filename);

        Response.Clear();
        Response.ContentType = "text/plain";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        Response.BinaryWrite(StringTools.StringToByteArray(filedata));       
        Response.Flush();
        Response.End();
        
    }

    protected void DownloadAllLinkButton_Click(object sender, EventArgs e)
    {
        var sequences = DomainSequencesDataHandler.getDomainsByClusterID(clusterid);
        var cluster = Clusters.getClusterByID(clusterid);
        string filename = cluster.ClusterBase.OrgName + "_" + cluster.CompoundFamily.FamilyName + "_" + cluster.ClusterBase.GenbankAccession + ".zip";
        filename = filename.Replace(' ', '_');
        Response.Clear();
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        FastaGenerator.PrepareZippedFASTAFiles(sequences, Response.OutputStream);
        Response.Flush();
        Response.End();
    }


    protected void InnerDomainsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = e.Item as ListViewDataItem;

        if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
        {
            LinkButton lb = (LinkButton)dataItem.FindControl("DownloadLinkButton");
            lb.CommandArgument = DataBinder.Eval(dataItem.DataItem, "ID").ToString();

            Label typelabel = (Label)dataItem.FindControl("DomainTypeLabel");
            typelabel.Text = (string)DataBinder.Eval(dataItem.DataItem, "DomainType");

            Label identifierlabel = (Label)dataItem.FindControl("IdentifierLabel");
            identifierlabel.Text = (string)DataBinder.Eval(dataItem.DataItem, "Identifier");

            Label lengthlabel = (Label)dataItem.FindControl("LengthLabel");
            lengthlabel.Text = DataBinder.Eval(dataItem.DataItem, "Length").ToString();

            Label infolabel = (Label)dataItem.FindControl("AdditionalInfoLabel");
            string info = "";

            string AorATPrediction = DataBinder.Eval(dataItem.DataItem, "AorATPrediction") != null ? "A or AT Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "AorATPrediction") : "";
            string KRActivityPrediction = DataBinder.Eval(dataItem.DataItem, "KRActivityPrediction") != null ? "KR Activity Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "KRActivityPrediction") : "";
            string KRStereoChemPrediction = DataBinder.Eval(dataItem.DataItem, "KRStereoChemPrediction") != null ? "KR Stereochemistry Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "KRStereoChemPrediction") : "";

            info = AorATPrediction + KRActivityPrediction + KRActivityPrediction;

            if (!String.IsNullOrEmpty(AorATPrediction))
            {
                info = AorATPrediction;
            }

            if ((!String.IsNullOrEmpty(KRActivityPrediction)) && (!String.IsNullOrEmpty(KRStereoChemPrediction)))
            {
                info = KRActivityPrediction + "<br />" + KRStereoChemPrediction;
            }

            infolabel.Text = info;
        }
    }


    protected void GenbankLink_DataBind(object sender, EventArgs e)
    {
        CommonDataBinding.GenbankLink_DataBind(sender, e);
    }

    protected void DetailsView1_DataBinding(object sender, EventArgs e)
    {
        DetailsView dv = (DetailsView)sender;
        
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        DetailsView dv = (DetailsView)sender;
        Dictionary<string, object> parameters = new Dictionary<string, object>();


        HtmlImage image = (HtmlImage)dv.FindControl("toggleimage");
        Panel togglepanel = (Panel)dv.FindControl("TogglePanel");
        togglepanel.Attributes.Add("style", "display:none");

        int imageidlength = image.ClientID.Length;
        string shortImageClientID = image.ClientID.Substring(imageidlength - 25, 25);
        string shortPanelClientID = togglepanel.ClientID.Substring(togglepanel.ClientID.Length - 25, 25);
        image.Attributes.Add("onClick", "togglePanel('" + shortImageClientID + "','" + shortPanelClientID + "');");


        ListView lv = (ListView)dv.FindControl("InnerDomainsListView");
        var domains = DomainSequencesDataHandler.getDomainsByClusterID(clusterid);

        lv.DataSource = domains;
        lv.DataBind();

        if (domains.Count == 0)
        {
            LinkButton downloadallbutton = (LinkButton)dv.FindControl("DownloadAllLinkButton");
            downloadallbutton.Visible = false;

            Label nodomains = (Label)dv.FindControl("NoDomainsLabel");
            nodomains.Visible = true;            
        }
        
        
        CodeControls codeControls = new CodeControls();

        parameters["urlrootpath"] = WebTools.getURLRootPath(Request.Url.OriginalString, 0);

        parameters["TemplateType"] = "BPStartEnd";
        PlaceHolder bpstartend = (PlaceHolder)dv.FindControl("BPStartEnd");
        bpstartend.Controls.Add(codeControls.getControl(parameters));
        bpstartend.DataBind();

        parameters["TemplateType"] = "FamilyName";
        PlaceHolder familyName = (PlaceHolder)dv.FindControl("FamilyName");
        familyName.Controls.Add(codeControls.getControl(parameters));
        familyName.DataBind();

        parameters["TemplateType"] = "Phylum";
        PlaceHolder phylum = (PlaceHolder)dv.FindControl("Phylum");
        phylum.Controls.Add(codeControls.getControl(parameters));
        phylum.DataBind();

        parameters["TemplateType"] = "OrgLineageDetailsView";
        PlaceHolder lineagefield = (PlaceHolder)dv.FindControl("Lineage");
        lineagefield.Controls.Add(codeControls.getControl(parameters));
        lineagefield.DataBind();

        parameters["TemplateType"] = "PathwayType";
        PlaceHolder pathwaytype = (PlaceHolder)dv.FindControl("PathwayType");
        pathwaytype.Controls.Add(codeControls.getControl(parameters));
        pathwaytype.DataBind();

        parameters["TemplateType"] = "antiSmashLink";
        PlaceHolder antismash = (PlaceHolder)dv.FindControl("antiSmash");
        antismash.Controls.Add(codeControls.getControl(parameters));
        antismash.DataBind();

        parameters["TemplateType"] = "SequenceExtractionStatus";
        PlaceHolder domainsequencestatus = (PlaceHolder)dv.FindControl("DomainSequenceStatus");
        domainsequencestatus.Controls.Add(codeControls.getControl(parameters));
        domainsequencestatus.DataBind();

        parameters["TemplateType"] = "Contributor";
        PlaceHolder contributorfield = (PlaceHolder)dv.FindControl("Contributor");
        contributorfield.Controls.Add(codeControls.getControl(parameters));
        contributorfield.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {        
        Response.Redirect("~/Contribute/EditCluster.aspx?" + "clusterid=" + clusterid);
    }

    protected void PreviousButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("~/ClusterDetails.aspx?clusterid=" + Clusters.getPreviousClusterID(clusterid));
    }

    protected void NextButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("~/ClusterDetails.aspx?clusterid=" + Clusters.getNextClusterID(clusterid));
    }
}