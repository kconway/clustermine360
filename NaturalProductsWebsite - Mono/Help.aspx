﻿<%@ Page Title="Help" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="Help" CodeBehind="Help.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="main maxwidth indent">
        <h2>Help</h2>
        <h3>Contributing to the Database</h3>
        <p>A user account is needed to contribute to the database. No user account is needed to view or download data from the database. To sign up for a free account, select the log in link at the top of any page then follow the link to the registration page.</p>
        <img src="Images/About/register.png" class="border indent" alt="Registration Form" />

        <h3>Adding a Compound Family</h3>
        <p>To add a compound family, select the 'Add' button in the top menu of any page then select 'Add a Compound Family' from the list of links.</p>

        <p>First, enter the name of the family you wish to add. If the name is similar in spelling to another family in the database, a checkbox list will appear and ask you if you want to proceed with the current name, or if the compound family you are trying to add is already present in the database under an alternate spelling.</p>
        <img src="Images/About/addcf_name.png" class="border indent" alt="Name" />
        <p>On the 'Pathway Type' screen, you can select the that pathways that are used to produce the compound.</p>
        <img src="Images/About/addcf_ptype.png" class="border indent" alt="Pathway Type" />
       <p>The 'Select Image' screen allows you to query the ChemSpider database for a image of the structure. Alternately, an image can be uploaded or generated from a SMILES string.</p>
        <img src="Images/About/addcf_image.png" class="border indent" alt="Image" />
       
       
       <p>On the 'Synonyms' screen, it is possible to add synonyms for the compound family. The ChemSpider database can be queried to view a list of possible synonyms from which you can choose those that are applicable.</p>
        <img src="Images/About/addcf_synonyms.png" class="border indent" alt="Synonyms" />
       <p>On the 'Relationships' screen, related compound families can be selected.</p>
        <img src="Images/About/addcf_related.png" class="border indent" alt="Relationships" />
        <p>The compound family has now been added. You can view its details page, add a cluster for that family, or restart the process to add a new family!</p>
        <img src="Images/About/addcf_final.png" class="border indent" alt="Final" />       
        
        <h3>Adding a Cluster</h3>
        <p>To add a cluster, select the Add button at the top of any page then select the 'Add Cluster' link. Choose the cluster's compound family from the dropdown list. If the compound family is not present in the list, add it then come back to add the cluster. Enter a valid GenBank/RefSeq ID in the textbox - only GenBank/RefSeq DNA sequence records will be accepted. Finally, if the cluster is located within a large sequence, select the 'Partial Sequence' radio button and indicate the start and end positions of the cluster in the file.</p>
        <img src="Images/About/addcluster.png" alt="Add Cluster" class="border indent" />

        <h3>Adding a Non-Cluster Sequence</h3>
        <p>
        Large sequences, such as genomes, can be submitted to the database for analysis by antiSMASH after which the PKS/NRPS domains will be extracted and saved into the sequence repository. All that is needed is a valid GenBank ID for GenBank DNA record.</p>
        <img src="Images/About/addnoncluster.png" alt="Non-Cluster" class="border indent" />
        <h3>Downloading Sequences</h3>

        <h4><b>Sequences can be downloaded from several different places:</b></h4>

        <ul class="definitions">        
        <li><strong><i>Sequence Repository Downloads Page</i></strong>
            <br />
            Sequences that match a variety of criteria can be downloaded in bulk from the Sequence Repository Details Page. Users can choose to filter the sequences by phylum, pathway type(s), domain type, and domain characteristics (for A/AT and KR domains). For pathway types, there is a choice to 'Match All' or 'Match Any'. The 'Match All' option will only return sequences from clusters that match all of the pathway types selected while the 'Match Any' will return sequences that come from clusters that match any of the select pathway types. All of the sequences are output in a FASTA file. Please note that it may take 15-20 minutes before the file is ready to download.
            <br /><br />
            <img src="Images/About/download_domains_bulk.png" alt="Download domains from Sequence Repository Downloads Page" class="border indent" />
            </li>
            <li><strong><i>Cluster Details Page</i></strong>
            <br />
            Sequences for a given cluster can be downloaded from its Details page. The sequences for each domain can be downloaded individually or all of the domains in the cluster can be downloaded together in a zip file.
            <br /><br />
                <img src="Images/About/download_domains_cluster_details.png" alt="Download domains from Cluster Details Page" class="border indent" />            
            </li>
            <li><strong><i>Sequence Repository Details Page</i></strong> <br />
            Sequences can also be downloaded from the Sequence Repository Details Page. This page lists all of the clusters with sequences in the Sequence Repository. These include clusters that are not in the main database as they were extracted from large sequences such as genomes and are therefore not linked to a compound family. The download options are similar to the Cluster Details page. Individual domains can be downloaded or all of the extracted domains in the cluster can be downloaded at once in a zip file.
            <br /> <br />
                <img src="Images/About/download_domains_repository_details.png" alt="Download domains from Sequence Repository Details Page" class="border indent" />
            </li>
            </ul>

      


       </div>
</asp:Content>