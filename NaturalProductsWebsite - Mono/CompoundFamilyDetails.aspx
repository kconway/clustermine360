﻿<%@ Page Title="Compound Family Details" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" Inherits="CompoundFamilyDetails" CodeBehind="CompoundFamilyDetails.aspx.cs" %>
<%@ Register Src="~/Controls/PharmActionsControl.ascx" TagPrefix="uc" TagName="PharmActionsControl" %>
<%@ Register Src="~/Controls/PathwayTypesControl.ascx" TagPrefix="uc" TagName="PathwayTypesControl" %>

<%@ Register Src="~/Controls/ClusterCharacteristicsControl.ascx" TagPrefix="uc" TagName="ClusterCharacteristicsControl" %>
<%@ Register Src="~/Controls/ClusterGridviewControl.ascx" TagPrefix="uc" TagName="ClusterGridviewControl" %>
<%@ Register Src="~/Controls/LastModifiedControl.ascx" TagPrefix="uc" TagName="LastModifiedControl" %>
<%@ Register Src="~/Controls/RelatedFamiliesControl.ascx" TagPrefix="uc" TagName="RelatedFamiliesControl" %>
<%@ Register Src="~/Controls/SynonymsControl.ascx" TagPrefix="uc" TagName="SynonymsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script src="Scripts/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
<link href="Styles/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
   <asp:ImageButton ID="NextButton" runat="server" 
        CssClass="navbuttons" AlternateText="Next" 
        ImageUrl="~/Images/icon_move_right.png" onclick="NextButton_Click" /> <asp:ImageButton ID="PreviousButton" runat="server" CssClass="navbuttons" 
        AlternateText="Previous" ImageUrl="~/Images/icon_move_left.png" 
        onclick="PreviousButton_Click" />
    <h2>
        Compound Family Details
    </h2>
    <br />
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="CompoundFamilies"
        SelectMethod="getCompoundFamilyByID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="cfid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="dv" runat="server" AutoGenerateRows="False" CellPadding="4"
        DataKeyNames="ID" DataSourceID="ObjectDataSource1" ForeColor="#333333" GridLines="None"
        Width="250px" EnableModelValidation="True" OnDataBound="dv_DataBound">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="FamilyName" HeaderText="Family Name"  />
            <asp:TemplateField HeaderText="Synonyms">
                <ItemTemplate>
                    <uc:SynonymsControl runat="server" ID="ucSynonymsControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField HeaderText="Related Compound Families">
                <ItemTemplate>
                    <uc:RelatedFamiliesControl runat="server" ID="ucRelatedFamiliesControl" mode="0" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pathway Type" >
                <ItemTemplate>
               <uc:PathwayTypesControl runat="server" ID="ucPathwayTypesControl" mode="0"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Characteristics" >
                <ItemTemplate>
<uc:ClusterCharacteristicsControl runat="server" ID="ucClusterCharacteristicsControl" mode="0" />
                </ItemTemplate>
                </asp:TemplateField>
   <asp:TemplateField HeaderText="Pharmacological Identifiers" >
                <ItemTemplate>
                    <asp:HyperLink ID="PubChemLink" runat="server" Target="_blank">Link to PubChem</asp:HyperLink>
<uc:PharmActionsControl runat="server" ID="ucPharmActionsControl" />
                </ItemTemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="Image">
                <ItemTemplate>                   
                <a href="#" onclick="$('#ImageViewer').dialog('open');">
                    <asp:Image runat="server" ID="StructureImage" Width="250px" Visible="true" ImageUrl="StructureImageHandler.ashx"
                        AlternateText="Structure" /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contributor">
                <ItemTemplate>
                    <asp:PlaceHolder ID="ContributorPlaceHolder" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Clusters">
                <ItemTemplate>
                    <uc:ClusterGridviewControl runat="server" ID="ucClusterGridviewControl" />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <asp:Button ID="Button1" runat="server" CssClass="submitButton" Text="Edit" OnClick="Button1_Click" />
    <asp:Button runat="server" CssClass="submitButton" Text="Close" OnClientClick="window.close();"
        ID="closeButton" />
    <br />
    <uc:LastModifiedControl runat="server" ID="ucLastModifiedControl" />
    <div id="ImageViewer" class="page main">
        <asp:Image ID="LargeImage" Width="500px" runat="server" />    
    </div>
</asp:Content>