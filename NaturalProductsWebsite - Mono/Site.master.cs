﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    

        HeaderTitle.Text = ConfigurationManager.AppSettings["WebsiteTitle"];
        if (HttpContext.Current.User.Identity.IsAuthenticated)
        {
            var Profile = HttpContext.Current.Profile;
            if (Profile != null)
            {
                Label lbl = (Label)(LoginView1.FindControl("FirstName"));
                lbl.Text = (string)Profile.GetPropertyValue("FirstName");
            }
        }
        else
        {
            HtmlAnchor link = (HtmlAnchor)LoginView1.FindControl("HeadLoginStatus");
            link.HRef = "~/Account/Login.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.RawUrl);
        }
    }
}