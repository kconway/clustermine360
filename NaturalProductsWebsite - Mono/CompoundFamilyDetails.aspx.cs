﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

public partial class CompoundFamilyDetails : System.Web.UI.Page
{
    private int cfid;

    private string initializeMainScript(string controlid)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("$(\"#ImageViewer\").dialog({ autoOpen: false, buttons: { \"Close\": function () { $(this).dialog(\"close\"); } }, minWidth: 550, minHeight: 550, modal: true }).parent().appendTo(jQuery(\"form:first\"));");

        //sb.AppendLine("$(\"#"+controlid+"\").click(function () { $(\"#ImageViewer\").dialog('open'); });");

        return sb.ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["cfid"] != "" && Request.QueryString["cfid"] != null)
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
            if (!IsPostBack)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    if (!db.CompoundFamilies.Exists(x => x.ID == cfid))
                    {
                        Response.Redirect("~/CompoundFamilies.aspx");
                    }
                }
                dv.RowStyle.VerticalAlign = VerticalAlign.Top;
                dv.Width = Unit.Percentage(99.00);
                dv.FieldHeaderStyle.Width = Unit.Percentage(20.00);
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void dv_DataBound(object sender, EventArgs e)
    {
        CodeControls codeControls = new CodeControls();
        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters["TemplateType"] = "CFContributor";
        PlaceHolder contributorPlaceholder = (PlaceHolder)dv.FindControl("ContributorPlaceHolder");
        contributorPlaceholder.Controls.Add(codeControls.getControl(parameters));
        contributorPlaceholder.DataBind();

        DetailsView detailsview = (DetailsView)sender;
        HyperLink link = (HyperLink)dv.FindControl("PubChemLink");
        link.NavigateUrl = "http://www.ncbi.nlm.nih.gov/sites/entrez/?db=pccompound&term=" + HttpUtility.UrlEncode(CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName);
        link.Font.Underline = false;

        Image image = (Image)dv.FindControl("StructureImage");

        CompoundFamilyImage cfimage = CompoundFamilies.getCompoundFamilyImage(cfid);
        if (cfimage != null && cfimage.Image != null && cfimage.Image.Length != 0)
        {
            image.ImageUrl = "~/StructureImageHandler.ashx?cfid=" + cfid;
            image.Visible = true;
        }
        else
        {
            image.Visible = false;
        }
        string scripttoadd = initializeMainScript(image.ClientID);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "loadUI", scripttoadd, true);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(User.IsInRole("restricted")))
        {
            Button1.Visible = true;
        }
        else
        {
            Button1.Visible = false;
        }

        insertLineageScript();

        CompoundFamilyImage cfimage = CompoundFamilies.getCompoundFamilyImage(cfid);
        if (cfimage != null && cfimage.Image != null && cfimage.Image.Length != 0)
        {
            LargeImage.ImageUrl = "~/StructureImageHandler.ashx?cfid=" + cfid;
            LargeImage.Visible = true;
        }
        else
        {
            LargeImage.Visible = false;
        }
    }

    protected void insertLineageScript()
    {
        StringBuilder lineageScript = new StringBuilder();
        lineageScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
        lineageScript.AppendLine("function toggleLineagePanel(imageID, phylumPanelID, lineagePanelID) {");
        lineageScript.AppendLine("phylumPanelID = formatID(phylumPanelID);");
        lineageScript.AppendLine("lineagePanelID = formatID(lineagePanelID);");
        lineageScript.AppendLine("imageID = formatID(imageID);");
        lineageScript.AppendLine("if ($(phylumPanelID).is(\":visible\")) {");
        lineageScript.AppendLine("$(phylumPanelID).hide('fast');");
        lineageScript.AppendLine("$(lineagePanelID).show('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_remove.gif\");");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("else {");
        lineageScript.AppendLine("$(lineagePanelID).hide('fast');");
        lineageScript.AppendLine("$(phylumPanelID).show('fast');");
        lineageScript.AppendLine("$(imageID).attr(\"src\", \"Images/action_add2.gif\")");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine();
        lineageScript.AppendLine("function formatID(id) {");
        lineageScript.AppendLine("var formattedID = '\"' + \"[id$=\" + \"'\" + id + \"'\" + \"]\" + '\"'");
        lineageScript.AppendLine("return formattedID;");
        lineageScript.AppendLine("}");
        lineageScript.AppendLine("</script>");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "lineageScript", lineageScript.ToString(), false);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Contribute/EditCompoundFamily.aspx?" + "cfid=" + Request.QueryString["cfid"]);
    }

    protected void PreviousButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/CompoundFamilyDetails.aspx?cfid=" + CompoundFamilies.getPreviousCFID(cfid));
    }

    protected void NextButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/CompoundFamilyDetails.aspx?cfid=" + CompoundFamilies.getNextCFID(cfid));
    }
}