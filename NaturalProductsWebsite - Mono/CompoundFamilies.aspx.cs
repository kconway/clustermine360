﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

public partial class CompoundFamiliesPage : System.Web.UI.Page
{
    Dictionary<char, int> recordsPerLetter;
    int used = 0;
    int approxNumPerColumn = 0;
    int numColumns = 4;


    protected void Page_Init(object sender, EventArgs e)
    {
        bool includeimage = true;
        if (Request.QueryString["showimages"] != null)
        {
            if (Request.QueryString["showimages"].ToString() == "no")
            {
                includeimage = false;
            }
        }
        else
        {
            includeimage = true;
        }
        recordsPerLetter = new Dictionary<char, int>();
        string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<char> existingAlpha = new List<char>();
        List<CompoundFamily> families = CompoundFamilies.getCompoundFamilies();

        foreach (char firstLetter in alpha)
        {
            int numRecords = families.Where(x => x.FamilyName.Substring(0, 1) == firstLetter.ToString()).Count();
            recordsPerLetter.Add(firstLetter, numRecords);
            if (numRecords > 0)
            {
                existingAlpha.Add(firstLetter);
            }
        }
        existingAlpha.Sort();
        int numnumeric = families.Where(x => Char.IsNumber(x.FamilyName.ToCharArray()[0])).Count();
        if (numnumeric > 0)
        {
            recordsPerLetter.Add('#', numnumeric);
            existingAlpha.Insert(0, '#');
        }

        

        approxNumPerColumn = (families.Count + existingAlpha.Count) / numColumns;
        mainList.ItemTemplate = new CompoundFamilyAlphabetTemplate(families, includeimage);
        mainList.ItemDataBound += dataList_ItemDataBound;
        mainList.DataSource = existingAlpha;
        mainList.DataBind();

        if (includeimage)
        {
            ViewImagesToggle.NavigateUrl = "~/CompoundFamilies.aspx?showimages=no";
            ViewImagesToggle.Text = "Hide Structures";

        }
        else
        {
            ViewImagesToggle.NavigateUrl = "~/CompoundFamilies.aspx";
            ViewImagesToggle.Text = "Display Structures";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var list = CompoundFamilies.getNamesOfCompoundFamilies();
        //list.RemoveAll(x => x.Contains("Unknown"));
        NumCompoundFamilies.Text = list.Count.ToString();
    }

    protected void dataList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            used = used + recordsPerLetter[(char)e.Item.DataItem] + 1; // +1 is for header
            if (used > approxNumPerColumn)
            {
                used = 0;
            }
        }
        else if (e.Item.ItemType == ListItemType.Separator)
        {
            if (used != 0)
            {
                e.Item.Controls.Clear();
            }
        }
    }
}