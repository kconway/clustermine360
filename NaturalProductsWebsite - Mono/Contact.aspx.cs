﻿using System;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Web;

public partial class Contact : System.Web.UI.Page
{
    private static readonly ILog log = LogManager.GetLogger(typeof(Contact));
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        Page.Validate();

        if (!Page.IsValid)
        {
            return;
        }
        bool error = false;
        try
        {
            Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], SubjectText.Text, "From: " + EmailText.Text + "<br />" +  MessageText.Text);
            
        }
        catch (Exception ex)
        {
            log.Debug("Tried to send contact email." + Environment.NewLine + ex.ToString());
            error = true;
            ErrorLabel.Visible = true;
        }

        if (error == false)
        {
Response.Redirect("~/Account/Success.aspx?type=" + HttpUtility.UrlEncode("messagesent"));
        }
    }
}