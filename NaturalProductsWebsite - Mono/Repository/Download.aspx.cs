﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace NaturalProductsWebsite.Repository
{
    public partial class Download : System.Web.UI.Page
    {
        string filepath;
        string downloadguid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["downloadguid"] != null)
            {
                downloadguid = Request.QueryString["downloadguid"].ToString();
                string filename = downloadguid + ".fasta";
                string root = ConfigurationManager.AppSettings["DownloadTempFolderLocation"];
                filepath = root + filename;

                if (File.Exists(filepath))
                {
                    StatusLabel.Text = "Ready";
                    StatusLabel.CssClass = "noerror";
                    DownloadLinkButton.Visible = true;
                    NoticeLabel.Visible = false;                    
                }
                else
                {
                    NoticeLabel.Visible = true;
                    DataClassesDataContext db = new DataClassesDataContext();
                    var record = db.Downloads.Where(x => x.DownloadGuid == downloadguid).SingleOrDefault();
                    if (record != null)
                    {
                        if (record.Status == 0)
                        {
                            StatusLabel.Text = "Processing - Please check again in 15-20 minutes.";
                        }
                        else if (record.Status == 20)
                        {
                            StatusLabel.Text = "No sequences matched your request.";
                            StatusLabel.CssClass = "error";
                        }
                        else if (record.Status == 25)
                        {
                            StatusLabel.Text = "There was an error processing your request. Please go to the <a href=\"DownloadSequences.aspx\">Download Request</a> page and resubmit your request.";
                            StatusLabel.CssClass = "error";
                        }
                    }
                    else
                    {
                        StatusLabel.Text = "There was a problem with your request. Please return to the <a href=\"DownloadSequences.aspx\">Download Request</a> page and resubmit your request.";
                        StatusLabel.CssClass = "error";
                    }
                    db.Dispose();
                }
            }
            else
            {
                Response.Redirect("~/Repository/DownloadSequences.aspx");
            }
        }
        protected void ServeFileToBrowser()
        {
            if (File.Exists(filepath))
            {
                FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
                long filesize = fs.Length;
                byte[] filebuffer = new byte[(int)filesize];
                fs.Read(filebuffer, 0, (int)filesize);
                fs.Close();

                Response.Clear();
                Response.ContentType = "text/plain";
                string filename = "sequences_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".fasta";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(filebuffer);
                Response.Flush();
                Response.End();
            }
            else
            {
                Response.Redirect("~/Repository/Download.aspx?downloadguid=" + downloadguid);
            }
        }

        protected void DownloadLinkButton_Click(object sender, EventArgs e)
        {
            ServeFileToBrowser();
        }
    }
}