﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

namespace NaturalProductsWebsite
{
    public partial class RepositorySummary : System.Web.UI.Page
    {
        public List<SequenceGroup> ProcessedSequences;

        protected void Page_Init(object sender, EventArgs e)
        {
            ProcessedSequences = DomainSequencesDataHandler.GetGroupedDomains().OrderBy(x => x.PathwayType).ThenBy(y => y.Phylum).ThenBy(z => z.Organism).ToList();

            PathWayTypeSummary.DataSource = ProcessedSequences.Select(y => y.PathwayType).Distinct().OrderBy(x => x).ToList().ToDictionary(z => z, z => ProcessedSequences.Where(q => q.PathwayType == z));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PathWayTypeSummary.DataBind();

            FillDomainTypeByClusterTypeSummaryTable();
            FillClusterTypeByPhylumAndOrganismTable();
        }

        private static string formatforzeroes(int num)
        {
            string text = "";
            if (num != 0)
            {
                text = num.ToString();
            }
            return text;
        }

        private void FillDomainTypeByClusterTypeSummaryTable()
        {
            var ptypes = ProcessedSequences.Select(y => y.PathwayType).Distinct().OrderBy(x => x).ToList();
            var dtypes = ProcessedSequences.SelectMany(z => z.Items).Select(q => q.DomainType).Distinct().OrderBy(x => x).ToList();

            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            cell.Text = "Domain Type / Pathway Type";
            cell.CssClass = "borderbottom borderright borderleft bordertop";
            row.Cells.Add(cell);
            Panel rotatedtext;
            foreach (string ptype in ptypes)
            {
                cell = new TableCell();
                rotatedtext = new Panel();
                rotatedtext.CssClass = "rotate_text";
                rotatedtext.Controls.Add(new Literal() { Text = ptype });
                cell.Controls.Add(rotatedtext);
                cell.CssClass = "rotated_cell borderbottom bordertop";
                row.Cells.Add(cell);
            }

            cell = new TableCell();
            rotatedtext = new Panel();
            rotatedtext.CssClass = "rotate_text";
            rotatedtext.Controls.Add(new Literal() { Text = "Total" });
            cell.Controls.Add(rotatedtext);
            cell.CssClass = "rotated_cell borderbottom borderleft borderright bordertop";
            row.Cells.Add(cell);

            DomainTypeByClusterTypeSummaryTable.Rows.Add(row);
            TableRow datarow;
            foreach (var dtype in dtypes)
            {
                datarow = new TableRow();
                cell = new TableCell();
                cell.Text = dtype;
                cell.CssClass = " dottedborder borderright borderleft";
                datarow.Cells.Add(cell);

                foreach (var ptype in ptypes)
                {
                    cell = new TableCell();
                    cell.CssClass = "dottedborder";
                    cell.Text = formatforzeroes(ProcessedSequences.SelectMany(x => x.Items).Where(y => y.DomainType == dtype && y.PathwayType == ptype).ToList().Count);
                    datarow.Cells.Add(cell);
                }
                cell = new TableCell();
                cell.CssClass = "dottedborder borderleft borderright";
                cell.Text = formatforzeroes(ProcessedSequences.SelectMany(x => x.Items).Where(y => y.DomainType == dtype).ToList().Count);
                datarow.Cells.Add(cell);

                DomainTypeByClusterTypeSummaryTable.Rows.Add(datarow);
            }

            datarow = new TableRow();
            cell = new TableCell();
            cell.Text = "Total";
            cell.CssClass = " dottedborder bordertop borderright borderleft borderbottom";
            datarow.Cells.Add(cell);

            foreach (var ptype in ptypes)
            {
                cell = new TableCell();
                cell.Text = formatforzeroes(ProcessedSequences.SelectMany(x => x.Items).Where(y => y.PathwayType == ptype).ToList().Count);
                cell.CssClass = " dottedborder bordertop borderbottom";
                datarow.Cells.Add(cell);
            }

            cell = new TableCell();
            cell.Text = formatforzeroes(ProcessedSequences.SelectMany(x => x.Items).ToList().Count);
            cell.CssClass = " dottedborder bordertop borderleft borderright borderbottom";
            datarow.Cells.Add(cell);
            DomainTypeByClusterTypeSummaryTable.Rows.Add(datarow);
        }

        private void FillClusterTypeByPhylumAndOrganismTable()
        {
            var ptypes = ProcessedSequences.Select(y => y.PathwayType).Distinct().OrderBy(x => x).ToList();
            var phyla = ProcessedSequences.Select(z => z.Phylum).Distinct().OrderBy(x => x).ToList();

            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            cell.Text = "Phylum and Organism / Pathway Type";
            cell.CssClass = "borderbottom borderright borderleft bordertop";
            row.Cells.Add(cell);
            Panel rotatedtext;
            foreach (string ptype in ptypes)
            {
                cell = new TableCell();
                rotatedtext = new Panel();
                rotatedtext.CssClass = "rotate_text";
                rotatedtext.Controls.Add(new Literal() { Text = ptype });
                cell.Controls.Add(rotatedtext);
                cell.CssClass = "rotated_cell borderbottom bordertop";
                row.Cells.Add(cell);
            }

            cell = new TableCell();
            rotatedtext = new Panel();
            rotatedtext.CssClass = "rotate_text";
            rotatedtext.Controls.Add(new Literal() { Text = "Total" });
            cell.Controls.Add(rotatedtext);
            cell.CssClass = "rotated_cell borderbottom borderleft borderright bordertop";
            row.Cells.Add(cell);

            ClusterTypeByPhylumAndOrganismTable.Rows.Add(row);
            TableRow datarow;
            foreach (var phylum in phyla)
            {
                datarow = new TableRow();
                cell = new TableCell();
                cell.Text = phylum;
                cell.CssClass = "bordertop borderbottom borderright borderleft bold";
                datarow.Cells.Add(cell);

                foreach (var ptype in ptypes)
                {
                    cell = new TableCell();
                    cell.CssClass = "dottedborder bordertop borderbottom bold";
                    cell.Text = formatforzeroes(ProcessedSequences.Where(y => y.Phylum == phylum && y.PathwayType == ptype).Select(x => x.GroupingID).Distinct().ToList().Count);
                    datarow.Cells.Add(cell);
                }
                cell = new TableCell();
                cell.CssClass = "borderleft borderright bordertop borderbottom bold";
                cell.Text = formatforzeroes(ProcessedSequences.Where(y => y.Phylum == phylum).Select(x => x.GroupingID).Distinct().ToList().Count);
                datarow.Cells.Add(cell);

                ClusterTypeByPhylumAndOrganismTable.Rows.Add(datarow);

                // add organisms
                var orgs = ProcessedSequences.Where(y => y.Phylum == phylum).Select(x => x.Organism).Distinct().OrderBy(x => x).ToList();
                foreach (var org in orgs)
                {
                    datarow = new TableRow();
                    cell = new TableCell();
                    cell.Text = org;
                    cell.CssClass = "dottedborder borderright borderleft italics";
                    datarow.Cells.Add(cell);

                    foreach (var ptype in ptypes)
                    {
                        cell = new TableCell();
                        cell.CssClass = "dottedborder";
                        cell.Text = formatforzeroes(ProcessedSequences.Where(y => y.Organism == org && y.PathwayType == ptype).Select(x => x.GroupingID).Distinct().ToList().Count);
                        datarow.Cells.Add(cell);
                    }
                    cell = new TableCell();
                    cell.CssClass = "dottedborder borderleft borderright";
                    cell.Text = formatforzeroes(ProcessedSequences.Where(y => y.Organism == org).Select(x => x.GroupingID).Distinct().ToList().Count);
                    datarow.Cells.Add(cell);

                    ClusterTypeByPhylumAndOrganismTable.Rows.Add(datarow);
                }
            }

            datarow = new TableRow();
            for (int i = 0; i < (ptypes.Count + 2); i++)
            {
                cell = new TableCell();
                cell.Text = "";
                cell.CssClass = "bordertop";
                datarow.Cells.Add(cell);
            }

            ClusterTypeByPhylumAndOrganismTable.Rows.Add(datarow);
        }

        protected void PathwayTypeSummary_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                KeyValuePair<string, IEnumerable<SequenceGroup>> datacontainer = (KeyValuePair<string, IEnumerable<SequenceGroup>>)dataItem.DataItem;
                Label typelabel = (Label)dataItem.FindControl("PathwayTypeLabel");
                string pathwaytype = datacontainer.Key;
                typelabel.Text = pathwaytype;

                Label phylumlabel = (Label)dataItem.FindControl("PhylumLabel");
                phylumlabel.Text = datacontainer.Value.Select(x => x.Phylum).Distinct().ToList().Count.ToString();

                Label orglabel = (Label)dataItem.FindControl("OrganismLabel");
                orglabel.Text = datacontainer.Value.Select(x => x.Organism).Distinct().ToList().Count.ToString();

                Label clusterlabel = (Label)dataItem.FindControl("ClusterLabel");
                clusterlabel.Text = datacontainer.Value.Select(x => x.GroupingID).Distinct().ToList().Count.ToString();

                Label domainlabel = (Label)dataItem.FindControl("DomainLabel");
                domainlabel.Text = datacontainer.Value.SelectMany(x => x.Items).ToList().Count.ToString();
            }
        }

        protected void PathwayTypeSummary_DataBound(object sender, EventArgs e)
        {
            Label ptotallabel = (Label)PathWayTypeSummary.FindControl("PhylumTotalLabel");
            ptotallabel.Text = ProcessedSequences.Select(x => x.Phylum).Distinct().ToList().Count.ToString();

            Label ototallabel = (Label)PathWayTypeSummary.FindControl("OrganismTotalLabel");
            ototallabel.Text = ProcessedSequences.Select(x => x.Organism).Distinct().ToList().Count.ToString();

            Label ctotallabel = (Label)PathWayTypeSummary.FindControl("ClusterTotalLabel");
            ctotallabel.Text = ProcessedSequences.Select(x => x.GroupingID).Distinct().ToList().Count.ToString();

            Label dtotallabel = (Label)PathWayTypeSummary.FindControl("DomainTotalLabel");
            dtotallabel.Text = ProcessedSequences.SelectMany(x => x.Items).ToList().Count.ToString();
        }       
    }
}