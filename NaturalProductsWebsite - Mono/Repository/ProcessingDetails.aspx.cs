﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
using Utilities;

namespace NaturalProductsWebsite
{
    public partial class ProcessingDetails : System.Web.UI.Page
    {
        
        public List<ProcessEntity> UnprocessedSequences;

        protected void Page_Init(object sender, EventArgs e)
        {
            UnprocessedSequences = DomainSequencesDataHandler.GetUnprocessedDomains();
            if (UnprocessedSequences.Count == 0)
            {
                NoneProcessingLabel.Visible = true;
            }
           
            
            gv.DataSource = UnprocessedSequences;            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewTemplate.applyStyletoGridView(gv);
            gv.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
            gv.RowStyle.HorizontalAlign = HorizontalAlign.Left;
            gv.DataBind();           
        }       
      
       
        protected void StatusLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            string status = "";
            GridViewRow container = (GridViewRow)label.NamingContainer;
            int? antismashstatus = (int?)DataBinder.Eval(container.DataItem, "ClusterBase.AntiSmashStatus");
            int? extractionstatus = (int?)DataBinder.Eval(container.DataItem, "ClusterBase.SequenceExtractionStatus");
            bool delaystatus = (bool)DataBinder.Eval(container.DataItem, "ClusterBase.Delayed");
            if (extractionstatus.HasValue && extractionstatus == 1)
            {
                status = "antiSMASH analysis has been completed. <br /> Sequence extraction is currently underway.";
            }
            else if (antismashstatus.HasValue && antismashstatus == 5)
            {
                status = "antiSMASH analysis has been completed.  <br /> Sequence extraction and submission to the repository should begin shortly.";
            }
            else if ((antismashstatus.HasValue) && (antismashstatus < 5) )
            {
                if (delaystatus == false)
                {
                    status = "antiSMASH analysis is currently underway.  <br /> This may take several hours to complete.";
                }
                else
                {
                    status = "antiSMASH analysis for this sequence is currently on hold.  <br /> It has been queued and processing will begin when capacity permits.";
                }
            }

            label.Text = status;
        }

        protected void OrganismLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            string organism = (!String.IsNullOrEmpty((string)DataBinder.Eval(container.DataItem, "ClusterBase.OrgName"))) ? (string)DataBinder.Eval(container.DataItem, "ClusterBase.OrgName") : "N/A";
            label.Text = organism;
        }

        protected void CompoundFamilyLiteral_DataBind(Object sender, EventArgs e)
        {
            Literal label = (Literal)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            ProcessEntityType type = (ProcessEntityType)DataBinder.Eval(container.DataItem, "Type");
            string cfname = "N/A";
            if (type == ProcessEntityType.Cluster)
            {
                int clusterid = (int)DataBinder.Eval(container.DataItem, "ID");
                cfname = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).FamilyName;
            }
            label.Text = cfname;
        }

        protected void HeaderLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            if (((DataBinder.Eval(container.DataItem, "BPStart")) != null) && ((DataBinder.Eval(container.DataItem, "BPEnd")) != null))
            {
                // do nothing
            }
            else
            {
                label.Visible = false;
            }
        }

        protected void GenBankStartEnd_DataBind(Object sender, EventArgs e)
        {
            Literal label = (Literal)sender;
            GridViewRow container = (GridViewRow)label.NamingContainer;
            if (((DataBinder.Eval(container.DataItem, "BPStart")) != null) && ((DataBinder.Eval(container.DataItem, "BPEnd")) != null))
            {
                int bpstartnum = (int)(DataBinder.Eval(container.DataItem, "BPStart"));
                int bpendnum = (int)(DataBinder.Eval(container.DataItem, "BPEnd"));

                label.Text = FormatBPStartEnd(bpstartnum, bpendnum);
            }
            else
            {
                label.Visible = false;
            }
        }

        protected void GenBankLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            GridViewRow container = (GridViewRow)link.NamingContainer;
            string genbankaccession = (string)(DataBinder.Eval(container.DataItem, "ClusterBase.GenbankAccession"));
            int genbankversion = (int)DataBinder.Eval(container.DataItem, "ClusterBase.GenbankVersion");
            string display = genbankaccession;
            if (genbankversion != 0)
            {
                display = genbankaccession + "." + genbankversion.ToString();
            }

            link.Text = display;
            link.NavigateUrl = NaturalProductsWebsite.Code.NCBIUtils.GetNCBIDisplayURL() + display;
            link.Target = "_blank";
        }

        protected void PhylumLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            int phylumid = (int)DataBinder.Eval(container.DataItem, "ClusterBase.PhylumID");
            label.Text = Phyla.getPhylumName(phylumid);
        }

        private string FormatBPStartEnd(int bpstartnum, int bpendnum)
        {
            NumberFormatInfo nf = new CultureInfo("en-US", false).NumberFormat;
            nf.NumberDecimalDigits = 0;

            string bpstart = bpstartnum.ToString("N", nf);
            string bpend = bpendnum.ToString("N", nf);

            string fromString = "From " + bpstart;
            string toString = "To " + bpend;
            int amtToAdd = toString.Length - fromString.Length;
            string finalFromString = "From" + "&nbsp;";
            string finalToString = "To" + "&nbsp;";

            if (amtToAdd > 0)
            {
                for (int i = 0; i <= amtToAdd; i++)
                {
                    finalFromString = finalFromString + "&nbsp;";
                }
            }
            else if (amtToAdd < 0)
            {
                finalToString = finalToString + "&nbsp;";
                for (int i = 0; i >= amtToAdd; i--)
                {
                    finalToString = finalToString + "&nbsp;";
                }
            }

            finalFromString = finalFromString + bpstart;
            finalToString = finalToString + bpend;
            return finalFromString + "<br />" + finalToString;
        }

        protected void PathwayType_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;

            GridViewRow container = (GridViewRow)label.NamingContainer;
            string ptype = "N/A";
            ProcessEntityType type = (ProcessEntityType)DataBinder.Eval(container.DataItem, "Type");
            if (type == ProcessEntityType.Cluster)
            {
                int clusterid = (int)DataBinder.Eval(container.DataItem, "ID");
                ptype =  StringTools.SerializeToSemiColonSeparatedList(PathwayTypes.getPathwayTypesByClusterID(clusterid)).Replace(";", "<br />");
            }
            label.Text = ptype;
        }

        protected void NameLabel_DataBind(Object sender, EventArgs e)
        {
            Label label = (Label)sender;
            string name = "";

            GridViewRow container = (GridViewRow)label.NamingContainer;
            int contributorid = (int)DataBinder.Eval(container.DataItem, "ClusterBase.ContributorID");
            UserInfo user = UserInfos.getUserInfoByID(contributorid);
            name = user.First_Name + " " + user.Last_Name;

            label.Text = name;
        }

        protected void LabLink_DataBind(Object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;

            GridViewRow container = (GridViewRow)link.NamingContainer;
            int contributorid = (int)DataBinder.Eval(container.DataItem, "ClusterBase.ContributorID");
            UserInfo user = UserInfos.getUserInfoByID(contributorid);

            link.Text = user.Lab_Name;
            link.Target = "_blank";
            link.NavigateUrl = user.Lab_Url;
        }

       

      

       
    }
}