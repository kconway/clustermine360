﻿<%@ Page Title="Details of Clusters/Sequences in Repository" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="RepositoryDetails.aspx.cs" Inherits="NaturalProductsWebsite.RepositoryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>Details of Clusters/Sequences in Sequence Repository</h2>
   <br />
    <div runat="server" id="detailpanel">
    <h3>Filter by:</h3>
    <br />      
    <div class="floatleftsmall">Compound Family</div><div class="floatleft">
        <asp:DropDownList ID="CompoundFamilyDropDownList" runat="server" 
            AutoPostBack="false" AppendDataBoundItems="True"><asp:ListItem Value="*" Text="*" Selected="True">  </asp:ListItem>
        </asp:DropDownList></div><br class="clear" />
    <div class="floatleftsmall">Phylum</div><div class="floatleft">
        <asp:DropDownList ID="PhylumDropDownList" runat="server" AutoPostBack="false" 
            AppendDataBoundItems="True"><asp:ListItem Value="*" Text="*" Selected="True">  </asp:ListItem>
        </asp:DropDownList></div><br class="clear" />
    <div class="floatleftsmall">Organism</div><div class="floatleft">
        <asp:DropDownList ID="OrganismDropDownList" runat="server" AutoPostBack="false" 
            AppendDataBoundItems="True"><asp:ListItem Value="*" Text="*" Selected="True">  </asp:ListItem>
        </asp:DropDownList></div>
               <br class="clear" />
               <div class="floatleftsmall">
    Pathway Type</div><div class="floatleft"><asp:DropDownList ID="PathwayTypeDropDownList" 
                runat="server" AutoPostBack="false" AppendDataBoundItems="True">
            <asp:ListItem Value="*" Text="*" Selected="True">  </asp:ListItem>
        </asp:DropDownList></div><br class="clear" />
         <div class="floatleftsmall">
    Pathway Type Text Search<br /><small>Separate multiple search terms with a semi-colon<br /><i>Ex: T1PKS;T3PKS</i></small></div><div class="floatleft">
     <asp:TextBox ID="PathwayTypeTextSearchBox" runat="server"></asp:TextBox>
    </div><br class="clear" />
     <div class="floatleftsmall">&nbsp;</div><div class="floatleft">
        <asp:Button ID="FilterButton" runat="server" Text="Filter Results" CssClass="submitButton" /></div>
       <br / class="clear">
       <br />
        <asp:ListView ID="ProcessedDomainsListView" runat="server" OnItemDataBound="ProcessDomainsListView_ItemDataBound"
            OnItemCommand="ProcessedDomains_ItemCommand" OnPagePropertiesChanging="ProcessDomainsListView_PagePropertiesChanging" OnDataBinding="ProcessedDomainsListView_DataBinding">
            <LayoutTemplate>
                <table runat="server" id="HeaderTable" style="padding: 5px; text-align: left">
                    <tr runat="server">
                        <th style="width: 12.5%; text-align: left">
                            <asp:LinkButton ID="AccessionLinkButton" CommandName="Accession" runat="server">Accession #</asp:LinkButton>
                        </th>
                        <th style="width: 15%; text-align: left">
                            <asp:LinkButton ID="PathwayLinkButton" CommandName="PathwayType" runat="server">Pathway Type</asp:LinkButton>
                        </th>
                        <th style="width: 12.5%; text-align: left">
                            <asp:LinkButton ID="CompoundFamilyLinkButton" CommandName="CompoundFamily" runat="server">Compound Family</asp:LinkButton>
                        </th>
                        <th style="width: 12.5%; text-align: left">
                            <asp:LinkButton ID="PhylumLinkButton" CommandName="Phylum" runat="server">Phylum</asp:LinkButton>
                        </th>
                        <th style="width: 20%; text-align: left">
                            <asp:LinkButton ID="OrganismLinkButton" CommandName="Organism" runat="server">Organism</asp:LinkButton>
                        </th>
                        <th style="width: 12.5%; text-align: left">
                            <asp:LinkButton ID="ContributorLinkButton" CommandName="Contributor" runat="server">Contributor</asp:LinkButton>
                        </th>
                        <th style="width: 15%">
                            Details
                        </th>
                    </tr>
                    <tr runat="server" id="itemPlaceholder" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td style="height: 0px">
                        <tr>
                            <td colspan="7">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="GenBankLink" runat="server" Style="display: block;"></asp:HyperLink>
                                <div style="width: 100%" class="toppadding5">
                                    <asp:Literal ID="GenBankStartEndLiteral" runat="server"></asp:Literal></div>
                            </td>
                            <td class="alignleft">
                                <asp:Label ID="PathwayTypeLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="CompoundFamilyNameLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="PhylumLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="OrganismLabel" runat="server" CssClass="italics"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="ContributorLabel" runat="server"></asp:Label><br />
                                <asp:HyperLink ID="ContributorLink" runat="server"></asp:HyperLink>
                            </td>
                             <td style="text-align: center">
                                <asp:HyperLink ID="imagelink" runat="server" ImageUrl="~/Images/folder_files.gif" CssClass="imagecenter"></asp:HyperLink>
                                 <asp:Label ID="SequenceClusterNumLabel" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <div style="margin-left: 10px">
                                    <img runat="server" id="toggleimage" class="toggle" style="top: 1px" src="~/Images/action_add2.gif"
                                        alt="Toggle" /><span style="font-size: smaller">&nbsp;View Domains</span></div>
                                <asp:Panel runat="server" ID="TogglePanel" CssClass="indentsmall">
                                    <asp:ListView ID="InnerDomainsListView" runat="server" OnItemDataBound="InnerDomainsListView_ItemDataBound">
                                        <LayoutTemplate>
                                            <table runat="server" id="InnerHeaderTable" style="padding: 5px; text-align: left">
                                                <tr id="Tr1" runat="server" class="border">
                                                    <th style="width: 20%; text-align: left">
                                                 Download (.fasta)                                                       
                                                    </th>
                                                    <th style="width: 25%; text-align: left">
                                                        Domain Type
                                                    </th>
                                                    <th style="width: 10%; text-align: left">
                                                        Identifier
                                                    </th>
                                                    <th style="width: 15%; text-align: left">
                                                        Length (AA)
                                                    </th>
                                                    <th style="width: 30%; text-align: left">
                                                        Additional Info
                                                    </th>          
                                                </tr>
                                                <tr runat="server" id="itemPlaceholder" />
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr runat="server">
                                            <td>
                                                <asp:LinkButton ID="DownloadLinkButton" runat="server" OnClick="DownloadLinkButton_Click">Download</asp:LinkButton>
                                            </td>
                                                <td>
                                                    <asp:Label ID="DomainTypeLabel" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="IdentifierLabel" runat="server"></asp:Label>
                                                </td>
                                                <td style="text-align:right;padding-right:15px">
                                                    <asp:Label ID="LengthLabel" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="AdditionalInfoLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                      <asp:LinkButton ID="DownloadAllLinkButton" OnClick="DownloadAllLinkButton_Click" runat="server" Text="Download all of the sequences above (.zip)" > </asp:LinkButton><asp:Label
                 ID="NoDomainsLabel" runat="server" Visible="false" Text="No domains for this cluster could be found in the sequence repository." CssClass="error"></asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <br />
        <div style="padding-left: 350px;">
            <asp:DataPager ID="ProcessedDomainsDataPager" runat="server" PagedControlID="ProcessedDomainsListView"
                PageSize="25">
                <Fields>
                    <asp:NumericPagerField />
                </Fields>
            </asp:DataPager>
        </div>
    </div>
</asp:Content>