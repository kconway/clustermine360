﻿<%@ Page Title="Summary of Clusters/Sequences in Repository" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="RepositorySummary.aspx.cs" Inherits="NaturalProductsWebsite.RepositorySummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
    <style type="text/css">
        .rotate_text
        {
            position: relative;
            -ms-writing-mode: bt-rl;
        }
    </style>
    <style type="text/css">
        .rotated_cell
        {
            height: 325px;
            width: 15px;
            vertical-align: bottom;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>
        Summary of Clusters/Sequences in Sequence Repository</h2>

   <h3>Includes Predicted-Only Clusters</h3>
        <br />
        
        <asp:ListView ID="PathWayTypeSummary" runat="server" OnItemDataBound="PathwayTypeSummary_ItemDataBound"
            OnDataBound="PathwayTypeSummary_DataBound">
            <LayoutTemplate>
                <table runat="server" id="HeaderTable" style="padding: 5px; text-align: left">
                    <tr id="Tr2" runat="server">
                        <th style="width: 40%; text-align: left">
                            Pathway Type
                        </th>
                        <th style="width: 15%; text-align: left">
                            # Phyla
                        </th>
                        <th style="width: 15%; text-align: left">
                            # Organisms
                        </th>
                        <th style="width: 15%; text-align: left">
                            # Clusters
                        </th>
                        <th style="width: 15%; text-align: left">
                            # Domains
                        </th>
                    </tr>
                    <tr runat="server" id="itemPlaceholder" />
                    <tr>
                        <td colspan="5">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Total (Unique)</b>
                        </td>
                        <td>
                            <asp:Label ID="PhylumTotalLabel" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="OrganismTotalLabel" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="ClusterTotalLabel" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="DomainTotalLabel" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr id="Tr3" runat="server">
                    <td style="height: 0px">
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="PathwayTypeLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="PhylumLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="OrganismLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="ClusterLabel" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="DomainLabel" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <br />
        <br />
        <h4 class="black"># of Domains by Cluster Type</h4>
        <asp:Table ID="DomainTypeByClusterTypeSummaryTable" runat="server" CellSpacing="0" CellPadding="0"
            CssClass="tablepadding">
        </asp:Table>

         <br />
        <br />
        <h4 class="black"># of Clusters by Phylum, Organism, and Cluster Type</h4>
        <asp:Table ID="ClusterTypeByPhylumAndOrganismTable" runat="server" CellSpacing="0" CellPadding="0"
            CssClass="tablepadding">
        </asp:Table>  

</asp:Content>