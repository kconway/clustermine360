﻿<%@ Page Title="Details of Clusters Currently In Process" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="ProcessingDetails.aspx.cs" Inherits="NaturalProductsWebsite.ProcessingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>Details of Clusters Currently In Process</h2>   
            <div id="currentlyprocessingpanel">
    <h5><b>
            <asp:Label ID="NoneProcessingLabel" runat="server" Text="None" Visible="false"></asp:Label></b></h5>
    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Accession #" SortExpression="Accession">
                <ItemTemplate>
                    <asp:HyperLink ID="genbanklink" runat="server" Style="display: block;" OnDataBinding="GenBankLink_DataBind"></asp:HyperLink>
                    <div style="width: 100%" class="toppadding5">
                        <asp:Literal ID="GenBankStartEndLiteral" runat="server" OnDataBinding="GenBankStartEnd_DataBind"></asp:Literal>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Phylum" SortExpression="Phylum">
                <ItemTemplate>
                    <asp:Label ID="PhylumLabel" runat="server" OnDataBinding="PhylumLabel_DataBind"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Organism" SortExpression="Organism">
                <ItemTemplate>
                    <asp:Label ID="OrganismLabel" runat="server" OnDataBinding="OrganismLabel_DataBind"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Compound Family" SortExpression="CompoundFamily">
                <ItemTemplate>
                    <asp:Literal ID="CompoundFamilyLiteral" runat="server" OnDataBinding="CompoundFamilyLiteral_DataBind"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Pathway Type" SortExpression="PathwayType">
                <ItemTemplate>
                    <asp:Label ID="PathwayTypeLabel" runat="server" OnDataBinding="PathwayType_DataBind"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Contributor" SortExpression="Contributor">
                <ItemTemplate>
                    <asp:Label runat="server" ID="contributorLabel" OnDataBinding="NameLabel_DataBind"></asp:Label>
                    <br />
                    <asp:HyperLink ID="LabLink" runat="server" OnDataBinding="LabLink_DataBind"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" ItemStyle-Width="40%" SortExpression="Status">
                <ItemTemplate>
                    <asp:Label ID="StatusLabel" runat="server" OnDataBinding="StatusLabel_DataBind" CssClass="alignleft"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
   
</asp:Content>