﻿<%@ Page Title="Microbial PKS/NRPS Sequence Repository" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" Inherits="DownloadSequences" CodeBehind="DownloadSequences.aspx.cs" EnableEventValidation="True" %>

<%@ Register Assembly="skmValidators" Namespace="skmValidators" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="../Scripts/jquery.validate.min.js"></script>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            var ids = [
                ["#<%=AllClusterTypeCB.ClientID%>", "#<%=ClusterTypeCBList.ClientID%>"],
                ["#<%=AllDomainsCB.ClientID%>", "#<%=DomainCBList.ClientID%>"],
                ["#<%=AllATCB.ClientID%>", "#<%=ATCBList.ClientID%>"],
                ["#<%=AllKRActivityCB.ClientID%>", "#<%=KRActivityCBList.ClientID%>"],
                ["#<%=AllKRStereoChem.ClientID%>", "#<%=KRStereoChemCBList.ClientID%>"],
                ["#<%=AllPhylaCB.ClientID%>", "#<%=PhylaCBList.ClientID%>"]
               ];

            for (var i = 0; i < ids.length; i++) {
                var allID = ids[i][0];
                var listID = ids[i][1];

                var tempCheckAllFunction = jqCheckAll(listID + " input", allID);
                tempCheckAllFunction();
                bindItem(allID, tempCheckAllFunction);

                var listTxtBoxes = listID + " input[type=checkbox]";
                var tempToggleAllFunction = toggleSelectAllCheckBox(allID, listID);
                bindItem(listTxtBoxes, tempToggleAllFunction);

            }

            var ATIDs = [
            ["<%=ATTypes.ClientID%>", "#<%=DomainCBList.ClientID%>", "Adenylation"],
            ["<%=ATTypes.ClientID%>", "#<%=DomainCBList.ClientID%>", "AT"]
            ];

            var firstCheckedID = "#" + $(ATIDs[0][1] + " label:contains(" + ATIDs[0][2] + ")").attr("for");
            var secondCheckedID = "#" + $(ATIDs[1][1] + " label:contains(" + ATIDs[1][2] + ")").attr("for");
            var divID = ATIDs[0][0];
            var tempATFunction = ATFunction(firstCheckedID, secondCheckedID, divID);
            tempATFunction();
            bindItem(firstCheckedID, tempATFunction);
            bindItem(secondCheckedID, tempATFunction);
            bindItem("#<%=AllDomainsCB.ClientID%>", tempATFunction);

            var KRIDs = [
            ["<%=KRActivityTypes.ClientID%>", "#<%=DomainCBList.ClientID%>", "KR"],
            ["<%=KRStereoChemTypes.ClientID%>", "#<%=DomainCBList.ClientID%>", "KR"]
            ];

            for (var j = 0; j < KRIDs.length; j++) {
                var divID = KRIDs[j][0];
                var cbListID = KRIDs[j][1];
                var labelText = KRIDs[j][2];
                var checkedID = "#" + $(cbListID + " label:contains(" + labelText + ")").attr("for");

                var tempFunction = jqEnable(divID, checkedID);
                tempFunction();
                bindItem(checkedID, tempFunction);
                bindItem("#<%=AllDomainsCB.ClientID%>", tempFunction);
            }

        });

        function ATFunction(firstCheckedID, secondCheckedID, divIDSelector) {
            var enableFunction = function () {
                if (($(firstCheckedID).is(':checked')) || ($(secondCheckedID).is(':checked'))) {
                    toggleDisabled(document.getElementById(divIDSelector), false);
                } else {
                    toggleDisabled(document.getElementById(divIDSelector), true);
                }
            }
            return enableFunction;
        }

        function bindItem(elem, func) {
            $(elem).click(function () { func(); });
        }

        function jqEnable(divSelector, checkedSelector) {
            var divIDSelector = divSelector;
            var cbIDSelector = checkedSelector;

            var enableFunction = function () {
                if ($(cbIDSelector).is(':checked')) {
                    toggleDisabled(document.getElementById(divIDSelector), false);
                } else {
                    toggleDisabled(document.getElementById(divIDSelector), true);
                }
            }
            return enableFunction;
        }

        function toggleDisabled(el, disabledStatus) {
            el.disabled = disabledStatus;

            if (el) {
                if (el.childNodes.length > 0) {
                    for (var x = 0; x < el.childNodes.length; x++) {
                        toggleDisabled(el.childNodes[x], disabledStatus);
                    }
                }
            }
        }

        function jqCheckAll(cbListSelector, cbAllSelector) {
            var allIDSelector = cbAllSelector;
            var listIDSelector = cbListSelector;
            var checkFunction = function () { $(listIDSelector).attr('checked', $(allIDSelector).is(':checked')); }
            return checkFunction;
        }

        function toggleSelectAllCheckBox(allCheckBox, obj) {
            var toggleFunction = function () {
                if (allCheckBox == null || allCheckBox == undefined) {
                    return;
                }

                var areAllChecked = true;
                $(obj + " input[type=checkbox]").each(function () {
                    if (!this.checked) {
                        areAllChecked = false;
                    }

                });
                $(allCheckBox).attr("checked", areAllChecked);
            }
            return toggleFunction;
        }

        function ValidatePhylumCBList(source, arguments) {
            var listselector = "<%=PhylaCBList.ClientID%>";
            ValidateCheckBoxList(listselector, arguments);
        }
        function ValidateClusterTypeCBList(source, arguments) {
            var listselector = "<%=ClusterTypeCBList.ClientID%>";
            ValidateCheckBoxList(listselector, arguments);
        }
        function ValidateDomainCBList(source, arguments) {
            var listselector = "<%=DomainCBList.ClientID%>";
            ValidateCheckBoxList(listselector, arguments);
        }
        function ValidateATCBList(source, arguments) {
            var listselector = "<%=ATCBList.ClientID%>";
            var disabledselector = "#" + listselector + " input[type=checkbox]:disabled";
            if ($(disabledselector).length > 0) {
                arguments.IsValid = true;
            } else {
                ValidateCheckBoxList(listselector, arguments);
            }
        }
        function ValidateKRActivityCBList(source, arguments) {
            var listselector = "<%=KRActivityCBList.ClientID%>";
            var disabledselector = "#" + listselector + " input[type=checkbox]:disabled";
            if ($(disabledselector).length > 0) {
                arguments.IsValid = true;
            } else {
                ValidateCheckBoxList(listselector, arguments);
            }
        }
        function ValidateKRStereoChemCBList(source, arguments) {
            var listselector = "<%=KRStereoChemCBList.ClientID%>";
            var disabledselector = "#" + listselector + " input[type=checkbox]:disabled";
            if ($(disabledselector).length > 0) {
                arguments.IsValid = true;
            } else {
                ValidateCheckBoxList(listselector, arguments);
            }
        }
      
        function ValidateCheckBoxList(cbListSelector, arguments) {

            var selector = "#" + cbListSelector + " input[type=checkbox]:checked";

            var num = $(selector).length;

            if (num > 0) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

    </script>
    <style type="text/css">
        .allCBStyle
        {
            padding-top: 4px;
            width: 70px;
        }
        .headerstyles
        {
            font-weight: bold;
            font-size: medium;
        }
        .cblist td
        {
            padding-right: 10px;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
   <asp:ObjectDataSource ID="PhylaDataSource" runat="server" SelectMethod="getPhylaNames"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ClusterTypesDataSource" runat="server" SelectMethod="getClusterTypes"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="DomainsDataSource" runat="server" SelectMethod="getDomainTypes"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ATTypesDataSource" runat="server" SelectMethod="getATTypes"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="KRActivityDataSource" runat="server" SelectMethod="getKRActivityTypes"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="KRStereoChemDataSource" runat="server" SelectMethod="getKRStereoChemTypes"
        TypeName="DomainSequencesDataHandler"></asp:ObjectDataSource>
    <h3>
        Download Sequences<b> (<a href="<%= Page.ResolveUrl("~/SequenceRepository.aspx") %>">View
            Repository Info</a>)</b>
    </h3>
    
    <br />
    <span class="noerror" style="font-size: small">The sequences for individual domains
        can also be downloaded from each cluster's details page.<br />
        Individual sequences can also be downloaded from the sequence repository <a href="<%= Page.ResolveUrl("~/Repository/RepositoryDetails.aspx") %>">
            details</a> page.</span>
    <br />
    <br />
    There are currently
    <asp:Label ID="NumDomains" runat="server"></asp:Label>
    domains in the sequence repository.
    <br />
    <p class="headerstyles">
        Please select at least one checkbox from each of the categories below.
    </p>
    <br />
    <br />
    <asp:Label ID="SubmitError" runat="server" Text="An error has occured. Please try again.<br />"
        CssClass="error" Visible="false"></asp:Label>
    <div id="selections" style="width: 90%">
        <div id="phyla" class="floatleftsmall">
            <span class="headerstyles">Phyla</span>
        </div>
        <div class="floatleftpadded">
            <div class="floatleft allCBStyle">
                <asp:CheckBox ID="AllPhylaCB" runat="server" Text="All" />
            </div>
            <div class="floatleft">
                <asp:CheckBoxList ID="PhylaCBList" runat="server" RepeatColumns="2" DataSourceID="PhylaDataSource"
                    CssClass="cblist">
                </asp:CheckBoxList>
                <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="error" ErrorMessage="At least one phylum must be selected."
                    Display="Dynamic"  ClientValidationFunction="ValidatePhylumCBList"  OnServerValidate="CheckBoxListValidate"
                    ValidationGroup="Download"></asp:CustomValidator>
                <%-- <cc1:CheckBoxListValidator ID="CheckBoxListValidator1" runat="server"
                    ErrorMessage= ControlToValidate="PhylaCBList"
                    MinimumNumberOfSelectedCheckBoxes="1" ValidationGroup="Download" ></cc1:CheckBoxListValidator> --%>
            </div>
            <br class="clear" />
        </div>
        <br class="clear" />
        <br />
        <div id="clustertypes" class="floatleftsmall">
            <span class="headerstyles">Cluster Types</span></div>
        <div class="floatleftpadded" style="width: 75%">
            <div class="floatleft allCBStyle">
                <asp:CheckBox ID="AllClusterTypeCB" runat="server" Text="All" />
            </div>
            <div class="floatleft">
                <asp:RadioButtonList ID="ClusteryTypeMatchOptionRadioButtonList" runat="server" CssClass="cblist">
                    <asp:ListItem Text="Match Any" Value="or" Selected="True">
                    </asp:ListItem>
                    <asp:ListItem Text="Match All" Value="and"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <asp:CheckBoxList ID="ClusterTypeCBList" runat="server" RepeatColumns="2" DataSourceID="ClusterTypesDataSource"
                    DataTextField="Type" DataValueField="ID" CssClass="cblist">
                </asp:CheckBoxList>
                  <asp:CustomValidator ID="CustomValidator2" runat="server" CssClass="error" ErrorMessage="At least one cluster type must be selected."
                    Display="Dynamic" ClientValidationFunction="ValidateClusterTypeCBList" OnServerValidate="CheckBoxListValidate"
                    ValidationGroup="Download"></asp:CustomValidator>
                <%--  <cc1:CheckBoxListValidator ID="CheckBoxListValidator2" runat="server" CssClass="error"
                    Display="Dynamic" ErrorMessage="At least one cluster type must be selected."
                    ControlToValidate="ClusterTypeCBList" MinimumNumberOfSelectedCheckBoxes="1"></cc1:CheckBoxListValidator>--%>
            </div>
        </div>
        <br class="clear" />
        <br />
        <div id="domaintypes" class="floatleftsmall">
            <span class="headerstyles">Domain Types</span></div>
        <div class="floatleftpadded">
            <div class="floatleft allCBStyle">
                <asp:CheckBox ID="AllDomainsCB" runat="server" Text="All" />
            </div>
            <div class="floatleft">
                <asp:CheckBoxList ID="DomainCBList" runat="server" RepeatColumns="2" DataSourceID="DomainsDataSource"
                    CssClass="cblist">
                </asp:CheckBoxList>
                 <asp:CustomValidator ID="CustomValidator3" runat="server" CssClass="error" ErrorMessage="At least one domain type must be selected."
                    Display="Dynamic" ClientValidationFunction="ValidateDomainCBList" OnServerValidate="CheckBoxListValidate"
                    ValidationGroup="Download"></asp:CustomValidator>
                <%--  <cc1:CheckBoxListValidator ID="CheckBoxListValidator3" runat="server" CssClass="error"
                    Display="Dynamic" ErrorMessage="At least one domain type must be selected." ControlToValidate="DomainCBList"
                    MinimumNumberOfSelectedCheckBoxes="1"></cc1:CheckBoxListValidator>--%>
            </div>
        </div>
        <br class="clear" />
        <br />
        <div runat="server" id="ATTypes">
            <div class="floatleftsmall">
                <span class="headerstyles">Predicted A/AT specificity (if applicable)</span></div>
            <div class="floatleftpadded">
                <div class="floatleft allCBStyle">
                    <asp:CheckBox ID="AllATCB" runat="server" Text="All" />
                </div>
                <div class="floatleft">
                    <asp:CheckBoxList ID="ATCBList" runat="server" RepeatColumns="2" DataSourceID="ATTypesDataSource"
                        CssClass="cblist">
                    </asp:CheckBoxList>
                     <asp:CustomValidator ID="CustomValidator4" runat="server" CssClass="error" ErrorMessage="Please select at least one AT specificity option."
                    Display="Dynamic" ClientValidationFunction="ValidateATCBList" OnServerValidate="CheckBoxListValidate"
                    ValidationGroup="Download"></asp:CustomValidator>
                    <%--  <cc1:CheckBoxListValidator ID="CheckBoxListValidator6" runat="server" MinimumNumberOfSelectedCheckBoxes="1"
                        CssClass="error" ErrorMessage="Please select at least one AT specificity option."
                        ControlToValidate="ATCBList" Display="Dynamic"></cc1:CheckBoxListValidator>--%>
                </div>
            </div>
            <br class="clear" />
            <br />
        </div>
        <div runat="server" id="KRActivityTypes">
            <div class="floatleftsmall">
                <span class="headerstyles">Predicted KR Activity (if applicable)</span></div>
            <div class="floatleftpadded">
                <div class="floatleft allCBStyle">
                    <asp:CheckBox ID="AllKRActivityCB" runat="server" Text="All" />
                </div>
                <div class="floatleft">
                    <asp:CheckBoxList ID="KRActivityCBList" runat="server" RepeatColumns="2" DataSourceID="KRActivityDataSource"
                        CssClass="cblist">
                    </asp:CheckBoxList>
                      <asp:CustomValidator ID="CustomValidator6" runat="server" CssClass="error" ErrorMessage="Please select at least one KR Activity option."
                    Display="Dynamic" ClientValidationFunction="ValidateKRActivityCBList" OnServerValidate="CheckBoxListValidate"
                    ValidationGroup="Download"></asp:CustomValidator>
                    <%--  <cc1:CheckBoxListValidator ID="CheckBoxListValidator5" runat="server" MinimumNumberOfSelectedCheckBoxes="1"
                        CssClass="error" ErrorMessage="Please select at least one KR Activity option."
                        ControlToValidate="KRActivityCBList" Display="Dynamic"></cc1:CheckBoxListValidator>--%>
                </div>
            </div>
            <br class="clear" />
            <br />
        </div>
        <div runat="server" id="KRStereoChemTypes">
            <div class="floatleftsmall">
                <span class="headerstyles">Predicted KR StereoChem (if applicable)</span></div>
            <div class="floatleftpadded">
                <div class="floatleft allCBStyle">
                    <asp:CheckBox ID="AllKRStereoChem" runat="server" Text="All" />
                </div>
                <div class="floatleft">
                    <asp:CheckBoxList ID="KRStereoChemCBList" runat="server" RepeatColumns="2" CssClass="cblist"
                        DataSourceID="KRStereoChemDataSource">
                    </asp:CheckBoxList>
                     <asp:CustomValidator ID="CustomValidator5" runat="server" CssClass="error" ErrorMessage="Please select at least one KR Stereochem option."
                    Display="Dynamic" ClientValidationFunction="ValidateKRStereoChemCBList" OnServerValidate="CheckBoxListValidate" 
                    ValidationGroup="Download"></asp:CustomValidator>
                    <%--  <cc1:CheckBoxListValidator ID="CheckBoxListValidator4" runat="server" MinimumNumberOfSelectedCheckBoxes="1"
                        CssClass="error" ErrorMessage="Please select at least one KR Stereochem option."
                        ControlToValidate="KRStereoChemCBList" Display="Dynamic"></cc1:CheckBoxListValidator> --%>
                </div>
            </div>
        </div>
        <br class="clear" />
        <br />
        <div class="floatleftsmall">
            <span class="headerstyles">Header</span></div>
        <div class="floatleft">
            <asp:CheckBox ID="HeaderCheckBox" runat="server" CssClass="cblist" /></div>
        <div class="floatleftpadded">
            Use a shortened header format. This will output shorter description headers for
            use in analysis programs that don't work with longer, more detailed headers. Spaces
            are also replaced with underscores to help with compatibility.</div>
        <br class="clear" />
        <div class="floatleftsmall">
            &nbsp;
        </div>
        <div id="submitdiv" class="floatleft">
            <p class="headerstyles">
                Pressing the submit button will generate a FASTA file that you can download containing
                the sequences that match the options selected above.
            </p>          
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Download"
                CssClass="error" />  <br />
            <asp:Button ID="SubmitButton" runat="server" Text="Submit" CssClass="submitButton floatleft"
                OnClick="SubmitButton_Click" ValidationGroup="Download" CausesValidation="True" />
        </div>
       
        <br />
        <br />
    </div>
</asp:Content>