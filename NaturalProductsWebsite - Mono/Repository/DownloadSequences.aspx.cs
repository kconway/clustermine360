﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
using Utilities;

public partial class DownloadSequences : System.Web.UI.Page
{
    private static readonly ILog log = LogManager.GetLogger(typeof(DownloadSequences));

    protected void Page_Load(object sender, EventArgs e)
    {
        XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
        Page.Validate();

        if (!Page.IsValid)
        {
            return;
        }

        try
        {
            if (!IsPostBack)
            {
                // DomainCBList.DataBind();
                AllATCB.Checked = true;
                AllDomainsCB.Checked = true;
                AllKRActivityCB.Checked = true;
                AllKRStereoChem.Checked = true;
                AllClusterTypeCB.Checked = true;
                AllPhylaCB.Checked = true;
            }
            NumDomains.Text = DomainSequencesDataHandler.NumDomainsInRepository().ToString();
        }
        catch (Exception ex)
        {
            log.Debug(ex.ToString());
        }
    }

    protected void CheckBoxListValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
       
        // get phyla, domaintypes, clustertypes and A/AT K, KR

        var innerOrPredicate = PredicateBuilder.False<DomainSequence>();
        var mainOuterPredicate = PredicateBuilder.True<DomainSequence>();

        var phyla = PredicateBuilder.False<DomainSequence>();
        foreach (ListItem phylumLI in PhylaCBList.Items)
        {
            string text = phylumLI.Text;
            if (phylumLI.Selected == true)
            {
                phyla = phyla.Or(p => p.Enum_Phyla.Phylum == text);
            }
        }

        mainOuterPredicate = mainOuterPredicate.And(phyla);

        var clustertypeids = new List<int>();
        //var clustertype = PredicateBuilder.False<DomainSequence>();
        bool matchall = false;
        if (ClusteryTypeMatchOptionRadioButtonList.SelectedValue == "and")
        {
            matchall = true;
            //clustertype = PredicateBuilder.True<DomainSequence>();
        }
        else if (ClusteryTypeMatchOptionRadioButtonList.SelectedValue == "or")
        {
            matchall = false;
            //clustertype = PredicateBuilder.False<DomainSequence>();
        }

        foreach (ListItem clusterLI in ClusterTypeCBList.Items)
        {
            if (clusterLI.Selected == true)
            {
                string text = clusterLI.Text;
                int value = int.Parse(clusterLI.Value);
                clustertypeids.Add(value);
            }
        }

        //using (DataClassesDataContext db = new DataClassesDataContext())
        //{
        //    var groupingids = db.DomainGroupings.Select(x => x.GroupingID).Distinct().ToList();
        //     foreach (string groupid in groupingids)
        //{
        //    clustertype = clustertype.And(c => c.GroupingID == groupid);
        //}

        //    foreach (int pathwaytypeid in clustertypeids)
        //    {
        //        if (matchall == true)
        //        {
        //        }
        //        else
        //        {
        //        }
        //    }
        //}

        // mainOuterPredicate = mainOuterPredicate.And(clustertype);

        var domaintype = PredicateBuilder.False<DomainSequence>();
        foreach (ListItem domainLI in DomainCBList.Items)
        {
            if (domainLI.Selected == true)
            {
                if (domainLI.Text != "KR" && domainLI.Text != "AT" && domainLI.Text != "Adenylation")
                {
                    string text = domainLI.Text;
                    domaintype = domaintype.Or(d => d.DomainType == text);
                }
            }
        }

        innerOrPredicate = innerOrPredicate.Or(domaintype);

        ListItem krItem = DomainCBList.Items.FindByText("KR");
        if (krItem != null)
        {
            if (krItem.Selected == true)
            {
                var krActivityType = PredicateBuilder.False<DomainSequence>();
                foreach (ListItem krActivityLI in KRActivityCBList.Items)
                {
                    if (krActivityLI.Selected == true)
                    {
                        string text = krActivityLI.Text;
                        krActivityType = krActivityType.Or(d => d.KRActivityPrediction == text);
                    }
                }

                // need to adjust so that if selected, won't affect other domains
                var krStereoChemType = PredicateBuilder.False<DomainSequence>();
                foreach (ListItem krStereoChemLI in KRStereoChemCBList.Items)
                {
                    if (krStereoChemLI.Selected == true)
                    {
                        string text = krStereoChemLI.Text;
                        krStereoChemType = krStereoChemType.Or(d => d.KRStereoChemPrediction == text);
                    }
                }
                var krPredicate = PredicateBuilder.True<DomainSequence>();
                krPredicate = krPredicate.And(d => d.DomainType == "KR").And(krStereoChemType).And(krActivityType);
                innerOrPredicate = innerOrPredicate.Or(krPredicate);
            }
        }

        ListItem atItem = DomainCBList.Items.FindByText("AT");
        ListItem aItem = DomainCBList.Items.FindByText("Adenylation");
        if (atItem != null && aItem != null)
        {
            if (atItem.Selected == true || atItem.Selected == true)
            {
                var atType = PredicateBuilder.False<DomainSequence>();
                foreach (ListItem atLI in ATCBList.Items)
                {
                    if (atLI.Selected == true)
                    {
                        string text = atLI.Text;
                        atType = atType.Or(d => (d.AorATPrediction == text));
                    }
                }
                var atPredicate = PredicateBuilder.True<DomainSequence>();
                atPredicate = atPredicate.And(d => d.DomainType == "AT" || d.DomainType == "Adenylation").And(atType);
                innerOrPredicate = innerOrPredicate.Or(atPredicate);
            }
        }

        mainOuterPredicate = mainOuterPredicate.And(innerOrPredicate);
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            //db.Log = new DebugTextWriter();
            try
            {
                var query = db.DomainSequences.Where(mainOuterPredicate).ToList();
                //.Select(x => new { ID = x.ID, GroupingID = x.GroupingID }).ToList();
                

                List<string> ids = new List<string>();
                var validgroupids = getValidGroupingIDs(clustertypeids, matchall);
                foreach(string validgroupid in validgroupids)
                {
                    var validids = query.Where(x => x.GroupingID == validgroupid).Select(x => x.ID.ToString()).ToList();
                    ids.AddRange(validids);
                }
               
                string idsInSemiColonSeparatedList = StringTools.SerializeToSemiColonSeparatedList(ids.Distinct().ToList());

                Download filerequest = new Download();
                filerequest.DownloadGuid = Guid.NewGuid().ToString("N");
                filerequest.WhenRequested = DateTime.Now;
                filerequest.SimpleHeaders = HeaderCheckBox.Checked;
                filerequest.Status = 0;
                filerequest.DomainIDs = idsInSemiColonSeparatedList;
                if (String.IsNullOrEmpty(idsInSemiColonSeparatedList))
                {
                    filerequest.Status = 20; // no matches
                }
                db.Downloads.InsertOnSubmit(filerequest);
                db.SubmitChanges();

                Response.Redirect("~/Repository/Download.aspx?downloadguid=" + filerequest.DownloadGuid);
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                string adminerror = "There was an error creating a FASTA file request. " + Environment.NewLine + ex.ToString();
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error with FASTA Request", adminerror);
                SubmitError.Visible = true;
            }
        }
    }

    private List<string> getValidGroupingIDs(List<int> pathwayids, bool matchall)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var domainpathwayrelationships = db.DomainPathwayTypes.ToList();
            List<string> groupids = db.DomainGroupings.Select(x => x.GroupingID).Distinct().ToList(); 
            //List<string> groupids = db.DomainSequences.Select(x => x.GroupingID).Distinct().ToList();
            List<string> groupidstoreturn = new List<string>();
            

            foreach (string groupid in groupids)
            {
                bool matchallinclude = true;
                bool matchanyinclude = false;
                foreach (int pid in pathwayids)
                {
                    if (matchall)
                    {
                        bool exists = domainpathwayrelationships.Exists(x => x.GroupingID == groupid && x.PathwayTypeID == pid);
                        if (exists == false)
                        {
                            matchallinclude = false;
                        }
                    }
                    else
                    {
                        bool exists = domainpathwayrelationships.Exists(x => x.GroupingID == groupid && x.PathwayTypeID == pid);
                        if (exists == true)
                        {
                            matchanyinclude = true;
                        }
                    }
                }

                if (matchall == true && matchallinclude == true)
                {
                    groupidstoreturn.Add(groupid);
                }

                if (matchall == false && matchanyinclude == true)
                {
                    groupidstoreturn.Add(groupid);
                }
            }
          
            return groupidstoreturn;
        }
    }
}