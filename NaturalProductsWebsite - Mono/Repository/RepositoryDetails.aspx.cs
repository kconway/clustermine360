﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
using Utilities;

namespace NaturalProductsWebsite
{
    public partial class RepositoryDetails : System.Web.UI.Page
    {
        public IEnumerable<SequenceGroup> ProcessedSequences;       

        protected void Page_Init(object sender, EventArgs e)
        {
           ProcessedSequences = DomainSequencesDataHandler.GetGroupedDomains().OrderBy(x => x.PathwayType).ThenBy(y => y.Phylum).ThenBy(z => z.Organism);
        }

        private void ApplyFilter()
        {
            
            if (PathwayTypeDropDownList.SelectedValue != "*")
            {
               ProcessedSequences = ProcessedSequences.Where(x => x.PathwayType == PathwayTypeDropDownList.SelectedValue);
            }

            if (PhylumDropDownList.SelectedValue != "*")
            {
                ProcessedSequences = ProcessedSequences.Where(x => x.Phylum == PhylumDropDownList.SelectedValue);
            }

            if (OrganismDropDownList.SelectedValue != "*")
            {
                ProcessedSequences = ProcessedSequences.Where(x => x.Organism == OrganismDropDownList.SelectedValue);
            }

            if (CompoundFamilyDropDownList.SelectedValue != "*")
            {
                ProcessedSequences = ProcessedSequences.Where(x => x.CompoundFamilyName == CompoundFamilyDropDownList.SelectedValue);
            }

            if (!String.IsNullOrEmpty(PathwayTypeTextSearchBox.Text))
            {
                string searchwords = PathwayTypeTextSearchBox.Text;
                string[] words = searchwords.Split(';');
                foreach (var word in words)
                {
                     ProcessedSequences = ProcessedSequences.Where(x => x.PathwayType.Contains(word.ToUpper()));
                }
            }
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                ProcessedDomainsListView.DataBind();
                PathwayTypeDropDownList.DataSource = ProcessedSequences.Select(x => x.PathwayType).Distinct().OrderBy(x => x).ToList();
                PathwayTypeDropDownList.DataBind();

                PhylumDropDownList.DataSource = ProcessedSequences.Select(p => p.Phylum).Distinct().OrderBy(x => x).ToList();
                PhylumDropDownList.DataBind();

                CompoundFamilyDropDownList.DataSource = ProcessedSequences.Select(c => c.CompoundFamilyName).Distinct().OrderBy(x => x).ToList();
                CompoundFamilyDropDownList.DataBind();

                OrganismDropDownList.DataSource = ProcessedSequences.Select(o => o.Organism).Distinct().OrderBy(x => x).ToList();
                OrganismDropDownList.DataBind();

                getSortDirection("PathwayType", false);
            }
           
            InsertToggleScript();
           
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            ProcessedDomainsListView.DataBind();
        }

        protected void InsertToggleScript()
        {
            string plusimageurl = ResolveUrl("~/Images/action_add2.gif");
            string minusimageurl = ResolveUrl("~/Images/action_remove.gif");
            StringBuilder lineageScript = new StringBuilder();
            lineageScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
            lineageScript.AppendLine("function togglePanel(imageID, PanelID) {");
            lineageScript.AppendLine("PanelID = formatID(PanelID);");
            lineageScript.AppendLine("imageID = formatID(imageID);");
            lineageScript.AppendLine("if ($(PanelID).is(\":visible\")) {");
            lineageScript.AppendLine("$(PanelID).hide('fast');");
            lineageScript.AppendLine("$(imageID).attr(\"src\", \""+ plusimageurl + "\");");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("else {");
            lineageScript.AppendLine("$(PanelID).show('fast');");
            lineageScript.AppendLine("$(imageID).attr(\"src\", \""+ minusimageurl + "\");");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine();
            lineageScript.AppendLine("function formatID(id) {");
            lineageScript.AppendLine("var formattedID = '\"' + \"[id$=\" + \"'\" + id + \"'\" + \"]\" + '\"'");
            lineageScript.AppendLine("return formattedID;");
            lineageScript.AppendLine("}");
            lineageScript.AppendLine("</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ToggleScript", lineageScript.ToString(), false);
        }

        protected void ProcessedDomainsListView_DataBinding(object sender, EventArgs e)
        {           
            ApplyFilter();
            ProcessedDomainsListView.DataSource =  ProcessedSequences.ToList();         
        }

        protected void DownloadLinkButton_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            int domainid = int.Parse(lb.CommandArgument);
            string filename = "";
            string filedata = FastaGenerator.PrepareFASTAFile(domainid, out filename);

            Response.Clear();
            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.BinaryWrite(StringTools.StringToByteArray(filedata));
            Response.Flush();
            Response.End();

        }

        protected void DownloadAllLinkButton_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string groupingid = lb.CommandArgument;
            var sequences = DomainSequencesDataHandler.getDomainsByGroupingID(groupingid);

            string filename = ExtractFileName(sequences);
                
            Response.Clear();
            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            FastaGenerator.PrepareZippedFASTAFiles(sequences, Response.OutputStream);
            Response.Flush();
            Response.End();
        }

        private string ExtractFileName(List<DomainSequence> sequences)
        {
            string filename = "NoDomains";
            string genbank = "";
            
            string cfamily = "";
            var domain = sequences.FirstOrDefault();
            if (domain != null)
            {
                if (domain.ClusterID != null)
                {
                    cfamily = domain.Cluster.CompoundFamily.FamilyName;
                    genbank = domain.Cluster.ClusterBase.GenbankAccession;
                } else if (domain.LargeSequenceID != null)
                {
                    genbank = domain.LargeSequence.ClusterBase.GenbankAccession;
                }


                filename = domain.Organism + "_";
                if (!String.IsNullOrEmpty(cfamily))
                {
                    filename += cfamily + "_";
                }
                   
                filename += domain.ClusterType + "_" + genbank + ".zip";
                filename = filename.Replace(' ', '_');
            }
            
            return filename;
        }

      
        protected void InnerDomainsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton lb = (LinkButton)dataItem.FindControl("DownloadLinkButton");
                lb.CommandArgument = DataBinder.Eval(dataItem.DataItem, "DomainID").ToString();

                Label typelabel = (Label)dataItem.FindControl("DomainTypeLabel");
                typelabel.Text = (string)DataBinder.Eval(dataItem.DataItem, "DomainType");

                Label identifierlabel = (Label)dataItem.FindControl("IdentifierLabel");
                identifierlabel.Text = (string)DataBinder.Eval(dataItem.DataItem, "Identifier");

                Label lengthlabel = (Label)dataItem.FindControl("LengthLabel");
                lengthlabel.Text = DataBinder.Eval(dataItem.DataItem, "Length").ToString();

                Label infolabel = (Label)dataItem.FindControl("AdditionalInfoLabel");
                string info = "";

                string AorATPrediction = DataBinder.Eval(dataItem.DataItem, "AorATPrediction") != null ? "A or AT Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "AorATPrediction") : "";
                string KRActivityPrediction = DataBinder.Eval(dataItem.DataItem, "KRActivityPrediction") != null ? "KR Activity Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "KRActivityPrediction") : "";
                string KRStereoChemPrediction = DataBinder.Eval(dataItem.DataItem, "KRStereoChemPrediction") != null ? "KR Stereochemistry Prediction: " + (string)DataBinder.Eval(dataItem.DataItem, "KRStereoChemPrediction") : "";

                info = AorATPrediction + KRActivityPrediction + KRActivityPrediction;

                if (!String.IsNullOrEmpty(AorATPrediction))
                {
                    info = AorATPrediction;
                }

                if ((!String.IsNullOrEmpty(KRActivityPrediction)) && (!String.IsNullOrEmpty(KRStereoChemPrediction)))
                {
                    info = KRActivityPrediction + "<br />" + KRStereoChemPrediction;
                }

                infolabel.Text = info;
            }
        }

        protected void ProcessDomainsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                SequenceGroup g = (SequenceGroup)dataItem.DataItem;
                
                HyperLink gblink = (HyperLink)dataItem.FindControl("GenBankLink");
                gblink.Target = "_blank";
                string gbaccession = g.GenbankAccession;
                int gbversion = g.GenbankVersion;

                string gbtext = gbaccession;
                if (gbversion != 0)
                {
                    gbtext = gbtext + "." + gbversion;
                }

                gblink.Text = gbtext;
                gblink.NavigateUrl = NaturalProductsWebsite.Code.NCBIUtils.GetNCBIDisplayURL() + gbtext;

                Literal startendliteral = (Literal)dataItem.FindControl("GenBankStartEndLiteral");
                int? bpstartnum = g.BPStart;
                int? bpendnum = g.BPEnd;
                if ((bpstartnum != null) && (bpendnum != null))
                {
                    startendliteral.Text = FormatBPStartEnd(bpstartnum.Value, bpendnum.Value);
                }

                Label typelabel = (Label)dataItem.FindControl("PathwayTypeLabel");
                typelabel.Text = g.PathwayType;

                Label cfnamelabel = (Label)dataItem.FindControl("CompoundFamilyNameLabel");
                cfnamelabel.Text = g.CompoundFamilyName;

                Label phylumlabel = (Label)dataItem.FindControl("PhylumLabel");
                phylumlabel.Text = g.Phylum;

                Label orglabel = (Label)dataItem.FindControl("OrganismLabel");
                orglabel.Text = g.Organism;

                Label contributorlabel = (Label)dataItem.FindControl("ContributorLabel");
                int contributorid = g.ContributorID;
                UserInfo user = UserInfos.getUserInfoByID(contributorid);
                string name = user.First_Name + " " + user.Last_Name;
                contributorlabel.Text = name;

                HyperLink contributorlink = (HyperLink)dataItem.FindControl("ContributorLink");
                contributorlink.Text = !String.IsNullOrEmpty(user.Lab_Name) ? user.Lab_Name : "";
                contributorlink.Target = "_blank";
                contributorlink.NavigateUrl = !String.IsNullOrEmpty(user.Lab_Url) ? user.Lab_Url : "";

                HyperLink detailslink = (HyperLink)dataItem.FindControl("imagelink");
                Label sequencenumlabel = (Label)dataItem.FindControl("SequenceClusterNumLabel");
                
                string url = "";
                if (g.EntityType == ProcessEntityType.Cluster)
                {
                    url = "~/antiSMASH/Clusters/";
                }
                else if (g.EntityType == ProcessEntityType.LargeSequence)
                {
                    url = "~/antiSMASH/Sequences/";
                    sequencenumlabel.Visible = true;

                    sequencenumlabel.Text = "See cluster #" + g.ClusterNum;
                }
                url += g.ParentID + "/display.xhtml";
                detailslink.NavigateUrl = url;
                detailslink.Target = "_blank";




                HtmlImage image = (HtmlImage)dataItem.FindControl("toggleimage");
                Panel togglepanel = (Panel)dataItem.FindControl("TogglePanel");
                togglepanel.Attributes.Add("style", "display:none");

                int imageidlength = image.ClientID.Length;
                string shortImageClientID = image.ClientID.Substring(imageidlength - 25, 25);
                string shortPanelClientID = togglepanel.ClientID.Substring(togglepanel.ClientID.Length - 25, 25);
                image.Attributes.Add("onClick", "togglePanel('" + shortImageClientID + "','" + shortPanelClientID + "');");

                ListView innerlistview = (ListView)dataItem.FindControl("InnerDomainsListView");
                var domains = g.Items;
                innerlistview.DataSource = domains;
                innerlistview.DataBind();
                
                LinkButton downloadallbutton = (LinkButton)dataItem.FindControl("DownloadAllLinkButton");
                if (domains.Count == 0)
                {                   
                    downloadallbutton.Visible = false;

                    Label nodomains = (Label)dataItem.FindControl("NoDomainsLabel");
                    nodomains.Visible = true;
                }
                else
                {
                    downloadallbutton.CommandArgument = g.GroupingID;
                }
            }
        }
        
        private string FormatBPStartEnd(int bpstartnum, int bpendnum)
        {
            NumberFormatInfo nf = new CultureInfo("en-US", false).NumberFormat;
            nf.NumberDecimalDigits = 0;

            string bpstart = bpstartnum.ToString("N", nf);
            string bpend = bpendnum.ToString("N", nf);

            string fromString = "From " + bpstart;
            string toString = "To " + bpend;
            int amtToAdd = toString.Length - fromString.Length;
            string finalFromString = "From" + "&nbsp;";
            string finalToString = "To" + "&nbsp;";

            if (amtToAdd > 0)
            {
                for (int i = 0; i <= amtToAdd; i++)
                {
                    finalFromString = finalFromString + "&nbsp;";
                }
            }
            else if (amtToAdd < 0)
            {
                finalToString = finalToString + "&nbsp;";
                for (int i = 0; i >= amtToAdd; i--)
                {
                    finalToString = finalToString + "&nbsp;";
                }
            }

            finalFromString = finalFromString + bpstart;
            finalToString = finalToString + bpend;
            return finalFromString + "<br />" + finalToString;
        }
        
        private bool getSortDirection(string column, bool flip)
        {
            bool ascending = true;

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if (lastDirection != null)
                    {
                        if (flip == false)
                        {
                            sortDirection = lastDirection;
                        }
                        else if (lastDirection == "ASC")
                        {
                            sortDirection = "DESC";
                        }
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            if (sortDirection == "ASC")
            {
                ascending = true;
            }
            else
            {
                ascending = false;
            }

            return ascending;
        }
        
        protected void ProcessDomainsListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            string sortExpression = ViewState["SortExpression"] as string;
            string lastDirection = ViewState["SortDirection"] as string;
            if ((sortExpression != null) && (lastDirection != null))
            {
                SortProcessedSequences(sortExpression, false);
            }
            ProcessedDomainsDataPager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            detailpanel.Attributes.Add("style", "display=\"visible\"");
           // BindProcessedSequences();
        }

        protected void ProcessedDomains_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string command = e.CommandName;
            SortProcessedSequences(command, true);
            //BindProcessedSequences();
            detailpanel.Attributes.Add("style", "display=\"visible\"");
        }

        private void BindProcessedSequences()
        {            
            ProcessedDomainsListView.DataBind();
        }

        private void SortProcessedSequences(string command, bool flip)
        {
            if (command == "Accession")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => x.GenbankAccession);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => x.GenbankAccession);
                }
            }
            else if (command == "CompoundFamily")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => x.CompoundFamilyName);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => x.CompoundFamilyName);
                }
            }
            else if (command == "PathwayType")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => x.PathwayType);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => x.PathwayType);
                }
            }
            else if (command == "Phylum")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => x.Phylum);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => x.Phylum);
                }
            }
            else if (command == "Organism")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => x.Organism);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => x.Organism);
                }
            }
            else if (command == "Contributor")
            {
                if (getSortDirection(command, flip))
                {
                    ProcessedSequences = ProcessedSequences.OrderBy(x => UserInfos.getUserInfoByID(x.ContributorID).First_Name);
                }
                else
                {
                    ProcessedSequences = ProcessedSequences.OrderByDescending(x => UserInfos.getUserInfoByID(x.ContributorID).First_Name);
                }
            }
        }
    }
}