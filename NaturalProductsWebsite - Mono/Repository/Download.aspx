﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Download.aspx.cs" Inherits="NaturalProductsWebsite.Repository.Download" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<meta http-equiv="refresh" content="150" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Download File</h2>
<br />
<br />
    <asp:Label ID="NoticeLabel" runat="server" CssClass="noerror" Visible="false">The file you have requested will be available from this webpage. It may take 15-20 minutes or longer before your file is ready. Please add this page to your favourites, or record the URL to be able to return to check the status of your request, and download your file.<br /><br /></asp:Label>
<span style="font-size:large"><b>Status: </b><asp:Label ID="StatusLabel" runat="server"></asp:Label>&nbsp;<asp:LinkButton
        ID="DownloadLinkButton" runat="server" Text="(Download)" Visible="false" 
        onclick="DownloadLinkButton_Click"></asp:LinkButton></span>
    <br />
    <br />
</asp:Content>