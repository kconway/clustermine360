﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

namespace NaturalProductsWebsite.Controls
{
    public partial class SortableClusterGridviewControl : System.Web.UI.UserControl
    {
        public List<Cluster> clusters;

        protected void Page_Init(object sender, EventArgs e)
        {
            gv.Sorting += gv_Sorting;
            clusters = Clusters.getClusters().OrderBy(x => x.CompoundFamily.FamilyName).ThenBy(x => x.ClusterBase.Enum_Phyla.Phylum).ToList();
            gv.DataSource = clusters;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewTemplate.applyStyletoGridView(gv);

            if (!IsPostBack)
            {
                gv.DataBind();
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string sortExpression = ViewState["SortExpression"] as string;
            if (!String.IsNullOrEmpty(sortExpression))
            {
                SortClusters(sortExpression, false);
            }

            gv.PageIndex = e.NewPageIndex;
            gv.DataSource = clusters;
            gv.DataBind();
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortexpression = e.SortExpression;
            SortClusters(sortexpression, true);
            gv.DataSource = clusters;
            gv.DataBind();
        }

        private void SortClusters(string sort, bool flip)
        {
            if (sort == "Accession")
            {
                if (getSortDirection(sort, flip))
                {
                    clusters = clusters.OrderBy(x => x.ClusterBase.GenbankAccession).ToList();
                }
                else
                {
                    clusters = clusters.OrderByDescending(x => x.ClusterBase.GenbankAccession).ToList();
                }
            }

            if (sort == "Lineage")
            {
                if (getSortDirection(sort, flip))
                {
                    clusters = clusters.OrderBy(x => x.ClusterBase.Enum_Phyla.Phylum).ToList();
                }
                else
                {
                    clusters = clusters.OrderByDescending(x => x.ClusterBase.Enum_Phyla.Phylum).ToList();
                }
            }

            if (sort == "CompoundFamily")
            {
                if (getSortDirection(sort, flip))
                {
                    clusters = clusters.OrderBy(x => x.CompoundFamily.FamilyName).ToList();
                }
                else
                {
                    clusters = clusters.OrderByDescending(x => x.CompoundFamily.FamilyName).ToList();
                }
            }

            //if (sort == "PathwayType")
            //{
            //    if (getSortDirection(sort, flip))
            //    {
            //        clusters = clusters.OrderBy(x => x.CompoundFamily.Enum_PathwayType.Type).ToList();
            //    }
            //    else
            //    {
            //        clusters = clusters.OrderByDescending(x => x.CompoundFamily.Enum_PathwayType.Type).ToList();
            //    }
            //}

            if (sort == "Contributor")
            {
                if (getSortDirection(sort, flip))
                {
                    clusters = clusters.OrderBy(x => x.ClusterBase.UserInfo1.First_Name).ToList();
                }
                else
                {
                    clusters = clusters.OrderByDescending(x => x.ClusterBase.UserInfo1.First_Name).ToList();
                }
            }
        }

        private bool getSortDirection(string column, bool flip)
        {
            bool ascending = true;

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if (lastDirection != null)
                    {
                        if (flip == false)
                        {
                            sortDirection = lastDirection;
                        }
                        else if (lastDirection == "ASC")
                        {
                            sortDirection = "DESC";
                        }
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            if (sortDirection == "ASC")
            {
                ascending = true;
            }
            else
            {
                ascending = false;
            }

            return ascending;
        }

        protected void CompoundFamilyLiteral_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.CompoundFamilyLiteral_DataBind(sender, e);
            
        }

        protected void DescriptionLiteral_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.DescriptionLiteral_DataBind(sender, e);
           
        }

        protected void HeaderLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.HeaderLabel_DataBind(sender, e);
           
        }

        protected void GenbankStartEnd_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.GenbankStartEnd_DataBind(sender, e);
           
        }

        protected void GenbankLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.GenbankLink_DataBind(sender, e);
          
        }

        protected void TogglePlaceholder_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.TogglePlaceholder_DataBind(sender, e);
           
        }

        protected void PhylumLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.PhylumLabel_DataBind(sender, e);
          
        }

        protected void LineageLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.LineageLabel_DataBind(sender, e);
         
        }

        protected void PathwayType_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.PathwayType_DataBind(sender, e);
          
        }

        protected void antiSmashLink_DataBind(object sender, EventArgs e)
        {
            CommonDataBinding.antiSmashLink_DataBind(sender, e);
           
        }

        protected void NameLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.NameLabel_DataBind(sender, e);
          
        }

        protected void LabLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.LabLink_DataBind(sender, e);
           
        }

        protected void ImageLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.ImageLink_DataBind(sender, e);
            
        }
    }
}