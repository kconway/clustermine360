﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_RelatedFamiliesControl"
    CodeBehind="RelatedFamiliesControl.ascx.cs" %>

<asp:UpdatePanel ID="RFUpdatePanel" runat="server">
    <ContentTemplate>
    
        <asp:Panel runat="server" ID="ReadOnlyPanel">
            <asp:DataList ID="DataList1" runat="server" ShowFooter="False" ShowHeader="False"
                RepeatLayout="Flow" >
                <ItemTemplate>
                    <asp:HyperLink ID="relatedFamilyLink" runat="server" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
               
            </asp:DataList>
        </asp:Panel>
        <asp:Panel runat="server" ID="EditPanel">
        <script type="text/javascript">
            $(document).ready(function () {

                var chk = $('#<%= CheckBoxList1.ClientID %> input[type=checkbox]:checked').parent().addClass('highlight');

                var chkboxes = $('#<%= CheckBoxList1.ClientID %> input[type=checkbox]').click(
              function () {
                  if ($(this).is(':checked')) {
                      $(this).parent().addClass('highlight');
                  } else {
                      $(this).parent().removeClass('highlight');
                  }
              });              

            });
            </script>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="5" RepeatLayout="Table"
                DataTextField="FamilyName" DataValueField="ID" CssClass="floatleft">
            </asp:CheckBoxList>
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click"
                class="submitButton hidden" />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>