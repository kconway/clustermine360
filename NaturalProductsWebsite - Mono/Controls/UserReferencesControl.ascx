﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_UserReferencesControl"
    CodeBehind="UserReferencesControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Repeater ID="DisplayRepeater" runat="server" OnItemCommand="DisplayRepeater_ItemCommand">
            <ItemTemplate>
                <div class="userPMIDWrapper">
                    <asp:Label ID="ReferenceDetails" runat="server"></asp:Label><asp:HiddenField ID="HiddenPMID"
                        runat="server" />
                    <asp:Panel runat="server" CssClass="floatleft width20" ID="RemovePanel">
                        <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="submitButton"
                            CommandName="RemoveItem" />
                        <ajaxToolkit:ConfirmButtonExtender ID="RemoveButtonConfirmButtonExtender" runat="server"
                            TargetControlID="RemoveButton" ConfirmText="Are you sure that you want to delete this item?">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </asp:Panel>
                </div>
            </ItemTemplate>
            <HeaderTemplate>
                <div class="toppadding5" />
            </HeaderTemplate>
            <SeparatorTemplate>
                <div class="toppadding10" />
            </SeparatorTemplate>
            <FooterTemplate>
                <div class="toppadding5" />
            </FooterTemplate>
        </asp:Repeater>
        <div id="addUserPMIDDiv" class="userPMIDWrapper toppadding10 clear" runat="server">
            <asp:TextBox ID="AddPMID" runat="server"></asp:TextBox>
            <asp:Button ID="Add" runat="server" Text="Add" OnClick="Add_Click" CssClass="submitButton" />
            <asp:Button ID="CancelButton" runat="server" Text="Done" OnClick="CancelButton_Click"
                CssClass="submitButton" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter a valid PubMed ID #"
                ValidationGroup="AddPMIDGroup" ValidationExpression="\d+" ControlToValidate="AddPMID"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a PubMed ID #"
                ValidationGroup="AddPMIDGroup" ControlToValidate="AddPMID"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AddPMIDGroup"
                CssClass="error" />
        </div>
        <div id="ModeButtons" class="userPMIDWrapper toppadding10" runat="server">
            <asp:Button ID="AddRemoveButton" runat="server" Text="Add/Remove" OnClick="AddRemoveButton_Click"
                CssClass="submitButton" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>