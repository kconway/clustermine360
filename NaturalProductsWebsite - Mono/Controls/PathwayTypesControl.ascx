﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_PathwayTypesControl"
    CodeBehind="PathwayTypesControl.ascx.cs" %>

<asp:UpdatePanel ID="RFUpdatePanel" runat="server">
    <ContentTemplate>
    
        <asp:Panel runat="server" ID="ReadOnlyPanel">
            <asp:DataList ID="DataList1" runat="server" ShowFooter="False" ShowHeader="False"
                RepeatLayout="Table" OnItemDataBound="DataList_OnItemDataBound">
                <ItemTemplate>
                    <asp:Label ID="TypeName" runat="server"></asp:Label>
                    <br />
                </ItemTemplate>
            </asp:DataList>
        </asp:Panel>
        <asp:Panel runat="server" ID="EditPanel">
        <script type="text/javascript">
            $(document).ready(function () {

                var chk = $('#<%= CheckBoxList1.ClientID %> input[type=checkbox]:checked').parent().addClass('highlight');

                var chkboxes = $('#<%= CheckBoxList1.ClientID %> input[type=checkbox]').click(
              function () {
                  if ($(this).is(':checked')) {
                      $(this).parent().addClass('highlight');
                  } else {
                      $(this).parent().removeClass('highlight');
                  }
              });              

            });
            </script>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                DataTextField="Type" DataValueField="ID" CssClass="floatleft bordercollapse" >
            </asp:CheckBoxList>           
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>