﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompoundFamilyThenOrganismControl.ascx.cs"
    Inherits="NaturalProductsWebsite.Controls.CompoundFamilyThenOrganismControl"
    EnableViewState="True" %>
<%@ Register Src="~/Controls/ClusterGridviewControl.ascx" TagPrefix="uc" TagName="ClusterGridviewControl" %>
<asp:ObjectDataSource ID="CompoundFamilyDataSource" runat="server" TypeName="NaturalProductsWebsite.Controls.AnonymousILists"
    SelectMethod="getNamesOfCompoundFamiliesInAnonymousObject"></asp:ObjectDataSource>
<asp:ListView ID="compoundfamilylist" runat="server" DataKeyNames="Name" OnItemCommand="compoundfamilylink_ItemCommand"
    OnItemDataBound="compoundfamilylist_ItemDataBound" DataSourceID="CompoundFamilyDataSource">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
    </LayoutTemplate>
    <ItemTemplate>
        <asp:LinkButton runat="server" ID="compoundfamilylink" CommandName="FamilyLink" CssClass="browse"
            CommandArgument='<%#Container.DisplayIndex%>'></asp:LinkButton>
        <asp:Panel ID="innerpanel" CssClass="inner" runat="server" Visible="false">
            <asp:ListView ID="organismlist" runat="server" DataKeyNames="Name,ParentID,ParentName"
                OnItemCommand="organismlink_OnItemCommand" OnItemDataBound="organismlist_ItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="organismlink" CssClass="browse" CommandName="OrganismLink"
                        CommandArgument='<%#Container.DisplayIndex %>'></asp:LinkButton>
                    <asp:PlaceHolder ID="GridViewPlaceHolder" runat="server" Visible="false"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </ItemTemplate>
</asp:ListView>