﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhylumThenOrganismThenCompoundFamilyControl.ascx.cs"
    Inherits="NaturalProductsWebsite.Controls.PhylumThenOrganismThenCompoundFamilyControl"
    EnableViewState="True" %>
<%@ Register Src="~/Controls/ClusterGridviewControl.ascx" TagPrefix="uc" TagName="ClusterGridviewControl" %>
<asp:ObjectDataSource ID="PhylumDataSource" runat="server" TypeName="NaturalProductsWebsite.Controls.AnonymousILists"
    SelectMethod="getNamesOfPhylaInAnonymousObject"></asp:ObjectDataSource>
<asp:ListView ID="phylumlist" runat="server" DataKeyNames="Name" OnItemCommand="phylumlink_ItemCommand"
    OnItemDataBound="phylumlist_ItemDataBound" DataSourceID="PhylumDataSource">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
    </LayoutTemplate>
    <ItemTemplate>
        <asp:LinkButton runat="server" ID="phylumlink" CommandName="PhylumLink" CssClass="browse"
            CommandArgument='<%#Container.DisplayIndex%>'></asp:LinkButton>
        <asp:Panel ID="innerpanel" CssClass="inner" runat="server" Visible="false">
            <asp:ListView ID="organismlist" runat="server" DataKeyNames="Name,Parent" OnItemCommand="organismlink_OnItemCommand"
                OnItemDataBound="organismlist_ItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="organismlink" CssClass="browse" CommandName="OrganismLink"
                        CommandArgument='<%#Container.DisplayIndex %>'></asp:LinkButton>
                    <asp:Panel ID="innerpanel" CssClass="inner" runat="server" Visible="false">
                        <asp:ListView ID="compoundfamilylist" runat="server" DataKeyNames="CFName,Org,Phylum"
                            OnItemCommand="compoundfamilylink_OnItemCommand" OnItemDataBound="compoundfamilylist_ItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="compoundfamilylink" CssClass="browse" CommandName="CompoundFamilyLink"
                                    CommandArgument='<%#Container.DisplayIndex %>'></asp:LinkButton>
                                <asp:PlaceHolder ID="GridViewPlaceHolder" runat="server" Visible="false"></asp:PlaceHolder>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:Panel>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </ItemTemplate>
</asp:ListView>