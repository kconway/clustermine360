﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_PubMedIDControl" CodeBehind="PubChemIDControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
        <asp:Repeater ID="DisplayRepeater" runat="server" OnItemCommand="DisplayRepeater_ItemCommand">
            <ItemTemplate>
                <div style="height:25px">
                    <asp:Label ID="PubChemID" runat="server"></asp:Label><asp:HiddenField ID="HiddenPCID"
                        runat="server" />
                    <span runat="server" class="indent" ID="RemovePanel">
                        <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="submitButton"
                            CommandName="RemoveItem" />
                        <ajaxToolkit:ConfirmButtonExtender ID="RemoveButtonConfirmButtonExtender" runat="server"
                            TargetControlID="RemoveButton" ConfirmText="Are you sure that you want to delete this item?">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </span>
                </div>
            </ItemTemplate>
            <HeaderTemplate>
               
            </HeaderTemplate>
            <SeparatorTemplate>
            
            </SeparatorTemplate>
            <FooterTemplate>
              
            </FooterTemplate>
        </asp:Repeater>
        <div id="addUserPCIDDiv" class="userPMIDWrapper toppadding10 clear" runat="server">
            <asp:DropDownList ID="PubChemDropDownList" runat="server">
            <asp:ListItem Text="PubChem Substance" Value="S" Selected="True">            
            </asp:ListItem>
            <asp:ListItem Text="PubChem Compound" Value="C"></asp:ListItem>         
            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                    runat="server" ErrorMessage="Please select the type of ID" ControlToValidate="PubChemDropDownList" Display="Dynamic" CssClass="error"></asp:RequiredFieldValidator>
            <asp:TextBox ID="AddPCID" runat="server"></asp:TextBox>
            <asp:Button ID="Add" runat="server" Text="Add" OnClick="Add_Click" CssClass="submitButton" />
            <asp:Button ID="CancelButton" runat="server" Text="Done" OnClick="CancelButton_Click"
                CssClass="submitButton" />
            
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a PubChem ID #"
                ValidationGroup="AddPCIDGroup" ControlToValidate="AddPCID"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                    ID="PubChemIDValidator" runat="server" ErrorMessage="Please enter a valid PubChem ID" ControlToValidate="AddPCID" Display="Dynamic" CssClass="error" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AddPCIDGroup"
                CssClass="error" />
            
        </div>
        <div id="ModeButtons" class="userPMIDWrapper toppadding10" runat="server">
            <asp:Button ID="AddRemoveButton" runat="server" Text="Add/Remove" OnClick="AddRemoveButton_Click"
                CssClass="submitButton" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>