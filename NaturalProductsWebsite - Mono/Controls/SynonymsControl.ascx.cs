﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
using NaturalProductsWebsite.Code;
using Utilities;

public partial class Controls_SynonymsControl : System.Web.UI.UserControl
{
    //private static readonly ILog log = LogManager.GetLogger(typeof(Controls_SynonymsControl));
    public int mode;

    // mode = 0, displayonly
    // mode = 1, display with button to go to edit mode
    // mode = 2, add
    // mode = 3, edit
    public int cfid;
    bool render = false;
    List<Synonym> synonyms;

    protected void Page_Init(object sender, EventArgs e)
    {
        //XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
        UpdatePanel1.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        
        if (render)
        {
            synonyms = Synonyms.getSynonymsByCompoundFamilyID(cfid);           
            updateDisplay(synonyms);
            ViewState["mode"] = mode;
        }
    }

    public void Reset()
    {
        synonyms = null;
        cfid = 0;
        ChemSpiderSynonymsDiv.Visible = false;
        ChemSpiderSynonymsButton.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["mode"] = null;
        }

        if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
        {
            int clusterid = int.Parse(Request.QueryString["clusterid"]);
            cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;
        }

        if (ViewState["mode"] != null)
        {
            mode = (int)ViewState["mode"];
        }
        DataClassesDataContext db = new DataClassesDataContext();
        render = db.CompoundFamilies.Exists(x => x.ID == cfid);
        db.Dispose();
    }

    private void updateDisplay(List<Synonym> synonyms)
    {       
        if (mode == 0)
        {
            DisplayRepeater.Visible = true;
            AddAndEditDiv.Visible = false;

            DisplayRepeater.DataSource = synonyms;
            DisplayRepeater.ItemDataBound += Repeater_OnItemDataBound;
            DisplayRepeater.DataBind();
        }
        else if (mode == 1)
        {
            DisplayRepeater.Visible = true;
            AddAndEditDiv.Visible = true;
            EditRepeater.Visible = false;
            addSynonymDiv.Visible = false;
            SynonymButtons.Visible = true;
            EditButton.Visible = true;
            CancelButton.Visible = false;
            SaveButton.Visible = false;
            AddButton.Visible = true;
            EditInfoLabel.Visible = false;

            DisplayRepeater.DataSource = synonyms;
            DisplayRepeater.ItemDataBound += Repeater_OnItemDataBound;
            DisplayRepeater.DataBind();
        }
        else if (mode == 2)
        {
            
                DisplayRepeater.Visible = true;
                AddAndEditDiv.Visible = true;
                EditRepeater.Visible = false;
                addSynonymDiv.Visible = true;
                SynonymButtons.Visible = false;
                CancelButton.Visible = false;
                SaveButton.Visible = false;
                EditInfoLabel.Visible = false;

                DisplayRepeater.DataSource = synonyms;
                DisplayRepeater.ItemDataBound += Repeater_OnItemDataBound;
                DisplayRepeater.DataBind();
           
        }
        else if (mode == 3)
        {
            DisplayRepeater.Visible = false;
            AddAndEditDiv.Visible = true;
            EditRepeater.Visible = true;
            addSynonymDiv.Visible = false;
            SynonymButtons.Visible = true;
            EditButton.Visible = false;
            CancelButton.Visible = true;
            SaveButton.Visible = true;
            AddButton.Visible = false;
            EditInfoLabel.Visible = true;

            EditRepeater.DataSource = synonyms;
            EditRepeater.ItemDataBound += Repeater_OnItemDataBound;
            EditRepeater.DataBind();
        }
    }

    private void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {        
        RepeaterItem item = (RepeaterItem)e.Item;
        if (mode == 0 || mode == 1 || mode == 2)
        {
            Label synonymName = (Label)item.FindControl("SynonymName");
            synonymName.Text = (string)DataBinder.Eval(item.DataItem, "Synonym1");
        }
        else if (mode == 3)
        {
            TextBox tb = (TextBox)item.FindControl("SynonymTextBox");
            tb.Text = (string)DataBinder.Eval(item.DataItem, "Synonym1");

            HiddenField old = (HiddenField)item.FindControl("OldSynonym");
            old.Value = (string)DataBinder.Eval(item.DataItem, "Synonym1");

            HiddenField synID = (HiddenField)item.FindControl("SynonymID");
            synID.Value = DataBinder.Eval(item.DataItem, "ID").ToString();

            Button deleteButton = (Button)item.FindControl("RemoveButton");
            deleteButton.UseSubmitBehavior = true;
            deleteButton.CommandName = "ItemDeleteButton";
        }
    }

    protected void EditRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int synonymid = int.Parse(((HiddenField)e.Item.FindControl("SynonymID")).Value);
        if (e.CommandName == "ItemDeleteButton")
        {
            // cfid, synonymname, delete
            Synonyms.deleteSynonym(cfid, synonymid);
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in EditRepeater.Items)
        {
            int synonymid = int.Parse(((HiddenField)item.FindControl("SynonymID")).Value);
            string oldvalue = ((HiddenField)item.FindControl("OldSynonym")).Value;
            string newvalue = ((TextBox)item.FindControl("SynonymTextBox")).Text;

            if (oldvalue != newvalue)
            {
                // value has changed
                Synonyms.updateSynonym(cfid, synonymid, newvalue);
            }
        }

        CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
    }

    protected void Add_Click(object sender, EventArgs e)
    {
        string synonymToAdd = AddSynonym.Text;
        if (!String.IsNullOrEmpty(synonymToAdd))
        {
            synonymToAdd = StringTools.ToUpperCaseFirstLetterOnly(synonymToAdd);
            Synonyms.insertSynonym(cfid, synonymToAdd);
        }

        AddSynonym.Text = "";
        CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
    }

    protected void AddCancel_Click(object sender, EventArgs e)
    {
        mode = 1;
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        mode = 1;
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        mode = 2;
    }

    protected void EditButton_Click(object sender, EventArgs e)
    {
        mode = 3;
    }

    protected void LoadSynonyms_Click(object sender, EventArgs e)
    {
        foreach (ListItem item in ChemSpiderCheckBoxList.Items)
        {
            if (item.Selected)
            {
                string synonymToAdd = StringTools.ToUpperCaseFirstLetterOnly(item.Text);
                Synonyms.insertSynonym(cfid, synonymToAdd);
            }
        }
        ChemSpiderSynonymsDiv.Visible = false;
        ChemSpiderSynonymsButton.Visible = true;
        CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);

    }

    protected void ChemSpiderSynonymsButton_Click(object sender, EventArgs e)
    {
        ChemSpiderSynonymsDiv.Visible = true;
        ChemSpiderSynonymsButton.Visible = false;

        synonyms = Synonyms.getSynonymsByCompoundFamilyID(cfid);

        Structure image = new Structure();
        int[] csids = image.getCSIDs(cfid);

        if (csids != null && csids.Length != 0)
        {
            int csid = csids[0];
            List<string> ChemSpiderSynonyms = image.retrieveSynonyms(csid);
            ChemSpiderSynonyms = ChemSpiderSynonyms.Where(s => !s.Any(Char.IsDigit)).Select(w => StringTools.ToUpperCaseFirstLetterOnly(w)).ToList();
            string cfname = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;
            ChemSpiderSynonyms.Remove(cfname);
            ChemSpiderSynonyms.RemoveAll(x => x.Contains(cfname + " "));
            ChemSpiderCheckBoxList.DataSource = ChemSpiderSynonyms;

            if (ChemSpiderSynonyms.Count == 0)
            {
                NoSynonymsLabel.Visible = true;
            }
            else
            {
                NoSynonymsLabel.Visible = false;
            }
            
            ChemSpiderCheckBoxList.DataBind();
        }
        else
        {
            NoSynonymsLabel.Visible = true;
        }
    }

    protected void CloseButton_Click(object sender, EventArgs e)
    {
        ChemSpiderSynonymsDiv.Visible = false;
        ChemSpiderSynonymsButton.Visible = true;
    }
}