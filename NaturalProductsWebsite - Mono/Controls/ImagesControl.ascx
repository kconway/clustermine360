﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImagesControl.ascx.cs"
    Inherits="NaturalProductsWebsite.Controls.ImagesControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script src="../Scripts/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
<link href="../Styles/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
<span id="SelectButton" class="link">Select Image</span>
<asp:Image ID="UploadedImageIcon" ImageUrl="~/Images/success.png" runat="server"
    Visible="false" CssClass="floatright" />
<div id="ImageChoiceSelector" class="page main">
    <h3>
        Add an Image for
        <asp:Label ID="CFNameAdd" runat="server"></asp:Label></h3>
    <ul>
        <li id="ChemSpiderli" class="link">Query the ChemSpider Database for an image. </li>
        <li id="SMILESli" class="link">Generate an image from a SMILES string. </li>
        <li id="ManualUploadli" class="link">Upload an image. </li>
    </ul>
    <br />
    <h3>
        Remove the Current Image for
        <asp:Label ID="CFNameRemove" runat="server"></asp:Label></h3>
    <p>
        <b>
            <asp:Label ID="NoCurrentImage" runat="server" Text="No image has been uploaded for this compound family."
                Visible="false"></asp:Label>
        </b>
    </p>
    <br />
    <asp:Image ID="CFViewImage" runat="server" Visible="false" CssClass="border" AlternateText="No image has been uploaded."  Width="250px" />
    <br />
    <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="submitButton floatleft"
        OnClick="RemoveButton_Click" />
    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="RemoveButton"
        ConfirmText="Are you sure you want to remove this image?" />
</div>
<div id="ChemSpiderDiv" class="page main">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>
                Find an image for
                <asp:Label ID="CFNameChemSpider" runat="server"></asp:Label>
                in the ChemSpider Database</h2>
            <br />
            <div id="ErrorDiv" runat="server">
                <asp:Label runat="server" ID="ErrorLabel" CssClass="error"></asp:Label>
                <br />
                <br />
            </div>
            <div runat="server" id="autoImage">
                <div runat="server" id="ImageDiv">
                    <asp:Image ID="ImageChemSpider" runat="server" AlternateText="Image not currently available. Please try again later." Width="250px" />
                    <br />
                    <br />
                    <asp:HyperLink ID="ChemSpiderHyperLink" runat="server" Target="_blank"></asp:HyperLink>
                    <br />
                    <br />
                </div>
                <div id="ButtonsDiv" runat="server" class="leftImageColumn">
                    <asp:Button ID="UseThisImageButton" runat="server" Text="Use This Image" CssClass="submitButton floatleft"
                        OnClick="UseChemSpider_Click" Width="200px" />
                    <br class="clear" />
                    <asp:Button ID="NextImageButton" runat="server" Text="Next Image" CssClass="submitButton floatleft"
                        OnClick="NextChemSpider_Click" Width="200px" />
                    <br class="clear" />
                    <asp:Button ID="PreviousImageButton" runat="server" Text="Previous Image" CssClass="submitButton floatleft"
                        Width="200px" OnClick="PreviousChemSpider_Click" />
                    <br class="clear" />
                    <asp:ConfirmButtonExtender ID="SaveConfirmButtonExtender" runat="server" TargetControlID="UseThisImageButton"
                        ConfirmText="Are you sure you want to use this image?" />
                </div>
                <div id="SynonymsDiv" runat="server" class="floatleft overflowauto">
                    <div class="alignleft rightImageColumn">
                        <b style="font-size: large">Names/Synonyms</b>
                        <br />
                        <br />
                        <asp:ListView ID="ListView1" runat="server">
                            <LayoutTemplate>
                                <div id="itemPlaceholder" runat="server">
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <%# Container.DataItem %>
                                <br />
                            </ItemTemplate>
                        </asp:ListView>
                        <br class="clear" />
                    </div>
                </div>
                <div class="floatleft aligncenter">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                        <ProgressTemplate>
                            <img alt="Updating" src="../Images/ajax-loader.gif" style="vertical-align: middle" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br class="clear" />
                </div>
                <br class="clear" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div id="ManualUploadDiv" class="page main aligncenter">
    <h2>
        Manual Image Uploader
    </h2>
    <h3>
        Select a image for
        <asp:Label ID="CFNameManual" runat="server"></asp:Label>
    </h3>
    <p>
        Image should be 500 x 500 pixels.
        <br />
        Preferred images types: png, gif, jpg/jpeg
    </p>
    <br />
    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="submitButton" Height="25px"
        Width="385px" />
    <asp:Button ID="UploadButton" runat="server" Text="Upload" CssClass="submitButton"
        OnClick="UploadManualButton_Click" />
    <br />
    <br />
    <asp:Label ID="ManualErrorLabel" Visible="false" runat="server"></asp:Label><br />
    <br />
    <p>
        <b>
            <asp:Label ID="ImageManualNotPresentLabel" runat="server" Text="No image has been uploaded for this compound family."
                Visible="false"></asp:Label>
        </b>
    </p>
    <br />
    <asp:Image ID="ImageViewManual" runat="server" Visible="false" CssClass="border"
        AlternateText="No image has been uploaded."  Width="250px" />
</div>
<div id="SMILESDiv" class="page main aligncenter">
    <h2>
        Generate Image From SMILES
    </h2>
    <h3>
        Please enter a canonical SMILES string for
        <asp:Label ID="CFNameSmiles" runat="server"></asp:Label>
    </h3>
    <br />
    <asp:TextBox ID="SMILESTextBox" runat="server"></asp:TextBox><asp:Button ID="GenerateSMILESButton"
        runat="server" Text="Generate" CssClass="submitbutton" OnClick="GenerateSMILESButton_Click"
        ValidationGroup="SMILES" />
    <br />
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a SMILES string."
        ValidationGroup="SMILES" ControlToValidate="SMILESTextBox" Display="Dynamic"
        CssClass="error"></asp:RequiredFieldValidator><asp:CustomValidator ID="SMILESCustomValidator"
            runat="server" ErrorMessage="Unfortunately, no R groups are permitted." Display="Dynamic" ControlToValidate="SMILESTextBox" CssClass="error" ValidationGroup="SMILES" OnServerValidate="SMILESValidateRGroups"></asp:CustomValidator>
    <asp:Label ID="SMILESErrorLabel" Visible="false" runat="server"></asp:Label><br />
    <br />
    <asp:Image ID="SMILESImage" runat="server" Visible="false" CssClass="border" AlternateText="No image has been generated."  Width="250px" />
    <br class="clear" />
</div>