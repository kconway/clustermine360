﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Controls_RelatedFamiliesControl : System.Web.UI.UserControl
{
    public int mode;
    // mode = 0, displayonly
    // mode = 1, edit: checkboxlist

    public int cfid;
    bool render = false;
    List<string> relatedfamilies;

    protected void Page_Init(object sender, EventArgs e)
    {
        RFUpdatePanel.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (render)
        {
            relatedfamilies = RelatedCompoundFamilies.getRelatedCompoundFamiliesByCompoundFamily(cfid);
            updateDisplay();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
        {
            int clusterid = int.Parse(Request.QueryString["clusterid"]);
            cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;
        }

        DataClassesDataContext db = new DataClassesDataContext();
        render = db.CompoundFamilies.Exists(x => x.ID == cfid);
        db.Dispose();
    }

    private void updateDisplay()
    {
        if (mode == 0)
        {
            EditPanel.Visible = false;
            ReadOnlyPanel.Visible = true;

            DataList1.DataSource = relatedfamilies;
            DataList1.ItemDataBound += DataList_OnItemDataBound;
            DataList1.DataBind();
        }
        else if (mode == 1)
        {
            EditPanel.Visible = true;
            ReadOnlyPanel.Visible = false;

            List<CompoundFamily> cfs = CompoundFamilies.getCompoundFamilies();
            cfs = cfs.Where(x => x.ID != cfid).ToList<CompoundFamily>();
            cfs = cfs.Where(x => !x.FamilyName.Contains("Unknown")).ToList<CompoundFamily>();
            CheckBoxList1.DataSource = cfs;
            CheckBoxList1.DataBind();

            relatedfamilies = RelatedCompoundFamilies.getRelatedCompoundFamiliesByCompoundFamily(cfid);
            foreach (ListItem cb in CheckBoxList1.Items)
            {
                if (relatedfamilies.Exists(x => x == cb.Text))
                {
                    cb.Selected = true;
                }
                else
                {
                    cb.Selected = false;
                }
            }
        }
    }

    private void DataList_OnItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataListItem item = (DataListItem)e.Item;
        if (mode == 0)
        {
            HyperLink relatedFamilyName = (HyperLink)item.FindControl("relatedFamilyLink");
            relatedFamilyName.Text = (string)e.Item.DataItem;
            int relatedCFID = CompoundFamilies.getCompoundFamilyByName(relatedFamilyName.Text).ID;
            relatedFamilyName.NavigateUrl = "~/CompoundFamilyDetails.aspx?cfid=" + relatedCFID;
            relatedFamilyName.Font.Underline = false;
        }
    }

    public void SaveButton_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        relatedfamilies = RelatedCompoundFamilies.getRelatedCompoundFamiliesByCompoundFamily(cfid);
        foreach (ListItem listItem in CheckBoxList1.Items)
        {
            if (relatedfamilies.Exists(x => x == listItem.Text))
            {
                if (!listItem.Selected)
                {
                    // remove non-existing relationships
                    int relatedFamilyID = CompoundFamilies.getCompoundFamilyByName(listItem.Text).ID;
                    RelatedCompoundFamily relationshipToDelete = (from x in db.RelatedCompoundFamilies
                                                                  where ((x.FamilyNameID == cfid && x.RelatedFamilyNameID == relatedFamilyID)
                                                                  || (x.RelatedFamilyNameID == cfid && x.FamilyNameID == relatedFamilyID))
                                                                  select x).SingleOrDefault();
                    if (relationshipToDelete != null)
                    {
                        db.RelatedCompoundFamilies.DeleteOnSubmit(relationshipToDelete);
                        db.SubmitChanges();
                    }
                }
            }
            else if (listItem.Selected)
            {
                // add missing relationships             
                int relatedFamilyID = CompoundFamilies.getCompoundFamilyByName(listItem.Text).ID;
                RelatedCompoundFamilies.insertNewRelatedCompoundFamilyRelationship(cfid, relatedFamilyID);               
            }
        }
        relatedfamilies = RelatedCompoundFamilies.getRelatedCompoundFamiliesByCompoundFamily(cfid);
        updateDisplay();
        db.Dispose();
    }
}