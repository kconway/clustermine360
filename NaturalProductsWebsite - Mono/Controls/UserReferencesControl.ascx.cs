﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;

public partial class Controls_UserReferencesControl : System.Web.UI.UserControl
{
    private enum UpdateType
    {
        Add,
        Remove,
        Edit
    };

    public int mode;
    // mode = 0, display only
    // mode = 1, display with button to go to edit mode
    // mode = 2, add / remove

    public int clusterid;
    List<PubMedArticle> pubMedArticles;

    protected void Page_Init(object sender, EventArgs e)
    {
        UpdatePanel1.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        updateDisplay();
        pubMedArticles = PubMedArticles.getUserArticlesByClusterID(clusterid);
        DisplayRepeater.DataSource = pubMedArticles;
        DisplayRepeater.ItemDataBound += Repeater_OnItemDataBound;
        DisplayRepeater.DataBind();
        ViewState["referencesmode"] = mode;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["referencesmode"] = null;
        }

        if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
        {
            clusterid = int.Parse(Request.QueryString["clusterid"]);
        }
        if (ViewState["referencesmode"] != null)
        {
            mode = (int)ViewState["referencesmode"];
        }

        pubMedArticles = PubMedArticles.getUserArticlesByClusterID(clusterid);
    }

    private void updateDisplay()
    {
        if (mode == 0)
        {
            ModeButtons.Visible = false;
            addUserPMIDDiv.Visible = false;
        }
        else if (mode == 1)
        {
            ModeButtons.Visible = true;
            addUserPMIDDiv.Visible = false;
            AddRemoveButton.Visible = true;
            CancelButton.Visible = false;
        }
        else if (mode == 2)
        {
            ModeButtons.Visible = true;
            addUserPMIDDiv.Visible = true;
            AddRemoveButton.Visible = false;
            CancelButton.Visible = true;
        }
    }

    protected void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = (RepeaterItem)e.Item;

        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            Label refDetails = (Label)item.FindControl("ReferenceDetails");
            string details = (string)DataBinder.Eval(item.DataItem, "Details");
            string pmidValue = DataBinder.Eval(item.DataItem, "PMID").ToString();

            if (String.IsNullOrEmpty(details))
            {
                refDetails.Text = "Record is currently being processed. <b>PMID:</b> <i>" + pmidValue + "</i>";
            }
            else
            {
                refDetails.Text = details;
            }

            HiddenField pmid = (HiddenField)item.FindControl("HiddenPMID");
            pmid.Value = pmidValue;

            Panel removePanel = (Panel)item.FindControl("RemovePanel");
            if (mode == 0 || mode == 1)
            {
                removePanel.Visible = false;
                refDetails.CssClass = "";
            }
            else if (mode == 2)
            {
                removePanel.Visible = true;
                refDetails.CssClass = "floatleft width80";
            }
        }
    }

    protected void DisplayRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        long pmid = long.Parse(((HiddenField)e.Item.FindControl("HiddenPMID")).Value);
        if (e.CommandName == "RemoveItem")
        {
            // remove from list
            pubMedArticles.RemoveAll(x => x.PMID == pmid);
            List<string> test = pubMedArticles.Select(x => x.PMID.ToString()).ToList<string>();
            updateClusterUserReferences(test, UpdateType.Remove);
            triggerReferenceRemoval(pmid.ToString());
        }
    }

    protected void Add_Click(object sender, EventArgs e)
    {
        string pmidToAdd = AddPMID.Text.Trim();
        List<string> pmids = pubMedArticles.Select(x => x.PMID.ToString()).ToList<string>();
        if (!String.IsNullOrEmpty(pmidToAdd))
        {
            pmids.Add(pmidToAdd);
            addPubMedArticle(pmidToAdd);
            updateClusterUserReferences(pmids, UpdateType.Add);
        }

        // update references field
        AddPMID.Text = "";
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        mode = 1;
    }

    protected void AddRemoveButton_Click(object sender, EventArgs e)
    {
        mode = 2;
    }

    private void addPubMedArticle(string pmid)
    {
        DataClassesDataContext db2 = new DataClassesDataContext();

        long articlePMID = long.Parse(pmid);
        if (!db2.PubMedArticles.Exists(x => x.PMID == articlePMID))
        {
            PubMedArticle newArticle = new PubMedArticle();
            newArticle.PMID = articlePMID;
            newArticle.Details = "";
            newArticle.Type = 1;
            db2.PubMedArticles.InsertOnSubmit(newArticle);
            db2.SubmitChanges();
        }
        db2.Dispose();
    }

    private void updateClusterUserReferences(List<string> articles, UpdateType type)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        Cluster record = (db.Clusters.Where(x => x.ID == clusterid)).Single();
        record.UserReferences = StringTools.SerializeToSemiColonSeparatedList(articles);
        int userid = UserInfos.getUserInfoByUserName(HttpContext.Current.User.Identity.Name).ID;
        record.ClusterBase.LastModifiedUserID = userid;
        record.ClusterBase.LastModified = DateTime.Now;
        if (type == UpdateType.Add)
        {
            record.ClusterBase.NCBIStatus = 0;
            record.ClusterBase.NCBIModified = DateTime.Now;
        }

        db.SubmitChanges();
        db.Dispose();
    }

    private void triggerReferenceRemoval(string pmid)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        bool containsPMID = db.Clusters.Exists(x => x.UserReferences.Contains(pmid) || x.SequencingReferences.Contains(pmid));
        if (containsPMID == false)
        {
            // remove PubMedArticle record
            long pmidValue = long.Parse(pmid);
            PubMedArticle articleToRemove = (from p in db.PubMedArticles where p.PMID == pmidValue select p).SingleOrDefault();
            if (articleToRemove != null)
            {
                db.PubMedArticles.DeleteOnSubmit(articleToRemove);
                db.SubmitChanges();
            }
        }
        db.Dispose();
    }
}