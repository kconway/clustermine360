﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class Controls_PathwayTypesControl : System.Web.UI.UserControl
{
    public int mode;

    // mode = 0, displayonly
    // mode = 1, edit: checkboxlist
    // mode = 2, add
    public int cfid;

    private bool render = true;

    protected void Page_Init(object sender, EventArgs e)
    {
        RFUpdatePanel.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (render)
        {
            updateDisplay();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
        {
            int clusterid = int.Parse(Request.QueryString["clusterid"]);
            cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;
        }

        //DataClassesDataContext db = new DataClassesDataContext();
        //render = db.CompoundFamilies.Exists(x => x.ID == cfid);
        //db.Dispose();
    }

    private void updateDisplay()
    {
        if (mode == 0)
        {
            EditPanel.Visible = false;
            ReadOnlyPanel.Visible = true;

            DataList1.DataSource = PathwayTypes.getPathwayTypesByCompoundFamilyID(cfid);
            DataList1.DataBind();
        }
        else if ((mode == 1) || (mode == 2))
        {
            EditPanel.Visible = true;
            ReadOnlyPanel.Visible = false;

            CheckBoxList1.DataSource = PathwayTypes.getPathwayTypes();
            CheckBoxList1.DataBind();
            UpdateCheckBoxes();
        }
    }

    private void UpdateCheckBoxes()
    {
        List<int> paths = PathwayTypes.getPathwayTypeIDsByCompoundFamilyID(cfid);
        foreach (ListItem cb in CheckBoxList1.Items)
        {
            int val = int.Parse(cb.Value);
            if (paths.Contains(val))
            {
                cb.Selected = true;
            }
            else
            {
                cb.Selected = false;
            }

            int unknownpid = PathwayTypes.getPathwayID("Unknown");
            if ((mode == 2) && (int.Parse(cb.Value) == unknownpid))
            {
                cb.Text = "Assign Automatically";
            }
        }
    }

    protected void DataList_OnItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataListItem item = (DataListItem)e.Item;
            Label typeName = (Label)item.FindControl("TypeName");
            typeName.Text = (string)e.Item.DataItem;
        }
    }

    public void Save()
    {
        List<int> paths = PathwayTypes.getPathwayTypeIDsByCompoundFamilyID(cfid);
        List<int> idsToDelete = new List<int>();
        List<int> idsToAdd = new List<int>();
        foreach (ListItem listItem in CheckBoxList1.Items)
        {
            int val = int.Parse(listItem.Value);

            if ((!listItem.Selected) && (paths.Contains(val)))
            {
                // remove non-existing relationships
                idsToDelete.Add(val);
            }
            else if ((listItem.Selected) && (!paths.Contains(val)))
            {
                // add missing relationships
                idsToAdd.Add(val);
            }
        }

        PathwayTypes.DeletePathwayToCompoundFamilyRelationship(cfid, idsToDelete);
        PathwayTypes.AddPathwayTypeToCompoundFamilyRelationship(cfid, idsToAdd, false);
        int unknownpid = PathwayTypes.getPathwayID("Unknown");

        paths = PathwayTypes.getPathwayTypeIDsByCompoundFamilyID(cfid);
        if (paths.Count == 0)
        {  // add unknown if no other relationship
            PathwayTypes.AddPathwayTypeToCompoundFamilyRelationship(cfid, new List<int>() { unknownpid }, false);
        }
        else if (paths.Count > 1)
        {
            //remove unknown if present
            PathwayTypes.DeletePathwayToCompoundFamilyRelationship(cfid, new List<int>() { unknownpid });

            //check if hybrid and add anotation if this is the case
            int hybridid = 2;
            List<string> types = PathwayTypes.getPathwayTypesByCompoundFamilyID(cfid);
            bool containspks = false;
            bool containsnrps = false;
            foreach (var type in types)
            {
                if (type.Contains("PKS"))
                {
                    containspks = true;
                }

                if (type.Contains("NRPS"))
                {
                    containsnrps = true;
                }
            }
            if (containsnrps && containspks)
            {
                PathwayTypes.AddPathwayTypeToCompoundFamilyRelationship(cfid, new List<int>() { hybridid }, false);
            }
        }

        updateDisplay();
    }
}