﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
using Utilities;
using log4net;

namespace NaturalProductsWebsite.Controls
{
    
    public partial class ImagesControl : System.Web.UI.UserControl
    {
        public int cfid;
        private bool render = false;
        private bool ChemSpiderIsDirty = false;
        private string script = "";
        private int csid;
        private bool ChemSpiderLast = false;
 private static readonly ILog log = LogManager.GetLogger(typeof(ImagesControl));
        protected void Page_Init(object sender, EventArgs e)
        {
            UpdatePanel1.PreRender += Page_LoadComplete;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
            {
                cfid = int.Parse(Request.QueryString["cfid"]);
            }

            if (!IsPostBack)
            {
                if (Session["ChemSpiderPostBack"] != null)
                {
                    bool chemspiderpostback = (bool)Session["ChemSpiderPostBack"];
                    if (chemspiderpostback)
                    {
                        // do nothing
                    }
                    else
                    {
                        ViewState["csid"] = null;
                        csid = 0;
                    }
                }
                else
                {
                    ViewState["csid"] = null;
                    csid = 0;
                }
            }
            else
            {
                if (ViewState["csid"] != null)
                {
                    csid = (int)ViewState["csid"];
                }
                else
                {
                    csid = 0;
                }
            }

            // Main
            ManualErrorLabel.Visible = false;
            ErrorDiv.Visible = false;

            // ChemSpider
            ChemSpiderIsDirty = false;
            if (ViewState["ChemSpiderLast"] != null)
            {
                ChemSpiderLast = bool.Parse(ViewState["ChemSpiderLast"].ToString());
            }
        }

        public void Reset()
        {
            ViewState["csid"] = null;
            ViewState["csid"] = null;
            Session["ChemSpiderPostBack"] = null;
            ViewState["ChemSpiderLast"] = null;
            csid = 0;
            cfid = 0;
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            render = db.CompoundFamilies.Exists(x => x.ID == cfid);
            db.Dispose();
            if (render)
            {
                string familyname = CompoundFamilies.getCompoundFamilyByID(cfid).FamilyName;

                CFNameAdd.Text = familyname;
                CFNameRemove.Text = familyname;
                CFNameChemSpider.Text = familyname;
                CFNameSmiles.Text = familyname;
                CFNameManual.Text = familyname;

                // Main
                CompoundFamilyImage cfimage = CompoundFamilies.getCompoundFamilyImage(cfid);
                if (cfimage != null && cfimage.Image != null && cfimage.Image.Length != 0)
                {
                    CFViewImage.Visible = true;
                    ImageViewManual.Visible = true;
                    SMILESImage.Visible = true;
                    string imageurl = "~/StructureImageHandler.ashx?cfid=" + cfid;
                    CFViewImage.ImageUrl = imageurl;
                    ImageViewManual.ImageUrl = imageurl;
                    SMILESImage.ImageUrl = imageurl;
                    RemoveButton.Visible = true;
                    ImageManualNotPresentLabel.Visible = false;
                    UploadedImageIcon.Visible = true;
                    NoCurrentImage.Visible = false;
                }
                else
                {
                    CFViewImage.Visible = false;
                    ImageViewManual.Visible = false;
                    SMILESImage.Visible = false;
                    NoCurrentImage.Visible = true;
                    RemoveButton.Visible = false;
                    ImageManualNotPresentLabel.Visible = true;
                    UploadedImageIcon.Visible = false;
                }

                // ChemSpider
                Structure image = new Structure();
                if (csid == 0)
                {
                    int[] csids = image.getCSIDs(cfid);
                    // int[] csids = null;
                    if (csids != null && csids.Length != 0)
                    {
                        csid = csids[0];
                        ViewState["csid"] = csid;
                        PreviousImageButton.Visible = false;
                    }
                    else
                    {
                        ErrorLabel.Text = "No compounds were found to match " + familyname + " in the ChemSpider database. Please generate an image from a SMILES string or upload one manually.";

                        SynonymsDiv.Visible = false;
                        ImageDiv.Visible = false;
                        ErrorDiv.Visible = true;
                        NextImageButton.Visible = false;
                        PreviousImageButton.Visible = false;
                        UseThisImageButton.Visible = false;
                    }
                }

                if (csid != 0)
                {
                    ImageChemSpider.ImageUrl = "~/Contribute/CSStructureImageHandler.ashx?csid=" + csid;
                    string chemspiderlink = "http://www.chemspider.com/Chemical-Structure." + csid + ".html";
                    ChemSpiderHyperLink.NavigateUrl = chemspiderlink;
                    ChemSpiderHyperLink.Text = chemspiderlink;
                    ListView1.DataSource = image.retrieveSynonyms(csid).Select(x => StringTools.ToUpperCaseFirstLetterOnly(x));
                    ListView1.DataBind();
                    UseThisImageButton.Visible = true;
                    NextImageButton.Visible = true;

                    int? previouscsid = Structure.previousCSID(cfid, csid);
                    if (previouscsid == null)
                    {
                        PreviousImageButton.Visible = false;
                    }
                    else
                    {
                        PreviousImageButton.Visible = true;
                    }

                    if (ChemSpiderLast)
                    {
                        PreviousImageButton.Visible = true;
                        UseThisImageButton.Visible = false;
                        NextImageButton.Visible = false;
                    }
                }
            }
            ChemSpiderIsDirty = true;
            Session["ChemSpiderPostBack"] = null;
            if (ChemSpiderIsDirty)
            {
                script += "$(\"#ImageChoiceSelector\").bind(\"dialogclose\", function(event, ui) {" + Page.ClientScript.GetPostBackEventReference(this.Page, "ChemSpiderIsDirty") + "});";
                Session["ChemSpiderPostBack"] = true;
            }
            string scripttoadd = addScript(script);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "loadUI", scripttoadd, true);
        }

        protected void SMILESValidateRGroups(object sender, ServerValidateEventArgs e)
        {
            if (SMILESTextBox.Text.Contains("R") || SMILESTextBox.Text.Contains("r"))
            {
                e.IsValid = false;
            }
            else
            {
                e.IsValid = true;
            }
        }

        protected void RemoveButton_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            CompoundFamilyImage compoundFamilyImageRecord = (from c in db.CompoundFamilyImages where c.cfid == cfid select c).SingleOrDefault();

            if (compoundFamilyImageRecord != null)
            {
                compoundFamilyImageRecord.Image = null;
                compoundFamilyImageRecord.ImageType = null;
                db.SubmitChanges();
            }
            db.Dispose();

            CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
            NoCurrentImage.Visible = true;
            CFViewImage.Visible = false;
            script += "$(\"#ImageChoiceSelector\").dialog('open');";
        }

        protected void NextChemSpider_Click(object sender, EventArgs e)
        {
            int? nextcsid = Structure.nextCSID(cfid, csid);
            if (nextcsid != null)
            {
                csid = nextcsid.Value;
                ViewState["csid"] = csid;
                ChemSpiderLast = false;
                PreviousImageButton.Visible = true;
            }
            else
            {
                NextImageButton.Visible = false;
                ErrorDiv.Visible = true;
                ErrorLabel.CssClass = "error";
                ErrorLabel.Text = "No more images available.";
                UseThisImageButton.Visible = false;
                PreviousImageButton.Visible = true;
                ViewState["ChemSpiderLast"] = true;
                ChemSpiderLast = true;
                ImageDiv.Visible = false;
                SynonymsDiv.Visible = false;
            }
        }

        protected void PreviousChemSpider_Click(object sender, EventArgs e)
        {
            if (!ChemSpiderLast)
            {
                int? previouscsid = Structure.previousCSID(cfid, csid);
                if (previouscsid != null)
                {
                    csid = previouscsid.Value;
                    ViewState["csid"] = csid;
                    NextImageButton.Visible = true;
                    UseThisImageButton.Visible = true;
                    ErrorDiv.Visible = false;
                    ImageDiv.Visible = true;
                    ViewState["ChemSpiderLast"] = false;
                }
            }
            else
            {
                csid = int.Parse(ViewState["csid"].ToString());
                ViewState["ChemSpiderLast"] = false;
                ChemSpiderLast = false;
                NextImageButton.Visible = true;
                UseThisImageButton.Visible = true;
                ErrorDiv.Visible = false;
                ImageDiv.Visible = true;
            }
        }

        protected void UseChemSpider_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext(); ;
            CompoundFamilyImage cfImageRecord = (from c in db.CompoundFamilyImages
                                                 where c.cfid == cfid
                                                 select c).SingleOrDefault();

            if (cfImageRecord == null)
            {
                cfImageRecord = new CompoundFamilyImage();
                cfImageRecord.cfid = cfid;
                db.CompoundFamilyImages.InsertOnSubmit(cfImageRecord);
            }

            Structure image = new Structure();
            byte[] imagearray = image.getStructureImage(csid);
            if (imagearray != null && imagearray.Length != 0)
            {
                cfImageRecord.Image = imagearray;
                cfImageRecord.ImageType = "image/png";
                db.SubmitChanges();
                db.Dispose();

                ErrorLabel.CssClass = "noerror";
                ErrorDiv.Visible = true;
                ErrorLabel.Text = "The image was successfully saved.";

                ImageDiv.Visible = false;
                SynonymsDiv.Visible = false;
                ButtonsDiv.Visible = false;

                ChemSpiderIsDirty = true;
            }
            else
            {
                ErrorLabel.CssClass = "error";
                ErrorDiv.Visible = true;
                ErrorLabel.Text = "An error occured. The image was not saved.";

                ImageDiv.Visible = false;
                SynonymsDiv.Visible = false;
                ButtonsDiv.Visible = false;
            }
            CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
        }

        protected void UploadManualButton_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    DataClassesDataContext db = new DataClassesDataContext();
                    CompoundFamilyImage compoundFamilyImageRecord = (from c in db.CompoundFamilyImages where c.cfid == cfid select c).SingleOrDefault();

                    if (compoundFamilyImageRecord == null)
                    {
                        compoundFamilyImageRecord = new CompoundFamilyImage();
                        db.CompoundFamilyImages.InsertOnSubmit(compoundFamilyImageRecord);
                    }
                    compoundFamilyImageRecord.Image = FileUpload1.FileBytes;
                    compoundFamilyImageRecord.ImageType = FileUpload1.PostedFile.ContentType;

                    db.SubmitChanges();
                    db.Dispose();

                    ImageManualNotPresentLabel.Visible = false;

                    ImageViewManual.Visible = true;
                    ImageViewManual.ImageUrl = "~/StructureImageHandler.ashx?cfid=" + cfid;

                    ManualErrorLabel.CssClass = "noerror";
                    ManualErrorLabel.Text = "The image was successfully saved.";
                    ManualErrorLabel.Visible = true;
                    CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                    ManualErrorLabel.CssClass = "error";
                    ManualErrorLabel.Visible = true;
                    ManualErrorLabel.Text = "An error has occured. Please try again. If the problem persists, please notify the site administrator.";
                }
            }
            else
            {
                ManualErrorLabel.CssClass = "error";
                ManualErrorLabel.Visible = true;
                ManualErrorLabel.Text = "Please select a file to upload.";
            }
            script += "$(\"#ImageChoiceSelector\").dialog('open');$(\"#ManualUploadDiv\").dialog('open');";
        }

        protected void GenerateSMILESButton_Click(object sender, EventArgs e)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            CompoundFamilyImage cfImageRecord = (from c in db.CompoundFamilyImages
                                                 where c.cfid == cfid
                                                 select c).SingleOrDefault();

            if (cfImageRecord == null)
            {
                cfImageRecord = new CompoundFamilyImage();
                db.CompoundFamilyImages.InsertOnSubmit(cfImageRecord);
            }

            try
            {
                string smilestring = SMILESTextBox.Text.Trim().Replace(" ", String.Empty);
                byte[] generatedimage = Structure.structureFromSMILES(smilestring);

                if (generatedimage != null)
                {
                    cfImageRecord.Image = generatedimage;
                    cfImageRecord.ImageType = "image/png";
                    db.SubmitChanges();

                    SMILESErrorLabel.CssClass = "noerror";
                    SMILESErrorLabel.Text = "The image was successfully saved.";
                    SMILESErrorLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Debug(ex.ToString());
                SMILESErrorLabel.CssClass = "error";
                SMILESErrorLabel.Text = "There was an error generating the image. Please try a different SMILES string. Error Msg: " + ex.Message;
                SMILESErrorLabel.Visible = true;
            }

            db.Dispose();
            CompoundFamilies.updateClusterLastModified(cfid, Page.User.Identity.Name);
            script += "$(\"#ImageChoiceSelector\").dialog('open');$(\"#SMILESDiv\").dialog('open');";
        }

        private string addScript(string script)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(initializeMainScript());
            sb.AppendLine(script);
            return sb.ToString();
        }

        private string surroundWithOnLoad(string script)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("window.onload = function () {");
            sb.AppendLine(script);
            sb.AppendLine("}");

            return sb.ToString();
        }

        private string initializeMainScript()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("$(\"#ImageChoiceSelector\").dialog({ autoOpen: false, buttons: { \"Close\": function () { $(this).dialog(\"close\"); } }, minWidth: 600, minHeight: 400, modal: true }).parent().appendTo(jQuery(\"form:first\"));");
            sb.AppendLine("$(\"#ManualUploadDiv\").dialog({ autoOpen: false, buttons: { \"Back\": function () { $(this).dialog(\"close\"); } }, minWidth: 600, minHeight: 400, modal: true }).parent().appendTo(jQuery(\"form:first\"));");
            sb.AppendLine("$(\"#ChemSpiderDiv\").dialog({ autoOpen: false, buttons: { \"Back\": function () { $(this).dialog(\"close\"); } }, minWidth: 600, minHeight: 400, modal: true }).parent().appendTo(jQuery(\"form:first\"));");
            sb.AppendLine("$(\"#SMILESDiv\").dialog({ autoOpen: false, buttons: { \"Back\": function () { $(this).dialog(\"close\"); } }, minWidth: 600, minHeight: 400, modal: true }).parent().appendTo(jQuery(\"form:first\"));");

            sb.AppendLine("$(\"#SelectButton\").click(function () { $(\"#ImageChoiceSelector\").dialog('open'); });");
            sb.AppendLine("$(\"#SMILESli\").click(function () { $(\"#SMILESDiv\").dialog('open'); });");
            sb.AppendLine("$(\"#ManualUploadli\").click(function () { $(\"#ManualUploadDiv\").dialog('open'); });");
            sb.AppendLine("$(\"#ChemSpiderli\").click(function () { $(\"#ChemSpiderDiv\").dialog('open'); });");

            return sb.ToString();
        }
    }
}