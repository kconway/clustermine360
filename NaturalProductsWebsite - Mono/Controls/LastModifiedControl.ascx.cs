﻿using System;

namespace NaturalProductsWebsite.Controls
{
    public partial class LastModifiedControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int userid = 0;
            DateTime lastModified = DateTime.Now;
            if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
            {
                int cfid = int.Parse(Request.QueryString["cfid"]);
                var cf = CompoundFamilies.getCompoundFamilyByID(cfid);
                userid = cf.LastModifiedUserID;
                lastModified = cf.LastModified;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
            {
                int clusterid = int.Parse(Request.QueryString["clusterid"]);
                var cluster = Clusters.getClusterByID(clusterid);
                userid = cluster.ClusterBase.LastModifiedUserID;
                lastModified = cluster.ClusterBase.LastModified;
            }

            var user = UserInfos.getUserInfoByID(userid);
            LastModifiedLabel.Text = "Last modified " + lastModified.ToString("f") + " by " + user.First_Name + " " + user.Last_Name;
        }
    }
}