﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PathwayTypeThenCompoundFamilyControl.ascx.cs"
    Inherits="NaturalProductsWebsite.Controls.PathwayTypeThenCompoundFamilyControl"
    EnableViewState="True" %>
<%@ Register Src="~/Controls/ClusterGridviewControl.ascx" TagPrefix="uc" TagName="ClusterGridviewControl" %>
<asp:ObjectDataSource ID="PathwayTypeDataSource" runat="server" TypeName="NaturalProductsWebsite.Controls.AnonymousILists"
    SelectMethod="getNamesOfPathwayTypesInAnonymousObject"></asp:ObjectDataSource>
<asp:ListView ID="pathwaytypelist" runat="server" DataKeyNames="Name" OnItemCommand="pathwaytypelink_ItemCommand"
    OnItemDataBound="pathwaytypelist_ItemDataBound" DataSourceID="PathwayTypeDataSource">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
    </LayoutTemplate>
    <ItemTemplate>
        <asp:LinkButton runat="server" ID="pathwaytypelink" CommandName="PathwayTypeLink"
            CssClass="browse" CommandArgument='<%#Container.DisplayIndex%>'></asp:LinkButton>
        <asp:Panel ID="innerpanel" CssClass="inner" runat="server" Visible="false">
            <asp:ListView ID="compoundfamilylist" runat="server" DataKeyNames="Name,Parent" OnItemCommand="compoundfamilylink_OnItemCommand"
                OnItemDataBound="compoundfamilylist_ItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="compoundfamilylink" CssClass="browse" CommandName="CompoundFamilyLink"
                        CommandArgument='<%#Container.DisplayIndex %>'></asp:LinkButton>
                    <asp:PlaceHolder ID="GridViewPlaceHolder" runat="server" Visible="false"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </ItemTemplate>
</asp:ListView>