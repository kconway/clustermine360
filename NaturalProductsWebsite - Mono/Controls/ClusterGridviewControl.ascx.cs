﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
using NaturalProductsWebsite.Code;

namespace NaturalProductsWebsite.Controls
{
    public partial class ClusterGridviewControl : System.Web.UI.UserControl
    {
        public List<Cluster> clusters;

        int cfid;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                cfid = int.Parse(Request.QueryString["cfid"]);
            }
            catch
            {
                cfid = 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridViewTemplate.applyStyletoGridView(gv);

            if (cfid != 0)
            {
                clusters = Clusters.getClustersByCompoundFamilyID(cfid);
            }

            gv.DataSource = clusters;

            gv.DataBind();
        }

        protected void CompoundFamilyLiteral_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.CompoundFamilyLiteral_DataBind(sender, e);
           
        }

        protected void DescriptionLiteral_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.DescriptionLiteral_DataBind(sender, e);
            
        }

        protected void HeaderLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.HeaderLabel_DataBind(sender, e);
            
        }

        protected void GenbankStartEnd_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.GenbankStartEnd_DataBind(sender, e);
           
        }

        protected void GenbankLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.GenbankLink_DataBind(sender, e);
            
        }

        protected void TogglePlaceholder_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.TogglePlaceholder_DataBind(sender, e);
           
        }

        protected void PhylumLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.PhylumLabel_DataBind(sender, e);
           
        }

        protected void LineageLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.LineageLabel_DataBind(sender, e);            
        }

        protected void PathwayType_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.PathwayType_DataBind(sender, e);            
        }

        protected void antiSmashLink_DataBind(object sender, EventArgs e)
        {
            CommonDataBinding.antiSmashLink_DataBind(sender, e);     
        }

        protected void NameLabel_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.NameLabel_DataBind(sender, e);
  
        }

        protected void LabLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.LabLink_DataBind(sender, e);
     
        }

        protected void ImageLink_DataBind(Object sender, EventArgs e)
        {
            CommonDataBinding.ImageLink_DataBind(sender, e);
        }
    }
}