﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace NaturalProductsWebsite.Controls
{
    public partial class PharmActionsControl : System.Web.UI.UserControl
    {
        public int cfid;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
            {
                cfid = int.Parse(Request.QueryString["cfid"]);
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
            {
                int clusterid = int.Parse(Request.QueryString["clusterid"]);
                cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;
            }

            // get data
            List<int> allowedtypes;
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                allowedtypes = db.PharmActions.Where(x => x.cfid == cfid).Select(x => x.Type).Distinct().OrderBy(x => x).ToList();

                List<PharmActionsType> roots = PubChemPharmActionDataHandler.getRootNodes();

                foreach (PharmActionsType root in roots)
                {
                    TreeNode toplevel = new TreeNode(root.TypeDescription, root.ID.ToString());
                    toplevel.SelectAction = TreeNodeSelectAction.None;
                    // add node only if children exist

                    List<PharmActionsType> allowedchildren = getListOfAllowedChildren(root.ID, allowedtypes);
                    if (allowedchildren.Count > 0)
                    {
                        AddChildren(toplevel, allowedchildren, allowedtypes);
                        TreeView1.Nodes.Add(toplevel);
                    }
                   
                }
            }
        }

        protected List<PharmActionsType> getListOfAllowedChildren(int id, List<int> allowedtypes)
        {
            List<PharmActionsType> allowedchildren = new List<PharmActionsType>();
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                List<PharmActionsType> children = db.PharmActionsTypes.Where(x => x.ParentID == id).OrderBy(x => x.TypeDescription).ToList();

                foreach (PharmActionsType child in children)
                {
                    if (allowedtypes.Contains(child.ID))
                    {
                        allowedchildren.Add(child);
                    }
                }
            }

            return allowedchildren;
        }

        protected void AddChildren(TreeNode parent, List<PharmActionsType> children, List<int> allowedtypes)
        {
            // find children
            int parentid = int.Parse(parent.Value);

            foreach (PharmActionsType child in children)
            {
                TreeNode newlevel = new TreeNode(child.TypeDescription, child.ID.ToString());
                newlevel.SelectAction = TreeNodeSelectAction.None;
                List<PharmActionsType> allowedchildren = getListOfAllowedChildren(child.ID, allowedtypes);
                if (allowedchildren.Count > 0)
                {
                    foreach (PharmActionsType allowed in allowedchildren)
                    {
                        AddChildren(newlevel, allowedchildren, allowedtypes);
                    }
                }
                parent.ChildNodes.Add(newlevel);
            }
        }
    }
}