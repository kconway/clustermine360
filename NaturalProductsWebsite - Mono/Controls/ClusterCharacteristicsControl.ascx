﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ClusterCharacteristicsControl"
    CodeBehind="ClusterCharacteristicsControl.ascx.cs" %>
<asp:UpdatePanel ID="RFUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel runat="server" ID="ReadOnlyPanel">
            <asp:DataList ID="OuterDataList" runat="server" OnItemDataBound="OuterDataList_ItemDataBound">               
                <ItemTemplate> <b>
                        <asp:Label ID="GroupTitle" runat="server"></asp:Label></b><br />
                    <asp:DataList ID="InnerDataList" runat="server" ShowFooter="False" ShowHeader="False"
                        RepeatLayout="Flow" RepeatColumns="1">
                        <ItemTemplate>
                            <asp:Label ID="CharacteristicLabel" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                </ItemTemplate>                
            </asp:DataList>
        </asp:Panel>
        <asp:Panel runat="server" ID="EditPanel">
          <script type="text/javascript">
              $(document).ready(function () {
                  var chk = $('#<%= EditPanel.ClientID %> input[type=checkbox]:checked').parent().addClass('highlight');

                  var chkboxes = $('#<%= EditPanel.ClientID %> input[type=checkbox]').click(
              function () {
                  if ($(this).is(':checked')) {
                      $(this).parent().addClass('highlight');
                  } else {
                      $(this).parent().removeClass('highlight');
                  }
              });
              });
                    </script>
            <asp:ListView ID="CharCheckBoxListView" runat="server" OnItemDataBound="CharCheckBoxListView_ItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                  <div class="floatleft pad">
                    <b>
                        <asp:Label ID="GroupTitle" runat="server" CssClass="floatleft"></asp:Label></b><br />
                    <asp:CheckBoxList ID="CharCheckBoxList" runat="server" RepeatColumns="1" RepeatLayout="Table"
                        DataTextField="Characteristic" DataValueField="ID" CssClass="floatleft">
                    </asp:CheckBoxList></div>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>