﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_SynonymsControl"
    CodeBehind="SynonymsControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Repeater ID="DisplayRepeater" runat="server">
            <ItemTemplate>
                <div class="synonymWrapper">
                    <asp:Label ID="SynonymName" runat="server"></asp:Label>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Panel runat="server" ID="AddAndEditDiv" CssClass="synonymWrapper">
            <asp:Repeater ID="EditRepeater" runat="server" OnItemCommand="EditRepeater_ItemCommand">
                <ItemTemplate>
                    <div class="synonymWrapper">
                        <asp:TextBox ID="SynonymTextBox" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="OldSynonym" runat="server" />
                        <asp:HiddenField ID="SynonymID" runat="server" />
                        <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="submitButton" />
                        <ajaxToolkit:ConfirmButtonExtender ID="RemoveButtonConfirmButtonExtender" runat="server"
                            TargetControlID="RemoveButton" ConfirmText="Do you want to delete this item?">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <br />
            <asp:Label ID="EditInfoLabel" runat="server" Text="To save changes, press 'Save' then press 'Done' to exit the edit mode. To cancel without saving changes, press 'Done'."
                CssClass="noerror"></asp:Label><br />
            <asp:Panel ID="addSynonymDiv" CssClass="synonymWrapper" runat="server">
                <asp:TextBox ID="AddSynonym" runat="server"></asp:TextBox>
                <asp:Button ID="Add" runat="server" Text="Add" OnClick="Add_Click" CssClass="submitButton" />
                <asp:Button ID="AddCancel" runat="server" Text="Done" OnClick="AddCancel_Click" CssClass="submitButton" />
                </asp:Panel>
                <asp:Panel ID="SynonymButtons" CssClass="synonymWrapper" runat="server">
                    <asp:Button ID="AddButton" runat="server" Text="Add" OnClick="AddButton_Click" CssClass="submitButton" />
                    <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click"
                        CssClass="submitButton" />
                    <asp:Button ID="EditButton" runat="server" Text="Edit" OnClick="EditButton_Click"
                        CssClass="submitButton" />
                    <asp:Button ID="CancelButton" runat="server" Text="Done" OnClick="CancelButton_Click"
                        CssClass="submitButton" />
                    <br />
                    <ajaxToolkit:ConfirmButtonExtender ID="SaveButtonConfirmButtonExtender" runat="server"
                        TargetControlID="SaveButton" ConfirmText="Do you want to save changes made to these items?">
                    </ajaxToolkit:ConfirmButtonExtender>
                    <asp:Button ID="ChemSpiderSynonymsButton" runat="server" Text="View Synonyms from ChemSpider"
                        CssClass="submitButton" OnClick="ChemSpiderSynonymsButton_Click" />
                    <br />
                    </asp:Panel>
                    <asp:Panel ID="ChemSpiderSynonymsDiv" runat="server" CssClass="alignleft chemspiderdiv"
                        Visible="false">
                        <h3>
                            <span style="position: relative; top: 5px"><b>Synonyms Retrieved From ChemSpider</b></span></h3>
                        <br />
                        <div class="floatleft">
                            <asp:CheckBoxList ID="ChemSpiderCheckBoxList" runat="server">
                            </asp:CheckBoxList>
                            <asp:Label ID="NoSynonymsLabel" Text="No synonyms found in ChemSpider database."
                                runat="server" CssClass="error" Visible="false" />
                        </div>
                        <br class="clear" />
                        <br />
                        <asp:Button ID="LoadSynonyms" runat="server" CssClass="submitButton" Text="Save Selected Synonyms"
                            OnClick="LoadSynonyms_Click" />
                        <asp:Button ID="CloseButton" runat="server" CssClass="submitButton" Text="Close"
                            OnClick="CloseButton_Click" />
        </asp:Panel> </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>