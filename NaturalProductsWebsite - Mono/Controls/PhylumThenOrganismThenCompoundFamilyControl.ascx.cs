﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NaturalProductsWebsite.Controls
{
    public partial class PhylumThenOrganismThenCompoundFamilyControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void phylumlist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton phylumlink = (LinkButton)dataItem.FindControl("phylumlink");
                string phylum = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                phylumlink.Text = phylum;
            }
        }

        protected void phylumlink_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //pass index of item in command argument
            int itemIndex = Convert.ToInt32(e.CommandArgument);

            //find the pnlChildView control
            Panel childViewPanel = (Panel)phylumlist.Items[itemIndex].FindControl("innerpanel");
            if (childViewPanel != null)
            {
                //toggle visibility of childViewPanel and bind child list if panel is visible

                if (childViewPanel.Visible)
                {
                    childViewPanel.Visible = false;
                }
                else
                {
                    childViewPanel.Visible = true;
                    ListView childList = (ListView)childViewPanel.FindControl("organismlist");
                    if (childList != null)
                    {
                        string keyValue = (string)phylumlist.DataKeys[itemIndex].Value;

                        //bind the list using DataList1 data key value
                        var ds = Organisms.getOrganismNamesByPhylum(keyValue).Select(x => new { Name = x, Parent = keyValue }).OrderBy(x => x.Name); //get data using keyValue
                        childList.DataSource = ds;
                        childList.DataBind();
                    }
                }
            }
        }

        protected void organismlist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton organismlink = (LinkButton)dataItem.FindControl("organismlink");
                string organism = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                organismlink.Text = organism;
            }
        }

        protected void organismlink_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            ListView organismlist = (ListView)sender;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int innerItemIndex = Convert.ToInt32(e.CommandArgument);

            //find the pnlChildView control
            Panel childViewPanel = (Panel)organismlist.Items[innerItemIndex].FindControl("innerpanel");
            if (childViewPanel != null)
            {
                //toggle visibility of childViewPanel and bind child list if panel is visible

                if (childViewPanel.Visible)
                {
                    childViewPanel.Visible = false;
                }
                else
                {
                    childViewPanel.Visible = true;
                    ListView childList = (ListView)childViewPanel.FindControl("compoundfamilylist");
                    if (childList != null)
                    {
                        string nameKeyValue = (string)organismlist.DataKeys[innerItemIndex].Values["Name"];
                        string parentKeyValue = (string)organismlist.DataKeys[innerItemIndex].Values["Parent"];

                        //bind the list using DataList1 data key value
                        var ds = CompoundFamilies.getCompoundFamiliesByOrganism(nameKeyValue).Select(x => new { CFName = x, Org = nameKeyValue, Phylum = parentKeyValue }).OrderBy(x => x.CFName);
                        childList.DataSource = ds;
                        childList.DataBind();
                    }
                }
            }
        }

        protected void compoundfamilylist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton compoundfamilylink = (LinkButton)dataItem.FindControl("compoundfamilylink");
                string cf = (string)DataBinder.Eval(dataItem.DataItem, "CFName");
                compoundfamilylink.Text = cf;
            }
        }

        protected void compoundfamilylink_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            ListView compoundfamilylist = (ListView)sender;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int innerItemIndex = Convert.ToInt32(e.CommandArgument);

            //find the GridView control
            PlaceHolder gvplaceholder = (PlaceHolder)dataItem.FindControl("GridViewPlaceHolder");
            if (gvplaceholder != null)
            {
                if (gvplaceholder.Visible)
                {
                    gvplaceholder.Visible = false;
                }
                else
                {
                    gvplaceholder.Visible = true;
                    string cfKeyValue = (string)compoundfamilylist.DataKeys[innerItemIndex].Values["CFName"];
                    string orgKeyValue = (string)compoundfamilylist.DataKeys[innerItemIndex].Values["Org"];
                    string phylumKeyValue = (string)compoundfamilylist.DataKeys[innerItemIndex].Values["Phylum"];

                    ClusterGridviewControl clustergridview = (ClusterGridviewControl)LoadControl("~/Controls/ClusterGridviewControl.ascx");
                    clustergridview.ID = orgKeyValue + "_" + cfKeyValue;

                    var ds = Clusters.getClustersByOrgAndFamilyName(orgKeyValue, cfKeyValue).OrderBy(x => x.ClusterBase.OrgName).ToList();
                    clustergridview.clusters = ds;
                    gvplaceholder.Controls.Add(clustergridview);
                }
            }
        }
    }

    public partial class AnonymousILists
    {
        public static IList getNamesOfPhylaInAnonymousObject()
        {
            return Phyla.getPhylaNames().Select(x => new { Name = x }).ToList();
        }
    }
}