﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Controls_ClusterCharacteristicsControl : System.Web.UI.UserControl
{
    public int mode;
    // mode = 0, displayonly
    // mode = 1, edit: checkboxlist

    public int cfid;
    bool render = true;
    //List<string> characteristics;

    protected void Page_Init(object sender, EventArgs e)
    {
        RFUpdatePanel.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (render)
        {
            updateDisplay();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["clusterid"]))
        {
            int clusterid = int.Parse(Request.QueryString["clusterid"]);
            cfid = CompoundFamilies.getCompoundFamilyByClusterID(clusterid).ID;
        }

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            render = db.CompoundFamilies.Exists(x => x.ID == cfid);
        }
    }

    private void updateDisplay()
    {
        if (mode == 0)
        {
            EditPanel.Visible = false;
            ReadOnlyPanel.Visible = true;
            OuterDataList.DataSource = Characteristics.getCharacteristicsCategoriesByCompoundFamilyID(cfid);
            OuterDataList.DataBind();
        }
        else if (mode == 1)
        {
            CharCheckBoxListView.DataSource = Characteristics.getCharacteristicsCategories();
            CharCheckBoxListView.DataBind();
            SelectCheckBoxes();
            EditPanel.Visible = true;
            ReadOnlyPanel.Visible = false;
        }
    }

    protected void CharCheckBoxListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem item = e.Item as ListViewDataItem;
            if (item != null)
            {
                Enum_CharacteristicsCategory cat = (Enum_CharacteristicsCategory)item.DataItem;
                Label title = (Label)e.Item.FindControl("GroupTitle");
                title.Text = cat.CategoryName;

                CheckBoxList list = (CheckBoxList)e.Item.FindControl("CharCheckBoxList");
                list.DataSource = Characteristics.getCharacteristicsByCategory(cat.ID);
                list.DataBind();
            }
        }
    }

    protected void OuterDataList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataListItem item = (DataListItem)e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            Label grouptitle = (Label)item.FindControl("GroupTitle");
            Enum_CharacteristicsCategory cat = (Enum_CharacteristicsCategory)item.DataItem;
            grouptitle.Text = cat.CategoryName;
            DataList innerlist = (DataList)item.FindControl("InnerDataList");
            innerlist.DataSource = Characteristics.getListOfCharacteristicsByCompoundFamilyIDAndCategoryID(cfid, cat.ID);
            innerlist.ItemDataBound += InnerDataList_ItemDataBound;
            innerlist.DataBind();
        }
    }

    protected void InnerDataList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataListItem item = (DataListItem)e.Item;

        Label characteristic = (Label)item.FindControl("CharacteristicLabel");
        characteristic.Text = (string)(item.DataItem);
    }

    protected void SelectCheckBoxes()
    {
        List<int> charids = Characteristics.getListOfCharacteristicsByCompoundFamilyID(cfid).Select(x => x.ID).ToList();
        foreach (ListViewItem lvi in CharCheckBoxListView.Items)
        {
            CheckBoxList cblist = (CheckBoxList)lvi.FindControl("CharCheckBoxList");
            if (cblist != null)
            {
                foreach (ListItem listItem in cblist.Items)
                {
                    if (charids.Contains(int.Parse(listItem.Value)))
                    {
                        listItem.Selected = true;
                    }
                    else { listItem.Selected = false; }
                }
            }
        }
    }

    public void Save()
    {
        List<int> charIDsToDelete = new List<int>();
        List<int> charIDsToAdd = new List<int>();
        foreach (ListViewItem lvi in CharCheckBoxListView.Items)
        {
            CheckBoxList cblist = (CheckBoxList)lvi.FindControl("CharCheckBoxList");
            if (cblist != null)
            {
                foreach (ListItem listItem in cblist.Items)
                {
                    int charid = int.Parse(listItem.Value);

                    if (!listItem.Selected)
                    {
                        // remove non-existing relationships
                        charIDsToDelete.Add(charid);
                    }
                    else if (listItem.Selected)
                    {
                        charIDsToAdd.Add(charid);
                    }
                }
            }
        }
        Characteristics.DeleteCompoundFamilyToCharacteristicRelationship(cfid, charIDsToDelete);
        Characteristics.AddCompoundFamilyToCharacteristicRelationship(cfid, charIDsToAdd);
        //characteristics = Characteristics.getListOfCharacteristicsByCompoundFamilyID(cfid);
        updateDisplay();
    }
}