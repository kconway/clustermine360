﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClusterGridviewControl.ascx.cs"
    Inherits="NaturalProductsWebsite.Controls.ClusterGridviewControl" %>
<asp:GridView ID="gv" runat="server" AutoGenerateColumns="False">
    <Columns>
        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Accession #">
            <ItemTemplate>
                <asp:HyperLink ID="genbanklink" runat="server" Style="display: block;" OnDataBinding="GenbankLink_DataBind"></asp:HyperLink>
                <div style="width: 100%" class="toppadding5">
                    <asp:Literal ID="genbankstartendliteral" runat="server" OnDataBinding="GenbankStartEnd_DataBind"></asp:Literal>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="15%" HeaderText="Organism Lineage">
            <ItemTemplate>
                <asp:PlaceHolder ID="toggleplaceholder" runat="server" OnDataBinding="TogglePlaceholder_DataBind">
                    <img runat="server" id="toggleimage" class="toggle" src="../Images/action_add2.gif"
                        alt="Toggle" />
                    <asp:Panel ID="lineagepanel" runat="server" CssClass="lineage">
                        <asp:Label ID="lineagelabel" runat="server" OnDataBinding="LineageLabel_DataBind"></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="phylumpanel" runat="server" CssClass="lineage">
                        <asp:Label ID="phylumlabel" runat="server" OnDataBinding="PhylumLabel_DataBind"></asp:Label>
                    </asp:Panel>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Pathway Type" ItemStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Label ID="pathwaytypelabel" runat="server" OnDataBinding="PathwayType_DataBind"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="42.5%" HeaderText="Description" ItemStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Literal ID="DescriptionLiteral" runat="server" OnDataBinding="DescriptionLiteral_DataBind"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="5%" HeaderText="antiSMASH Link">
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" Text="View" Target="_blank" Visible="false"
                    OnDataBinding="antiSmashLink_DataBind"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="15%" HeaderText="Contributor">
            <ItemTemplate>
                <asp:Label runat="server" ID="contributorLabel" OnDataBinding="NameLabel_DataBind"></asp:Label>
                <br />
                <asp:HyperLink ID="lablink" runat="server" OnDataBinding="LabLink_DataBind"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-Width="2.5%" HeaderText="Details">
            <ItemTemplate>
                <asp:HyperLink ID="imagelink" runat="server" ImageUrl="~/Images/folder_files.gif"
                    OnDataBinding="ImageLink_DataBind"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>