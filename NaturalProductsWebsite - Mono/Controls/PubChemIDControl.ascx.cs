﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using System.Web.UI.HtmlControls;

public partial class Controls_PubMedIDControl : System.Web.UI.UserControl
{
    private enum UpdateType
    {
        Add,
        Remove,
        Edit
    };

    public int mode;
    // mode = 0, display only
    // mode = 1, display with button to go to edit mode
    // mode = 2, add / remove

    public int cfid;
    List<PubChemIDs> pubChemIDs;
    List<PharmActions> pharmActions;
    

    protected void Page_Init(object sender, EventArgs e)
    {
        UpdatePanel1.PreRender += Page_LoadComplete;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        updateDisplay();
        pubChemIDs = PubChemIDDataHandler.getPubChemIDsByCFID(cfid);
        DisplayRepeater.DataSource = pubChemIDs;
        DisplayRepeater.ItemDataBound += Repeater_OnItemDataBound;
        DisplayRepeater.DataBind();
        ViewState["referencesmode"] = mode;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["referencesmode"] = null;
        }

        if (!String.IsNullOrEmpty(Request.QueryString["cfid"]))
        {
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        if (ViewState["referencesmode"] != null)
        {
            mode = (int)ViewState["referencesmode"];
        }

        pubChemIDs = PubChemIDDataHandler.getPubChemIDsByCFID(cfid);
    }

    private void updateDisplay()
    {
        if (mode == 0)
        {
            ModeButtons.Visible = false;
            addUserPCIDDiv.Visible = false;
        }
        else if (mode == 1)
        {
            ModeButtons.Visible = true;
            addUserPCIDDiv.Visible = false;
            AddRemoveButton.Visible = true;
            CancelButton.Visible = false;
        }
        else if (mode == 2)
        {
            ModeButtons.Visible = true;
            addUserPCIDDiv.Visible = true;
            AddRemoveButton.Visible = false;
            CancelButton.Visible = true;
        }
    }

    protected void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = (RepeaterItem)e.Item;

        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            Label refDetails = (Label)item.FindControl("PubChemID");
            
            string pcidValue = DataBinder.Eval(item.DataItem, "PubChemID").ToString();
            string pcidType = DataBinder.Eval(item.DataItem, "Type").ToString();
            string pcidDisplay = pcidValue;

            if (pcidType == "S")
            {
pcidDisplay = "PubChem Substance ID #" + pcidValue;
            }
            else
            {
                pcidDisplay = "PubChem Compound ID #" + pcidValue;
            }

            refDetails.Text = pcidDisplay;

            HiddenField pcid = (HiddenField)item.FindControl("HiddenPCID");
            pcid.Value = pcidType + pcidValue;

            HtmlGenericControl removePanel = (HtmlGenericControl)item.FindControl("RemovePanel");
            if (mode == 0 || mode == 1)
            {
                removePanel.Visible = false;
                refDetails.CssClass = "";
            }
            else if (mode == 2)
            {
                removePanel.Visible = true;
                refDetails.CssClass = "width40 inlineblock";
                
            }
        }
    }

    protected void DisplayRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string pcid = ((HiddenField)e.Item.FindControl("HiddenPCID")).Value;
        if (e.CommandName == "RemoveItem")
        {
            char firstletter = pcid[0];
            string remainder = pcid.Substring(1);

            // remove from list
            pubChemIDs.RemoveAll(x => x.PubChemID == remainder);
            PubChemIDDataHandler.removePubChemId(cfid, firstletter.ToString(), remainder);            
        }
    }

    protected void Add_Click(object sender, EventArgs e)
    {
        string pcidToAdd = AddPCID.Text.Trim();
        string type = "S";
        
        if (PubChemDropDownList.SelectedValue == "C")
        {
            type = "C";
        }

        if (!String.IsNullOrEmpty(pcidToAdd))
        {           
           PubChemIDDataHandler.addPubChemId(cfid, type, pcidToAdd);            
        }

        // update references field
        AddPCID.Text = "";
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        mode = 1;
    }

    protected void AddRemoveButton_Click(object sender, EventArgs e)
    {
        mode = 2;
    }

  
    
}