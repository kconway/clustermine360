﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NaturalProductsWebsite.Controls
{
    public partial class PathwayTypeThenCompoundFamilyControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void pathwaytypelist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton pathwaytypelink = (LinkButton)dataItem.FindControl("pathwaytypelink");
                string pathwaytype = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                pathwaytypelink.Text = pathwaytype;
            }
        }

        protected void pathwaytypelink_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //pass index of item in command argument
            int itemIndex = Convert.ToInt32(e.CommandArgument);

            //find the pnlChildView control
            Panel childViewPanel = (Panel)pathwaytypelist.Items[itemIndex].FindControl("innerpanel");
            if (childViewPanel != null)
            {
                //toggle visibility of childViewPanel and bind child list if panel is visible

                if (childViewPanel.Visible)
                {
                    childViewPanel.Visible = false;
                }
                else
                {
                    childViewPanel.Visible = true;
                    ListView childList = (ListView)childViewPanel.FindControl("compoundfamilylist");
                    if (childList != null)
                    {
                        string keyValue = (string)pathwaytypelist.DataKeys[itemIndex].Value;

                        //bind the list using DataList1 data key value
                        var ds = CompoundFamilies.getNamesOfCompoundFamiliesByPathwayType(keyValue).Select(x => new { Name = x, Parent = keyValue }).Distinct().OrderBy(x => x.Name); //get data using keyValue
                        childList.DataSource = ds;
                        childList.DataBind();
                    }
                }
            }
        }

        protected void compoundfamilylist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton compoundfamilylink = (LinkButton)dataItem.FindControl("compoundfamilylink");
                string compoundfamily = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                compoundfamilylink.Text = compoundfamily;
            }
        }

        protected void compoundfamilylink_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            ListView compoundfamilylist = (ListView)sender;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int innerItemIndex = Convert.ToInt32(e.CommandArgument);

            //find the GridView control
            PlaceHolder gvplaceholder = (PlaceHolder)dataItem.FindControl("GridViewPlaceHolder");
            if (gvplaceholder != null)
            {
                if (gvplaceholder.Visible)
                {
                    gvplaceholder.Visible = false;
                }
                else
                {
                    gvplaceholder.Visible = true;
                    string nameKeyValue = (string)compoundfamilylist.DataKeys[innerItemIndex].Values["Name"];
                    string parentKeyValue = (string)compoundfamilylist.DataKeys[innerItemIndex].Values["Parent"];
                    ClusterGridviewControl clustergridview = (ClusterGridviewControl)LoadControl("~/Controls/ClusterGridviewControl.ascx");
                    clustergridview.ID = parentKeyValue + "_" + nameKeyValue;

                    var ds = Clusters.getClustersByTypeAndFamilyName(parentKeyValue, nameKeyValue).Distinct().OrderBy(x => x.ClusterBase.OrgName).ToList();
                    clustergridview.clusters = ds;
                    gvplaceholder.Controls.Add(clustergridview);
                }
            }
        }
    }

    public partial class AnonymousILists
    {
        public static IList getNamesOfPathwayTypesInAnonymousObject()
        {
            return PathwayTypes.getPathwayTypeNames().Select(x => new { Name = x }).ToList();
        }
    }
}