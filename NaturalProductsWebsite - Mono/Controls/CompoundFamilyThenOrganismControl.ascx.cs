﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

namespace NaturalProductsWebsite.Controls
{
    public partial class CompoundFamilyThenOrganismControl : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

       

        protected void compoundfamilylist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton compoundfamilylink = (LinkButton)dataItem.FindControl("compoundfamilylink");
                string compoundfamily = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                compoundfamilylink.Text = compoundfamily;
            }
        }

        protected void compoundfamilylink_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            //pass index of item in command argument
            int itemIndex = Convert.ToInt32(e.CommandArgument);

            //find the pnlChildView control
            Panel childViewPanel = (Panel)compoundfamilylist.Items[itemIndex].FindControl("innerpanel");
            if (childViewPanel != null)
            {
                //toggle visibility of childViewPanel and bind child list if panel is visible

                if (childViewPanel.Visible)
                {
                    childViewPanel.Visible = false;
                }
                else
                {
                    childViewPanel.Visible = true;
                    ListView childList = (ListView)childViewPanel.FindControl("organismlist");
                    if (childList != null)
                    {
                        string keyValue = (string)compoundfamilylist.DataKeys[itemIndex].Value;

                        //bind the list using DataList1 data key value
                        var ds = Organisms.getOrganismNamesByCompoundFamily(keyValue).Select(x => new { Name = x, ParentID = itemIndex, ParentName = keyValue }).OrderBy(x => x.Name); //get data using keyValue
                        childList.DataSource = ds;
                        childList.DataBind();
                    }
                }
            }
        }

        protected void organismlist_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = e.Item as ListViewDataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem && dataItem != null)
            {
                LinkButton organismlink = (LinkButton)dataItem.FindControl("organismlink");
                string organism = (string)DataBinder.Eval(dataItem.DataItem, "Name");
                organismlink.Text = organism;
            }
        }

        protected void organismlink_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            ListView organismlist = (ListView)sender;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int innerItemIndex = Convert.ToInt32(e.CommandArgument);

            //find the GridView control

            PlaceHolder gvplaceholder = (PlaceHolder)dataItem.FindControl("GridViewPlaceHolder");

            if (gvplaceholder != null)
            {
                if (gvplaceholder.Visible)
                {
                    gvplaceholder.Visible = false;
                }
                else
                {
                    gvplaceholder.Visible = true;
                    string nameKeyValue = (string)organismlist.DataKeys[innerItemIndex].Values["Name"];
                    string parentKeyValue = (string)organismlist.DataKeys[innerItemIndex].Values["ParentName"];
                    int parentIDValue = (int)organismlist.DataKeys[innerItemIndex].Values["ParentID"];
                    ClusterGridviewControl clustergridview = (ClusterGridviewControl)LoadControl("~/Controls/ClusterGridviewControl.ascx");
                    clustergridview.ID = parentKeyValue + "_" + nameKeyValue;

                    var ds = Clusters.getClustersByOrgAndFamilyName(nameKeyValue, parentKeyValue).OrderBy(x => x.ClusterBase.OrgName).ToList();
                    clustergridview.clusters = ds;
                    gvplaceholder.Controls.Add(clustergridview);
                    
                }
            }
        }

     
    }

    public partial class AnonymousILists
    {
        public static IList getNamesOfCompoundFamiliesInAnonymousObject()
        {
            return CompoundFamilies.getNamesOfCompoundFamilies().Select(x => new { Name = x }).ToList();
        }
    }
}