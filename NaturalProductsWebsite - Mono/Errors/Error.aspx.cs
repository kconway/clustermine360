﻿using System;
using System.Web;

public partial class Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["errordetails"]))
        {
            Panel1.Visible = true;
            Label1.Text = HttpUtility.UrlDecode(Request.QueryString["errordetails"]);
        }
    }
}