﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="Error" CodeBehind="Error.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
        .style2
        {
            font-size: x-large;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 class="style2">
        Error
    </h2>
    <br />
    <p>
        <span class="style1">An error has occured. Please try again later.</span></p>
    <asp:Panel ID="Panel1" runat="server" Visible="False">
        <p>
            <h3>
                Error Details:
            </h3>
            <span class="style1">
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></span>
        </p>
    </asp:Panel>
</asp:Content>