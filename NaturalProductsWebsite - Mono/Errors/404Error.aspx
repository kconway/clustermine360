﻿<%@ Page Title="404 Error" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="Error404" CodeBehind="404Error.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
        .style2
        {
            font-size: x-large;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 class="style2">
        The resource cannot be found.
    </h2>
    <br />
    <p>
        <span class="style1"><b>Description:</b> HTTP 404. The resource you are looking for
            (or one of its dependencies) could have been removed, had its name changed, or is
            temporarily unavailable. Please review the following URL and make sure that it is
            spelled correctly. </span>
    </p>
    <p>
        <span class="style1"><b>Requested URL:</b>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </span>
    </p>
</asp:Content>