﻿<%@ Page Title="404 Error" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="ResourceError" CodeBehind="ResourceError.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
        .style2
        {
            font-size: x-large;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 class="style2">
        The resource cannot be found.
    </h2>
    <br />
    <p>
        <span class="style1"><b>Description:</b> HTTP 404. The resource you are looking for
            is unavailable. This is probably because you are trying to view a resource provided by an antiSMASH page but the file it links to was too large to keep permanently on the server. If the data is absolutely necessary, go to the antiSMASH website at http://antismash.secondarymetabolites.org/ and rerun the analysis for the sequence in question. Sorry for the inconvenience.</span>
    </p>
    <p>
        <span class="style1"><b>Requested URL:</b>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </span>
    </p>
</asp:Content>