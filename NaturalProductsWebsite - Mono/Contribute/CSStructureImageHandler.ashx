﻿<%@ WebHandler Language="C#" Class="CSStructureImageHandler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
public class CSStructureImageHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        int csid = int.Parse(context.Request["csid"]);
        Structure image = new Structure();
        byte[] imageData = image.getStructureImage(csid);
        if (imageData != null)
        {
            context.Response.OutputStream.Write(imageData, 0, imageData.Length);
            context.Response.ContentType = "image/png";
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}