﻿<%@ Page Title="Edit" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="EditCluster" CodeBehind="EditCluster.aspx.cs" %>
<%@ Register Src="~/Controls/PathwayTypesControl.ascx" TagPrefix="uc" TagName="PathwayTypesControl" %>

<%@ Register Src="~/Controls/ClusterCharacteristicsControl.ascx" TagPrefix="uc" TagName="ClusterCharacteristicsControl" %>
<%@ Register Src="~/Controls/UserReferencesControl.ascx" TagPrefix="uc" TagName="UserReferencesControl" %>
<%@ Register Src="~/Controls/LastModifiedControl.ascx" TagPrefix="uc" TagName="LastModifiedControl" %>
<%@ Register Src="~/Controls/RelatedFamiliesControl.ascx" TagPrefix="uc" TagName="RelatedFamiliesControl" %>
<%@ Register Src="~/Controls/SynonymsControl.ascx" TagPrefix="uc" TagName="SynonymsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function enableValidator(textboxid, validatorid) {
            var validator;
            if (str.match(/^\s*$/)) {
                // nothing, or nothing but whitespace
                validator = enableValidator(texboxid, validatorid);
                validator.enable = false;
                ValidatorUpdateDisplay(validator);
            }
            else {
                // something
                validator = enableValidator(texboxid, validatorid);
                validator.enable = true;
                ValidatorUpdateDisplay(validator);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
       <asp:ImageButton ID="NextButton" runat="server" 
        CssClass="navbuttons" AlternateText="Next" 
        ImageUrl="~/Images/icon_move_right.png" onclick="NextButton_Click" /> <asp:ImageButton ID="PreviousButton" runat="server" CssClass="navbuttons" 
        AlternateText="Previous" ImageUrl="~/Images/icon_move_left.png" 
        onclick="PreviousButton_Click" />
    <h2 style="display: inline; float: left">
        Cluster Details
    </h2>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="Updating" src="../Images/ajax-loader.gif" style="display: inline; float: left;
                margin-left: 25px;" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <br />
    <div id="ErrorDiv" runat="server" class="clear">
        <asp:Label runat="server" ID="ErrorLabel" CssClass="error"></asp:Label>
    </div>
    <asp:ObjectDataSource ID="ClusterDataSource" runat="server" TypeName="Clusters" SelectMethod="getClusterByID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="clusterid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="CompoundFamiliesDataSource" runat="server" TypeName="CompoundFamilies"
        SelectMethod="getCompoundFamilies"></asp:ObjectDataSource>
    <asp:DetailsView ID="dv" runat="server" AutoGenerateRows="False" CellPadding="4"
        DataKeyNames="ID" DataSourceID="ClusterDataSource" ForeColor="#333333" GridLines="None"
        DefaultMode="Edit" OnItemCommand="DetailsView1_ItemCommand" OnLoad="DetailsView1_Load"
        OnDataBound="DetailsView1_DataBound" EnableModelValidation="True">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#EFF3FB" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>           
            <asp:TemplateField HeaderText="Accession #">
            <ItemTemplate>
                <asp:HyperLink ID="GenbankLink" runat="server" OnDataBinding="GenbankLink_DataBind"></asp:HyperLink>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="BP Start">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BPStart") %>' AutoPostBack="False"></asp:TextBox>
                    <asp:RangeValidator CssClass="error" ID="RangeValidator1" runat="server" ErrorMessage="Value must be greater than 0."
                        MaximumValue="100000000" Display="Dynamic" ControlToValidate="TextBox1" MinimumValue="0"
                        Type="Integer">
                    </asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="BP Start is required if there is a BP End."
                        ControlToValidate="TextBox1" Enabled="False"></asp:RequiredFieldValidator>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="BP End">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("BPEnd") %>'></asp:TextBox>
                    <asp:RangeValidator CssClass="error" ID="RangeValidator2" runat="server" ErrorMessage="Value must be greater than 0."
                        MaximumValue="100000000" MinimumValue="0" Display="Dynamic" ControlToValidate="TextBox2"
                        Type="Integer">
                    </asp:RangeValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The start residue (From) must be smaller than the end residue (To)."
                        CssClass="error" Display="Dynamic" Type="Integer" ControlToCompare="TextBox1"
                        ControlToValidate="TextBox2" Operator="GreaterThan"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="BP End is required if there is a BP Start."
                        Enabled="False" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Compound Family
                    <br />
                    <small>
                        <asp:HyperLink ID="CFEditLink" Text="Edit" runat="server" Target="_blank">Edit</asp:HyperLink></small>
                </HeaderTemplate>
                <EditItemTemplate>
                    <asp:DropDownList DataTextField="FamilyName" DataValueField="FamilyName" ID="DropDownList1"
                        runat="server" DataSourceID="CompoundFamiliesDataSource" OnDataBinding="cf_DataBinding">
                    </asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Synonyms
                    <br />
                    <small>
                        <asp:HyperLink ID="SynEditLink" Text="Edit" runat="server" Target="_blank">Edit</asp:HyperLink></small>
                </HeaderTemplate>
                <EditItemTemplate>
                    <uc:SynonymsControl runat="server" ID="ucSynonymsControl" mode="0" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Related Compound Families
                    <br />
                    <small>
                        <asp:HyperLink ID="RCEditLink" Text="Edit" runat="server" Target="_blank">Edit</asp:HyperLink></small>
                </HeaderTemplate>
                <EditItemTemplate>
                    <uc:RelatedFamiliesControl runat="server" ID="ucRelatedFamiliesControl" mode="0" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Pathway Type
                    <br />
                    <small>
                        <asp:HyperLink ID="PathwayEditLink" Text="Edit" runat="server" Target="_blank">Edit</asp:HyperLink></small>
                </HeaderTemplate>
                <EditItemTemplate>
                   <uc:PathwayTypesControl runat="server" ID="ucPathwayTypesControl" mode="0" />
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
                   Characteristics
                    <br />
                    <small>
                        <asp:HyperLink ID="CharEditLink" Text="Edit" runat="server" Target="_blank">Edit</asp:HyperLink></small>
                </HeaderTemplate>
                <EditItemTemplate>
                   <uc:ClusterCharacteristicsControl runat="server" ID="ucClusterCharacteristicsControl" mode="0" />
                </EditItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField HeaderText="Producing Organism">
            <ItemTemplate>
            <i><asp:Label runat="server" ID="OrganismLabel"></asp:Label></i>
            </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phylum">
                <EditItemTemplate>
                    <asp:Label ID="PhylumLabel" runat="server" Text='<%# Eval("ClusterBase.Enum_Phyla.Phylum") %>'></asp:Label>
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Organism Lineage">
                <ItemTemplate>
                    <asp:PlaceHolder ID="Lineage" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Description"><ItemTemplate >
            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClusterBase.Description") %>'></asp:Label></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Add/Remove User Contributed References">
                <ItemTemplate>
                    <uc:UserReferencesControl runat="server" ID="ucUserReferencesControl" mode="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trigger NCBI Data Reset">
                <EditItemTemplate>
                    <asp:CheckBox runat="server" ID="NCBIReset" /></EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trigger antiSmash Data Reset">
                <EditItemTemplate>
                    <asp:CheckBox runat="server" ID="antiSmashReset" /><br />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trigger Domain Sequence Extraction Reset">
                <EditItemTemplate>
                    <asp:CheckBox runat="server" ID="DomainSequenceReset" /></EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:Button runat="server" CssClass="submitButton" Text="Update" CommandName="SaveButton"
                        ID="updateButton" />
                    <asp:Button runat="server" CssClass="submitButton" Text="Delete" CommandName="Delete"
                        ID="deleteButton" />
                    <asp:Button runat="server" CssClass="submitButton" Text="Done" CommandName="Back"
                        ID="cancelButton" />
                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this record?"
                        Enabled="True" TargetControlID="deleteButton">
                    </asp:ConfirmButtonExtender>
                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Have you saved all your changes by pressing 'Update'? If not, press 'Cancel' then press 'Update' to save changes."
                        Enabled="True" TargetControlID="cancelButton">
                    </asp:ConfirmButtonExtender>
                </EditItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <uc:LastModifiedControl runat="server" ID="ucLastModifiedControl" />
</asp:Content>