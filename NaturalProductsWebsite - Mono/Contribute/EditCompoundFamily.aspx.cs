﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;

public partial class EditCompoundFamily : System.Web.UI.Page
{
    int cfid;
   // private static readonly ILog log = LogManager.GetLogger(typeof(EditCompoundFamily));
    protected void Page_Init(object sender, EventArgs e)
    {
       // XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));      
        if (Request.QueryString["cfid"] != "" && Request.QueryString["cfid"] != null)
        {
            dv.RowStyle.VerticalAlign = VerticalAlign.Top;
            dv.Width = Unit.Percentage(90.00);
            dv.FieldHeaderStyle.Width = Unit.Percentage(20.00);
            dv.RowStyle.Width = Unit.Percentage(40.00);
            cfid = int.Parse(Request.QueryString["cfid"]);
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
            if ((Request.UrlReferrer != null) && (!IsPostBack))
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
                CFErrorLabel.Visible = false;
            }
        
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {     
        
    }

   

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {       
        DetailsView dv = (DetailsView)sender;
        Button deleteButton = (Button)dv.FindControl("deleteButton");
        Button updateButton = (Button)dv.FindControl("updateButton");

        if (User.Identity.IsAuthenticated && !User.IsInRole("restricted"))
        {
            deleteButton.Visible = true;
            updateButton.Visible = true;
        }
        else
        {
            deleteButton.Visible = false;
            updateButton.Visible = false;
        }

        TextBox nameTextBox = (TextBox)dv.FindControl("FamilyNameTextBox");
        nameTextBox.Text = DataBinder.Eval(dv.DataItem, "FamilyName").ToString();


        Image image = (Image)dv.FindControl("Image1");
        CompoundFamilyImage cfimage = CompoundFamilies.getCompoundFamilyImage(cfid);
        if (cfimage != null)
        {
            if ((cfimage.Image != null) && (cfimage.Image.Length != 0))
            {
                image.ImageUrl = "~/StructureImageHandler.ashx?cfid=" + cfid;
                image.Visible = true;
            }
            else
            {
                image.Visible = false;
            }
        }
        else
        {
            image.Visible = false;
        }


        if (!IsPostBack)
        {           
            Controls_SynonymsControl synonymsControl = (Controls_SynonymsControl)dv.FindControl("ucSynonymsControl");
            synonymsControl.mode = 1;
            ViewState["mode"] = 1;
        }
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
      
        try
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                CompoundFamily cf;
                int cfid = int.Parse(Request.QueryString["cfid"]);
                cf = (from c in db.CompoundFamilies
                      where c.ID == cfid
                      select c).Single();

                UserInfo user = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x).Single();
                cf.LastModifiedUserID = user.ID;
                cf.LastModified = DateTime.Now;

                // update: send info back to db
                TextBox nameTextBox = (TextBox)dv.FindControl("FamilyNameTextBox");
                cf.FamilyName = nameTextBox.Text;

                CheckBox cb = (CheckBox)dv.FindControl("PubChemReset");
                if (cb.Checked)
                {
                    cf.PubChemStatus = 0;
                }

                Controls_RelatedFamiliesControl rfcontrol = (Controls_RelatedFamiliesControl)dv.FindControl("ucRelatedFamiliesControl");
                rfcontrol.SaveButton_Click(new object(), new EventArgs());

                Controls_ClusterCharacteristicsControl charcontrol = (Controls_ClusterCharacteristicsControl)dv.FindControl("ucClusterCharacteristicsControl");
                charcontrol.Save();
                
                Controls_PathwayTypesControl typescontrol = (Controls_PathwayTypesControl)dv.FindControl("ucPathwayTypesControl");
                typescontrol.Save();

                db.SubmitChanges();

                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Compound Family Updated", user.First_Name + " " + user.Last_Name + " (" + user.User_Name + ") " + "has updated compound family " + cf.FamilyName + ".");

                CFErrorLabel.Visible = false;
                Response.Redirect(Request.RawUrl);
            }
        }
        catch (Exception ex)
        {
            CFErrorLabel.Visible = true;
            CFErrorLabel.Text = "An error has occured. No changes have been made. Please report this error to the site administrator.";
            Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + Request.RawUrl +"<br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
        }
    }

    protected void DetailsView1_ItemCommand(object sender, DetailsViewCommandEventArgs e)
    {
       
        int cfid = int.Parse(Request.QueryString["cfid"]);
        switch (e.CommandName)
        {
            case "Back":
                object refUrl = ViewState["RefUrl"];
                if (refUrl != null)
                {
                    string referer = (string)refUrl;
                    if (referer.Contains("Contribute"))
                    {
                        Response.Redirect("~/CompoundFamilyDetails.aspx?cfid=" + cfid);
                    }
                    else
                    {
                        Response.Redirect((string)refUrl);
                    }
                }
                else
                {
                    Response.Redirect("~/CompoundFamilyDetails.aspx?cfid=" + cfid);
                }
                break;

            case "DeleteButton":
                try
                {
                    DataClassesDataContext db = new DataClassesDataContext();
                    DataClassesDataContext db2 = new DataClassesDataContext();
                    CompoundFamily cf = (from c in db.CompoundFamilies
                                         where c.ID == cfid
                                         select c).Single();

                    if (cf.Clusters.Count == 0)
                    {
                        // delete synonyms
                        // delete related compound families
                        // delete image record
                        List<RelatedCompoundFamily> relatedFamilies = (from r in db.RelatedCompoundFamilies
                                                                       where r.CompoundFamily.FamilyName == cf.FamilyName
                                                                       || r.CompoundFamily1.FamilyName == cf.FamilyName
                                                                       select r).ToList<RelatedCompoundFamily>();

                        db.RelatedCompoundFamilies.DeleteAllOnSubmit(relatedFamilies);

                        List<Synonym> synonyms = (from s in db.Synonyms where s.CompoundFamilyID == cfid select s).ToList();
                        db.Synonyms.DeleteAllOnSubmit(synonyms);

                        db.SubmitChanges();
                        db.Dispose();

                        CompoundFamilyImage cfimage = (from c in db.CompoundFamilyImages where c.cfid == cfid select c).SingleOrDefault();
                        if (cfimage != null)
                        {
                            DataClassesDataContext dbImageDelete = new DataClassesDataContext();
                            dbImageDelete.CompoundFamilyImages.DeleteOnSubmit(cfimage);
                            dbImageDelete.SubmitChanges();
                            dbImageDelete.Dispose();
                        }

                        cf = (from c in db2.CompoundFamilies
                              where c.ID == cfid
                              select c).Single();
                        db2.CompoundFamilies.DeleteOnSubmit(cf);
                        db2.SubmitChanges();
                        UserInfo user = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x).Single();
                        Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Compound Family Deleted", user.First_Name + " " + user.Last_Name + " (" + user.User_Name + ") " + "has deleted compound family " + cf.FamilyName + ".");
                        Response.Redirect("~/Account/Success.aspx?type=" + HttpUtility.UrlEncode("cfdeleted"));
                    }
                    else
                    {
                        CFErrorLabel.Text = "Can't delete as long as there are clusters associated with this record. " +
                         "<br />Either delete or reallocate the clusters if you want to delete this record.";
                        CFErrorLabel.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);

                    CFErrorLabel.Text = "An error has occured while trying to delete this record. Please contact the site administrator to let them know of the problem.";
                    CFErrorLabel.Visible = true;
                }
                break;
        }
    }

    protected void PreviousButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Contribute/EditCompoundFamily.aspx?cfid=" + CompoundFamilies.getPreviousCFID(cfid));
    }

    protected void NextButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Contribute/EditCompoundFamily.aspx?cfid=" + CompoundFamilies.getNextCFID(cfid));
 
    }
}