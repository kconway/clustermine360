﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;

public partial class EditCluster : System.Web.UI.Page
{
    private int clusterid;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["clusterid"] != "" && Request.QueryString["clusterid"] != null)
        {
            dv.RowStyle.VerticalAlign = VerticalAlign.Top;
            dv.Width = Unit.Percentage(90.00);
            dv.FieldHeaderStyle.Width = Unit.Percentage(20.00);
            dv.RowStyle.Width = Unit.Percentage(40.00);

            ErrorDiv.Visible = false;
            clusterid = int.Parse(Request.QueryString["clusterid"]);
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null)
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
        }
    }

    protected void GenbankLink_DataBind(object sender, EventArgs e)
    {
        CommonDataBinding.GenbankLink_DataBind(sender, e);
    }

    protected void cf_DataBinding(object sender, EventArgs e)
    {
        DropDownList ddl = (DropDownList)sender;
        int clusterid = int.Parse(Request.QueryString["clusterid"]);
        ddl.SelectedValue = Clusters.getClusterByID(clusterid).CompoundFamily.FamilyName;
    }

    protected void updateItem(object sender, EventArgs e)
    {
      
        DataClassesDataContext db = new DataClassesDataContext();
        DataClassesDataContext db2 = new DataClassesDataContext();
        try
        {
            Cluster cluster = (from c in db.Clusters
                               where c.ID == clusterid
                               select c).Single();
            UserInfo user = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x).Single();

            cluster.ClusterBase.LastModifiedUserID = user.ID;
            cluster.ClusterBase.LastModified = DateTime.Now;

            DetailsView dv = (DetailsView)sender;

            TextBox start = (TextBox)dv.FindControl("TextBox1");
            TextBox end = (TextBox)dv.FindControl("TextBox2");

            if (!String.IsNullOrEmpty(start.Text) && !String.IsNullOrEmpty(end.Text))
            {
                cluster.BPStart = int.Parse(start.Text);
                cluster.BPEnd = int.Parse(end.Text);
            }

            CheckBox cb = (CheckBox)dv.FindControl("NCBIReset");
            bool NCBIreset = cb.Checked;

            if (NCBIreset)
            {
                cluster.ClusterBase.NCBIStatus = 0;
                cluster.ClusterBase.NCBIModified = DateTime.Now;
            }

            CheckBox antiSmashcb = (CheckBox)dv.FindControl("antiSmashReset");
            bool antiSmashReset = antiSmashcb.Checked;
            if (antiSmashReset)
            {
                cluster.ClusterBase.AntiSmashLink = "";
                cluster.ClusterBase.AntiSmashID = null;

                cluster.ClusterBase.AntiSmashStatus = 0;
                cluster.ClusterBase.AntiSmashModified = DateTime.Now;
            }

            CheckBox domainSequenceCB = (CheckBox)dv.FindControl("DomainSequenceReset");
            bool domainSequenceReset = domainSequenceCB.Checked;
            if (domainSequenceReset)
            {
                cluster.ClusterBase.SequenceExtractionStatus = 0;
                cluster.ClusterBase.SequenceExtractionModified = DateTime.Now;
            }

            db.SubmitChanges();

            DropDownList cfddl = (DropDownList)dv.FindControl("DropDownList1");

            cluster = (from c in db2.Clusters
                       where c.ID == clusterid
                       select c).Single();

            CompoundFamily cfamily = (from c in db2.CompoundFamilies
                                      where c.FamilyName == cfddl.SelectedValue
                                      select c).Single();

            cluster.CompoundFamilyID = cfamily.ID;

            db2.SubmitChanges();


            Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Cluster Updated", user.First_Name + " " + user.Last_Name + " (" + user.User_Name + ") " + "has updated cluster " + clusterid + " with GenBank Accession " + cluster.ClusterBase.GenbankAccession + " and description " + cluster.ClusterBase.Description + " associated with " + cluster.CompoundFamily.FamilyName + ".");


            cb.Checked = false;
            antiSmashcb.Checked = false;
            domainSequenceCB.Checked = false;

            ErrorDiv.Visible = true;
            ErrorLabel.CssClass = "noerror";
            ErrorLabel.Text = "The record was successfully updated.";
        }
        catch (Exception ex)
        {
            ErrorDiv.Visible = true;
            ErrorLabel.CssClass = "error";
            ErrorLabel.Text = "There was an error saving this record. Please contact the site administrator to let them know of the problem.";
            Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
        }
        finally
        {
            db.Dispose();
            db2.Dispose();
        }
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        DetailsView dv = (DetailsView)sender;

        Button deleteButton = (Button)dv.FindControl("deleteButton");
        Button updateButton = (Button)dv.FindControl("updateButton");
        if (User.Identity.IsAuthenticated && (!User.IsInRole("restricted")))
        {
            deleteButton.Visible = true;
            updateButton.Visible = true;
        }
        else
        {
            deleteButton.Visible = false;
            updateButton.Visible = false;
        }

        Label orgname = (Label)dv.FindControl("OrganismLabel");
        orgname.Text = Clusters.getClusterByID(clusterid).ClusterBase.OrgName;

        TextBox tb1 = new TextBox();
        RequiredFieldValidator rfv1 = new RequiredFieldValidator();

        TextBox tb2 = new TextBox();
        RequiredFieldValidator rfv2 = new RequiredFieldValidator();
        foreach (DetailsViewRow dvrow in dv.Rows)
        {
            tb1 = (TextBox)dvrow.FindControl("TextBox1");
            rfv1 = (RequiredFieldValidator)dvrow.FindControl("RequiredFieldValidator1");

            tb2 = (TextBox)dvrow.FindControl("TextBox2");
            rfv2 = (RequiredFieldValidator)dvrow.FindControl("RequiredFieldValidator2");
        }

        if ((tb1 != null) && (rfv2 != null))
        {
            tb1.Attributes.Add("onChange", "enableValidator(" + tb1.ClientID + "," + rfv2.ClientID + ");");
        }
        if ((tb2 != null) && (rfv1 != null))
        {
            tb2.Attributes.Add("onChange", "enableValidator(" + tb2.ClientID + "," + rfv1.ClientID + ");");
        }

        HyperLink editCFhl = (HyperLink)dv.FindControl("CFEditLink");
        HyperLink editSynhl = (HyperLink)dv.FindControl("SynEditLink");
        HyperLink editRChl = (HyperLink)dv.FindControl("RCEditLink");
        HyperLink editChar = (HyperLink)dv.FindControl("CharEditLink");
        HyperLink editPathwayType = (HyperLink)dv.FindControl("PathwayEditLink");
        int cfid = CompoundFamilies.getCompoundFamilyIDByClusterID(int.Parse(Request.QueryString["clusterid"]));
        string cfurl = "~/Contribute/EditCompoundFamily.aspx?cfid=" + cfid;
        editCFhl.NavigateUrl = cfurl;
        editSynhl.NavigateUrl = cfurl;
        editRChl.NavigateUrl = cfurl;
        editPathwayType.NavigateUrl = cfurl;
        editChar.NavigateUrl = cfurl;

        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters["TemplateType"] = "OrgLineageDetailsView";
        PlaceHolder lineagefield = (PlaceHolder)dv.FindControl("Lineage");
        CodeControls codeControl = new CodeControls();
        lineagefield.Controls.Add(codeControl.getControl(parameters));
        lineagefield.DataBind();
    }

    protected void DetailsView1_Load(object sender, EventArgs e)
    {
    }

    protected void DetailsView1_ItemCommand(object sender, DetailsViewCommandEventArgs e)
    {
       
        switch (e.CommandName)
        {
            case "Back":
                object refUrl = ViewState["RefUrl"];
                if (refUrl != null)
                    Response.Redirect((string)refUrl);
                break;
            case "SaveButton":
                updateItem(sender, new EventArgs());
                break;
            case "Delete":
                DataClassesDataContext db = new DataClassesDataContext();

                Cluster cluster = (from c in db.Clusters
                                   where c.ID == clusterid
                                   select c).Single();
                db.Clusters.DeleteOnSubmit(cluster);
                try
                {
                    db.SubmitChanges();
                    UserInfo user = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x).Single();

                    try
                    {
                        string directorypath = Utilities.FileTools.getantiSMASHClustersFolder() + clusterid + System.IO.Path.DirectorySeparatorChar;
                        if (System.IO.Directory.Exists(directorypath))
                        {
                            System.IO.Directory.Delete(directorypath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Could not delete cluster folder", "<b>Error Details</b><br />Cluster ID: " + clusterid + "<br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                    }
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Cluster Deleted", user.First_Name + " " + user.Last_Name + " (" + user.User_Name + ") " + "has deleted cluster " + clusterid + " with GenBank Accession " + cluster.ClusterBase.GenbankAccession + " and description " + cluster.ClusterBase.Description + " associated with " + cluster.CompoundFamily.FamilyName + ".");
                    Response.Redirect("~/Account/Success.aspx?type=" + HttpUtility.UrlEncode("clusterdeleted"));
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                    ErrorDiv.Visible = true;
                    ErrorLabel.CssClass = "error";
                    ErrorLabel.Text = "There was an error deleting this record. Please contact the site administrator to let them know of the problem.";
                }
                finally
                {
                    db.Dispose();
                }
                break;
        }
    }

    protected void NextButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("~/Contribute/EditCluster.aspx?clusterid=" + Clusters.getNextClusterID(clusterid));
    }

    protected void PreviousButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("~/Contribute/EditCluster.aspx?clusterid=" + Clusters.getPreviousClusterID(clusterid));
    }
}