﻿<%@ Page Title="Add" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master"
    Inherits="Add" CodeBehind="Add.aspx.cs" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PathwayTypesControl.ascx" TagPrefix="uc" TagName="PathwayTypesControl" %>

<%@ Register Src="~/Controls/ClusterCharacteristicsControl.ascx" TagPrefix="uc" TagName="ClusterCharacteristicsControl" %>
<%@ Register Src="~/Controls/ImagesControl.ascx" TagPrefix="uc" TagName="ImagesControl" %>
<%@ Register Src="~/Controls/RelatedFamiliesControl.ascx" TagPrefix="uc" TagName="RelatedFamiliesControl" %>
<%@ Register Src="~/Controls/SynonymsControl.ascx" TagPrefix="uc" TagName="SynonymsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">

        function validateNamesGroup(source, args) {
            var radio_buttons = $('#FamiliesWithSimilarNames input[type=checkbox]');
            if (radio_buttons.filter(':checked').length == 0) {
                args.IsValid = false;
            } else {
                args.IsValid = true;
            }
        }

        function validateClusterSize(source, args) {
            var bpstart = $('#<%= BPStartTextBox.ClientID %>').val();
            var bpend = $('#<%= BPEndTextBox.ClientID %>').val();
            var diff = bpend - bpstart;
            if (diff > 150000) {
                args.IsValid = false;
            } else {
                args.IsValid = true;
            }
        }

        $(document).ready(function () {

            var chk = $('#FamiliesWithSimilarNames input[type=checkbox]');
            //$chk.trace();

            chk.click(function () {
                chk.not(this).removeAttr('checked');

            });

            $('#GenBankDiv input').click(function () {
                if ($('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Partial Sequence']").is(':checked')) {
                    $('#<%= RequiredFieldValidator3.ClientID %>').removeAttr('disabled');
                    $('#<%= RequiredFieldValidator4.ClientID %>').removeAttr('disabled');
                    $('#<%= RangeValidator1.ClientID %>').removeAttr('disabled');
                    $('#<%= RangeValidator2.ClientID %>').removeAttr('disabled');
                    $('#<%= CompareValidator1.ClientID %>').removeAttr('disabled');                    
                }
                else {
                    $('#<%= RequiredFieldValidator3.ClientID %>').attr('disabled', true);
                    $('#<%= RequiredFieldValidator4.ClientID %>').attr('disabled', true);
                    $('#<%= RangeValidator1.ClientID %>').attr('disabled', true);
                    $('#<%= RangeValidator2.ClientID %>').attr('disabled', true);
                    $('#<%= CompareValidator1.ClientID %>').attr('disabled', true);                   
                }
            });

            $('#<%= BPStartTextBox.ClientID %>').keyup(function () {
                //$('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Entire Sequence']").trace();
                selectRBBPStartEnd();
            });

            $('#<%= BPEndTextBox.ClientID %>').keyup(function () {
                //$('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Partial Sequence']").trace();
                selectRBBPStartEnd();
            });
        });

        function selectRBBPStartEnd() {
            if ((($.trim($('#<%= BPStartTextBox.ClientID %>').val())).length === 0)
        && (($.trim($('#<%= BPEndTextBox.ClientID %>').val())).length === 0)) {
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Partial Sequence']").prop("checked", false);
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Entire Sequence']").prop("checked", true);
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Entire Sequence']").trigger('click');
            }
            else {
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Entire Sequence']").prop("checked", false);
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Partial Sequence']").prop("checked", true);
                $('#<%=SequenceRadioButtonList.ClientID %>').find("input[value='Partial Sequence']").trigger('click');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>
        Add a Record
    </h2>
    <asp:ObjectDataSource ID="PathwayTypesObjectDataSource" runat="server" TypeName="PathwayTypes"
        SelectMethod="getPathwayTypeNames"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="CompoundFamiliesObjectDataSource" runat="server" TypeName="CompoundFamilies"
        SelectMethod="getCompoundFamilies"></asp:ObjectDataSource>
    <br />
    <div>
        <asp:LinkButton ID="CompoundFamilyLinkButton" runat="server" CssClass="browse">Add a Compound Family</asp:LinkButton>
        <asp:Panel ID="AddCompoundFamilyPanel" runat="server">
            <br />
            <asp:Wizard DisplaySideBar="false" ID="CompoundFamilyWizard" runat="server" Font-Names="Verdana"
                BackColor="#dde4ec" Font-Size="1em" NavigationStyle-HorizontalAlign="Left" OnNextButtonClick="OnNextOrPreviousButtonClick"
                OnCancelButtonClick="OnCancelButtonClick" OnPreviousButtonClick="OnNextOrPreviousButtonClick"
                ActiveStepIndex="0" DisplayCancelButton="True">
                <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                    Font-Bold="True" Font-Size="1em" ForeColor="White" HorizontalAlign="Center" />
                <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Verdana" ForeColor="#284E98" CssClass="navigationButton" />
                <StepStyle Font-Size="1em" />
                <WizardSteps>
                    <asp:WizardStep ID="InputNameStep" Title="Add Compound Family" runat="server" StepType="Start">
                        <fieldset class="addFieldSet">
                            <legend>Add a Compound Family</legend>Name of Compound Family to add:
                            <br />
                            <asp:TextBox runat="server" CssClass="textEntry" ID="FamilyNameToAdd"></asp:TextBox>
                            <br class="clear" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter a name in the textbox."
                                CssClass="error" Display="Dynamic" ControlToValidate="FamilyNameToAdd"></asp:RequiredFieldValidator>
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="ConfirmNameStep" runat="server" Title="Similar Names" StepType="Step">
                        <fieldset class="addFieldSet">
                            <legend>Similar Names</legend>
                            <div id="FamiliesWithSimilarNames">
                                <div class="floatlefthalf">
                                    <b>Did you mean:</b>
                                </div>
                                <asp:Panel ID="SimilarNamesPanel" runat="server" CssClass="floatleft">
                                    <asp:Repeater runat="server" ID="RBRepeater">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="GroupRadioButton1" runat="server" CssClass="cbchecklist" />
                                            <br class="clear" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                                <br class="clear" />
                                <br />
                                <div class="floatlefthalf">
                                    <b>Continue with:</b>
                                </div>
                                <div class="floatleft">
                                    <asp:CheckBox ID="OriginalNameRB" runat="server" CssClass="cbchecklist" />
                                </div>
                                <br class="clear" />
                                <asp:CustomValidator ClientValidationFunction="validateNamesGroup" ID="CustomValidator1"
                                    runat="server" ErrorMessage="Please select one of the options above." CssClass="error"></asp:CustomValidator>
                            </div>
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="PathwayTypeStep" runat="server" Title="Pathway Type" StepType="Step">
                        <fieldset class="addFieldSet">
                            <legend>
                                <asp:Label runat="server" ID="CompoundNameStep3"></asp:Label></legend>
                                  <div class="floatleft">
                                <span class="noerror">Compound Family has been successfully added!<br />Please continue by
                                    providing a few additional details.</span></div>
                            <br class="clear" />
                            <br class="clear" />
                            <div class="floatleft">
                                <b>Pathway Type</b></div>
                                 <br class="clear" />
                            <div class="floatleft">
                               <uc:PathwayTypesControl runat="server" ID="ucPathwayTypesControl" mode="1" />                              
                            </div>
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="SelectImageWizardStep" runat="server" Title="Structure" StepType="Start">
                        <fieldset class="addFieldSet">
                            <legend>Upload an Image for
                                <asp:Label ID="cfNameImages" runat="server" Text="Label"></asp:Label></legend>
                          
                            <div class="floatlefthalf">
                                <b>Select or upload a structure image:</b>
                            </div>
                            <div class="floatleft indentsmall">
                                <uc:ImagesControl runat="server" ID="ucImagesControl" />
                            </div>
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="CharacteristicWizardStep" runat="server" Title="Characteristics"
                        StepType="Step">
                        <fieldset class="addFieldSet">
                            <legend>Select Characteristics of 
                                <asp:Label ID="charLabel" runat="server"></asp:Label></legend>
                           <uc:ClusterCharacteristicsControl runat="server" ID="ucClusterCharacteristicsControl" mode="1" />
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="SynonymsWizardStep" runat="server" Title="Synonyms" StepType="Step">
                        <fieldset class="addFieldSet">
                            <legend>Enter Synonyms of
                                <asp:Label ID="cfNameSynonyms" runat="server"></asp:Label></legend>
                            <uc:SynonymsControl runat="server" ID="ucSynonymsControl" mode="1" />
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="RelatedFamiliesWizardStep" runat="server" Title="Related Families"
                        StepType="Step">
                        <fieldset class="addFieldSet">
                            <legend>Select Compound Families Related To
                                <asp:Label ID="cfNameRelated" runat="server" Text="Label"></asp:Label></legend>
                            <uc:RelatedFamiliesControl runat="server" ID="ucRelatedFamiliesControl" mode="1" />
                        </fieldset>
                    </asp:WizardStep>
                    <asp:WizardStep ID="ContinueToClusterWizardStep" runat="server" Title="Add Cluster?"
                        StepType="Complete">
                        <fieldset class="addFieldSet">
                            <legend>Completed</legend>
                            <ul style="margin-left: -20px; margin-top: -5px">
                                <li>
                                    <asp:LinkButton ID="ViewCFRecordLinkButton" CssClass="cffinishlistitems" runat="server"></asp:LinkButton>
                                </li>
                                <li>
                                    <asp:LinkButton ID="MoveToClusterWizard" CssClass="cffinishlistitems" runat="server"
                                        OnClick="MoveToClusterWizard_Click"></asp:LinkButton>
                                </li>
                                <li>
                                    <asp:LinkButton ID="ReturnToStart" runat="server" CssClass="cffinishlistitems" OnClick="ReturnToStart_Click">Add another Compound Family</asp:LinkButton>
                                </li>
                            </ul>
                        </fieldset>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
            <br />
            <asp:Label ID="CompoundFamilyErrorLabel" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <asp:CollapsiblePanelExtender ID="AddCompoundFamily_CollapsiblePanelExtender" runat="server"
            Enabled="True" TargetControlID="AddCompoundFamilyPanel" CollapseControlID="CompoundFamilyLinkButton"
            ExpandControlID="CompoundFamilyLinkButton" Collapsed="True" SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
        <asp:LinkButton ID="ClusterLinkButton" runat="server" CssClass="browse">Add a Cluster</asp:LinkButton>
        <asp:Panel ID="AddClusterPanel" runat="server">
            <br />
            <asp:Wizard DisplaySideBar="false" ID="AddClusterWizard" runat="server" Font-Names="Verdana"
                BackColor="#dde4ec" Font-Size="1em" NavigationStyle-HorizontalAlign="Left" DisplayCancelButton="True">
                <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                    Font-Bold="True" Font-Size="1em" ForeColor="White" HorizontalAlign="Center" />
                <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Verdana" ForeColor="#284E98" CssClass="navigationButton" />
                <StepStyle Font-Size="1em" />
                <StartNavigationTemplate>
                    <asp:Button ID="ClusterSubmitButton" runat="server" CssClass="navigationButton" Text="Submit"
                        CausesValidation="true" ValidationGroup="cluster" OnClick="ClusterSubmitButton_Click" />
                    <asp:Button ID="ClusterClearButton" runat="server" CssClass="navigationButton" Text="Clear"
                        CausesValidation="false" OnClick="ClusterClearSubmitButton_Click" />
                </StartNavigationTemplate>
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Add Cluster?" StepType="Start">
                        <fieldset class="addFieldSet" style="width: 500px">
                            <legend>Add a Cluster</legend>
                            <div class="floatleftpadded">
                                Compound Family <span class="required">*</span>
                                <br />
                                <small><i>If the compound family is not already in the database, please add it first
                                    then return to add any associated clusters.</i></small>
                            </div>
                            <div class="floatleftpadded" style="white-space: nowrap">
                                <asp:DropDownList ID="CFDropDownList" runat="server" CssClass="clusterWizardInput textEntry"
                                    DataSourceID="CompoundFamiliesObjectDataSource" DataTextField="FamilyName" DataValueField="ID"
                                    AppendDataBoundItems="True">
                                    <asp:ListItem Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select a compound family."
                                    CssClass="error" ControlToValidate="CFDropDownList" ID="RequiredFieldValidator1"
                                    Display="None" ValidationGroup="cluster"></asp:RequiredFieldValidator>
                            </div>
                            <br class="clear" />
                            <div class="floatleftpadded">
                                GenBank/RefSeq Accession ID <span class="required">*</span>
                                <br />
                                <small>Must be a nucleotide record.</small><br />
                                <small>(e.g. <i>AB123456.1</i>)</small>
                                <br />
                                <br />
                                <small><i>Sequences larger than 150 kb will not be processed. If the GenBank/RefSeq sequence is larger than this, please enter the start and end positions (in bp) of the cluster in the sequence file.</i></small>
                                <br />
                               
                            </div>
                            <div id="GenBankDiv" class="floatleftpadded" >
                                <asp:TextBox ID="GenBankTextBox" runat="server" CssClass="clusterWizardInput textEntry"></asp:TextBox>
                                <br />
                                 <br />
                                 <br />
                                <asp:RadioButtonList ID="SequenceRadioButtonList" runat="server" CellSpacing="5">
                                    <asp:ListItem Text="Entire Sequence" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Partial Sequence"></asp:ListItem>                                    
                                </asp:RadioButtonList>
                                <div class="indent pad">
                                    <div class="floatleft">
                                        From</div>
                                    <div class="floatleft absoluteindent">
                                        <asp:TextBox ID="BPStartTextBox" runat="server" CssClass="bpEntry"></asp:TextBox></div>
                                    <br class="clear" />
                                </div>
                                <div class="indent pad">
                                    <div class="floatleft">
                                        To</div>
                                    <div class="floatleft absoluteindent">
                                        <asp:TextBox ID="BPEndTextBox" runat="server" CssClass="bpEntry"></asp:TextBox></div>
                                    <br class="clear" />
                                </div>
                                <br class="clear" />
                                
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter a valid GenBank Accession ID."
                                CssClass="error" ControlToValidate="GenBankTextBox" Display="None" ValidationGroup="cluster"></asp:RequiredFieldValidator>
                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter the cluster's start residue in the From field."
                                ControlToValidate="BPStartTextBox" CssClass="error" Display="None" Enabled="False"
                                ValidationGroup="cluster"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter the cluster's end residue in the To field."
                                ControlToValidate="BPEndTextBox" CssClass="error" Display="None" Enabled="False"
                                ValidationGroup="cluster"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Please enter a valid start residue in the From field. This must be a positive integer value."
                                CssClass="error" Display="None" ControlToValidate="BPStartTextBox" MinimumValue="0"
                                Type="Integer" Enabled="False" MaximumValue="2000000000" ValidationGroup="cluster"></asp:RangeValidator>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Please enter a valid end residue in the To field. This must be a positive integer value."
                                CssClass="error" Display="None" ControlToValidate="BPEndTextBox" MinimumValue="0"
                                Type="Integer" Enabled="False" MaximumValue="2000000000" ValidationGroup="cluster"></asp:RangeValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The start residue (From) must be smaller than the end residue (To)."
                                CssClass="error" Display="None" Type="Integer" ControlToCompare="BPStartTextBox"
                                ControlToValidate="BPEndTextBox" Operator="GreaterThan" Enabled="False" ValidationGroup="cluster"></asp:CompareValidator>
                               
                         

                            <br class="clear" />
                        </fieldset>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="cluster" />
            <asp:Label ID="ClusterErrorLabel" Visible="false" runat="server"></asp:Label>
            <br />
            <br />
        </asp:Panel>
        <asp:CollapsiblePanelExtender ID="AddCluster_CollapsiblePanelExtender" runat="server"
            Enabled="True" TargetControlID="AddClusterPanel" CollapseControlID="ClusterLinkButton"
            ExpandControlID="ClusterLinkButton" Collapsed="True" SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
         <asp:LinkButton ID="LargeSequenceLinkButton" runat="server" CssClass="browse">Add a Non-Cluster Sequence to Repository</asp:LinkButton>
          <asp:CollapsiblePanelExtender ID="AddLarge_SequenceCollapsiblePanelExtender" runat="server"
            Enabled="True" TargetControlID="AddLargeSequencePanel" CollapseControlID="LargeSequenceLinkButton"
            ExpandControlID="LargeSequenceLinkButton" Collapsed="True" SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
        
        <asp:Panel ID="AddLargeSequencePanel" runat="server">
       
         <asp:Wizard DisplaySideBar="false" ID="LargeSequenceWizard" runat="server" Font-Names="Verdana"
        BackColor="#dde4ec" Font-Size="1em" NavigationStyle-HorizontalAlign="Left" DisplayCancelButton="True">
        <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
            Font-Bold="True" Font-Size="1em" ForeColor="White" HorizontalAlign="Center" />
        <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#284E98" CssClass="navigationButton" />
        <StepStyle Font-Size="1em" />
             <NavigationStyle HorizontalAlign="Left" />
        <StartNavigationTemplate>
            <asp:Button ID="LargeSequenceSubmitButton" runat="server" CssClass="navigationButton"
                Text="Submit" CausesValidation="true" ValidationGroup="largesequence" OnClick="LargeSequenceSubmitButton_Click" />
            <asp:Button ID="LargeSequenceClearButton" runat="server" CssClass="navigationButton"
                Text="Clear" CausesValidation="false" OnClick="LargeSequenceClearSubmitButton_Click" />
        </StartNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="LargeSequenceWizardStep" runat="server" Title="Submit a sequence for analysis and inclusion in sequence repository?" StepType="Start">
                <fieldset class="addFieldSet" style="width: 500px">
                    <legend>Submit a Sequence to Repository</legend>
                    <div> Sequences that may contain more than one gene cluster (such as genome or 
                        metagenomic DNA) can be submitted for analysis so that any PKS/NRPS domains 
                        identified in that sequence can be added to the Sequence Repository. These 
                        sequences are not linked to a specific cluster or compound family.</div>
                    <br />
                    <div class="floatleftpadded">
                        GenBank/RefSeq Accession ID <span class="required">*</span>
                        <br />
                        <small>Must be a nucleotide record.</small><br />
                        <small>(e.g. <i>AB123456.1</i>)</small>
                    </div>
                    <div id="Div1" class="floatleftpadded" style="white-space: nowrap">
                        <asp:TextBox ID="LargeSequenceGenBankTextBox" runat="server" CssClass="clusterWizardInput textEntry"></asp:TextBox>
                    </div> 
                    <br class="clear" />
                     <div class="floatleftpadded">
                     Email me a link to antiSMASH results. <br /> <small><i>Please note that antiSMASH data will be available for approximately 1-3 months after which the data is removed from the server.</i></small>

                     </div>
                     <div class="floatleftpadded" style="white-space: nowrap">
<asp:CheckBox ID="EmailantiSMASHCheckBox" runat="server" />
                     </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter a valid GenBank Accession ID."
                        CssClass="error" ControlToValidate="LargeSequenceGenBankTextBox" Display="None"
                        ValidationGroup="largesequence"></asp:RequiredFieldValidator>
                   
                   
                </fieldset>
            </asp:WizardStep>
        </WizardSteps>
    </asp:Wizard>
    <br />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="largesequence" />
    <asp:Label ID="LargeSequenceErrorLabel" Visible="false" runat="server"></asp:Label>
      
        </asp:Panel>
        
    </div>
</asp:Content>
