﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class Add {
    
    /// <summary>
    /// ToolkitScriptManager1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::AjaxControlToolkit.ToolkitScriptManager ToolkitScriptManager1;
    
    /// <summary>
    /// PathwayTypesObjectDataSource control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource PathwayTypesObjectDataSource;
    
    /// <summary>
    /// CompoundFamiliesObjectDataSource control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource CompoundFamiliesObjectDataSource;
    
    /// <summary>
    /// CompoundFamilyLinkButton control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton CompoundFamilyLinkButton;
    
    /// <summary>
    /// AddCompoundFamilyPanel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel AddCompoundFamilyPanel;
    
    /// <summary>
    /// CompoundFamilyWizard control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Wizard CompoundFamilyWizard;
    
    /// <summary>
    /// InputNameStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep InputNameStep;
    
    /// <summary>
    /// FamilyNameToAdd control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox FamilyNameToAdd;
    
    /// <summary>
    /// RequiredFieldValidator6 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator6;
    
    /// <summary>
    /// ConfirmNameStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep ConfirmNameStep;
    
    /// <summary>
    /// SimilarNamesPanel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel SimilarNamesPanel;
    
    /// <summary>
    /// RBRepeater control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Repeater RBRepeater;
    
    /// <summary>
    /// OriginalNameRB control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.CheckBox OriginalNameRB;
    
    /// <summary>
    /// CustomValidator1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.CustomValidator CustomValidator1;
    
    /// <summary>
    /// PathwayTypeStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep PathwayTypeStep;
    
    /// <summary>
    /// CompoundNameStep3 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label CompoundNameStep3;
    
    /// <summary>
    /// ucPathwayTypesControl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_PathwayTypesControl ucPathwayTypesControl;
    
    /// <summary>
    /// SelectImageWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep SelectImageWizardStep;
    
    /// <summary>
    /// cfNameImages control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label cfNameImages;
    
    /// <summary>
    /// ucImagesControl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::NaturalProductsWebsite.Controls.ImagesControl ucImagesControl;
    
    /// <summary>
    /// CharacteristicWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep CharacteristicWizardStep;
    
    /// <summary>
    /// charLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label charLabel;
    
    /// <summary>
    /// ucClusterCharacteristicsControl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_ClusterCharacteristicsControl ucClusterCharacteristicsControl;
    
    /// <summary>
    /// SynonymsWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep SynonymsWizardStep;
    
    /// <summary>
    /// cfNameSynonyms control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label cfNameSynonyms;
    
    /// <summary>
    /// ucSynonymsControl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_SynonymsControl ucSynonymsControl;
    
    /// <summary>
    /// RelatedFamiliesWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep RelatedFamiliesWizardStep;
    
    /// <summary>
    /// cfNameRelated control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label cfNameRelated;
    
    /// <summary>
    /// ucRelatedFamiliesControl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_RelatedFamiliesControl ucRelatedFamiliesControl;
    
    /// <summary>
    /// ContinueToClusterWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep ContinueToClusterWizardStep;
    
    /// <summary>
    /// ViewCFRecordLinkButton control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton ViewCFRecordLinkButton;
    
    /// <summary>
    /// MoveToClusterWizard control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton MoveToClusterWizard;
    
    /// <summary>
    /// ReturnToStart control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton ReturnToStart;
    
    /// <summary>
    /// CompoundFamilyErrorLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label CompoundFamilyErrorLabel;
    
    /// <summary>
    /// AddCompoundFamily_CollapsiblePanelExtender control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::AjaxControlToolkit.CollapsiblePanelExtender AddCompoundFamily_CollapsiblePanelExtender;
    
    /// <summary>
    /// ClusterLinkButton control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton ClusterLinkButton;
    
    /// <summary>
    /// AddClusterPanel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel AddClusterPanel;
    
    /// <summary>
    /// AddClusterWizard control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Wizard AddClusterWizard;
    
    /// <summary>
    /// WizardStep1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep WizardStep1;
    
    /// <summary>
    /// CFDropDownList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.DropDownList CFDropDownList;
    
    /// <summary>
    /// RequiredFieldValidator1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
    
    /// <summary>
    /// GenBankTextBox control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox GenBankTextBox;
    
    /// <summary>
    /// SequenceRadioButtonList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RadioButtonList SequenceRadioButtonList;
    
    /// <summary>
    /// BPStartTextBox control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox BPStartTextBox;
    
    /// <summary>
    /// BPEndTextBox control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox BPEndTextBox;
    
    /// <summary>
    /// RequiredFieldValidator2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
    
    /// <summary>
    /// RequiredFieldValidator3 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
    
    /// <summary>
    /// RequiredFieldValidator4 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
    
    /// <summary>
    /// RangeValidator1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RangeValidator RangeValidator1;
    
    /// <summary>
    /// RangeValidator2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RangeValidator RangeValidator2;
    
    /// <summary>
    /// CompareValidator1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.CompareValidator CompareValidator1;
    
    /// <summary>
    /// ValidationSummary1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
    
    /// <summary>
    /// ClusterErrorLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label ClusterErrorLabel;
    
    /// <summary>
    /// AddCluster_CollapsiblePanelExtender control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::AjaxControlToolkit.CollapsiblePanelExtender AddCluster_CollapsiblePanelExtender;
    
    /// <summary>
    /// LargeSequenceLinkButton control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton LargeSequenceLinkButton;
    
    /// <summary>
    /// AddLarge_SequenceCollapsiblePanelExtender control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::AjaxControlToolkit.CollapsiblePanelExtender AddLarge_SequenceCollapsiblePanelExtender;
    
    /// <summary>
    /// AddLargeSequencePanel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel AddLargeSequencePanel;
    
    /// <summary>
    /// LargeSequenceWizard control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Wizard LargeSequenceWizard;
    
    /// <summary>
    /// LargeSequenceWizardStep control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.WizardStep LargeSequenceWizardStep;
    
    /// <summary>
    /// LargeSequenceGenBankTextBox control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox LargeSequenceGenBankTextBox;
    
    /// <summary>
    /// EmailantiSMASHCheckBox control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.CheckBox EmailantiSMASHCheckBox;
    
    /// <summary>
    /// RequiredFieldValidator8 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator8;
    
    /// <summary>
    /// ValidationSummary2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ValidationSummary ValidationSummary2;
    
    /// <summary>
    /// LargeSequenceErrorLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label LargeSequenceErrorLabel;
}
