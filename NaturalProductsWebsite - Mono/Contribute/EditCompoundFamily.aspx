﻿<%@ Page Title="Edit" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="EditCompoundFamily" CodeBehind="EditCompoundFamily.aspx.cs" %>
<%@ Register Src="~/Controls/PubChemIDControl.ascx" TagPrefix="uc" TagName="PubChemIDControl" %>
<%@ Register Src="~/Controls/PathwayTypesControl.ascx" TagPrefix="uc" TagName="PathwayTypesControl" %>

<%@ Register Src="~/Controls/ClusterCharacteristicsControl.ascx" TagPrefix="uc" TagName="ClusterCharacteristicsControl" %>
<%@ Register Src="~/Controls/LastModifiedControl.ascx" TagPrefix="uc" TagName="LastModifiedControl" %>
<%@ Register Src="~/Controls/ImagesControl.ascx" TagPrefix="uc" TagName="ImagesControl" %>
<%@ Register Src="~/Controls/RelatedFamiliesControl.ascx" TagPrefix="uc" TagName="RelatedFamiliesControl" %>
<%@ Register Src="~/Controls/SynonymsControl.ascx" TagPrefix="uc" TagName="SynonymsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    
    <script type="text/javascript">
        function enableValidator(textboxid, validatorid) {
            var validator;
            if (str.match(/^\s*$/)) {
                // nothing, or nothing but whitespace
                validator = enableValidator(texboxid, validatorid);
                validator.enable = false;
                ValidatorUpdateDisplay(validator);
            }
            else {
                // something
                validator = enableValidator(texboxid, validatorid);
                validator.enable = true;
                ValidatorUpdateDisplay(validator);
            }
        }
    </script>
   
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="CompoundFamilies"
        SelectMethod="getCompoundFamilyByID">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="cfid" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
  
           <asp:ImageButton ID="NextButton" runat="server" 
        CssClass="navbuttons" AlternateText="Next" 
        ImageUrl="~/Images/icon_move_right.png" onclick="NextButton_Click" /> <asp:ImageButton ID="PreviousButton" runat="server" CssClass="navbuttons" 
        AlternateText="Previous" ImageUrl="~/Images/icon_move_left.png" 
        onclick="PreviousButton_Click" />
    <h2 style="display: inline; float: left">
        Edit Compound Family Details
    </h2>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="Updating" src="../Images/ajax-loader.gif" style="display: inline; float: left;
                margin-left: 25px;" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <br />
    <div id="ErrorDiv" runat="server" class="clear">
        <asp:Label runat="server" ID="CFErrorLabel" CssClass="error" Visible="false"></asp:Label>
    </div>
    <asp:DetailsView ID="dv" runat="server" AutoGenerateRows="False" CellPadding="4"
        DataKeyNames="ID" DataSourceID="ObjectDataSource1" ForeColor="#333333" GridLines="None"
        DefaultMode="Edit" OnItemCommand="DetailsView1_ItemCommand" OnDataBound="DetailsView1_DataBound">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#EFF3FB" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:TemplateField HeaderText="Name of Compound Family">
                <EditItemTemplate>
                    <asp:TextBox ID="FamilyNameTextBox" runat="server">
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
               <asp:TemplateField HeaderText="Pathway Type">
                <EditItemTemplate>
                   <uc:PathwayTypesControl runat="server" ID="ucPathwayTypesControl" mode="1" />
                   
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Characteristics">
                <EditItemTemplate>
                  <uc:ClusterCharacteristicsControl runat="server" ID="ucClusterCharacteristicsControl" mode="1" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Synonyms">
                <EditItemTemplate>
                    <uc:SynonymsControl runat="server" ID="ucSynonymsControl" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Add/Remove PubChem IDs">
                <ItemTemplate>
                    <uc:PubChemIDControl runat="server" ID="ucPubChemIDControl" mode="1" />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Trigger PubChem Data Reset">
                <EditItemTemplate>
                    <asp:CheckBox runat="server" ID="PubChemReset" /></EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Related Compounds">
                <EditItemTemplate>
                    <uc:RelatedFamiliesControl runat="server" ID="ucRelatedFamiliesControl" mode="1" />
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <HeaderTemplate>
                    Image
                    <br />
                    <small>
                        <uc:ImagesControl runat="server" ID="ucImagesControl" />
                    </small>
                </HeaderTemplate>
                <EditItemTemplate>
                    <asp:Image ID="Image1" runat="server" AlternateText="No image has been uploaded."
                        Visible="False" Width="250" />
                </EditItemTemplate>
            </asp:TemplateField>
         

            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:Button runat="server" CssClass="submitButton" Text="Update" CommandName="Save"
                        ID="updateButton" OnClick="UpdateButton_Click" />
                    <asp:Button runat="server" CssClass="submitButton" Text="Delete" CommandName="DeleteButton"
                        ID="deleteButton" />
                    <asp:Button runat="server" CssClass="submitButton" Text="Done" CommandName="Back"
                        ID="cancelButton" />
                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this record?"
                        Enabled="True" TargetControlID="deleteButton">
                    </asp:ConfirmButtonExtender>
                    <asp:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Have you saved all your changes by pressing 'Update'? If not, press 'Cancel' then press 'Update' to save changes."
                        Enabled="True" TargetControlID="cancelButton">
                    </asp:ConfirmButtonExtender>
                </EditItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <uc:LastModifiedControl runat="server" ID="ucLastModifiedControl" />
</asp:Content>