﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using NaturalProductsWebsite.Code;
using Utilities;

public partial class Add : System.Web.UI.Page
{
    private string cfName = String.Empty;
    private string cfViewName = String.Empty;
    private int cfid;
    private string regexstring = @"(\b[A-Z]\d{5}(\.\d(\d)?)?\b)|(\b[A-Z]{2}\d{6}(\.\d(\d)?)?\b)|(\b[A-Z]{2}_\d{6}(\.\d(\d)?)?\b)";

    //private static readonly ILog log = LogManager.GetLogger(typeof(Add));

    protected void Page_Load(object sender, EventArgs e)
    {
        // XmlConfigurator.Configure(new System.IO.FileInfo("Log4Net.config"));
        if (Request.QueryString["PanelToOpen"] != null)
        {
            int paneltoopen = int.Parse(Request.QueryString["PanelToOpen"]);
            if (paneltoopen == 1)
            {
                AddCompoundFamily_CollapsiblePanelExtender.Collapsed = false;
            }
            else if (paneltoopen == 2)
            {
                AddCluster_CollapsiblePanelExtender.Collapsed = false;
            }
            else if (paneltoopen == 3)
            {
                AddLarge_SequenceCollapsiblePanelExtender.Collapsed = false;
            }
        }
        if (ViewState["cfViewName"] != null)
        {
            cfViewName = (string)ViewState["cfViewName"];

            CompoundNameStep3.Text = cfViewName;
            cfNameImages.Text = cfViewName;
            charLabel.Text = cfViewName;
            cfNameSynonyms.Text = cfViewName;
            cfNameRelated.Text = cfViewName;
            setCFName();
        }
        if (ViewState["cfName"] != null)
        {
            cfName = (string)ViewState["cfName"];
            if (String.IsNullOrEmpty(cfViewName))
            {
                cfViewName = cfName;
                setCFName();
            }
        }

        if (ViewState["cfid"] != null)
        {
            cfid = (int)ViewState["cfid"];
            ucPathwayTypesControl.cfid = cfid;
            ucImagesControl.cfid = cfid;
            ucSynonymsControl.cfid = cfid;
            ucRelatedFamiliesControl.cfid = cfid;
            ucClusterCharacteristicsControl.cfid = cfid;
            setCFViewLink();
        }

        CompoundFamilyErrorLabel.Visible = false;
    }

    protected void OnCancelButtonClick(object sender, EventArgs e)
    {
        //ClearForm();
        //FamilyNameToAdd.Text = "";
        //CompoundFamilyWizard.ActiveStepIndex = 0;
        Response.Redirect("~/Contribute/Add.aspx");
    }

    protected void MoveToClusterWizard_Click(object sender, EventArgs e)
    {
        AddCompoundFamily_CollapsiblePanelExtender.Collapsed = true;
        AddCompoundFamily_CollapsiblePanelExtender.ClientState = "true";
        AddCluster_CollapsiblePanelExtender.Collapsed = false;
        AddCluster_CollapsiblePanelExtender.ClientState = "false";
        CompoundFamiliesObjectDataSource.Select();
        CFDropDownList.DataBind();
        if (cfid != 0)
        {
            CFDropDownList.SelectedValue = cfid.ToString();
        }
    }

    protected void ReturnToStart_Click(object sender, EventArgs e)
    {
        //ClearForm();
        //FamilyNameToAdd.Text = "";
        //CompoundFamilyWizard.ActiveStepIndex = 0;
        Response.Redirect("~/Contribute/Add.aspx?PanelToOpen=1");
    }

    protected void OnNextOrPreviousButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (e.CurrentStepIndex == 0)
        {
            cfName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(FamilyNameToAdd.Text);
            cfViewName = cfName;
            ClearForm();
            if (!String.IsNullOrEmpty(cfName) && Char.IsLetterOrDigit(cfName[0]))
            {
                // check to see if exists
                if (AlreadyExistsValidator(cfName))
                {
                    setCFName();
                    CompoundFamilyErrorLabel.Visible = true;
                    CompoundFamilyErrorLabel.CssClass = "noerror";
                    CompoundFamilyErrorLabel.Text = "<br />This compound family already exists in the database!<br /><br />";
                    cfid = CompoundFamilies.getCompoundFamilyByName(cfName).ID;
                    ViewState["cfid"] = cfid;
                    setCFViewLink();
                    CompoundFamilyWizard.MoveTo(ContinueToClusterWizardStep);
                }
                else if (!addSoundex())
                {
                    CompoundNameStep3.Text = cfViewName;

                    // insert cfrecord
                    InsertCompoundFamily(e);
                    CompoundFamilyWizard.MoveTo(PathwayTypeStep);
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
        else if (e.CurrentStepIndex == 1)
        {
            if (OriginalNameRB.Checked == true)
            {
                // keep current cfName
                cfName = OriginalNameRB.Text;
                cfViewName = cfName;
                CompoundNameStep3.Text = cfViewName;

                // insert cfrecord
                InsertCompoundFamily(e);
            }
            else
            {
                // need selected value
                foreach (RepeaterItem item in RBRepeater.Items)
                {
                    CheckBox rb = (CheckBox)item.FindControl("GroupRadioButton1");
                    if (rb.Checked == true)
                    {
                        cfViewName = rb.Text;
                        cfName = rb.InputAttributes["data"].ToString();
                    }
                }

                if (!String.IsNullOrEmpty(cfName))
                {
                    setCFName();
                    CompoundFamilyErrorLabel.Visible = true;
                    CompoundFamilyErrorLabel.CssClass = "noerror";
                    CompoundFamilyErrorLabel.Text = "<br />This compound family already exists in the database!<br />";
                    cfid = CompoundFamilies.getCompoundFamilyByName(cfName).ID;
                    ViewState["cfid"] = cfid;
                    setCFViewLink();
                    CompoundFamilyWizard.MoveTo(ContinueToClusterWizardStep);
                }
            }
        }
        else if (e.CurrentStepIndex == 2)
        {
            ucPathwayTypesControl.Save();
        }
        else if (e.CurrentStepIndex == 5)
        {
            ucClusterCharacteristicsControl.Save();
        }
        else if (e.CurrentStepIndex == 6)
        {
            ucRelatedFamiliesControl.SaveButton_Click(new object(), new EventArgs());
        }
        ViewState["cfName"] = cfName;
        ViewState["cfViewName"] = cfViewName;
        ViewState["cfid"] = cfid;
    }

    private void InsertCompoundFamily(WizardNavigationEventArgs e)
    {
        DataClassesDataContext dbCheck = new DataClassesDataContext();
        if (!dbCheck.CompoundFamilies.Exists(x => x.FamilyName == cfName))
        {
            dbCheck.Dispose();
            try
            {
                DataClassesDataContext db = new DataClassesDataContext();
                CompoundFamily cf = new CompoundFamily();
                int unknownpid = PathwayTypes.getPathwayID("Unknown");
                cf.FamilyName = cfName;
                cf.PathwayTypeID = unknownpid;
                int userid = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x.ID).Single();
                cf.ContributorID = userid;
                cf.Created = DateTime.Now;
                cf.LastModifiedUserID = userid;
                cf.LastModified = DateTime.Now;

                db.CompoundFamilies.InsertOnSubmit(cf);
                db.SubmitChanges();
                db.Dispose();

                DataClassesDataContext db2 = new DataClassesDataContext();
                CompoundFamilyImage cfimage = new CompoundFamilyImage();
                cfimage.cfid = CompoundFamilies.getCompoundFamilyByName(cfName).ID;
                db2.CompoundFamilyImages.InsertOnSubmit(cfimage);
                db2.SubmitChanges();
                db2.Dispose();

                //ucPathwayTypesControl.Save();

                string absolutepath = WebTools.getURLRootPath(Request.Url.OriginalString, 1) + "CompoundFamilyDetails.aspx?cfid=" + cf.ID;
                Utilities.Email.SendMultiple(new List<string>() { ConfigurationManager.AppSettings["AdminEmail"], ConfigurationManager.AppSettings["BoddyEmail"] }, "New Compound Family", "<b>A new compound family (" + cfName + ") has been added!</b><br /><br />" +
                "<a href=" + absolutepath + ">" + absolutepath + "</a>");

                CompoundFamiliesObjectDataSource.Select();
                CFDropDownList.DataBind();
            }
            catch (Exception ex)
            {
                CompoundFamilyErrorLabel.Visible = true;
                CompoundFamilyErrorLabel.CssClass = "error";
                CompoundFamilyErrorLabel.Text = "There was an error trying to add this compound family. Please try again. If the error persists, contact the site administrator.";
                Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                e.Cancel = true;
            }
        }
        cfid = CompoundFamilies.getCompoundFamilyByName(cfName).ID;
        ViewState["cfid"] = cfid;
        ucImagesControl.cfid = cfid;
        cfNameImages.Text = cfViewName;
    }

    private void setCFName()
    {
        MoveToClusterWizard.Text = "Add a cluster for " + cfViewName;
    }

    private void setCFViewLink()
    {
        string url = ResolveClientUrl("~/CompoundFamilyDetails.aspx?cfid=" + cfid);
        ViewCFRecordLinkButton.OnClientClick = "window.open('" + url + "', '_blank');";
        ViewCFRecordLinkButton.Text = "View record for " + cfViewName;
    }

    private bool addSoundex()
    {
        bool soundex = false;
        Dictionary<KeyValuePair<string, string>, int> soundexDifferences = new Dictionary<KeyValuePair<string, string>, int>();
        Dictionary<string, string> synonyms = Synonyms.getSynonyms().ToDictionary(s => s.Synonym1, c => c.CompoundFamily.FamilyName);
        Dictionary<string, string> cfnames = CompoundFamilies.getNamesOfCompoundFamilies().ToDictionary(c => c, c => c);

        //do soundex
        foreach (var nametocheck in cfnames)
        {
            int value = Utilities.StringTools.Difference(cfName, nametocheck.Value);
            soundexDifferences.Add(nametocheck, value);
        }

        foreach (var synonymtocheck in synonyms)
        {
            int value = Utilities.StringTools.Difference(cfName, synonymtocheck.Key);
            soundexDifferences.Add(synonymtocheck, value);
        }

        var soundexResults = (from x in soundexDifferences where (x.Key.Value != cfName) && (x.Value >= 4) orderby x.Value descending select x).ToList();
        if (soundexResults.Count != 0)
        {
            OriginalNameRB.Text = cfName;
            RBRepeater.DataSource = soundexResults;
            RBRepeater.ItemDataBound += RBRepeater_ItemDataBound;
            RBRepeater.DataBind();
            soundex = true;
        }
        return soundex;
    }

    protected void RBRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        CheckBox rb = (CheckBox)e.Item.FindControl("GroupRadioButton1");
        KeyValuePair<KeyValuePair<string, string>, int> outerpair = (KeyValuePair<KeyValuePair<string, string>, int>)e.Item.DataItem;
        KeyValuePair<string, string> pair = outerpair.Key;
        string key = pair.Key;
        string value = pair.Value;
        if (key != value)
        {
            rb.InputAttributes.Add("data", value);
            rb.Text = value + " (" + key + ")";
        }
        else
        {
            rb.InputAttributes.Add("data", value);
            rb.Text = key;
        }
    }

    protected bool AlreadyExistsValidator(string name)
    {
        bool exists;
        DataClassesDataContext db = new DataClassesDataContext();

        // check if already exists
        cfName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(FamilyNameToAdd.Text);
        if (db.CompoundFamilies.Exists(x => x.FamilyName == cfName))
        {
            exists = true;
        }
        else
        {
            exists = false;
        }
        db.Dispose();
        return exists;
    }

    protected void ClearForm()
    {
        GenBankTextBox.Text = "";
        BPStartTextBox.Text = "";
        BPEndTextBox.Text = "";
        CFDropDownList.ClearSelection();
        SequenceRadioButtonList.ClearSelection();
        ClusterErrorLabel.Visible = false;

        ViewState["cfid"] = null;
        ViewState["cfName"] = null;
        ViewState["cfViewName"] = null;
        cfid = 0;

        ucImagesControl.Reset();
        ucSynonymsControl.Reset();

        ClearChildViewState();
        ClearChildControlState();
        ViewState.Clear();
    }

    protected void ClusterClearSubmitButton_Click(object sender, EventArgs e)
    {
        ClearForm();
        Response.Redirect("~/Contribute/Add.aspx?PanelToOpen=2");
    }

    protected bool ValidateClusterLength()
    {
        if (SequenceRadioButtonList.SelectedValue.Equals("Partial Sequence"))
        {
            return true;
        }

        int seqlength = NCBI.GetSequenceLength(GenBankTextBox.Text.Trim());
        int maxclusterlength = int.Parse(ConfigurationManager.AppSettings["MaxClusterLength"]);
        if (seqlength > 0)
        {
            if (seqlength > maxclusterlength)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            // error occurred, allow just in case
            // Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "Error validating sequence length", "There was an error validating the sequence length of " + GenBankTextBox.Text);
            return true;
        }
    }

    protected bool ValidateGenBank(string genbank)
    {
        Regex genbankregex = new Regex(regexstring);

        if (genbankregex.IsMatch(genbank))
        {
            return true;
        }
        else if (genbank.StartsWith("AC_") || genbank.StartsWith("NC_") || genbank.StartsWith("NG") || genbank.StartsWith("NZ_") || genbank.StartsWith("NW_") || genbank.StartsWith("NT_"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //private int CreateNewClusterBaseAndReturnID(ClusterBase baserecord)
    //{
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        db.ClusterBases.InsertOnSubmit(baserecord);
    //        db.SubmitChanges();

    //    }
    //}

    protected void ClusterSubmitButton_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        string errormessage = "";
        string genbankid = GenBankTextBox.Text.Trim();
        string accession = "";
        string version = "";
        int parsedversion;

        if (ValidateGenBank(genbankid) && ValidateClusterLength())
        {
            if (genbankid.Contains('.'))
            {
                string[] genbankarray = genbankid.Split(new Char[] { '.' });
                accession = genbankarray[0];
                version = genbankarray[1];
                parsedversion = int.Parse(version);
            }
            else
            {
                accession = genbankid;
                parsedversion = 0;
            }

            List<Cluster> clusters = new List<Cluster>();

            if (SequenceRadioButtonList.SelectedValue.Equals("Partial Sequence"))
            {
                int clusterstart = int.Parse(BPStartTextBox.Text);
                int clusterend = int.Parse(BPEndTextBox.Text);
                clusters = (from x in db.Clusters
                            where x.ClusterBase.GenbankAccession == accession
                            && x.BPStart == clusterstart
                            && x.BPEnd == clusterend
                            select x).ToList<Cluster>();
            }
            else
            {
                clusters = (from x in db.Clusters
                            where x.ClusterBase.GenbankAccession == accession
                            select x).ToList<Cluster>();
            }
            int num = clusters.Count;

            if (num > 0)
            {
                errormessage += "This accession number already exists in the database. <br />";
            }
            else
            {
                Cluster newcluster = new Cluster();
                newcluster.ClusterBase = new ClusterBase();
                int userid = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x.ID).Single();
                newcluster.ClusterBase.ContributorID = userid;
                newcluster.ClusterBase.Created = DateTime.Now;
                newcluster.ClusterBase.LastModifiedUserID = userid;
                newcluster.ClusterBase.LastModified = DateTime.Now;

                newcluster.ClusterBase.GenbankAccession = accession;
                newcluster.ClusterBase.GenbankVersion = parsedversion;
                int cfid = int.Parse(CFDropDownList.SelectedValue);
                newcluster.CompoundFamilyID = (from x in db.CompoundFamilies
                                               where x.ID == cfid
                                               select x.ID).Single();

                if (SequenceRadioButtonList.SelectedValue.Equals("Partial Sequence"))
                {
                    newcluster.ClusterIsEntireSequence = false;
                    newcluster.BPStart = int.Parse(BPStartTextBox.Text);
                    newcluster.BPEnd = int.Parse(BPEndTextBox.Text);
                }
                else
                {
                    newcluster.ClusterIsEntireSequence = true;
                }
                newcluster.ClusterBase.Delayed = true;
                newcluster.ClusterBase.PhylumID = Phyla.getPhylumID("Unknown");
                newcluster.ClusterBase.OrgName = "";
                newcluster.ClusterBase.NCBIStatus = 0;
                newcluster.ClusterBase.NCBIModified = DateTime.Now;
                newcluster.ClusterBase.AntiSmashStatus = 0;
                newcluster.ClusterBase.AntiSmashModified = DateTime.Now;
                newcluster.ClusterBase.SequenceExtractionStatus = 0;
                newcluster.ClusterBase.SequenceExtractionModified = DateTime.Now;
                db.ClusterBases.InsertOnSubmit(newcluster.ClusterBase);
                db.SubmitChanges();

                //db.Clusters.InsertOnSubmit(newcluster);
                //db.SubmitChanges();

                try
                {
                    string absolutepath = WebTools.getURLRootPath(Request.Url.OriginalString, 1) + "ClusterDetails.aspx?clusterid=" + newcluster.ID;
                    Utilities.Email.SendMultiple(new List<string>() { ConfigurationManager.AppSettings["AdminEmail"], ConfigurationManager.AppSettings["BoddyEmail"] }, "New Cluster", "<b>A new cluster for " + newcluster.CompoundFamily.FamilyName + " has been added!</b><br />" +
                    "<a href=" + absolutepath + ">" + absolutepath + "</a>");
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                }
            }
        }
        else
        {
            errormessage += "Validation failed.";
            if (!ValidateGenBank(genbankid))
            {
                errormessage += "<br />Please enter a valid GenBank ID.";
            }

            if (!ValidateClusterLength())
            {
                int maxclusterlength = int.Parse(ConfigurationManager.AppSettings["MaxClusterLength"]);

                errormessage += "<br />The maximum cluster size is " + (maxclusterlength / 1000) + " kb.";
            }
        }
        db.Dispose();

        if (!String.IsNullOrEmpty(errormessage))
        {
            ClusterErrorLabel.Visible = true;
            ClusterErrorLabel.Text = errormessage;
            ClusterErrorLabel.CssClass = "error";
        }
        else
        {
            ClusterErrorLabel.Visible = true;
            ClusterErrorLabel.Text = "Cluster added successfully!";
            ClusterErrorLabel.CssClass = "noerror";
        }
    }

    protected void LargeSequenceSubmitButton_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext(); ;
        string errormessage = "";
        string genbankid = LargeSequenceGenBankTextBox.Text.Trim();
        string accession = "";
        string version = "";
        int parsedversion;
        if (ValidateGenBank(genbankid))
        {
            if (genbankid.Contains('.'))
            {
                string[] genbankarray = genbankid.Split(new Char[] { '.' });
                accession = genbankarray[0];
                version = genbankarray[1];
                parsedversion = int.Parse(version);
            }
            else
            {
                accession = genbankid;
                parsedversion = 0;
            }

            if (db.LargeSequences.Exists(x => x.ClusterBase.GenbankAccession == accession && x.ClusterBase.GenbankVersion == parsedversion))
            {
                errormessage += "This accession number already exists in the database. <br />";
            }
            else
            {
                LargeSequence record = new LargeSequence();
                record.ClusterBase = new ClusterBase();
                int userid = (from x in db.UserInfo where x.User_Name == User.Identity.Name select x.ID).Single();
                record.ClusterBase.ContributorID = userid;
                record.ClusterBase.Created = DateTime.Now;
                record.ClusterBase.LastModifiedUserID = userid;
                record.ClusterBase.LastModified = DateTime.Now;

                record.ClusterBase.GenbankAccession = accession;
                record.ClusterBase.GenbankVersion = parsedversion;

                record.ClusterBase.PhylumID = Phyla.getPhylumID("Unknown");
                record.ClusterBase.OrgName = "";
                record.ClusterBase.NCBIStatus = 0;
                record.ClusterBase.NCBIModified = DateTime.Now;
                record.ClusterBase.AntiSmashStatus = 0;
                record.ClusterBase.AntiSmashModified = DateTime.Now;
                record.ClusterBase.SequenceExtractionStatus = 0;
                record.ClusterBase.SequenceExtractionModified = DateTime.Now;
                record.ClusterBase.EmailContributor = EmailantiSMASHCheckBox.Checked;
                record.ClusterBase.Delayed = true;

                db.ClusterBases.InsertOnSubmit(record.ClusterBase);
                db.SubmitChanges();

                try
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "New Large Sequence Record", "<b>A new large sequence with GenBank Accession " + genbankid + " has been added!</b>");
                }
                catch (Exception ex)
                {
                    Utilities.Email.Send(ConfigurationManager.AppSettings["AdminEmail"], "An error has occured!", "<b>Error Details</b><br /><br />" + ex.GetType().ToString() + "<br />" + ex.Message + "<br />" + ex.InnerException + "<br />" + ex.Data + "<br />" + ex.StackTrace);
                }
            }
        }
        else
        {
            errormessage += "Validation failed.";
            if (!ValidateGenBank(genbankid))
            {
                errormessage += "<br />Please enter a valid GenBank ID.";
            }
        }
        db.Dispose();

        if (!String.IsNullOrEmpty(errormessage))
        {
            LargeSequenceErrorLabel.Visible = true;
            LargeSequenceErrorLabel.Text = errormessage;
            LargeSequenceErrorLabel.CssClass = "error";
        }
        else
        {
            LargeSequenceErrorLabel.Visible = true;
            LargeSequenceGenBankTextBox.Text = "";
            LargeSequenceErrorLabel.Text = "Sequence submitted successfully!";
            LargeSequenceErrorLabel.CssClass = "noerror";
        }
    }

    protected void LargeSequenceClearSubmitButton_Click(object sender, EventArgs e)
    {
        LargeSequenceGenBankTextBox.Text = "";
        LargeSequenceErrorLabel.Visible = false;
        EmailantiSMASHCheckBox.Checked = false;
        Response.Redirect("~/Contribute/Add.aspx?PanelToOpen=3");
    }
}