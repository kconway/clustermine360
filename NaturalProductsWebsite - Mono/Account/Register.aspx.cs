﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class Account_Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //
        RegisterUser.ContinueDestinationPageUrl = HttpUtility.UrlDecode(Request.QueryString["ReturnUrl"]);
    }

    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {
        Page.Validate();

        if (!Page.IsValid)
        {
            return;
        }
        FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);

        Roles.AddUserToRole(RegisterUser.UserName, "default");

        string laburl = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("LabURL")).Text;

        DataClassesDataContext db = new DataClassesDataContext(); ;
        UserInfo newuser = new UserInfo();
        newuser.User_Name = RegisterUser.UserName;
        newuser.First_Name = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("FirstName")).Text;
        newuser.Last_Name = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("LastName")).Text;
        newuser.Lab_Name = ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("LabName")).Text;
        newuser.Lab_Url = EnsureProtocol(laburl, Protocol.http);
        db.UserInfo.InsertOnSubmit(newuser);
        db.SubmitChanges();
        db.Dispose();

        Utilities.Email.SendMultiple(new List<string>() { ConfigurationManager.AppSettings["AdminEmail"], ConfigurationManager.AppSettings["BoddyEmail"] }, "New User", "A new user has created an account for ClusterMine360 <br /><br /><b>First Name:</b> " + newuser.First_Name + "<br/><b>Last Name:</b> " + newuser.Last_Name + "<br /><b>Lab Name:</b> " + newuser.Lab_Name + "<br /><b>Lab URL:</b> <a href=\"" + newuser.Lab_Url + "\">" + newuser.Lab_Url + "</a>");

        // Create an empty Profile for the newly created user
        var profile = ProfileBase.Create(RegisterUser.UserName);

        // Populate some Profile properties off of the create user wizard
        profile.SetPropertyValue("FirstName", ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("FirstName")).Text);
        profile.SetPropertyValue("LastName", ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("LastName")).Text);
        profile.SetPropertyValue("LabName", ((TextBox)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("LabName")).Text);
        profile.SetPropertyValue("LabURL", EnsureProtocol(laburl, Protocol.http));

        // Save profile - must be done since we explicitly created it
        profile.Save();

        string continueUrl = RegisterUser.ContinueDestinationPageUrl;
        if (String.IsNullOrEmpty(continueUrl))
        {
            continueUrl = "~/Account/Success.aspx?" + "type=" + HttpUtility.UrlEncode("created");
        }
        Response.Redirect(continueUrl);
    }

    public static string EnsureProtocol(string url, Protocol protocol)
    {
        string output = url;

        if (!string.IsNullOrEmpty(output) && !output.StartsWith(protocol + "://", StringComparison.OrdinalIgnoreCase))
            output = string.Format("{0}://{1}", protocol.ToString().ToLower(), url);

        return output;
    }

    public enum Protocol
    {
        http,
        https
    }
}