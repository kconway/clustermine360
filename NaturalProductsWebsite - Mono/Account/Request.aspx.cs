﻿using System;
using System.Web;
using System.Web.Security;
using Utilities;

public partial class Request : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void RequestButton_Click(object sender, EventArgs e)
    {
        Page.Validate();

        if (!Page.IsValid)
        {
            return;
        }
        string username = Membership.GetUserNameByEmail(EmailTextBox.Text);
        string continueUrl = "~/Account/Success.aspx?";
        if (username != null)
        {
            MembershipUser member = Membership.GetUser(username);
            var loginUrl = WebTools.getURLRootPath(Request.Url.OriginalString, 1) + "Account/Login.aspx";
            string body = "Here is your account information: <br /><br />User Name: " + member.UserName + "<br />Password: " + member.GetPassword() + "<br /><br />You can login at: <a href=\"" + loginUrl + "\">" + loginUrl + "</a>";

            Email.Send(member.Email, "Your Account Information", body);

            continueUrl += "type=" + HttpUtility.UrlEncode("emailsent") + "&email=" + HttpUtility.UrlEncode(member.Email);
            Response.Redirect(continueUrl);
        }
        else
        {
            continueUrl += "type=" + HttpUtility.UrlEncode("emailnotsent") + "&email=" + HttpUtility.UrlEncode(EmailTextBox.Text);
            Response.Redirect(continueUrl);
        }
    }
}