﻿using System;
using System.Web;

public partial class Success : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string type = HttpUtility.UrlDecode(Request.QueryString["type"]);
        string emailaddress = HttpUtility.UrlDecode(Request.QueryString["email"]);
        string error = HttpUtility.UrlDecode(Request.QueryString["error"]);
        string message = "";

        if (type == "created")
        {
            message = "Your account has been successfully created.";
        }
        else if (type == "updated")
        {
            message = "Your account has been successfully updated.";
        }
        else if (type == "emailsent")
        {
            message = "An email with your account info has been sent to " + emailaddress + ".";
        }
        else if (type == "emailnotsent")
        {
            message = "The email address you entered, " + emailaddress + ", was not found. Please return and try another email address or create a new account.";
            Label1.CssClass = "error";
        }
        else if (type == "messagesent")
        {
            message = "Message was sent successfully!";
        }
        else if (type == "clusterdeleted")
        {
            message = "The cluster has been successfully deleted.";
        }
        else if (type == "cfdeleted")
        {
            message = "The compound family record has been successfully deleted.";
        }

        Label1.Text = message;
        Label1.Visible = true;
    }
}