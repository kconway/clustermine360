﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;

public partial class Account_Update : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            fillFields();
        }
       
    }

    public void fillFields()
    {
        MembershipUser member = Membership.GetUser(User.Identity.Name);

        UserName.Text = member.UserName;
        UserName.Enabled = false;
        var Profile = HttpContext.Current.Profile;
        FirstName.Text = (string)Profile.GetPropertyValue("FirstName");
        LastName.Text = (string)Profile.GetPropertyValue("LastName");
        Email.Text = member.Email;
        LabName.Text = (string)Profile.GetPropertyValue("LabName");
        LabURL.Text = (string)Profile.GetPropertyValue("LabURL");
    }

    public static string EnsureProtocol(string url, Protocol protocol)
    {
        string output = url;

        if (!string.IsNullOrEmpty(output) && !output.StartsWith(protocol + "://", StringComparison.OrdinalIgnoreCase))
            output = string.Format("{0}://{1}", protocol.ToString().ToLower(), url);

        return output;
    }

    public enum Protocol
    {
        http,
        https
    }

    protected void UpdateUserButton_Click(object sender, EventArgs e)
    {
        Page.Validate();

            if (!Page.IsValid)
            {
                return;
            }
        MembershipUser member = Membership.GetUser(User.Identity.Name);
        var Profile = HttpContext.Current.Profile;
        string laburl = LabURL.Text;
        Profile.SetPropertyValue("FirstName", FirstName.Text);
        Profile.SetPropertyValue("LastName", LastName.Text);
        member.Email = Email.Text;
        Profile.SetPropertyValue("LabName", LabName.Text);
        Profile.SetPropertyValue("LabURL", EnsureProtocol(laburl, Protocol.http));
        Profile.Save();

        Membership.UpdateUser(member);

        DataClassesDataContext db = new DataClassesDataContext(); ;
        UserInfo updateuser = (from u in db.UserInfo
                               where u.User_Name == member.UserName
                               select u).Single();
        updateuser.First_Name = FirstName.Text;
        updateuser.Last_Name = LastName.Text;
        updateuser.Email = Email.Text;
        updateuser.Lab_Name = LabName.Text;
        updateuser.Lab_Url = EnsureProtocol(laburl, Protocol.http);
        db.SubmitChanges();
        db.Dispose();

        string continueUrl = "~/Account/Success.aspx?" + "type=" + HttpUtility.UrlEncode("updated");
        Response.Redirect(continueUrl);
    }
}