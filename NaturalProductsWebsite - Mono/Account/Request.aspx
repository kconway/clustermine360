﻿<%@ Page Title="Request User Name and Password" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" Inherits="Request" CodeBehind="Request.aspx.cs" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        request username and/or password</h2>
    <p>
        Please enter the email address you used to sign up for your account.
        <br />
        Your username and password will be sent to this address.</p>
    <span class="failureNotification">
        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
    </span>
    <asp:ValidationSummary ID="EmailUserValidationSummary" runat="server" CssClass="failureNotification"
        ValidationGroup="EmailUserValidationGroup" />
    <div class="accountInfo">
        <fieldset class="login">
            <p>
                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="EmailTextBox">E-mail:</asp:Label>
                <asp:TextBox ID="EmailTextBox" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="EmailTextBox"
                    CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail address is required."
                    ValidationGroup="EmailUserValidationGroup" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter a valid email address."
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="failureNotification"
                    ControlToValidate="EmailTextBox" ValidationGroup="EmailUserValidationGroup" Display="Dynamic">*</asp:RegularExpressionValidator>
            </p>
        </fieldset>
        <p class="submitButton">
            <asp:Button ID="RequestButton" runat="server" Text="Submit" ValidationGroup="EmailUserValidationGroup"
                OnClick="RequestButton_Click" />
        </p>
    </div>
</asp:Content>