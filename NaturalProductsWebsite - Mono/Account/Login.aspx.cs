﻿using System;
using System.Web;

public partial class Account_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        HyperLink1.NavigateUrl = "Request.aspx";
    }

    protected void LoginUser_LoggedIn(object sender, EventArgs e)
    {
    }
}