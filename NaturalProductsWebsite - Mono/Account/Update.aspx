﻿<%@ Page Title="Update" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="Account_Update" CodeBehind="Update.aspx.cs" EnableEventValidation="True" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <h2>
        Update Your Account
    </h2>
    <p>
        Use the form below to update your account. To change your password, please do so
        <a href="ChangePassword.aspx">here.</a>
    </p>
    <span class="failureNotification">
        <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
    </span>
    <asp:ValidationSummary ID="UpdateUserValidationSummary" runat="server" CssClass="failureNotification"
        ValidationGroup="UpdateUserValidationGroup" />
    <div class="accountInfo">
        <fieldset class="register">
            <legend>Account Information</legend>
            <p>
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                    ValidationGroup="UpdateUserValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName">First Name:</asp:Label>
                <asp:TextBox ID="FirstName" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="FirstName"
                    CssClass="failureNotification" ErrorMessage="Please enter your first name." ToolTip="First name is required."
                    ValidationGroup="UpdateUserValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName">Last Name:</asp:Label>
                <asp:TextBox ID="LastName" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="LastName"
                    CssClass="failureNotification" ErrorMessage="Please enter your last name." ToolTip="Last name is required."
                    ValidationGroup="UpdateUserValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                <asp:TextBox ID="Email" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                    CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required."
                    ValidationGroup="UpdateUserValidationGroup" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter a valid email address."
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="failureNotification"
                    ControlToValidate="Email" ValidationGroup="UpdateUserValidationGroup" Display="Dynamic">*</asp:RegularExpressionValidator>
            </p>
            <p>
                <asp:Label ID="LabNameLabel" runat="server" AssociatedControlID="LabName">Name of Lab/Research Group:</asp:Label>
                <asp:TextBox ID="LabName" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LabName"
                    CssClass="failureNotification" ErrorMessage="The name of your lab or research group is required."
                    ToolTip="The name of your lab or research group is required." ValidationGroup="UpdateUserValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="LabURLLabel" runat="server" AssociatedControlID="LabURL">Lab/Research Group Website URL:</asp:Label>
                <asp:TextBox ID="LabURL" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter a valid URL. Leave blank if your lab/research group does not have a website."
                    ValidationExpression="(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?" CssClass="failureNotification"
                    ControlToValidate="LabURL" ValidationGroup="UpdateUserValidationGroup">*</asp:RegularExpressionValidator>
            </p>
        </fieldset>
        <p class="submitButton">
            <asp:Button ID="UpdateUserButton" runat="server" Text="Update User" ValidationGroup="UpdateUserValidationGroup"
                OnClick="UpdateUserButton_Click" CausesValidation="True" />
        </p>
    </div>
</asp:Content>