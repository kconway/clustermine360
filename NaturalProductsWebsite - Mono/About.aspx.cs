﻿using System;
using System.Configuration;

public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HeaderTitle.Text = "About the " + ConfigurationManager.AppSettings["WebsiteTitle"];
    }
}