﻿<%@ Page Title="Search" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Search.aspx.cs" Inherits="NaturalProductsWebsite.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $.fn.replaceText = function (search, replace, text_only) {
            return this.each(function () {
                var node = this.firstChild,
               val, new_val, remove = [];
                if (node) {
                    do {
                        if (node.nodeType === 3) {
                            val = node.nodeValue;
                            new_val = val.replace(search, replace);
                            if (new_val !== val) {
                                if (!text_only && /</.test(new_val)) {
                                    $(node).before(new_val);
                                    remove.push(node);
                                } else {
                                    node.nodeValue = new_val;
                                }
                            }
                        }
                    } while (node = node.nextSibling);
                }
                remove.length && $(remove).remove();
            });
        };
</script>
        
            <script language="javascript" type="text/javascript">
        function getSMILESString() {           
            var ketcherFrame = document.getElementById('ifKetcher');
            var ketcher = null;

            if ('contentDocument' in ketcherFrame)
                ketcher = ketcherFrame.contentWindow.ketcher;            

            $('#<%= SearchTextBox.ClientID%>').val(ketcher.getSmiles());
        }
        $(document).ready(function () {
            $("#ifKetcher").css("visibility", "hidden");
            $("#importButton").css("display", "none");
            $("#<%=RadioButton1.ClientID %>, #<%=RadioButton2.ClientID %>").change(
        function () {

            if ($("#<%=RadioButton1.ClientID %>").is(":checked")) {                
                $("#ifKetcher").css("visibility", "hidden");
                $("#importButton").css("display", "none");
            }
            else {
                $("#ifKetcher").css("visibility", "visible");
                $("#importButton").css("display", "inline");
            }
        }
        );
        });
        </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Search</h2>
    <br /><span class="width80 noerror" style="display:inline-block">Search will return matches related to fields such as synonyms, pharmacological identifiers, NCBI accession #, lineage, phylum, organism, cluster descriptions and more! Try searching using short words or parts of words for best results.</span><br /><br />
    <h3>
    <asp:RadioButton ID="RadioButton1" runat="server" Text="Text Search" Checked="True" GroupName="searchtype" />
    <asp:RadioButton ID="RadioButton2" runat="server" Text="Sub-Structure Search" Checked="False" GroupName="searchtype" /></h3>
    
    
    <asp:TextBox ID="SearchTextBox" runat="server" CssClass="textEntry">
    </asp:TextBox><input id="importButton" class="submitButton" type="button" value="Convert Structure to SMILES" onclick="getSMILESString()" /><asp:Button ID="SearchButton" CssClass="submitButton" runat="server"
        Text="Search" OnClick="SearchButton_Click" /><asp:Button ID="NewSearchButton" runat="server"
            Text="New Search" OnClick="NewSearch_Click" />
    <br />
    
    <asp:Panel ID="ResultsPanel" runat="server" Visible="false">
        <asp:Label ID="NoResultsLabel" runat="server" Text="No results were found." Visible="false"
            CssClass="error"></asp:Label>
        <asp:Panel ID="CompoundFamilyResults" runat="server" Visible="false">
            <h3>
                Compound Families</h3>
            <br />
            <asp:Repeater ID="CompoundFamilyResultsRepeater" runat="server">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow>
                            <asp:TableCell Width="200px" VerticalAlign="Top">
                                <asp:Label ID="ColumnName" runat="server" Font-Bold="True"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="ColumnContents" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="200px">
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:HyperLink ID="CompoundFamilyLink" runat="server">View</asp:HyperLink>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
        <asp:Panel ID="ClusterResults" runat="server" Visible="false">
            <h3>
                Clusters</h3>
            <br />
            <asp:Repeater ID="ClusterResultsRepeater" runat="server">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow>
                            <asp:TableCell Width="200px" VerticalAlign="Top">
                                <asp:Label ID="ColumnName" runat="server" Font-Bold="True"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="ColumnContents" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="200px">
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:HyperLink ID="ClusterLink" runat="server">View</asp:HyperLink>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </asp:Panel>
    
    <iframe id="ifKetcher" src="ketcher/ketcher.html" width="800" height="515" style="border-style:none" ></iframe>
  
</asp:Content>