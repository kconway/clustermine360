﻿<%@ Page Title="Clusters" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="ClustersPage" CodeBehind="Clusters.aspx.cs" %>

<%@ Register Src="~/Controls/SortableClusterGridviewControl.ascx" TagPrefix="uc"
    TagName="SortableClusterGridviewControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <h2>
        Clusters</h2>
        <hr />
    <br />
    <span style="margin-left:10px"><b>Total # Clusters: <asp:Label ID="NumClusters" runat="server"></asp:Label></b></span>
    <br />
    <br />
    
    <div style="position: relative">
        <uc:SortableClusterGridviewControl runat="server" ID="ucSortableClusterGridviewControl" />
    </div>
</asp:Content>