﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    Inherits="_Default" CodeBehind="Default.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1 style="font-size:xx-large"><strong>
        <asp:Label ID="HeaderTitle" runat="server" Text="Label"></asp:Label></strong>
    </h1><!--<h2>Database Purpose</h2>
    <h2>Getting Started</h2>    
    <h3>Show Me The Data</h3>
    might also help to learn some key definitions and how the data <h3>How the data is organized see the about page</h3>
    <h3>How to contribute?</h3>
    <p>Instructions on how to sign up for an account and add data can be found on the <a href="~/Help.aspx">Help</a> page.</p>
    -->
    <br />
   
    <asp:ImageMap ID="DBImageMap" runat="server" AlternateText="DB Diagram" CssClass="image border indent" ImageUrl="~/Images/About/db_structure.png" HotSpotMode="Navigate" BorderStyle="Solid" BorderWidth="1px" >
    <asp:RectangleHotSpot NavigateUrl="~/CompoundFamilies.aspx" Left="229" Top="22" Right="417" Bottom="175"/>
     <asp:RectangleHotSpot NavigateUrl="~/Browse.aspx" Left="484" Top="129" Right="622" Bottom="179"/>
    <asp:RectangleHotSpot NavigateUrl="~/About.aspx#defs" Left="10" Top="49" Right="161" Bottom="114"/>
    <asp:RectangleHotSpot NavigateUrl="~/About.aspx#defs" Left="487" Top="71" Right="621" Bottom="120"/>
    <asp:RectangleHotSpot NavigateUrl="~/Clusters.aspx" Left="10" Top="191" Right="281" Bottom="384"/>
     <asp:RectangleHotSpot NavigateUrl="~/About.aspx#defs" Left="383" Top="360" Right="593" Bottom="396"/>
       <asp:RectangleHotSpot NavigateUrl="~/About.aspx#ack" Left="383" Top="319" Right="593" Bottom="355"/>
         <asp:RectangleHotSpot NavigateUrl="~/Browse.aspx" Left="383" Top="285" Right="593" Bottom="314"/>
    </asp:ImageMap>
    <br />
    <br />
 <a href="Help.aspx" style="padding-left:605px; font-size: larger;"><strong>Need help?</strong></a>

           
</asp:Content>